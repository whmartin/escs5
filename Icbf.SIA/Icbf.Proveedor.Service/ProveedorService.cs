﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using System.Configuration;
using Icbf.Proveedor.Entity;
using Icbf.Proveedor.Business;
using Acuerdos = Icbf.Proveedor.Entity.Acuerdos;
using Revision = Icbf.Proveedor.Entity.Revision;
using Icbf.Seguridad.Entity;

namespace Icbf.Proveedor.Service
{
    /// <summary>
    /// Clase que contiene todos los métodos de acceso a las entidades de proveedores para uso desde página web o web service
    /// </summary>
    public partial class ProveedorService
    {
        private TipoCiiuBLL vTipoCiiuBLL;
        private Icbf.Proveedor.Business.TablaParametricaBLL vTablaParametricaBLL;
        private Icbf.Proveedor.Business.EntidadProvOferenteBLL vEntidadProvOferenteBLL;
        private Icbf.Oferente.Business.TerceroBLL vTerceroBLL;
        private DocAdjuntoTerceroBLL vDocAdjuntoTerceroBLL;
        private Icbf.Proveedor.Business.EstadoTerceroBLL vEstadoTerceroBLL;
        private Icbf.Oferente.Business.EstadoTerceroBLL vEstadoTerceroOferenteBLL;
        private TipoDocumentoProgramaBLL vTipoDocumentoProgramaBLL;
        private DocNotificacionJudicialBLL vDocNotificacionJudicialBLL;
        private NotificacionJudicialBLL vNotificacionJudicialBLL;
        private InfoFinancieraEntidadBLL vInfoFinancieraEntidadBLL;
        private TipoSectorEntidadBLL vTipoSectorEntidadBLL;
        private DocFinancieraProvBLL vDocFinancieraProvBLL;
        private TipoentidadBLL vTipoentidadBLL;
        private RamaoEstructuraBLL vRamaoEstructuraBLL;
        private NiveldegobiernoBLL vNiveldegobiernoBLL;
        private NivelOrganizacionalBLL vNivelOrganizacionalBLL;
        private TipodeentidadPublicaBLL vTipodeentidadPublicaBLL;
        private Icbf.Proveedor.Business.TipoRegimenTributarioBLL vTipoRegimenTributarioBLL;
        private TipodeActividadBLL vTipodeActividadBLL;
        private ClaseActividadBLL vClaseActividadBLL;
        private EstadoValidacionDocumentalBLL vEstadoValidacionDocumentalBLL;
        private ClasedeEntidadBLL vClasedeEntidadBLL;
        private ContactoEntidadBLL vContactoEntidadBLL;
        private InfoAdminEntidadBLL vInfoAdminEntidadBLL;
        private DocDatosBasicoProvBLL vDocDatosBasicoProvBLL;
        private TipoDocIdentificaBLL vTipoDocIdentificaBLL;
        private TipoCargoEntidadBLL vTipoCargoEntidadBLL;
        private EstadoDatosBasicosBLL vEstadoDatosBasicosBLL;
        private SucursalBLL vSucursalBLL;
        private InfoExperienciaEntidadBLL vInfoExperienciaEntidadBLL;
        private TipoCodigoUNSPSCBLL vTipoCodigoUNSPSCBLL;
        private DocExperienciaEntidadBLL vDocExperienciaEntidadBLL;
        private ExperienciaCodUNSPSCEntidadBLL vExperienciaCodUNSPSCEntidadBLL;
        private TipoCodigoUNSPSCBLL vTipoCodigoUNSPSBLL;
        private CodigoUNSPSCProveedorBLL vCodigoUNSPSCProveedorBLL;
        private AcuerdosBLL vAcuerdos;
        private ValidarTerceroBLL vValidarTerceroBLL;
        private ValidarInfoFinancieraEntidadBLL vValidarInfoFinancieraEntidadBLL;
        private ValidarInfoExperienciaEntidadBLL vValidarInfoExperienciaEntidadBLL;
        private ValidarInfoDatosBasicosEntidadBLL vValidarInfoDatosBasicosEntidadBLL;
        private ValidarInfoIntegrantesEntidadBLL vValidarInfoIntegrantesEntidadBLL;

        private ValidarProveedorBLL vValidarProveedorBLL;
        private RevisionBLL vRevisionBLL;
        private IntegrantesBLL vIntegrantesBLL;
        private EstadoProveedorBLL vEstadoProveedorBLL;
        private ValidacionIntegrantesEntidadBLL vValidacionIntegrantesEntidad;
        private FormacionBLL vFormacionBLL;
        private ReporteTercerosImportadosBLL vReporteImportado;

        public ProveedorService()
        {
            vReporteImportado = new ReporteTercerosImportadosBLL();
            vTablaParametricaBLL = new Icbf.Proveedor.Business.TablaParametricaBLL();
            vTipoCiiuBLL = new TipoCiiuBLL();
            vEntidadProvOferenteBLL = new Icbf.Proveedor.Business.EntidadProvOferenteBLL();
            vTerceroBLL = new Oferente.Business.TerceroBLL();
            vDocAdjuntoTerceroBLL = new DocAdjuntoTerceroBLL();
            vEstadoTerceroBLL = new Icbf.Proveedor.Business.EstadoTerceroBLL();
            vEstadoTerceroOferenteBLL = new Oferente.Business.EstadoTerceroBLL();
            vTipoDocumentoProgramaBLL = new TipoDocumentoProgramaBLL();
            vNotificacionJudicialBLL = new NotificacionJudicialBLL();
            vDocNotificacionJudicialBLL = new DocNotificacionJudicialBLL();
            vInfoFinancieraEntidadBLL = new InfoFinancieraEntidadBLL();
            vTipoSectorEntidadBLL = new TipoSectorEntidadBLL();
            vDocFinancieraProvBLL = new DocFinancieraProvBLL();
            vTipoRegimenTributarioBLL = new Icbf.Proveedor.Business.TipoRegimenTributarioBLL();
            vTipoentidadBLL = new TipoentidadBLL();
            vRamaoEstructuraBLL = new RamaoEstructuraBLL();
            vNiveldegobiernoBLL = new NiveldegobiernoBLL();
            vNivelOrganizacionalBLL = new NivelOrganizacionalBLL();
            vTipodeentidadPublicaBLL = new TipodeentidadPublicaBLL();
            vTipodeActividadBLL = new TipodeActividadBLL();
            vClaseActividadBLL = new ClaseActividadBLL();
            vEstadoValidacionDocumentalBLL = new EstadoValidacionDocumentalBLL();
            vClasedeEntidadBLL = new ClasedeEntidadBLL();
            vContactoEntidadBLL = new ContactoEntidadBLL();
            vInfoAdminEntidadBLL = new InfoAdminEntidadBLL();
            vDocDatosBasicoProvBLL = new DocDatosBasicoProvBLL();
            vTipoDocIdentificaBLL = new TipoDocIdentificaBLL();
            vTipoCargoEntidadBLL = new TipoCargoEntidadBLL();
            vEstadoDatosBasicosBLL = new EstadoDatosBasicosBLL();
            vSucursalBLL = new SucursalBLL();
            vInfoExperienciaEntidadBLL = new InfoExperienciaEntidadBLL();
            vTipoCodigoUNSPSCBLL = new TipoCodigoUNSPSCBLL();
            vDocExperienciaEntidadBLL = new DocExperienciaEntidadBLL();
            vExperienciaCodUNSPSCEntidadBLL = new ExperienciaCodUNSPSCEntidadBLL();
            vAcuerdos = new AcuerdosBLL();
            vValidarTerceroBLL = new ValidarTerceroBLL();
            vValidarInfoFinancieraEntidadBLL = new ValidarInfoFinancieraEntidadBLL();
            vValidarInfoExperienciaEntidadBLL = new ValidarInfoExperienciaEntidadBLL();
            vValidarInfoDatosBasicosEntidadBLL = new ValidarInfoDatosBasicosEntidadBLL();
            vValidarInfoIntegrantesEntidadBLL = new ValidarInfoIntegrantesEntidadBLL();
            vValidarProveedorBLL = new ValidarProveedorBLL();
            vRevisionBLL = new RevisionBLL();
            vCodigoUNSPSCProveedorBLL = new CodigoUNSPSCProveedorBLL();
            vIntegrantesBLL = new IntegrantesBLL();
            vEstadoProveedorBLL = new EstadoProveedorBLL();
            vValidacionIntegrantesEntidad = new ValidacionIntegrantesEntidadBLL();
            vFormacionBLL = new FormacionBLL();
        }

        #region TablaParametrica

        /// <summary>
        /// Inserta una nueva entidad Tabla paramétrica
        /// </summary>
        /// <param name="pTablaParametrica">Entidad Tabla paramétrica</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarTablaParametrica(Icbf.Proveedor.Entity.TablaParametrica pTablaParametrica)
        {
            try
            {
                return vTablaParametricaBLL.InsertarTablaParametrica(pTablaParametrica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Tabla paramétrica
        /// </summary>
        /// <param name="pTablaParametrica">Entidad Tabla paramétrica</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int ModificarTablaParametrica(Icbf.Proveedor.Entity.TablaParametrica pTablaParametrica)
        {
            try
            {
                return vTablaParametricaBLL.ModificarTablaParametrica(pTablaParametrica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Tabla paramétrica
        /// </summary>
        /// <param name="pTablaParametrica">Entidad Tabla paramétrica</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int EliminarTablaParametrica(Icbf.Proveedor.Entity.TablaParametrica pTablaParametrica)
        {
            try
            {
                return vTablaParametricaBLL.EliminarTablaParametrica(pTablaParametrica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Tabla paramétrica
        /// </summary>
        /// <param name="pIdTablaParametrica">Identificador de una entidad Tabla paramétrica</param>
        /// <returns>Entidad Tabla paramétrica</returns>
        public Icbf.Proveedor.Entity.TablaParametrica ConsultarTablaParametrica(int pIdTablaParametrica)
        {
            try
            {
                return vTablaParametricaBLL.ConsultarTablaParametrica(pIdTablaParametrica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tabla paramétrica
        /// </summary>
        /// <param name="pCodigoTablaParametrica">Còdigo de una entidad Tabla paramétrica</param>
        /// <param name="pNombreTablaParametrica">Nombre de una entidad Tabla paramétrica</param>
        /// <param name="pEstado">Estado de una entidad Tabla paramétrica</param>
        /// <returns>Lista de entidades Tabla paramétrica</returns>
        public List<Icbf.Proveedor.Entity.TablaParametrica> ConsultarTablaParametricas(String pCodigoTablaParametrica, String pNombreTablaParametrica, Boolean? pEstado)
        {
            try
            {
                return vTablaParametricaBLL.ConsultarTablaParametricas(pCodigoTablaParametrica, pNombreTablaParametrica, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region TipoCiiu

        /// <summary>
        /// Inserta una nueva entidad Tipo Ciuu
        /// </summary>
        /// <param name="pTipoCiiu">Entidad Tipo Ciuu</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarTipoCiiu(TipoCiiu pTipoCiiu)
        {
            try
            {
                return vTipoCiiuBLL.InsertarTipoCiiu(pTipoCiiu);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modificar una entidad Tipo Ciuu
        /// </summary>
        /// <param name="pTipoCiiu">Entidad Tipo Ciuu</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int ModificarTipoCiiu(TipoCiiu pTipoCiiu)
        {
            try
            {
                return vTipoCiiuBLL.ModificarTipoCiiu(pTipoCiiu);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar una entidad Tipo Ciuu
        /// </summary>
        /// <param name="pTipoCiiu">Entidad Tipo Ciuu</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int EliminarTipoCiiu(TipoCiiu pTipoCiiu)
        {
            try
            {
                return vTipoCiiuBLL.EliminarTipoCiiu(pTipoCiiu);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una entidad Tipo Ciuu
        /// </summary>
        /// <param name="pIdTipoCiiu">Identificador de una entidad Tipo Ciuu</param>
        /// <returns>Entidad Tipo Ciuu</returns>
        public TipoCiiu ConsultarTipoCiiu(int pIdTipoCiiu)
        {
            try
            {
                return vTipoCiiuBLL.ConsultarTipoCiiu(pIdTipoCiiu);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de entidades Tipo Ciuu
        /// </summary>
        /// <param name="pCodigoCiiu">Còdigo en una entidad Tipo Ciuu</param>
        /// <param name="pDescripcion">Descripciòn en una entidad Tipo Ciuu</param>
        /// <param name="pEstado">Estado en una entidad Tipo Ciuu</param>
        /// <returns>Lista de entidades Tipo Ciuu</returns>
        public List<TipoCiiu> ConsultarTipoCiius(String pCodigoCiiu, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vTipoCiiuBLL.ConsultarTipoCiius(pCodigoCiiu, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Gestion Entidad Prov Oferente

        /// <summary>
        /// Consultar Fecha de Migración
        /// </summary>
        /// <param name="IdEntidad">ID Entidad Proveedores Oferente</param>
        /// <returns>Fecha de MIgracion</returns>
        public DateTime GetFechaMigracion(int IdEntidad)
        {
            try
            {
                return vEntidadProvOferenteBLL.GetFechaMigracion(IdEntidad);
            }
            catch (UserInterfaceException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Obtiene la auditoria de acciones para una EntidadProvOferente
        /// </summary>
        /// <param name="IdEntidad">IdEntidad</param>
        public List<AuditoriaAccionesEntidad> GetAuditoriaAccionesEntidad(int pIdEntidad)
        {
            try
            {
                return vEntidadProvOferenteBLL.GetAuditoriaAccionesEntidad(pIdEntidad);
            }
            catch (UserInterfaceException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Inserta una nueva entidad Proveedores Oferente
        /// </summary>
        /// <param name="pEntidadProvOferente">Entidad Proveedores Oferente</param>
        /// <returns>Identificador de la entidad Proveedores Oferente en tabla</returns>
        public int InsertarEntidadProvOferente(Icbf.Proveedor.Entity.EntidadProvOferente pEntidadProvOferente)
        {
            try
            {
                int iResultado = vEntidadProvOferenteBLL.InsertarEntidadProvOferente(pEntidadProvOferente);
                if (iResultado >= 1)
                {
                    //Revision pRevision = new Revision();
                    //pRevision.IdEntidad = pEntidadProvOferente.IdEntidad;
                    //pRevision.NroRevision = 1;
                    //pRevision.Finalizado = false;
                    //pRevision.UsuarioCrea = pEntidadProvOferente.UsuarioCrea;                   

                    //pRevision.Componente = "Datos Básicos";
                    //int idRevision = vRevisionBLL.InsertarRevision(pRevision);

                    //pRevision.Componente = "Financiera";                    
                    //idRevision = vRevisionBLL.InsertarRevision(pRevision);

                    //pRevision.Componente = "Experiencias";
                    //idRevision = vRevisionBLL.InsertarRevision(pRevision);

                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (UserInterfaceException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Proveedores Oferente
        /// </summary>
        /// <param name="pEntidadProvOferente">Entidad Proveedores Oferente</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int ModificarEntidadProvOferente(Icbf.Proveedor.Entity.EntidadProvOferente pEntidadProvOferente)
        {
            try
            {
                int iResultado = vEntidadProvOferenteBLL.ModificarEntidadProvOferente(pEntidadProvOferente);
                if (iResultado >= 1)
                {
                    //Revision pRevision = new Revision();
                    //pRevision.IdEntidad = pEntidadProvOferente.IdEntidad;

                    //pRevision.Finalizado = false;
                    //pRevision.UsuarioModifica = pEntidadProvOferente.UsuarioModifica;

                    //pRevision.Componente = "Datos Básicos";
                    //int idRevision = vRevisionBLL.ModificarRevision(pRevision);

                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Proveedores Oferente
        /// </summary>
        /// <param name="pEntidadProvOferente">Entidad Proveedores Oferente</param>
        /// <returns>Identificador de la entidad Proveedores Oferente en tabla</returns>
        public int ModificarEntidadProvOferente_EstadoDocumental(Icbf.Proveedor.Entity.EntidadProvOferente pEntidadProvOferente)
        {
            try
            {
                return vEntidadProvOferenteBLL.ModificarEntidadProvOferente_EstadoDocumental(pEntidadProvOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Proveedores Oferente
        /// </summary>
        /// <param name="pEntidadProvOferente">Entidad Proveedores Oferente</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int EliminarEntidadProvOferente(Icbf.Proveedor.Entity.EntidadProvOferente pEntidadProvOferente)
        {
            try
            {
                return vEntidadProvOferenteBLL.EliminarEntidadProvOferente(pEntidadProvOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Proveedores Oferente
        /// </summary>
        /// <param name="pIdEntidad">Identificador de una entidad Proveedores Oferente</param>
        public Icbf.Proveedor.Entity.EntidadProvOferente ConsultarEntidadProvOferente(int pIdEntidad)
        {
            try
            {
                return vEntidadProvOferenteBLL.ConsultarEntidadProvOferente(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Proveedores Oferente
        /// </summary>
        /// <returns>Lista de entidades Proveedores Oferente</returns>
        public List<Icbf.Proveedor.Entity.EntidadProvOferente> ConsultarEntidadProvOferentes()
        {
            try
            {
                return vEntidadProvOferenteBLL.ConsultarEntidadProvOferentes();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Proveedores Oferente
        /// </summary>
        /// <param name="pTipoPersona">Tipo de persona en una entidad Proveedores Oferente</param>
        /// <param name="pTipoidentificacion">Tipo de identificaciòn en una entidad Proveedores Oferente</param>
        /// <param name="pIdentificacion">Identificaciòn en una entidad Proveedores Oferente</param>
        /// <param name="pProveedor">Proveedor en una entidad Proveedores Oferente</param>
        /// <param name="pEstado">Estado en una entidad Proveedores Oferente</param>
        /// <param name="pUsuarioCrea">Usuario que creaciòn para una entidad Proveedores Oferente</param>
        /// <returns>Lista de entidades Proveedores Oferente</returns>
        public List<Icbf.Proveedor.Entity.EntidadProvOferente> ConsultarEntidadProvOferentes(String pTipoPersona, String pTipoidentificacion, String pIdentificacion, String pProveedor, String pEstado, String pUsuarioCrea)
        {
            try
            {
                return vEntidadProvOferenteBLL.ConsultarEntidadProvOferentes(pTipoPersona, pTipoidentificacion, pIdentificacion, pProveedor, pEstado, pUsuarioCrea);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Proveedores Oferente
        /// </summary>
        /// <param name="pIdTercero">Id Tercero</param>
        /// <returns>Lista de sucursañes asociadas a los Proveedores Oferente</returns>
        public List<Icbf.Proveedor.Entity.EntidadProvOferente> ConsultarSucursales_EntidadProvOferentes(int? pIdTercero, int? pIdDepartamento, int? pIdMunicipio)
        {
            try
            {
                return vEntidadProvOferenteBLL.ConsultarSucursales_EntidadProvOferentes(pIdTercero, pIdDepartamento, pIdMunicipio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Proveedores Oferente Municipio
        /// </summary>
        /// <param name="pIdTercero">Id Tercero</param>
        /// <returns>Lista de sucursañes asociadas a los Proveedores Oferente</returns>
        public List<Icbf.Proveedor.Entity.EntidadProvOferente> ConsultarSucursales_EntidadProvOferentes_Municipio(int? pIdTercero, int? pIdDepartamento, int? pIdMunicipio)
        {
            try
            {
                return vEntidadProvOferenteBLL.ConsultarSucursales_EntidadProvOferentes_Municipio(pIdTercero, pIdDepartamento, pIdMunicipio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Proveedores Oferente
        /// </summary>
        /// <param name="pIdEstado">Identificador del Estado en una entidad Proveedores Oferente</param>
        /// <param name="pIdTipoPersona">Identificador del tipo de persona en una entidad Proveedores Oferente</param>
        /// <param name="pIDTIPODOCIDENTIFICA">Identificador del tipo de documento de identificaciòn en una entidad Proveedores Oferente</param>
        /// <param name="pNUMEROIDENTIFICACION">Nùmero de identificaciòn en una entidad Proveedores Oferente</param>
        /// <param name="pIdTipoCiiuPrincipal">Identificador del tipo de Ciiu principal en una entidad Proveedores Oferente</param>
        /// <param name="pIdMunicipioDirComercial">Identificador del municipio en una entidad Proveedores Oferente</param>
        /// <param name="pIdDepartamentoDirComercial">Identificador del departamento en una entidad Proveedores Oferente</param>
        /// <param name="pIdTipoSector">Identificador del tiepo de sector en una entidad Proveedores Oferente</param>
        /// <param name="pIdTipoRegimenTributario">Identificador del tipo de règimen tributario en una entidad Proveedores Oferente</param>
        /// <param name="pProveedor"> en una entidad Proveedores Oferente</param>
        /// <returns>Lista de entidades Proveedores Oferente</returns>
        public List<Icbf.Proveedor.Entity.EntidadProvOferente> ConsultarEntidadProvOferentes_Validar(String pIdEstado, String pIdTipoPersona,
            String pIDTIPODOCIDENTIFICA, String pNUMEROIDENTIFICACION, String pIdTipoCiiuPrincipal, String pIdMunicipioDirComercial,
            String pIdDepartamentoDirComercial, String pIdTipoSector, String pIdTipoRegimenTributario, String pProveedor)
        {
            try
            {
                return vEntidadProvOferenteBLL.ConsultarEntidadProvOferentes_Validar(pIdEstado, pIdTipoPersona,
            pIDTIPODOCIDENTIFICA, pNUMEROIDENTIFICACION, pIdTipoCiiuPrincipal, pIdMunicipioDirComercial,
            pIdDepartamentoDirComercial, pIdTipoSector, pIdTipoRegimenTributario, pProveedor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        #endregion

        #region Gestión Terceros

        /// <summary>
        /// Inserta una entidad Tercero
        /// </summary>
        /// <param name="pTercero"></param>
        /// <returns></returns>
        public int InsertarRepresentanteLegal(Tercero pTercero)
        {
            try
            {
                return vTerceroBLL.InsertarRepresentanteLegal(pTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Tercero
        /// </summary>
        /// <param name="pIdDocAdjunto"></param>
        /// <returns></returns>
        public DocAdjuntoTercero ConsultarDocAdjuntoTercero(int pIdDocAdjunto)
        {
            try
            {
                return vDocAdjuntoTerceroBLL.ConsultarDocAdjuntoTercero(pIdDocAdjunto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad de Documento Adjunto Tercero
        /// </summary>
        /// <param name="pIdDocAdjunto"></param>
        /// <param name="pIdTercero"></param>
        /// <param name="pIdDocumento"></param>
        /// <returns></returns>
        public DocAdjuntoTercero ConsultarDocAdjuntoTercero(int pIdDocAdjunto, int pIdTercero, int pIdDocumento)
        {
            try
            {
                return vDocAdjuntoTerceroBLL.ConsultarDocAdjuntoTercero(pIdDocAdjunto, pIdTercero, pIdDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades de Documento adjunto Tercero
        /// </summary>
        /// <param name="pIdTercero"></param>
        /// <param name="pcodigoDocumento"></param>
        /// <param name="programa"></param>
        /// <returns></returns>
        public List<DocAdjuntoTercero> ConsultarDocAdjuntoTercero(int? pIdTercero, string pcodigoDocumento, string programa)
        {
            try
            {
                return vDocAdjuntoTerceroBLL.ConsultarDocAdjuntoTercero(pIdTercero, pcodigoDocumento, programa);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad de documento adjunto tercero
        /// </summary>
        /// <param name="pDocAdjuntoTercero"></param>
        /// <returns></returns>
        public int EliminarDocAdjuntoTercero(DocAdjuntoTercero pDocAdjuntoTercero)
        {
            try
            {
                return vDocAdjuntoTerceroBLL.EliminarDocAdjuntoTercero(pDocAdjuntoTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una documento adjunto a Tercero
        /// </summary>
        /// <param name="pIdDocAdjunto"></param>
        /// <param name="pIdTercero"></param>
        /// <param name="pIdDocumento"></param>
        /// <returns></returns>
        public List<DocAdjuntoTercero> ConsultarDocAdjuntoTerceros(int? pIdDocAdjunto, int? pIdTercero, int? pIdDocumento)
        {
            try
            {
                return vDocAdjuntoTerceroBLL.ConsultarDocAdjuntoTerceros(pIdDocAdjunto, pIdTercero, pIdDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades de Documento Adjunto a Tercero
        /// </summary>
        /// <param name="pIdTercero"></param>
        /// <param name="pTipoPersona"></param>
        /// <returns></returns>
        public List<DocAdjuntoTercero> ConsultarDocAdjuntoTerceros_IdTercero_TipoPersona(int pIdTercero, string pTipoPersona, string idTemporal)
        {
            try
            {
                return vDocAdjuntoTerceroBLL.ConsultarDocAdjuntoTerceros_IdTercero_TipoPersona(pIdTercero, pTipoPersona, idTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Documento Adjunto Tercero
        /// </summary>
        /// <param name="pIdTemporal"></param>
        /// <param name="pTipoPersona"></param>
        /// <returns></returns>
        public List<DocAdjuntoTercero> ConsultarDocAdjuntoTerceros_IdTemporal_TipoPersona(string pIdTemporal, string pTipoPersona)
        {
            try
            {
                return vDocAdjuntoTerceroBLL.ConsultarDocAdjuntoTerceros_IdTemporal_TipoPersona(pIdTemporal, pTipoPersona);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta una nueva entidad Documento Adjunto Tercero
        /// </summary>
        /// <param name="pDocAdjuntoTercero"></param>
        /// <returns></returns>
        public int InsertarDocAdjuntoTercero(DocAdjuntoTercero pDocAdjuntoTercero)
        {
            try
            {
                return vDocAdjuntoTerceroBLL.InsertarDocAdjuntoTercero(pDocAdjuntoTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad De Documento Adjunto Tercero
        /// </summary>
        /// <param name="pDocAdjuntoTercero"></param>
        /// <returns></returns>
        public int ModificarDocAdjuntoTercero(DocAdjuntoTercero pDocAdjuntoTercero)
        {
            try
            {
                return vDocAdjuntoTerceroBLL.ModificarDocAdjuntoTercero(pDocAdjuntoTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta una entidad Estado Tercero
        /// </summary>
        /// <param name="pEstadoTercero"></param>
        /// <returns></returns>
        public int InsertarEstadoTercero(Icbf.Proveedor.Entity.EstadoTercero pEstadoTercero)
        {
            try
            {
                return vEstadoTerceroBLL.InsertarEstadoTercero(pEstadoTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica entidad Esado Tercero
        /// </summary>
        /// <param name="pEstadoTercero"></param>
        /// <returns></returns>
        public int ModificarEstadoTercero(Icbf.Proveedor.Entity.EstadoTercero pEstadoTercero)
        {
            try
            {
                return vEstadoTerceroBLL.ModificarEstadoTercero(pEstadoTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Estado Tercero
        /// </summary>
        /// <param name="pEstadoTercero"></param>
        /// <returns></returns>
        public int EliminarEstadoTercero(Icbf.Proveedor.Entity.EstadoTercero pEstadoTercero)
        {
            try
            {
                return vEstadoTerceroBLL.EliminarEstadoTercero(pEstadoTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta una entidad Motivo de cambio de Estado de Proveedor
        /// </summary>
        /// <param name="idTercero"></param>
        /// <param name="datosBasicos"></param>
        /// <param name="financiera"></param>
        /// <param name="experiencia"></param>
        /// <param name="IdTemporal"></param>
        /// <param name="motivo"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public int InsertarMotivoCambioEstado(int? idTercero, bool datosBasicos, bool financiera, bool experiencia, bool integrantes, string IdTemporal, string motivo, string usuario)
        {
            try
            {
                return vEstadoTerceroBLL.InsertarMotivoCambioEstado(idTercero, datosBasicos, financiera, experiencia, integrantes, IdTemporal, motivo, usuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta una entidad Motivo de cambio de Estado de Tercero
        /// </summary>
        /// <param name="idTercero"></param>
        /// <param name="datosBasicos"></param>
        /// <param name="financiera"></param>
        /// <param name="experiencia"></param>
        /// <param name="IdTemporal"></param>
        /// <param name="motivo"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public int InsertarMotivoCambioEstadoTercero(int? idTercero, string IdTemporal, string motivo, string usuario)
        {
            try
            {
                return vEstadoTerceroBLL.InsertarMotivoCambioEstadoTercero(idTercero, IdTemporal, motivo, usuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Envìa correo de cambio de estado a Tercero
        /// </summary>
        /// <param name="Tercero"></param>
        /// <param name="IdTemporal"></param>
        /// <returns></returns>
        public int EnviarMailsCambioEstadoTerceroProveedor(string Tercero, string IdTemporal, string appName, string rolName)
        {
            try
            {
                //Obtiene los correos de los usuarios que tengan perfil Gestor proveedor, para enviarles los terceros o proveedores que le cambiaron el estado
                string correos = vEstadoTerceroBLL.ObtenerMailsGestorProveedor(appName, rolName);

                if (string.IsNullOrEmpty(correos)) { throw new Exception("No hay usuarios con el perfil GESTOR PROVEEDOR"); }

                return vEstadoTerceroBLL.EnviarMailsCambioEstadoTerceroProveedor(Tercero, IdTemporal, correos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Obtiene el cuerpo de correo por cambio de estado
        /// </summary>
        /// <param name="Tercero">Tercero</param>
        /// <param name="IdTemporal">Identificador del temporal</param>
        /// <returns>string de Cuero de Correo</returns>
        public string ObtenerCuerpoDeCorreo_CambioEstadoTerceroProveedor(string Tercero, string IdTemporal)
        {
            try
            {

                return vEstadoTerceroBLL.ObtenerCuerpoDeCorreo_CambioEstadoTerceroProveedor(Tercero, IdTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public string ObtenerMailsGestorProveedor(string appName, string rolName)
        {
            try
            {
                return vEstadoTerceroBLL.ObtenerMailsGestorProveedor(appName, rolName);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Envìa correo por inconsistencia
        /// </summary>
        /// <param name="idEntidad"></param>
        /// <returns></returns>
        public int EnviarMailsInconsistenciasProveedor(Int32 idEntidad, string pAsunto)
        {
            try
            {


                return vEntidadProvOferenteBLL.EnviarMailsInconsistenciasProveedor(idEntidad, pAsunto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Estructura el correo electrònico a enviar por inconsistencia en una entidad Proveedores Oferente
        /// </summary>
        /// <param name="idEntidad">Identificador de una entidad Proveedores Oferente</param>
        /// <returns>String Mail Body HTML</returns>
        //public string[] EstructurarMailsInconsistenciasProveedor(Int32 idEntidad)
        //{
        //    try
        //    {
        //        return vEntidadProvOferenteBLL.EstructurarMailsInconsistenciasProveedor(idEntidad);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}
        ///// <summary>
        /// Consulta una entidad Estado Tercero
        /// </summary>
        /// <param name="pIdEstadoTercero"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public Icbf.Proveedor.Entity.EstadoTercero ConsultarEstadoTercero(int pIdEstadoTercero, int pEstado)
        {
            try
            {
                return vEstadoTerceroBLL.ConsultarEstadoTercero(pIdEstadoTercero, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades de Estado Tercero
        /// </summary>
        /// <param name="pIdEstadoTercero"></param>
        /// <returns></returns>
        public List<Icbf.Proveedor.Entity.EstadoTercero> ConsultarEstadoTerceros(int? pIdEstadoTercero)
        {
            try
            {
                return vEstadoTerceroBLL.ConsultarEstadoTerceros(pIdEstadoTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad de Estado Tercero
        /// </summary>
        /// <param name="codigoEstado"></param>
        /// <returns></returns>
        public Icbf.Oferente.Entity.EstadoTercero ConsultarEstadoTercero(string codigoEstado)
        {
            try
            {
                return vEstadoTerceroOferenteBLL.ConsultarEstadoTercero(codigoEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta una entidad de Tercero
        /// </summary>
        /// <param name="pTercero"></param>
        /// <returns></returns>
        public int InsertarTercero(Tercero pTercero)
        {
            try
            {
                return vTerceroBLL.InsertarTercero(pTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta una responsable legal
        /// </summary>
        /// <param name="pIdTercero"></param>
        /// <param name="IdRepresentanteLegal"></param>
        /// <returns></returns>
        public int InsertarTerceroRepesentanteLegal(int pIdTercero, int IdRepresentanteLegal)
        {
            try
            {

                return vTerceroBLL.InsertarTerceroRepesentanteLegal(pIdTercero, IdRepresentanteLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Tercero
        /// </summary>
        /// <param name="pTercero"></param>
        /// <returns></returns>
        public int ModificarTercero(Tercero pTercero, string idTemporal)
        {
            try
            {
                return vTerceroBLL.ModificarTercero(pTercero, idTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Tercero
        /// </summary>
        /// <param name="pIdTercero"></param>
        /// <returns></returns>
        public Tercero ConsultarTercero(int pIdTercero)
        {
            try
            {
                return vTerceroBLL.ConsultarTercero(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Tercero por nùmero de identificaciòn
        /// </summary>
        /// <param name="pTipoidentificacion"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <returns></returns>
        public Tercero ConsultarTerceroTipoNumeroIdentificacion(int pTipoidentificacion, string pNumeroIdentificacion)
        {
            try
            {
                return vTerceroBLL.ConsultarTerceroTipoNumeroIdentificacion(pTipoidentificacion, pNumeroIdentificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Tercero por nùmero de identificaciòn
        /// </summary>
        /// <param name="pTipoidentificacion"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <returns></returns>
        public Tercero ConsultarTerceroTipoNumeroIdentificacionParaIntegrantes(int pTipoidentificacion, string pNumeroIdentificacion)
        {
            try
            {
                return vTerceroBLL.ConsultarTerceroTipoNumeroIdentificacionParaIntegrantes(pTipoidentificacion, pNumeroIdentificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consulta una lista de Terceros tomando el criterìo
        /// </summary>
        /// <param name="vIdDListaTipoDocumento"></param>
        /// <param name="vIdEstadoTercero"></param>
        /// <param name="vIdTipoPersona"></param>
        /// <param name="vNumeroIdentificacion"></param>
        /// <param name="vPrimerNombre"></param>
        /// <param name="vSegundoNombre"></param>
        /// <param name="vPrimerApellido"></param>
        /// <param name="vSegundoApellido"></param>
        /// <returns></returns>
        public List<Tercero> ConsultarTerceros(int? vIdDListaTipoDocumento, int? vIdEstadoTercero, int? vIdTipoPersona, int? vNumeroIdentificacion,
                                                string vPrimerNombre, string vSegundoNombre, string vPrimerApellido, string vSegundoApellido)
        {
            try
            {
                return vTerceroBLL.ConsultarTerceros(vIdDListaTipoDocumento, vIdEstadoTercero, vIdTipoPersona,
                                                     vNumeroIdentificacion,
                                                     vPrimerNombre, vSegundoNombre, vPrimerApellido, vSegundoApellido);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una list a de entidades Tercero
        /// </summary>
        /// <param name="vIdDListaTipoDocumento"></param>
        /// <param name="vIdEstadoTercero"></param>
        /// <param name="vIdTipoPersona"></param>
        /// <param name="vNumeroIdentificacion"></param>
        /// <param name="vUsuario"></param>
        /// <param name="vtercero"></param>
        /// <returns></returns>
        public List<Tercero> ConsultarTerceros(int? vIdDListaTipoDocumento, int? vIdEstadoTercero, int? vIdTipoPersona, string vNumeroIdentificacion, string vUsuario, string vtercero, int? vClaseActividad)
        {
            try
            {
                return vTerceroBLL.ConsultarTerceros(vIdDListaTipoDocumento, vIdEstadoTercero, vIdTipoPersona, vNumeroIdentificacion, vUsuario, vtercero, vClaseActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades terceros
        /// </summary>
        /// <param name="vIdDListaTipoDocumento"></param>
        /// <param name="vIdEstadoTercero"></param>
        /// <param name="vIdTipoPersona"></param>
        /// <param name="vNumeroIdentificacion"></param>
        /// <param name="vUsuario"></param>
        /// <returns></returns>
        public List<Tercero> ConsultarTerceros(int? vIdDListaTipoDocumento, int? vIdEstadoTercero, int? vIdTipoPersona, string vNumeroIdentificacion, string vUsuario)
        {
            try
            {
                return vTerceroBLL.ConsultarTerceros(vIdDListaTipoDocumento, vIdEstadoTercero, vIdTipoPersona, vNumeroIdentificacion, vUsuario, null, null);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades terceros Que aun no son Proveedores
        /// </summary>
        /// <param name="vIdDListaTipoDocumento"></param>
        /// <param name="vIdEstadoTercero"></param>
        /// <param name="vIdTipoPersona"></param>
        /// <param name="vNumeroIdentificacion"></param>
        /// <param name="vUsuario"></param>
        /// <returns></returns>
        public List<Tercero> ConsultarTercerosNoProveedores(int? vIdDListaTipoDocumento, int? vIdEstadoTercero, int? vIdTipoPersona, string vNumeroIdentificacion, string vUsuario)
        {
            try
            {
                return vTerceroBLL.ConsultarTercerosNoProveedores(vIdDListaTipoDocumento, vIdEstadoTercero, vIdTipoPersona, vNumeroIdentificacion, vUsuario, null);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tercero
        /// </summary>
        /// <param name="vIdDListaTipoDocumento"></param>
        /// <param name="vIdEstadoTercero"></param>
        /// <param name="vIdTipoPersona"></param>
        /// <param name="vNumeroIdentificacion"></param>
        /// <param name="vFechaRegistro"></param>
        /// <param name="vTercero"></param>
        /// <returns></returns>
        public List<Tercero> ConsultarTerceros(int? vIdDListaTipoDocumento, int? vIdEstadoTercero, int? vIdTipoPersona, string vNumeroIdentificacion, DateTime vFechaRegistro, string vTercero)
        {
            try
            {
                return vTerceroBLL.ConsultarTerceros(vIdDListaTipoDocumento, vIdEstadoTercero, vIdTipoPersona, vNumeroIdentificacion, vFechaRegistro, vTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tercero
        /// </summary>
        /// <param name="TipoConsulta"></param>
        /// <param name="vIdTipoPersona"></param>
        /// <param name="vIdDListaTipoDocumento"></param>
        /// <param name="vNumeroIdentificacion"></param>
        /// <param name="vNombreTercero"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public List<Tercero> ConsultarTerceros_Proveedores_Validados(int TipoConsulta, int? vIdTipoPersona, int? vIdDListaTipoDocumento, string vNumeroIdentificacion, string vNombreTercero, string usuario)
        {
            try
            {
                return vTerceroBLL.ConsultarTerceros_Proveedores_Validados(TipoConsulta, vIdTipoPersona, vIdDListaTipoDocumento, vNumeroIdentificacion, vNombreTercero, usuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Tercero por clave de usuario
        /// </summary>
        /// <param name="ProviderUserKey"></param>
        /// <returns></returns>
        public Tercero ConsultarTerceroProviderUserKey(string ProviderUserKey)
        {
            try
            {
                return vTerceroBLL.ConsultarTerceroProviderUserKey(ProviderUserKey);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Tercero ConsultarTerceroPorEmail(string ProviderUserKey)
        {
            try
            {
                return vTerceroBLL.ConsultarTerceroPorEmail(ProviderUserKey);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta representante legal de una entidad Tercero
        /// </summary>
        /// <param name="pIdTercero"></param>
        /// <returns></returns>
        public int ConsultarReprentanteLogalTercero(int pIdTercero)
        {
            try
            {
                return vTerceroBLL.ConsultarReprentanteLogalTercero(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Estado Tercero de acuerdo al estado
        /// </summary>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<Icbf.Oferente.Entity.EstadoTercero> ConsultarEstadosTerceros(bool? pEstado)
        {
            try
            {
                return vEstadoTerceroOferenteBLL.ConsultarEstadosTercero(pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Estado tercero por si identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Icbf.Oferente.Entity.EstadoTercero ConsultarEstadoTercero(int id)
        {
            try
            {
                return vEstadoTerceroOferenteBLL.ConsultarEstadoTercero(id);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Verifica si un Tercero Puede Cambiar de Estado
        /// </summary>
        /// <param name="idTercero"></param>
        /// <returns></returns>
        /// 
        public bool PuedeTerceroCambiarEstado(int idTercero)
        {
            try
            {
                return vTerceroBLL.PuedeTerceroCambiarEstado(idTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Gestión documentos Programa

        /// <summary>
        /// Inserta una nueva entidad Tipo Documento programa
        /// </summary>
        /// <param name="pTipoDocumentoPrograma">Entidad Tipo Documento programa</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarTipoDocumentoPrograma(TipoDocumentoPrograma pTipoDocumentoPrograma)
        {
            try
            {
                return vTipoDocumentoProgramaBLL.InsertarTipoDocumentoPrograma(pTipoDocumentoPrograma);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Tipo Documento programa
        /// </summary>
        /// <param name="pTipoDocumentoPrograma">Entidad Tipo Documento programa</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int ModificarTipoDocumentoPrograma(TipoDocumentoPrograma pTipoDocumentoPrograma)
        {
            try
            {
                return vTipoDocumentoProgramaBLL.ModificarTipoDocumentoPrograma(pTipoDocumentoPrograma);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Tipo Documento programa
        /// </summary>
        /// <param name="pTipoDocumentoPrograma">Entidad Tipo Documento programa</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int EliminarTipoDocumentoPrograma(TipoDocumentoPrograma pTipoDocumentoPrograma)
        {
            try
            {
                return vTipoDocumentoProgramaBLL.EliminarTipoDocumentoPrograma(pTipoDocumentoPrograma);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Tipo Documento programa
        /// </summary>
        /// <param name="pIdTipoDocumentoPrograma">Identificador en una entidad Tipo Documento programa</param>
        /// <returns>Entidad Tipo Documento programa</returns>
        public TipoDocumentoPrograma ConsultarTipoDocumentoPrograma(int pIdTipoDocumentoPrograma)
        {
            try
            {
                return vTipoDocumentoProgramaBLL.ConsultarTipoDocumentoPrograma(pIdTipoDocumentoPrograma);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Documento programa
        /// </summary>
        /// <param name="pIdTipoDocumentoPrograma">Identificador del tipo de documento de programa en una entidad Tipo Documento programa</param>
        /// <param name="pIdTipoDocumento">Identificador del tipo de documento</param>
        /// <param name="pIdPrograma">Identificador del programa</param>
        /// <returns>Lista de entidades Tipo Documento programa</returns>
        public List<TipoDocumentoPrograma> ConsultarTipoDocumentoProgramas(int? pIdTipoDocumentoPrograma, int? pIdTipoDocumento, int? pIdPrograma)
        {
            try
            {
                return vTipoDocumentoProgramaBLL.ConsultarTipoDocumentoProgramas(pIdTipoDocumentoPrograma, pIdTipoDocumento, pIdPrograma);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de entidades Tipo Documento programa
        /// </summary>
        /// <param name="pIdTipoDocumento">Identificador del tipo del documento en una entidad Tipo Documento programa</param>
        /// <returns>Lista de entidades Tipo Documento programa</returns>
        public List<Programa> ConsultarProgramas(int pIdModulo)
        {
            try
            {
                return vTipoDocumentoProgramaBLL.ConsultarProgramas(pIdModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar programas
        /// </summary>
        /// <param name="pIdModulo">Identificador del mòdulo</param>
        public List<TipoDocumentoProveedor> ConsultarTipoDocumentos(int? pIdTipoDocumento)
        {
            try
            {
                return vTipoDocumentoProgramaBLL.ConsultarTipoDocumentos(pIdTipoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Gestión Notificaiones Judiciales

        /// <summary>
        /// Inserta una nueva entidad Documentos asociados a notificación judicial
        /// </summary>
        /// <param name="pDocNotificacionJudicial">Entidad Documentos asociados a notificación judicial</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int InsertarDocNotificacionJudicial(DocNotificacionJudicial pDocNotificacionJudicial)
        {
            try
            {
                return vDocNotificacionJudicialBLL.InsertarDocNotificacionJudicial(pDocNotificacionJudicial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Documentos asociados a notificación judicial
        /// </summary>
        /// <param name="pDocNotificacionJudicial">Entidad Documentos asociados a notificación judicial</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int ModificarDocNotificacionJudicial(DocNotificacionJudicial pDocNotificacionJudicial)
        {
            try
            {
                return vDocNotificacionJudicialBLL.ModificarDocNotificacionJudicial(pDocNotificacionJudicial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Documentos asociados a notificación judicial
        /// </summary>
        /// <param name="pDocNotificacionJudicial">Entidad Documentos asociados a notificación judicial</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int EliminarDocNotificacionJudicial(DocNotificacionJudicial pDocNotificacionJudicial)
        {
            try
            {
                return vDocNotificacionJudicialBLL.EliminarDocNotificacionJudicial(pDocNotificacionJudicial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Documentos asociados a notificación judicial
        /// </summary>
        /// <param name="pIdDocNotJudicial">Identificador en entidad Documentos asociados a notificación judicial</param>
        /// <param name="pIdEntidad">Identificador en entidad Documentos asociados a notificación judicial</param>
        /// <param name="pIdDocumento">Identificador de documento en entidad Documentos asociados a notificación judicial</param>
        /// <returns>Entidad Documentos asociados a notificación judicial</returns>
        public DocNotificacionJudicial ConsultarDocNotificacionJudicial(int pIdDocNotJudicial, int pIdEntidad, int pIdDocumento)
        {
            try
            {
                return vDocNotificacionJudicialBLL.ConsultarDocNotificacionJudicial(pIdDocNotJudicial, pIdEntidad, pIdDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Documentos asociados a notificación judicial
        /// </summary>
        /// <param name="pIdDocNotJudicial">Identifidor de una entidad Documentos asociados a notificación judicial</param>
        /// <param name="IdNotJudicial">Identificador de una notificaciòn en una entidad Documentos asociados a notificación judicial</param>
        /// <param name="pIdDocumento">Identificador de un documento en una entidad Documentos asociados a notificación judicial</param>
        /// <returns>Lista de entidades Documentos asociados a notificación judicial</returns>
        public List<DocNotificacionJudicial> ConsultarDocNotificacionJudicials(int? pIdDocNotJudicial, int? IdNotJudicial, int? pIdDocumento)
        {
            try
            {
                return vDocNotificacionJudicialBLL.ConsultarDocNotificacionJudicials(pIdDocNotJudicial, IdNotJudicial, pIdDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Documentos asociados a notificación judicial
        /// </summary>
        /// <param name="IdNotJudicial">Identificador de una notificaciòn en una entidad Documentos asociados a notificación judicial</param>
        /// <returns>Lista de entidades Documentos asociados a notificación judicial</returns>
        public List<DocNotificacionJudicial> ConsultarDocNotificacionJudicials_Consultar_IdNotJudicial(int? IdNotJudicial)
        {
            try
            {
                return vDocNotificacionJudicialBLL.ConsultarDocNotificacionJudicials_Consultar_IdNotJudicial(IdNotJudicial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta una nueva entidad Notificaciòn Judicial
        /// </summary>
        /// <param name="pNotificacionJudicial"></param>
        /// <returns></returns>
        public int InsertarNotificacionJudicial(NotificacionJudicial pNotificacionJudicial)
        {
            try
            {
                return vNotificacionJudicialBLL.InsertarNotificacionJudicial(pNotificacionJudicial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Notificaciòn judicial
        /// </summary>
        /// <param name="pNotificacionJudicial"></param>
        /// <returns></returns>
        public int ModificarNotificacionJudicial(NotificacionJudicial pNotificacionJudicial)
        {
            try
            {
                return vNotificacionJudicialBLL.ModificarNotificacionJudicial(pNotificacionJudicial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una notificaciòn judicial
        /// </summary>
        /// <param name="pNotificacionJudicial"></param>
        /// <returns></returns>
        public int EliminarNotificacionJudicial(NotificacionJudicial pNotificacionJudicial)
        {
            try
            {
                return vNotificacionJudicialBLL.EliminarNotificacionJudicial(pNotificacionJudicial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una notificaciòn judicial
        /// </summary>
        /// <param name="pIdNotJudicial"></param>
        /// <param name="pIdMunicipio"></param>
        /// <param name="pIdZona"></param>
        /// <param name="pDireccion"></param>
        /// <returns></returns>
        public NotificacionJudicial ConsultarNotificacionJudicial(int pIdNotJudicial, int pIdMunicipio, int pIdZona, int pDireccion)
        {
            try
            {
                return vNotificacionJudicialBLL.ConsultarNotificacionJudicial(pIdNotJudicial, pIdMunicipio, pIdZona, pDireccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Notificaciòn Judicial
        /// </summary>
        /// <param name="pIdNotJudicial"></param>
        /// <param name="pIdEntidad"></param>
        /// <param name="pIdDepartamento"></param>
        /// <param name="pIdMunicipio"></param>
        /// <returns></returns>
        public List<NotificacionJudicial> ConsultarNotificacionJudiciales(int? pIdNotJudicial, int? pIdEntidad, int? pIdDepartamento, int? pIdMunicipio)
        {
            try
            {
                return vNotificacionJudicialBLL.ConsultarNotificacionJudicials(pIdNotJudicial, pIdEntidad, pIdDepartamento, pIdMunicipio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de Notificaciòn judicial
        /// </summary>
        /// <param name="pIdEntidad"></param>
        /// <returns></returns>
        public List<NotificacionJudicial> ConsultarNotificacionJudiciales(int? pIdEntidad)
        {
            try
            {
                return vNotificacionJudicialBLL.ConsultarNotificacionJudicials(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Informacion Financiera

        /// <summary>
        /// Inserta una nueva entidad Información módulo financero
        /// </summary>
        /// <param name="pInfoFinancieraEntidad">Entidad Información módulo financero</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarInfoFinancieraEntidad(InfoFinancieraEntidad pInfoFinancieraEntidad, string pTipoPersona, string pTipoSector)
        {
            try
            {

                return vInfoFinancieraEntidadBLL.InsertarInfoFinancieraEntidad(pInfoFinancieraEntidad, pTipoPersona, pTipoSector);
            }
            //catch (UserInterfaceException)
            //{
            //    throw;
            //}
            catch (Exception ex)
            {
                //throw new GenericException(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Modificar una entidad Información módulo financero
        /// </summary>
        /// <param name="pInfoFinancieraEntidad">Entidad Información módulo financero</param>
        /// <returns>Identificador de la entidad Información módulo financero en tabla</returns>
        public int ModificarInfoFinancieraEntidad(InfoFinancieraEntidad pInfoFinancieraEntidad, string pTipopersona, string pTipoSector)
        {
            try
            {
                return vInfoFinancieraEntidadBLL.ModificarInfoFinancieraEntidad(pInfoFinancieraEntidad, pTipopersona, pTipoSector);
            }
            //catch (UserInterfaceException)
            //{
            //    throw;
            //}
            catch (Exception ex)
            {
                //throw new GenericException(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Eliminar una entidad Información módulo financero
        /// </summary>
        /// <param name="pInfoFinancieraEntidad">Identificador de una entidad Información módulo financero</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int EliminarInfoFinancieraEntidad(InfoFinancieraEntidad pInfoFinancieraEntidad)
        {
            try
            {
                return vInfoFinancieraEntidadBLL.EliminarInfoFinancieraEntidad(pInfoFinancieraEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta informaciòn de la Entidad Informaciòn financiera
        /// </summary>
        /// <param name="pIdInfoFin"></param>
        /// <returns></returns>
        public InfoFinancieraEntidad ConsultarInfoFinancieraEntidad(int pIdInfoFin)
        {
            try
            {
                return vInfoFinancieraEntidadBLL.ConsultarInfoFinancieraEntidad(pIdInfoFin);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Informaciòn financiera
        /// </summary>
        /// <param name="pIdEntidad"></param>
        /// <returns></returns>
        public List<InfoFinancieraEntidad> ConsultarInfoFinancieraEntidades(int? pIdEntidad)
        {
            try
            {
                return vInfoFinancieraEntidadBLL.ConsultarInfoFinancieraEntidads(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Valida documento oblogatorio
        /// </summary>
        /// <param name="pInfoFinancieraEntidad"></param>
        /// <param name="pTipoPersona"></param>
        /// <param name="pTipoSector"></param>
        public void ValidarDocumentosObligatorios(InfoFinancieraEntidad pInfoFinancieraEntidad, string pTipoPersona, string pTipoSector)
        {
            try
            {
                vInfoFinancieraEntidadBLL.ValidarDocumentosObligatorios(pInfoFinancieraEntidad, pTipoPersona, pTipoSector);
            }
            //catch (UserInterfaceException)
            //{
            //    throw;
            //}
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Información módulo financero
        /// </summary>
        /// <param name="pIdEntidad">Identificador general en una entidad Información módulo financero</param>
        /// <param name="pIdVigencia">Identificador de vigencia en una entidad Información módulo financero</param>
        /// <returns>Lista de entidades Información módulo financero</returns>
        public bool ValidaVigencia(int pIdEntidad, int pIdVigencia)
        {
            try
            {
                return vInfoFinancieraEntidadBLL.ValidaVigencia(pIdEntidad, pIdVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Finaliza InfoFinancieraEntidad
        /// </summary>
        /// <param name="pIdEntidad"> Id Entidad Proveedores Oferente</param>
        public int FinalizaInfoFinancieraEnidad(int pIdEntidad)
        {
            try
            {
                return vInfoFinancieraEntidadBLL.FinalizaInfoFinancieraEntidad(pIdEntidad);
            }
            catch (UserInterfaceException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion Informacion Financiera

        #region TipoSectorEntidad

        /// <summary>
        /// Inserta una nueva entidad Tipo Sector Entidad
        /// </summary>
        /// <param name="pTipoSectorEntidad">Entidad Tipo Sector Entidad</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarTipoSectorEntidad(TipoSectorEntidad pTipoSectorEntidad)
        {
            try
            {
                return vTipoSectorEntidadBLL.InsertarTipoSectorEntidad(pTipoSectorEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Tipo Sector Entidad
        /// </summary>
        /// <param name="pTipoSectorEntidad">Entidad Tipo Sector Entidad</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int ModificarTipoSectorEntidad(TipoSectorEntidad pTipoSectorEntidad)
        {
            try
            {
                return vTipoSectorEntidadBLL.ModificarTipoSectorEntidad(pTipoSectorEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Tipo Sector Entidad
        /// </summary>
        /// <param name="pTipoSectorEntidad">Entidad Tipo Sector Entidad</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int EliminarTipoSectorEntidad(TipoSectorEntidad pTipoSectorEntidad)
        {
            try
            {
                return vTipoSectorEntidadBLL.EliminarTipoSectorEntidad(pTipoSectorEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una entidad Tipo Sector Entidad
        /// </summary>
        /// <param name="pIdTipoSectorEntidad">Identificador en una entidad Tipo Sector Entidad</param>
        /// <returns>Entidad Tipo Sector Entidad</returns>
        public TipoSectorEntidad ConsultarTipoSectorEntidad(int pIdTipoSectorEntidad)
        {
            try
            {
                return vTipoSectorEntidadBLL.ConsultarTipoSectorEntidad(pIdTipoSectorEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de entidades Tipo Sector Entidad
        /// </summary>
        /// <param name="pCodigoSectorEntidad">Còdigo en una entidad Tipo Sector Entidad</param>
        /// <param name="pDescripcion">Descripciòn en una entidad Tipo Sector Entidad</param>
        /// <param name="pEstado">Estado en una entidad Tipo Sector Entidad</param>
        /// <returns>Lista de entidades Tipo Sector Entidad</returns>
        public List<TipoSectorEntidad> ConsultarTipoSectorEntidads(String pCodigoSectorEntidad, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vTipoSectorEntidadBLL.ConsultarTipoSectorEntidads(pCodigoSectorEntidad, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consultar una lista de entidades Tipo Sector Entidad
        /// </summary>
        /// <returns>Lista de entidades Tipo Sector Entidad</returns>
        public List<TipoSectorEntidad> ConsultarTipoSectorEntidadAll()
        {
            try
            {
                return vTipoSectorEntidadBLL.ConsultarTipoSectorEntidadAll();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region DocFinancieraProv

        /// <summary>
        /// Inserta una nueva entidad Documentos adjuntos en la información financiera del proveedor
        /// </summary>
        /// <param name="pDocFinancieraProv">Entidad Documentos adjuntos en la información financiera del proveedor</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int InsertarDocFinancieraProv(DocFinancieraProv pDocFinancieraProv)
        {
            try
            {
                return vDocFinancieraProvBLL.InsertarDocFinancieraProv(pDocFinancieraProv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Documentos adjuntos en la información financiera del proveedor
        /// </summary>
        /// <param name="pDocFinancieraProv">Entidad Documentos adjuntos en la información financiera del proveedor</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int ModificarDocFinancieraProv(DocFinancieraProv pDocFinancieraProv)
        {
            try
            {
                return vDocFinancieraProvBLL.ModificarDocFinancieraProv(pDocFinancieraProv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Documentos adjuntos en la información financiera del proveedor
        /// </summary>
        /// <param name="pDocFinancieraProv">Entidad Documentos adjuntos en la información financiera del proveedor</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int EliminarDocFinancieraProv(DocFinancieraProv pDocFinancieraProv)
        {
            try
            {
                return vDocFinancieraProvBLL.EliminarDocFinancieraProv(pDocFinancieraProv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Documentos adjuntos en la información financiera del proveedor
        /// </summary>
        /// <param name="pIdDocAdjunto">Identificador de una entidad Documentos adjuntos en la información financiera del proveedor</param>
        /// <returns>Entidad Documentos adjuntos en la información financiera del proveedor</returns>
        public DocFinancieraProv ConsultarDocFinancieraProv(int pIdDocAdjunto)
        {
            try
            {
                return vDocFinancieraProvBLL.ConsultarDocFinancieraProv(pIdDocAdjunto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Documentos adjuntos en la información financiera del proveedor
        /// </summary>
        /// <param name="pIdDocAdjunto">Identificador de la informaciòn financiera en una entidad Documentos adjuntos en la información financiera del proveedor</param>
        /// <param name="pRupRenovado">Rup renovado en una entidad Documentos adjuntos en la información financiera del proveedor</param>
        /// <param name="pTipoPersona">Tipo de persona en una entidad Documentos adjuntos en la información financiera del proveedor</param>
        /// <param name="pTipoSector">Tipo de sector en una entidad Documentos adjuntos en la información financiera del proveedor</param>
        /// <returns>Lista de entidades Documentos adjuntos en la información financiera del proveedor</returns>
        public List<DocFinancieraProv> ConsultarDocFinancieraProv_IdInfoFin_Rup(int pIdInfoFin, string pRupRenovado, string pTipoPersona, string pTipoSector)
        {
            try
            {
                return vDocFinancieraProvBLL.ConsultarDocFinancieraProv_IdInfoFin_Rup(pIdInfoFin, pRupRenovado, pTipoPersona, pTipoSector);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Documentos adjuntos en la información financiera del proveedor
        /// </summary>
        /// <param name="pIdTemporal">Identificador del Temporal en una entidad Documentos adjuntos en la información financiera del proveedor</param>
        /// <param name="pRupRenovado">Rup renovado en una entidad Documentos adjuntos en la información financiera del proveedor</param>
        /// <param name="pTipoPersona">Tipo de persona en una entidad Documentos adjuntos en la información financiera del proveedor</param>
        /// <param name="pTipoSector">Tipo de sector en una entidad Documentos adjuntos en la información financiera del proveedor</param>
        /// <returns>Lista de entidades Documentos adjuntos en la información financiera del proveedor</returns>
        public List<DocFinancieraProv> ConsultarDocFinancieraProv_IdTemporal_Rup(string pIdTemporal, string pRupRenovado, string pTipoPersona, string pTipoSector)
        {
            try
            {
                return vDocFinancieraProvBLL.ConsultarDocFinancieraProv_IdTemporal_Rup(pIdTemporal, pRupRenovado, pTipoPersona, pTipoSector);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion DocFinancieraProv

        #region TipoRegimenTributario

        /// <summary>
        /// Inserta una nueva entidad Tipo Régimen Tributario
        /// </summary>
        /// <param name="pTipoRegimenTributario">Entidad Tipo Régimen Tributario</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarTipoRegimenTributario(Icbf.Proveedor.Entity.TipoRegimenTributario pTipoRegimenTributario)
        {
            try
            {
                return vTipoRegimenTributarioBLL.InsertarTipoRegimenTributario(pTipoRegimenTributario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Tipo Régimen Tributario
        /// </summary>
        /// <param name="pTipoRegimenTributario">Entidad Tipo Régimen Tributario</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int ModificarTipoRegimenTributario(Icbf.Proveedor.Entity.TipoRegimenTributario pTipoRegimenTributario)
        {
            try
            {
                return vTipoRegimenTributarioBLL.ModificarTipoRegimenTributario(pTipoRegimenTributario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Tipo Régimen Tributario
        /// </summary>
        /// <param name="pTipoRegimenTributario">Entidad Tipo Régimen Tributario</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int EliminarTipoRegimenTributario(Icbf.Proveedor.Entity.TipoRegimenTributario pTipoRegimenTributario)
        {
            try
            {
                return vTipoRegimenTributarioBLL.EliminarTipoRegimenTributario(pTipoRegimenTributario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Tipo Régimen Tributario
        /// </summary>
        /// <param name="pIdTipoRegimenTributario">Identificador de una entidad Tipo Régimen Tributario</param>
        /// <returns>Entidad Tipo Régimen Tributario</returns>
        public Icbf.Proveedor.Entity.TipoRegimenTributario ConsultarTipoRegimenTributario(int pIdTipoRegimenTributario)
        {
            try
            {
                return vTipoRegimenTributarioBLL.ConsultarTipoRegimenTributario(pIdTipoRegimenTributario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Régimen Tributario
        /// </summary>
        /// <param name="pCodigoRegimenTributario">Còdigo en una entidad Tipo Régimen Tributario</param>
        /// <param name="pDescripcion">Descripciòn en una entidad Tipo Régimen Tributario</param>
        /// <param name="pIdTipoPersona">Identificador del tipo de persona en una entidad Tipo Régimen Tributario</param>
        /// <param name="pEstado">Estado en una entidad Tipo Régimen Tributario</param>
        /// <returns>Lista de entidades Tipo Régimen Tributario</returns>
        public List<Icbf.Proveedor.Entity.TipoRegimenTributario> ConsultarTipoRegimenTributarios(String pCodigoRegimenTributario, String pDescripcion, int? pIdTipoPersona, Boolean? pEstado)
        {
            try
            {
                return vTipoRegimenTributarioBLL.ConsultarTipoRegimenTributarios(pCodigoRegimenTributario, pDescripcion, pIdTipoPersona, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de entidades Tipo Régimen Tributario
        /// </summary>
        /// <param name="pIdTipoPersona">Identificador del tipo de persona en una entidad Tipo Régimen Tributario</param>
        /// <returns>Lista de entidades Tipo Régimen Tributario</returns>
        public List<Icbf.Proveedor.Entity.TipoRegimenTributario> ConsultarTipoRegimenTributarioTipoPersona(int? pIdTipoPersona)
        {
            try
            {
                return vTipoRegimenTributarioBLL.ConsultarTipoRegimenTributarioTipoPersona(pIdTipoPersona);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Tipoentidad

        /// <summary>
        /// Inserta una nueva entidad Tipo Entidad
        /// </summary>
        /// <param name="pTipoentidad">Entidad Tipo Entidad</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarTipoentidad(Tipoentidad pTipoentidad)
        {
            try
            {
                return vTipoentidadBLL.InsertarTipoentidad(pTipoentidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                //throw new GenericException(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Modifica una entidad Tipo Entidad
        /// </summary>
        /// <param name="pTipoentidad">Entidad Tipo Entidad</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int ModificarTipoentidad(Tipoentidad pTipoentidad)
        {
            try
            {
                return vTipoentidadBLL.ModificarTipoentidad(pTipoentidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                //throw new GenericException(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Elimina una entidad Tipo Entidad
        /// </summary>
        /// <param name="pTipoentidad">Entidad Tipo Entidad</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int EliminarTipoentidad(Tipoentidad pTipoentidad)
        {
            try
            {
                return vTipoentidadBLL.EliminarTipoentidad(pTipoentidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una entidad Tipo Entidad
        /// </summary>
        /// <param name="pIdTipoentidad">Identificador en una entidad Tipo Entidad</param>
        /// <returns>Entidad Tipo Entidad</returns>
        public Tipoentidad ConsultarTipoentidad(int pIdTipoentidad)
        {
            try
            {
                return vTipoentidadBLL.ConsultarTipoentidad(pIdTipoentidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Entidad
        /// </summary>
        /// <param name="pCodigoTipoentidad">Còdigo en una entidad Tipo Entidad</param>
        /// <param name="pDescripcion">Descripciòn en una entidad Tipo Entidad</param>
        /// <param name="pEstado">Estado en una entidad Tipo Entidad</param>
        /// <returns>Lista de entidades Tipo Entidad</returns>
        public List<Tipoentidad> ConsultarTipoentidads(String pCodigoTipoentidad, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vTipoentidadBLL.ConsultarTipoentidads(pCodigoTipoentidad, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Entidad
        /// </summary>
        /// <returns>Lista de entidades Tipo Entidad</returns>
        public List<Tipoentidad> ConsultarTipoentidadsAll(int? pIdTipoPersona, int? pIdSector)
        {
            try
            {
                return vTipoentidadBLL.ConsultarTipoentidadsAll(pIdTipoPersona, pIdSector);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region RamaOEstructura

        /// <summary>
        /// Inserta una nueva entidad Rama Estructura
        /// </summary>
        /// <param name="pRamaoEstructura">Entidad Rama Estructura</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarRamaoEstructura(RamaoEstructura pRamaoEstructura)
        {
            try
            {
                return vRamaoEstructuraBLL.InsertarRamaoEstructura(pRamaoEstructura);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Rama Estructura
        /// </summary>
        /// <param name="pRamaoEstructura">Entidad Rama Estructura</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int ModificarRamaoEstructura(RamaoEstructura pRamaoEstructura)
        {
            try
            {
                return vRamaoEstructuraBLL.ModificarRamaoEstructura(pRamaoEstructura);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Rama Estructura
        /// </summary>
        /// <param name="pRamaoEstructura">Entidad Rama Estructura</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int EliminarRamaoEstructura(RamaoEstructura pRamaoEstructura)
        {
            try
            {
                return vRamaoEstructuraBLL.EliminarRamaoEstructura(pRamaoEstructura);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Rama Estructura
        /// </summary>
        /// <param name="pRamaoEstructura">Identificador de una entidad Rama Estructura</param>
        /// <returns>Entidad Rama Estructura</returns>
        public RamaoEstructura ConsultarRamaoEstructura(int pIdRamaEstructura)
        {
            try
            {
                return vRamaoEstructuraBLL.ConsultarRamaoEstructura(pIdRamaEstructura);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Rama Estructura
        /// </summary>
        /// <param name="pCodigoRamaEstructura">Còdigo de una entidad Rama Estructura</param>
        /// <param name="pDescripcion">Descripciòn de una entidad Rama Estructura</param>
        /// <param name="pEstado">Estado de una entidad Rama Estructura</param>
        /// <returns>Lista de entidades Rama Estructura</returns>
        public List<RamaoEstructura> ConsultarRamaoEstructuras(String pCodigoRamaEstructura, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vRamaoEstructuraBLL.ConsultarRamaoEstructuras(pCodigoRamaEstructura, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Rama Estructura
        /// </summary>
        /// <returns>Lista de entidades Rama Estructura</returns>
        public List<RamaoEstructura> ConsultarRamaoEstructurasAll()
        {
            try
            {
                return vRamaoEstructuraBLL.ConsultarRamaoEstructurasAll();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region NiveldeGobierno

        /// <summary>
        /// Inserta una nueva entidad Nivel de gobierno
        /// </summary>
        /// <param name="pNiveldegobierno">Entidad Nivel de gobierno</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarNiveldegobierno(Niveldegobierno pNiveldegobierno)
        {
            try
            {
                return vNiveldegobiernoBLL.InsertarNiveldegobierno(pNiveldegobierno);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Nivel de gobierno
        /// </summary>
        /// <param name="pNiveldegobierno">Entidad Nivel de gobierno</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int ModificarNiveldegobierno(Niveldegobierno pNiveldegobierno)
        {
            try
            {
                return vNiveldegobiernoBLL.ModificarNiveldegobierno(pNiveldegobierno);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Nivel de gobierno
        /// </summary>
        /// <param name="pNiveldegobierno">Entidad Nivel de gobierno</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int EliminarNiveldegobierno(Niveldegobierno pNiveldegobierno)
        {
            try
            {
                return vNiveldegobiernoBLL.EliminarNiveldegobierno(pNiveldegobierno);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Nivel de gobierno
        /// </summary>
        /// <param name="pIdNiveldegobierno">Identificador de una entidad Nivel de gobierno</param>
        /// <returns>Entidad Nivel de gobierno</returns>
        public Niveldegobierno ConsultarNiveldegobierno(int pIdNiveldegobierno)
        {
            try
            {
                return vNiveldegobiernoBLL.ConsultarNiveldegobierno(pIdNiveldegobierno);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Nivel de gobierno
        /// </summary>
        /// <param name="pCodigoNiveldegobierno">Còdigo en una entidad Nivel de gobierno</param>
        /// <param name="pDescripcion">Descripciòn en una entidad Nivel de gobierno</param>
        /// <param name="pEstado">Estado en una entidad Nivel de gobierno</param>
        /// <returns>Lista de entidades Nivel de gobierno</returns>
        public List<Niveldegobierno> ConsultarNiveldegobiernos(String pCodigoNiveldegobierno, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vNiveldegobiernoBLL.ConsultarNiveldegobiernos(pCodigoNiveldegobierno, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Nivel de gobierno
        /// </summary>
        /// <returns>Lista de Entidades Nivel de gobierno</returns>
        public List<Niveldegobierno> ConsultarNiveldegobiernosAll()
        {
            try
            {
                return vNiveldegobiernoBLL.ConsultarNiveldegobiernosAll();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Nivel de gobierno
        /// </summary>
        /// <param name="pIdRamaEstructura">Identificador de la rama estructura en una entidad Nivel de gobierno</param>
        /// <returns>Lista de entidades Nivel de gobierno</returns>
        public List<Niveldegobierno> ConsultarNiveldegobiernosRamaoEstructura(int pIdRamaEstructura)
        {
            try
            {
                return vNiveldegobiernoBLL.ConsultarNiveldegobiernosRamaoEstructura(pIdRamaEstructura);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region NivelOrganizacional

        /// <summary>
        /// Inserta una nueva entidad Nivel organizacional
        /// </summary>
        /// <param name="pNivelOrganizacional">Entidad Nivel organizacional</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarNivelOrganizacional(NivelOrganizacional pNivelOrganizacional)
        {
            try
            {
                return vNivelOrganizacionalBLL.InsertarNivelOrganizacional(pNivelOrganizacional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Nivel organizacional
        /// </summary>
        /// <param name="pNivelOrganizacional">Entidad Nivel organizacional</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int ModificarNivelOrganizacional(NivelOrganizacional pNivelOrganizacional)
        {
            try
            {
                return vNivelOrganizacionalBLL.ModificarNivelOrganizacional(pNivelOrganizacional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Nivel organizacional
        /// </summary>
        /// <param name="pNivelOrganizacional">Entidad Nivel organizacional</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int EliminarNivelOrganizacional(NivelOrganizacional pNivelOrganizacional)
        {
            try
            {
                return vNivelOrganizacionalBLL.EliminarNivelOrganizacional(pNivelOrganizacional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Nivel organizacional
        /// </summary>
        /// <param name="pNivelOrganizacional">Identificador de una entidad Nivel organizacional</param>
        /// <returns>Entidad Nivel organizacional</returns>
        public NivelOrganizacional ConsultarNivelOrganizacional(int pIdNivelOrganizacional)
        {
            try
            {
                return vNivelOrganizacionalBLL.ConsultarNivelOrganizacional(pIdNivelOrganizacional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Nivel organizacional
        /// </summary>
        /// <param name="pCodigoNivelOrganizacional">Còdigo en una entidad Nivel organizacional</param>
        /// <param name="pDescripcion">Descripciòn en una entidad Nivel organizacional</param>
        /// <param name="pEstado">Estado en una entidad Nivel organizacional</param>
        /// <returns>Lista de entidades Nivel organizacional</returns>
        public List<NivelOrganizacional> ConsultarNivelOrganizacionals(String pCodigoNivelOrganizacional, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vNivelOrganizacionalBLL.ConsultarNivelOrganizacionals(pCodigoNivelOrganizacional, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Nivel organizacional
        /// </summary>
        /// <returns>Lista de entidades Nivel organizacional</returns>
        public List<NivelOrganizacional> ConsultarNivelOrganizacionalAll()
        {
            try
            {
                return vNivelOrganizacionalBLL.ConsultarNivelOrganizacionalAll();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Nivel organizacional de acuero a IdRama y IdNivelGob
        /// </summary>
        /// <returns>Lista de entidades Nivel organizacional</returns>
        public List<NivelOrganizacional> ConsultarNivelOrganizacional_PorRamaYNivelGobierno(int idRama, int idNivelGob)
        {
            try
            {
                return vNivelOrganizacionalBLL.ConsultarNivelOrganizacional_PorRamaYNivelGobierno(idRama, idNivelGob);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region TipoEntidadPublica

        /// <summary>
        /// Inserta una nueva entidad Tipo Entidad Pública
        /// </summary>
        /// <param name="pTipodeentidadPublica">Entidad Tipo Entidad Pública</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarTipodeentidadPublica(TipodeentidadPublica pTipodeentidadPublica)
        {
            try
            {
                return vTipodeentidadPublicaBLL.InsertarTipodeentidadPublica(pTipodeentidadPublica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                //throw new GenericException(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Modifica una entidad Tipo Entidad Pública
        /// </summary>
        /// <param name="pTipodeentidadPublica">Entidad Tipo Entidad Pública</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int ModificarTipodeentidadPublica(TipodeentidadPublica pTipodeentidadPublica)
        {
            try
            {
                return vTipodeentidadPublicaBLL.ModificarTipodeentidadPublica(pTipodeentidadPublica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                //throw new GenericException(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Elimina una entidad Tipo Entidad Pública
        /// </summary>
        /// <param name="pTipodeentidadPublica">Entidad Tipo Entidad Pública</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int EliminarTipodeentidadPublica(TipodeentidadPublica pTipodeentidadPublica)
        {
            try
            {
                return vTipodeentidadPublicaBLL.EliminarTipodeentidadPublica(pTipodeentidadPublica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Tipo Entidad Pública
        /// </summary>
        /// <param name="pIdTipodeentidadPublica">Identificador en una entidad Tipo Entidad Pública</param>
        /// <returns>Entidad Tipo Entidad Pública</returns>
        public TipodeentidadPublica ConsultarTipodeentidadPublica(int pIdTipodeentidadPublica)
        {
            try
            {
                return vTipodeentidadPublicaBLL.ConsultarTipodeentidadPublica(pIdTipodeentidadPublica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Entidad Pública
        /// </summary>
        /// <param name="pCodigoTipodeentidadPublica">Còdigo en una entidad Tipo Entidad Pública</param>
        /// <param name="pDescripcion">Descripciòn en una entidad Tipo Entidad Pública</param>
        /// <param name="pEstado">Estado en una entidad Tipo Entidad Pública</param>
        /// <returns>Lista de entidades Tipo Entidad Pública</returns>
        public List<TipodeentidadPublica> ConsultarTipodeentidadPublicas(String pCodigoTipodeentidadPublica, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vTipodeentidadPublicaBLL.ConsultarTipodeentidadPublicas(pCodigoTipodeentidadPublica, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Entidad Pública
        /// </summary>
        /// <returns>Lista de entidades Tipo Entidad Pública</returns>
        public List<TipodeentidadPublica> ConsultarTipodeentidadPublicasAll()
        {
            try
            {
                return vTipodeentidadPublicaBLL.ConsultarTipodeentidadPublicasAll();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Entidad Pública por IdRama. IdNivelGob, IdNivelOrganizacional
        /// </summary>
        /// <returns>Lista de entidades Tipo Entidad Pública</returns>
        public List<TipodeentidadPublica> ConsultarTipodeentidadPublicas_PorRamaNivelGobYNivelOrg(int idRama, int idNivelGob, int idNivelOrg)
        {
            try
            {
                return vTipodeentidadPublicaBLL.ConsultarTipodeentidadPublicas_PorRamaNivelGobYNivelOrg(idRama, idNivelGob, idNivelOrg);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region TipodeActividad

        /// <summary>
        /// Inserta una nueva entidad Tipo Actividad
        /// </summary>
        /// <param name="pTipodeActividad">Entidad Tipo Actividad</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarTipodeActividad(TipodeActividad pTipodeActividad)
        {
            try
            {
                return vTipodeActividadBLL.InsertarTipodeActividad(pTipodeActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Tipo Actividad
        /// </summary>
        /// <param name="pTipodeActividad">Entidad Tipo Actividad</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int ModificarTipodeActividad(TipodeActividad pTipodeActividad)
        {
            try
            {
                return vTipodeActividadBLL.ModificarTipodeActividad(pTipodeActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Tipo Actividad
        /// </summary>
        /// <param name="pTipodeActividad">Entidad Tipo Actividad</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int EliminarTipodeActividad(TipodeActividad pTipodeActividad)
        {
            try
            {
                return vTipodeActividadBLL.EliminarTipodeActividad(pTipodeActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Tipo Actividad
        /// </summary>
        /// <param name="pIdTipodeActividad">Identificador en una entidad Tipo Actividad</param>
        /// <returns>Entidad Tipo Actividad</returns>
        public TipodeActividad ConsultarTipodeActividad(int pIdTipodeActividad)
        {
            try
            {
                return vTipodeActividadBLL.ConsultarTipodeActividad(pIdTipodeActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Actividad
        /// </summary>
        /// <param name="pCodigoTipodeActividad">Còdigo en una entidad Tipo Actividad</param>
        /// <param name="pDescripcion">Descripciòn en una entidad Tipo Actividad</param>
        /// <param name="pEstado">Estado en una entidad Tipo Actividad</param>
        /// <returns>Lista de entidades Tipo Actividad</returns>
        public List<TipodeActividad> ConsultarTipodeActividads(String pCodigoTipodeActividad, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vTipodeActividadBLL.ConsultarTipodeActividads(pCodigoTipodeActividad, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch
                (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Actividad
        /// </summary>
        /// <returns>Lista de entidades Tipo Actividad</returns>
        public List<TipodeActividad> ConsultarTipodeActividadAll()
        {
            try
            {
                return vTipodeActividadBLL.ConsultarTipodeActividadAll();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region ClaseActividad

        /// <summary>
        /// Inserta una nueva entidad Clase de Actividad
        /// </summary>
        /// <param name="pClaseActividad">Entidad Clase de Actividad</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarClaseActividad(ClaseActividad pClaseActividad)
        {
            try
            {
                return vClaseActividadBLL.InsertarClaseActividad(pClaseActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Clase de Actividad
        /// </summary>
        /// <param name="pClaseActividad">Entidad Clase de Actividad</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int ModificarClaseActividad(ClaseActividad pClaseActividad)
        {
            try
            {
                return vClaseActividadBLL.ModificarClaseActividad(pClaseActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Clase de Actividad
        /// </summary>
        /// <param name="pIdClaseActividad">Identificador en una entidad Clase de Actividad</param>
        /// <returns>Entidad Clase de Actividad</returns>
        public ClaseActividad ConsultarClaseActividad(int pIdClaseActividad)
        {
            try
            {
                return vClaseActividadBLL.ConsultarClaseActividad(pIdClaseActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de clases de actividades
        /// </summary>
        /// <param name="pCodigo">Còdigo de la clase de actividad</param>
        /// <param name="pDescripcion">Descripciòn de la clase de actividad</param>
        /// <param name="pEstado">Estado de la clase de actividad</param>
        /// <returns>Lista de clases de actividades</returns>
        public List<ClaseActividad> ConsultarClaseActividads(String pCodigo, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vClaseActividadBLL.ConsultarClaseActividads(pCodigo, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch
                (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        #endregion

        #region Estado Validacion Documental

        /// <summary>
        /// Inserta una nueva entidad Estado validación Documental
        /// </summary>
        /// <param name="pEstadoValidacionDocumental">Entidad Estado validación Documental</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int InsertarEstadoValidacionDocumental(EstadoValidacionDocumental pEstadoValidacionDocumental)
        {
            try
            {
                return vEstadoValidacionDocumentalBLL.InsertarEstadoValidacionDocumental(pEstadoValidacionDocumental);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Estado validación Documental
        /// </summary>
        /// <param name="pEstadoValidacionDocumental">Entidad Estado validación Documental</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int ModificarEstadoValidacionDocumental(EstadoValidacionDocumental pEstadoValidacionDocumental)
        {
            try
            {
                return vEstadoValidacionDocumentalBLL.ModificarEstadoValidacionDocumental(pEstadoValidacionDocumental);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Estado validación Documental
        /// </summary>
        /// <param name="pEstadoValidacionDocumental">Entidad Estado validación Documental</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int EliminarEstadoValidacionDocumental(EstadoValidacionDocumental pEstadoValidacionDocumental)
        {
            try
            {
                return vEstadoValidacionDocumentalBLL.EliminarEstadoValidacionDocumental(pEstadoValidacionDocumental);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Estado validación Documental
        /// </summary>
        /// <param name="pIdEstadoValidacionDocumental">Identificador de la entidad Estado validación Documental</param>
        /// <returns>Entidad Estado validación Documental</returns>
        public EstadoValidacionDocumental ConsultarEstadoValidacionDocumental(int pIdEstadoValidacionDocumental)
        {
            try
            {
                return vEstadoValidacionDocumentalBLL.ConsultarEstadoValidacionDocumental(pIdEstadoValidacionDocumental);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Estado validación Documental
        /// </summary>
        /// <param name="pCodigoEstadoValidacionDocumental">Còdigo de estado de validaciòn en una entidad Estado validación Documental</param>
        /// <param name="pDescripcion">Descripciòn en una entidad Estado validación Documental</param>
        /// <param name="pEstado">Estado de una entidad Estado validación Documental</param>
        /// <returns>Lista de entidades Estado validación Documental</returns>
        public List<EstadoValidacionDocumental> ConsultarEstadoValidacionDocumentals(String pCodigoEstadoValidacionDocumental, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vEstadoValidacionDocumentalBLL.ConsultarEstadoValidacionDocumentals(pCodigoEstadoValidacionDocumental, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion Estado Validacion Documental

        #region ClasedeEntidad

        /// <summary>
        /// Inserta una nueva entidad Clase Entidad
        /// </summary>
        /// <param name="pClasedeEntidad">Entidad Clase Entidad</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarClasedeEntidad(ClasedeEntidad pClasedeEntidad)
        {
            try
            {
                return vClasedeEntidadBLL.InsertarClasedeEntidad(pClasedeEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una Clase Entidad
        /// </summary>
        /// <param name="pClasedeEntidad">Entidad Clase Entidad</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int ModificarClasedeEntidad(ClasedeEntidad pClasedeEntidad)
        {
            try
            {
                return vClasedeEntidadBLL.ModificarClasedeEntidad(pClasedeEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una Clase Entidad
        /// </summary>
        /// <param name="pClasedeEntidad">Entidad Clase Entidad</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int EliminarClasedeEntidad(ClasedeEntidad pClasedeEntidad)
        {
            try
            {
                return vClasedeEntidadBLL.EliminarClasedeEntidad(pClasedeEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una Clase Entidad
        /// </summary>
        /// <param name="pIdClasedeEntidad">Identificador de una Clase Entidad</param>
        /// <returns>Entidad Clase Entidad</returns>
        public ClasedeEntidad ConsultarClasedeEntidad(int pIdClasedeEntidad)
        {
            try
            {
                return vClasedeEntidadBLL.ConsultarClasedeEntidad(pIdClasedeEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Clase Entidad
        /// </summary>
        /// <param name="pIdTipodeActividad">Identificador del Tipo de Actividad</param>
        /// <param name="pDescripcion">Descripciòn de una Clase Entidad</param>
        /// <param name="pEstado">Estado de la Clase Entidad</param>
        /// <returns>Lista de entidades Clase Entidad</returns>
        public List<ClasedeEntidad> ConsultarClasedeEntidads(int? pIdTipodeActividad, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vClasedeEntidadBLL.ConsultarClasedeEntidads(pIdTipodeActividad, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Clase Entidad
        /// </summary>
        /// <param name="pIdTipodeActividad">Identificador del Tipo de Actividad</param>
        /// <returns>Lista de entidades Clase Entidad</returns>
        public List<ClasedeEntidad> ConsultarClasedeEntidadAll(int? pIdTipodeActividad)
        {
            try
            {
                return vClasedeEntidadBLL.ConsultarClasedeEntidadAll(pIdTipodeActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        ///// <summary>
        ///// Consulta una lista de entidades Clase Entidad
        ///// </summary>
        ///// <param name="pIdTipodeActividad">Identificador del Tipo de Actividad</param>
        ///// <param name="pIdTipoEntidad">Identificador del Tipo de Entidad</param>
        ///// <returns>Lista de entidades Clase Entidad</returns>
        //public List<ClasedeEntidad> ConsultarClasedeEntidadTipodeActividadTipoEntidad(int? pIdTipodeActividad, int? pIdTipoEntidad, int? pIdTipoPersona, int? pIdSector, int? pIdRegmenTributario)
        //{
        //    try
        //    {
        //        return vClasedeEntidadBLL.ConsultarClasedeEntidadTipodeActividadTipoEntidad(pIdTipodeActividad, pIdTipoEntidad, pIdTipoPersona, pIdSector, pIdRegmenTributario);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        /// <summary>
        /// Consulta una lista de entidades Clase Entidad
        /// </summary>
        /// <param name="pIdTipodeActividad">Identificador del Tipo de Actividad</param>
        /// <param name="pIdTipoEntidad">Identificador del Tipo de Entidad</param>
        /// <returns>Lista de entidades Clase Entidad</returns>
        //public List<ClasedeEntidad> ConsultarClasedeEntidadTipodeActividadTipoEntidad(int? pIdTipodeActividad, int? pIdTipoEntidad, int? pIdTipoPersona, int? pIdSector, int? pIdRegmenTributario)
        //{
        //    try
        //    {
        //        return vClasedeEntidadBLL.ConsultarClasedeEntidadTipodeActividadTipoEntidad(pIdTipodeActividad, pIdTipoEntidad, pIdTipoPersona, pIdSector, pIdRegmenTributario);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        /// <summary>
        /// Consulta una lista de entidades Clase Entidad
        /// </summary>
        /// <param name="pIdTipodeActividad">Identificador del Tipo de Actividad</param>
        /// <param name="pIdTipoEntidad">Identificador del Tipo de Entidad</param>
        /// <returns>Lista de entidades Clase Entidad</returns>
        public List<ClasedeEntidad> ConsultarClasedeEntidadTipodeActividadTipoEntidad2(int? pIdTipodeActividad, int? pIdTipoEntidad, int? pIdTipoPersona, int? pIdSector, int? pIdRegmenTributario)
        {
            try
            {
                return vClasedeEntidadBLL.ConsultarClasedeEntidadTipodeActividadTipoEntidad(pIdTipodeActividad, pIdTipoEntidad, pIdTipoPersona, pIdSector, pIdRegmenTributario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }





        #endregion

        #region ContactoEntidad

        /// <summary>
        /// Inserta un nuevo Contacto Entidad
        /// </summary>
        /// <param name="pContactoEntidad">Entidad Contacto Entidad</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int InsertarContactoEntidad(ContactoEntidad pContactoEntidad)
        {
            try
            {
                return vContactoEntidadBLL.InsertarContactoEntidad(pContactoEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica un Contacto Entidad
        /// </summary>
        /// <param name="pContactoEntidad">Entidad Contacto Entidad</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int ModificarContactoEntidad(ContactoEntidad pContactoEntidad)
        {
            try
            {
                return vContactoEntidadBLL.ModificarContactoEntidad(pContactoEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina un Contacto Entidad
        /// </summary>
        /// <param name="pContactoEntidad">Entidad Contacto Entidad</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int EliminarContactoEntidad(ContactoEntidad pContactoEntidad)
        {
            try
            {
                return vContactoEntidadBLL.EliminarContactoEntidad(pContactoEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta un Contacto Entidad
        /// </summary>
        /// <param name="pIdContacto">Identificador de un Contacto Entidad</param>
        /// <returns>Entidad Contacto Entidad</returns>
        public ContactoEntidad ConsultarContactoEntidad(int pIdContacto)
        {
            try
            {
                return vContactoEntidadBLL.ConsultarContactoEntidad(pIdContacto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Contacto Entidad
        /// </summary>
        /// <param name="pIdEntidad">Identificador del Contacto Entidad</param>
        /// <param name="pNumeroIdentificacion">Nùmero de identificaciòn del Contacto Entidad</param>
        /// <param name="pPrimerNombre">Primer nombre del Contacto Entidad</param>
        /// <param name="pSegundoNombre">Segundo nombre del Contacto Entidad</param>
        /// <param name="pPrimerApellido">Primer apellido del Contacto Entidad</param>
        /// <param name="pSegundoApellido">Segundo apellido del Contacto Entidad</param>
        /// <param name="pDependencia">Dependencia del Contacto Entidad</param>
        /// <param name="pEmail">Correo electrònico del Contacto Entidad</param>
        /// <param name="pEstado">Estado del Contacto Entidad</param>
        /// <returns>Lista de entidades Clase Entidad</returns>
        public List<ContactoEntidad> ConsultarContactoEntidads(int? pIdEntidad, Decimal? pNumeroIdentificacion, String pPrimerNombre, String pSegundoNombre, String pPrimerApellido, String pSegundoApellido, String pDependencia, String pEmail, Boolean? pEstado)
        {
            try
            {
                return vContactoEntidadBLL.ConsultarContactoEntidads(pIdEntidad, pNumeroIdentificacion, pPrimerNombre, pSegundoNombre, pPrimerApellido, pSegundoApellido, pDependencia, pEmail, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region InfoAdminEntidad

        /// <summary>
        /// Inserta una nueva entidad Información Administrativa
        /// </summary>
        /// <param name="pInfoAdminEntidad">Entidad Información Administrativa</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int InsertarInfoAdminEntidad(InfoAdminEntidad pInfoAdminEntidad)
        {
            try
            {
                return vInfoAdminEntidadBLL.InsertarInfoAdminEntidad(pInfoAdminEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Información Administrativa
        /// </summary>
        /// <param name="pInfoAdminEntidad">Entidad Información Administrativa</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int ModificarInfoAdminEntidad(InfoAdminEntidad pInfoAdminEntidad)
        {
            try
            {
                return vInfoAdminEntidadBLL.ModificarInfoAdminEntidad(pInfoAdminEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Información Administrativa
        /// </summary>
        /// <param name="pInfoAdminEntidad">Entidad Información Administrativa</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int EliminarInfoAdminEntidad(InfoAdminEntidad pInfoAdminEntidad)
        {
            try
            {
                return vInfoAdminEntidadBLL.EliminarInfoAdminEntidad(pInfoAdminEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Información Administrativa
        /// </summary>
        /// <param name="pIdInfoAdmin">Identificador en una entidad Información Administrativa</param>
        /// <returns>Entidad Información Administrativa</returns>
        public InfoAdminEntidad ConsultarInfoAdminEntidad(int pIdInfoAdmin)
        {
            try
            {
                return vInfoAdminEntidadBLL.ConsultarInfoAdminEntidad(pIdInfoAdmin);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Información Administrativa
        /// </summary>
        /// <param name="pIdEntidad">Identificador en una entidad Información Administrativa</param>
        /// <returns>Lista de entidades Información Administrativa</returns>
        public List<InfoAdminEntidad> ConsultarInfoAdminEntidads(int? pIdEntidad)
        {
            try
            {
                return vInfoAdminEntidadBLL.ConsultarInfoAdminEntidads(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {

                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Información Administrativa
        /// </summary>
        /// <param name="pIdEntidad">Identificador en una entidad Información Administrativa</param>
        /// <returns>Entidad Información Administrativa</returns>
        public InfoAdminEntidad ConsultarInfoAdminEntidadIdEntidad(int pIdEntidad)
        {
            try
            {
                return vInfoAdminEntidadBLL.ConsultarInfoAdminEntidadIdEntidad(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region DocDatosBasicoProv

        /// <summary>
        /// Inserta una nueva entidad Documentos asociados al proveedor
        /// </summary>
        /// <param name="pDocDatosBasicoProv">Entidad Documentos asociados al proveedor</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int InsertarDocDatosBasicoProv(DocDatosBasicoProv pDocDatosBasicoProv)
        {
            try
            {
                return vDocDatosBasicoProvBLL.InsertarDocDatosBasicoProv(pDocDatosBasicoProv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Documentos asociados al proveedor
        /// </summary>
        /// <param name="pDocDatosBasicoProv">Entidad Documentos asociados al proveedor</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int ModificarDocDatosBasicoProv(DocDatosBasicoProv pDocDatosBasicoProv)
        {
            try
            {
                return vDocDatosBasicoProvBLL.ModificarDocDatosBasicoProv(pDocDatosBasicoProv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Documentos asociados al proveedor
        /// </summary>
        /// <param name="pDocDatosBasicoProv">Entidad Documentos asociados al proveedor</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int EliminarDocDatosBasicoProv(DocDatosBasicoProv pDocDatosBasicoProv)
        {
            try
            {
                return vDocDatosBasicoProvBLL.EliminarDocDatosBasicoProv(pDocDatosBasicoProv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta una entidad Documentos asociados al proveedor
        /// </summary>
        /// <param name="pIdDocAdjunto">Identificador de la entidad Documentos asociados al proveedor</param>
        /// <returns>Entidad Documentos asociados al proveedor</returns>
        public DocDatosBasicoProv ConsultarDocDatosBasicoProv(int pIdDocAdjunto)
        {
            try
            {
                return vDocDatosBasicoProvBLL.ConsultarDocDatosBasicoProv(pIdDocAdjunto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta una lista de entidades Documentos asociados al proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador del tipo en la entidad Documentos asociados al proveedor</param>
        /// <param name="pTipoPersona">Tipo de persona en la entidad Documentos asociados al proveedor</param>
        /// <param name="pTipoSector">Tipo de Sector en la entidad Documentos asociados al proveedor</param>
        /// <returns>Lista de entidades Documentos asociados al proveedor</returns>
        public List<DocDatosBasicoProv> ConsultarDocDatosBasicooProv_IdEntidad_TipoPersona(int pIdEntidad, String pTipoPersona, String pTipoSector, string pIdTemporal, string pTipoEntidad)
        {
            try
            {
                return vDocDatosBasicoProvBLL.ConsultarDocDatosBasicoProv_IdEntidad_TipoPersona(pIdEntidad, pTipoPersona, pTipoSector, pIdTemporal, pTipoEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta una lista de entidades Documentos asociados al proveedor
        /// </summary>
        /// <param name="pIdTemporal">Identificador del Temporal en la entidad Documentos asociados al proveedor</param>
        /// <param name="pTipoPersona">Tipo de persona en la entidad Documentos asociados al proveedor</param>
        /// <param name="pTipoSector">Tipo de Sector en la entidad Documentos asociados al proveedor</param>
        /// <returns>Lista de entidades Documentos asociados al proveedor</returns>
        public List<DocDatosBasicoProv> ConsultarDocDatosBasicooProv_IdTemporal_TipoPersona(string pIdTemporal, String pTipoPersona, String pTipoSector, string pTipoEntidad)
        {
            try
            {
                return vDocDatosBasicoProvBLL.ConsultarDocDatosBasicoProv_IdTemporal_TipoPersona(pIdTemporal, pTipoPersona, pTipoSector, pTipoEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region TipoDocIdentifica

        /// <summary>
        /// Inserta una nueva entidad Tipo Identificación
        /// </summary>
        /// <param name="pTipoDocIdentifica">Entidad Tipo Identificación</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarTipoDocIdentifica(TipoDocIdentifica pTipoDocIdentifica)
        {
            try
            {
                return vTipoDocIdentificaBLL.InsertarTipoDocIdentifica(pTipoDocIdentifica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modificar una entidad Tipo Identificación
        /// </summary>
        /// <param name="pTipoDocIdentifica">Entidad Tipo Identificación</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int ModificarTipoDocIdentifica(TipoDocIdentifica pTipoDocIdentifica)
        {
            try
            {
                return vTipoDocIdentificaBLL.ModificarTipoDocIdentifica(pTipoDocIdentifica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Tipo Identificación
        /// </summary>
        /// <param name="pTipoDocIdentifica">Entidad Tipo Identificación</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int EliminarTipoDocIdentifica(TipoDocIdentifica pTipoDocIdentifica)
        {
            try
            {
                return vTipoDocIdentificaBLL.EliminarTipoDocIdentifica(pTipoDocIdentifica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Tipo Identificación
        /// </summary>
        /// <param name="pIdTipoDocIdentifica">Identificador en una entidad Tipo Identificación</param>
        /// <returns>Entidad Tipo Identificación</returns>
        public TipoDocIdentifica ConsultarTipoDocIdentifica(int pIdTipoDocIdentifica)
        {
            try
            {
                return vTipoDocIdentificaBLL.ConsultarTipoDocIdentifica(pIdTipoDocIdentifica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Obtiene los tipos de documentos a través del IDdel Documento y el Id del programa
        /// </summary>
        /// <param name="pIdTipoDocIdentifica">Identificador en una entidad Tipo Identificación</param>
        /// <param name="Programa">Identificador del programa</param>
        /// <returns>Entidad Tipo Identificación</returns>
        public TipoDocIdentifica ConsultarTipoDocIdentificaByIdPrograma(int pIdTipoDocIdentifica, string Programa)
        {
            try
            {
                return vTipoDocIdentificaBLL.ConsultarTipoDocIdentificaByIdPrograma(pIdTipoDocIdentifica, Programa);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Identificación
        /// </summary>
        /// <param name="pCodigoDocIdentifica">Còdigo en una entidad Tipo Identificación</param>
        /// <param name="pDescripcion">Descripciòn en una entidad Tipo Identificación</param>
        /// <returns>Lista de entidades Tipo Identificación</returns>
        public List<TipoDocIdentifica> ConsultarTipoDocIdentificas(Decimal? pCodigoDocIdentifica, String pDescripcion)
        {
            try
            {
                return vTipoDocIdentificaBLL.ConsultarTipoDocIdentificas(pCodigoDocIdentifica, pDescripcion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Identificación
        /// </summary>
        /// <returns>Lista de entidades Tipo Identificación</returns>
        public List<TipoDocIdentifica> ConsultarTipoDocIdentificaAll()
        {
            try
            {
                return vTipoDocIdentificaBLL.ConsultarTipoDocIdentificaAll();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Tipo Cargo Entidad

        /// <summary>
        /// Inserta una nueva entidad Tipo cargo
        /// </summary>
        /// <param name="pTipoCargoEntidad">Entidad Tipo cargo</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarTipoCargoEntidad(TipoCargoEntidad pTipoCargoEntidad)
        {
            try
            {
                return vTipoCargoEntidadBLL.InsertarTipoCargoEntidad(pTipoCargoEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                //throw new GenericException(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Modifica una entidad Tipo cargo
        /// </summary>
        /// <param name="pTipoCargoEntidad">Entidad Tipo cargo</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int ModificarTipoCargoEntidad(TipoCargoEntidad pTipoCargoEntidad)
        {
            try
            {
                return vTipoCargoEntidadBLL.ModificarTipoCargoEntidad(pTipoCargoEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                //throw new GenericException(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Eliminar una entidad Tipo cargo
        /// </summary>
        /// <param name="pTipoCargoEntidad">Entidad Tipo cargo</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int EliminarTipoCargoEntidad(TipoCargoEntidad pTipoCargoEntidad)
        {
            try
            {
                return vTipoCargoEntidadBLL.EliminarTipoCargoEntidad(pTipoCargoEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una entidad Tipo cargo
        /// </summary>
        /// <param name="pIdTipoCargoEntidad">Identificador de una entidad Tipo cargo</param>
        /// <returns>Entidad Tipo cargo</returns>
        public TipoCargoEntidad ConsultarTipoCargoEntidad(int pIdTipoCargoEntidad)
        {
            try
            {
                return vTipoCargoEntidadBLL.ConsultarTipoCargoEntidad(pIdTipoCargoEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de entidades Tipo cargo
        /// </summary>
        /// <param name="pCodigo">Còdigo de una Tipo cargo</param>
        /// <param name="pDescripcion">Descripciòn de una entidad Tipo cargo</param>
        /// <param name="pEstado">Estado de una entidad Tipo cargo</param>
        /// <returns>Lista de entidades Tipo cargo</returns>
        public List<TipoCargoEntidad> ConsultarTipoCargoEntidads(String pCodigo, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vTipoCargoEntidadBLL.ConsultarTipoCargoEntidads(pCodigo, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de entidades Tipo cargo
        /// </summary>
        /// <returns>Lista de entidades Tipo cargo</returns>
        public List<TipoCargoEntidad> ConsultarTipoCargoEntidadAll()
        {
            try
            {
                return vTipoCargoEntidadBLL.ConsultarTipoCargoEntidadAll();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region EstadoDatosBasicos

        /// <summary>
        /// Inserta una nueva entidad Estado Datos Básicos
        /// </summary>
        /// <param name="pEstadoDatosBasicos">Entidad Estado Datos Básicos</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int InsertarEstadoDatosBasicos(EstadoDatosBasicos pEstadoDatosBasicos)
        {
            try
            {
                return vEstadoDatosBasicosBLL.InsertarEstadoDatosBasicos(pEstadoDatosBasicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Estado Datos Básicos
        /// </summary>
        /// <param name="pEstadoDatosBasicos">Entidad Estado Datos Básicos</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int ModificarEstadoDatosBasicos(EstadoDatosBasicos pEstadoDatosBasicos)
        {
            try
            {
                return vEstadoDatosBasicosBLL.ModificarEstadoDatosBasicos(pEstadoDatosBasicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Estado Datos Básicos
        /// </summary>
        /// <param name="pEstadoDatosBasicos">Entidad Estado Datos Básicos</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int EliminarEstadoDatosBasicos(EstadoDatosBasicos pEstadoDatosBasicos)
        {
            try
            {
                return vEstadoDatosBasicosBLL.EliminarEstadoDatosBasicos(pEstadoDatosBasicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Estado Datos Básicos
        /// </summary>
        /// <param name="pIdEstadoDatosBasicos">Identificador de una entidad Estado Datos Básicos</param>
        /// <returns>Entidad Estado Datos Básicos</returns>
        public EstadoDatosBasicos ConsultarEstadoDatosBasicos(int pIdEstadoDatosBasicos)
        {
            try
            {
                return vEstadoDatosBasicosBLL.ConsultarEstadoDatosBasicos(pIdEstadoDatosBasicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Estado Datos Básicos
        /// </summary>
        /// <param name="pDescripcion">Descripciòn en una entidad Estado Datos Básicos</param>
        /// <param name="pEstado">Estado en una entidad Estado Datos Básicos</param>
        /// <returns>Lista de entidades Estado Datos Básicos</returns>
        public List<EstadoDatosBasicos> ConsultarEstadoDatosBasicoss(String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vEstadoDatosBasicosBLL.ConsultarEstadoDatosBasicossAll(pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Estado Datos Básicos
        /// </summary>
        /// <param name="pEstado">Estado en una entidad Estado Datos Básicos</param>
        /// <returns>Lista de entidades Estado Datos Básicos</returns>
        public List<EstadoDatosBasicos> ConsultarEstadoDatosBasicossAll(Boolean? pEstado)
        {
            try
            {
                return vEstadoDatosBasicosBLL.ConsultarEstadoDatosBasicossAll(pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Sucursal

        /// <summary>
        /// Inserta una nueva entidad Sucursal
        /// </summary>
        /// <param name="pSucursal">Entidad Sucursal</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarSucursal(Sucursal pSucursal)
        {
            try
            {
                return vSucursalBLL.InsertarSucursal(pSucursal);
            }
            //catch (UserInterfaceException)
            //{
            //    throw;
            //}
            catch (Exception ex)
            {
                throw ex; // new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Sucursal
        /// </summary>
        /// <param name="pSucursal">Entidad Sucursal</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int ModificarSucursal(Sucursal pSucursal)
        {
            try
            {
                return vSucursalBLL.ModificarSucursal(pSucursal);
            }
            //catch (UserInterfaceException)
            //{
            //    throw;
            //}
            catch (Exception ex)
            {
                throw ex;// new GenericException(ex);
            }
        }

        /// <summary>
        /// Método que Modifica la Sucursal Default
        /// </summary>
        /// <param name="pSucursal">Entidad Sucursal</param>
        /// <returns></returns>
        public int ModificarSucursalDefault(Sucursal pSucursal)
        {
            try
            {
                return vSucursalBLL.ModificarSucursalDefault(pSucursal);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Elimina una entidad Sucursal
        /// </summary>
        /// <param name="pSucursal">Entidad Sucursal</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int EliminarSucursal(Sucursal pSucursal)
        {
            try
            {
                return vSucursalBLL.EliminarSucursal(pSucursal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Sucursal
        /// </summary>
        /// <param name="pIdSucursal">Identificador de una entidad Sucursal</param>
        /// <returns>Entidad Sucursal</returns>
        public Sucursal ConsultarSucursal(int IdSucursal)
        {
            try
            {
                return vSucursalBLL.ConsultarSucursal(IdSucursal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Sucursal
        /// </summary>
        /// <param name="pIdEntidad">Identificador general de una entidad Sucursal</param>
        /// <param name="pEstado">estado de una entidad Sucursal</param>
        /// <returns>Lista de entidades Sucursal</returns>
        public List<Sucursal> ConsultarSucursals(int? pIdEntidad, int? pEstado)
        {
            try
            {
                return vSucursalBLL.ConsultarSucursals(pIdEntidad, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion Sucursal

        #region InfoExperienciaEntidad

        /// <summary>
        /// Inserta una nueva entidad Información Módulo Experiencia
        /// </summary>
        /// <param name="pInfoExperienciaEntidad">Entidad Información Módulo Experiencia</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int InsertarInfoExperienciaEntidad(InfoExperienciaEntidad pInfoExperienciaEntidad)
        {
            try
            {
                return vInfoExperienciaEntidadBLL.InsertarInfoExperienciaEntidad(pInfoExperienciaEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Información Módulo Experiencia
        /// </summary>
        /// <param name="pInfoExperienciaEntidad">Entidad Información Módulo Experiencia</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int ModificarInfoExperienciaEntidad(InfoExperienciaEntidad pInfoExperienciaEntidad)
        {
            try
            {
                return vInfoExperienciaEntidadBLL.ModificarInfoExperienciaEntidad(pInfoExperienciaEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Información Módulo Experiencia
        /// </summary>
        /// <param name="pInfoExperienciaEntidad">Entidad Información Módulo Experiencia</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int EliminarInfoExperienciaEntidad(InfoExperienciaEntidad pInfoExperienciaEntidad)
        {
            try
            {
                return vInfoExperienciaEntidadBLL.EliminarInfoExperienciaEntidad(pInfoExperienciaEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Información Módulo Experiencia
        /// </summary>
        /// <param name="pIdExpEntidad">Identificador de una entidad Información Módulo Experiencia</param>
        /// <returns>Entidad Información Módulo Experiencia</returns>
        public InfoExperienciaEntidad ConsultarInfoExperienciaEntidad(int pIdExpEntidad)
        {
            try
            {
                return vInfoExperienciaEntidadBLL.ConsultarInfoExperienciaEntidad(pIdExpEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Información Módulo Experiencia
        /// </summary>
        /// <param name="pIdEntidad">Identificador general de una entidad Información Módulo Experiencia</param>
        /// <returns>Lista de entidades Información Módulo Experiencia</returns>
        public List<InfoExperienciaEntidad> ConsultarInfoExperienciaEntidads(int? pIdEntidad)
        {
            try
            {
                return vInfoExperienciaEntidadBLL.ConsultarInfoExperienciaEntidads(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Finaliza InfoFinancieraEntidad
        /// </summary>
        /// <param name="pIdEntidad"> Id Entidad Proveedores Oferente</param>
        public int FinalizaInfoExperienciaEntidad(int pIdEntidad)
        {
            try
            {
                return vInfoExperienciaEntidadBLL.FinalizaInfoExperienciaEntidad(pIdEntidad);
            }
            catch (UserInterfaceException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region TipoCodigoUNSPSC

        /// <summary>
        /// Inserta una nueva entidad Código UNSPSC
        /// </summary>
        /// <param name="pTipoCodigoUNSPSC">Entidad ódigo UNSPSC</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarTipoCodigoUNSPSC(TipoCodigoUNSPSC pTipoCodigoUNSPSC)
        {
            try
            {
                return vTipoCodigoUNSPSCBLL.InsertarTipoCodigoUNSPSC(pTipoCodigoUNSPSC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Código UNSPSC
        /// </summary>
        /// <param name="pTipoCodigoUNSPSC">Entidad ódigo UNSPSC</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int ModificarTipoCodigoUNSPSC(TipoCodigoUNSPSC pTipoCodigoUNSPSC)
        {
            try
            {
                return vTipoCodigoUNSPSCBLL.ModificarTipoCodigoUNSPSC(pTipoCodigoUNSPSC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Código UNSPSC
        /// </summary>
        /// <param name="pTipoCodigoUNSPSC">Entidad ódigo UNSPSC</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int EliminarTipoCodigoUNSPSC(TipoCodigoUNSPSC pTipoCodigoUNSPSC)
        {
            try
            {
                return vTipoCodigoUNSPSCBLL.EliminarTipoCodigoUNSPSC(pTipoCodigoUNSPSC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una entidad Código UNSPSC
        /// </summary>
        /// <param name="pIdTipoCodUNSPSC">Identificador de una entidad Código UNSPSC</param>
        /// <returns>Entidad Código UNSPSC</returns>
        public TipoCodigoUNSPSC ConsultarTipoCodigoUNSPSC(int pIdTipoCodUNSPSC)
        {
            try
            {
                return vTipoCodigoUNSPSCBLL.ConsultarTipoCodigoUNSPSC(pIdTipoCodUNSPSC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Código UNSPSC
        /// </summary>
        /// <param name="pCodigo">Còdigo en una entidad Código UNSPSC</param>
        /// <param name="pDescripcion">Descripciòn en una entidad Código UNSPSC</param>
        /// <param name="pEstado">Estado en una entidad Código UNSPSC</param>
        /// <param name="pLongitudUNSPSC">Longitud en una entidad Código UNSPSC</param>
        /// <returns>Lista de entidades Código UNSPSC</returns>
        public List<TipoCodigoUNSPSC> ConsultarTipoCodigoUNSPSCs(String pCodigo, String pDescripcion, Boolean? pEstado, int? pLongitudUNSPSC, String pCodigoSegmento, String pCodigoFamilia, String pCodigoClase)
        {
            try
            {
                return vTipoCodigoUNSPSCBLL.ConsultarTipoCodigoUNSPSCs(pCodigo, pDescripcion, pEstado, pLongitudUNSPSC, pCodigoSegmento, pCodigoFamilia, pCodigoClase);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Metodo que Consulta los Segmentos para la entidad TipoCodigoUNSPSC
        /// </summary>
        /// <returns></returns>
        public List<TipoCodigoUNSPSC> ConsultarSegmento()
        {
            try
            {
                return vTipoCodigoUNSPSCBLL.ConsultarSegmento();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Metodo que Consulta las Familias por Segmento para la entidad TipoCodigoUNSPSC
        /// </summary>
        /// <param name="pCodigoSegmento">Código del Segmento</param>
        /// <returns></returns>
        public List<TipoCodigoUNSPSC> ConsultarFamilia(String pCodigoSegmento)
        {
            try
            {
                return vTipoCodigoUNSPSCBLL.ConsultarFamilia(pCodigoSegmento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Metodo que Consulta las Clases por Familias para la entidad TipoCodigoUNSPSC
        /// </summary>
        /// <param name="pCodigoFamilia">Código de la Familia</param>
        /// <returns></returns>
        public List<TipoCodigoUNSPSC> ConsultarClase(String pCodigoFamilia)
        {
            try
            {
                return vTipoCodigoUNSPSCBLL.ConsultarClase(pCodigoFamilia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region CodigoUNSPSCProveedor

        /// <summary>
        /// Consultar los códigos UNSPSC de un proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador del proveedor</param>
        /// <returns>Entidad Código UNSPSC</returns>
        public List<CodigoUNSPSCProveedor> ConsultarCodigoUNSPSCProveedor(int pIdTercero)
        {
            try
            {
                return vCodigoUNSPSCProveedorBLL.ConsultarCodigoUNSPSCProveedor(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta una nueva entidad Códigos UNSPSC de un proveedor
        /// </summary>
        /// <param name="pCodigoUNSPSCProveedor">Entidad Códigos UNSPSC de Proveedor</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int InsertarCodUNSPSCProveedor(CodigoUNSPSCProveedor pCodigoUNSPSCProveedor)
        {
            try
            {
                return vCodigoUNSPSCProveedorBLL.InsertarCodUNSPSProveedorCEntidad(pCodigoUNSPSCProveedor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Códigos UNSPSC de Proveedor
        /// </summary>
        /// <param name="pCodigoUNSPSCProveedor">Entidad Códigos UNSPSC de experiencia</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int EliminarCodigoUNSPSCProveedor(CodigoUNSPSCProveedor pCodigoUNSPSCProveedor)
        {
            try
            {
                return vCodigoUNSPSCProveedorBLL.EliminarCodUNSPSCProveedor(pCodigoUNSPSCProveedor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        #endregion

        #region DocExperienciaEntidad

        /// <summary>
        /// Consulta una lista de entidades Documentos adjuntos a información de experiencia
        /// </summary>
        /// <param name="pIdExpEntidad">Identificador de experiencia en una entidad Documentos adjuntos a información de experiencia</param>
        /// <param name="pRupRenovado">Rup Renovado en una entidad Documentos adjuntos a información de experiencia</param>
        /// <param name="pTipoPersona">Tipo de persona en una entidad Documentos adjuntos a información de experiencia</param>
        /// <param name="pTipoSector">Tipo de sector en una entidad Documentos adjuntos a información de experiencia</param>
        /// <returns>Lista de entidades Documentos adjuntos a información de experiencia</returns>
        public List<DocExperienciaEntidad> ConsultarDocExperienciaEntidad_IdExpEntidad_Rup(int pIdExpEntidad, string pRupRenovado, string pTipoPersona, string pTipoSector)
        {
            try
            {
                return vDocExperienciaEntidadBLL.ConsultarDocExperienciaEntidad_IdExpEntidad_Rup(pIdExpEntidad, pRupRenovado, pTipoPersona, pTipoSector);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Documentos adjuntos a información de experiencia
        /// </summary>
        /// <param name="pIdTemporal">Identificador del temporal en una entidad Documentos adjuntos a información de experiencia</param>
        /// <param name="pRupRenovado">Rup Renovado en una entidad Documentos adjuntos a información de experiencia</param>
        /// <param name="pTipoPersona">Tipo de persona en una entidad Documentos adjuntos a información de experiencia</param>
        /// <param name="pTipoSector">Tipo de sector en una entidad Documentos adjuntos a información de experiencia</param>
        /// <returns>Lista de entidades Documentos adjuntos a información de experiencia</returns>
        public List<DocExperienciaEntidad> ConsultarDocExperienciaEntidad_IdTemporal_Rup(string pIdTemporal, string pRupRenovado, string pTipoPersona, string pTipoSector)
        {
            try
            {
                return vDocExperienciaEntidadBLL.ConsultarDocExperienciaEntidad_IdTemporal_Rup(pIdTemporal, pRupRenovado, pTipoPersona, pTipoSector);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta una nueva entidad Documentos adjuntos a información de experiencia
        /// </summary>
        /// <param name="pDocExperienciaEntidad">Entidad Documentos adjuntos a información de experiencia</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int InsertarDocExperienciaEntidad(DocExperienciaEntidad pDocExperienciaEntidad)
        {
            try
            {
                return vDocExperienciaEntidadBLL.InsertarDocExperienciaEntidad(pDocExperienciaEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// modifica una entidad Documentos adjuntos a información de experiencia
        /// </summary>
        /// <param name="pDocExperienciaEntidad">Entidad Documentos adjuntos a información de experiencia</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int ModificarDocExperienciaEntidad(DocExperienciaEntidad pDocExperienciaEntidad)
        {
            try
            {
                return vDocExperienciaEntidadBLL.ModificarDocExperienciaEntidad(pDocExperienciaEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Documentos adjuntos a información de experiencia
        /// </summary>
        /// <param name="pDocExperienciaEntidad">Entidad Documentos adjuntos a información de experiencia</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int EliminarDocExperienciaEntidad(DocExperienciaEntidad pDocExperienciaEntidad)
        {
            try
            {
                return vDocExperienciaEntidadBLL.EliminarDocExperienciaEntidad(pDocExperienciaEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Documentos adjuntos a información de experiencia
        /// </summary>
        /// <param name="pIdDocAdjunto">Identificador de una entidad Documentos adjuntos a información de experiencia</param>
        /// <returns>Entidad Documentos adjuntos a información de experiencia</returns>
        public DocExperienciaEntidad ConsultarDocExperienciaEntidad(int pIdDocAdjunto)
        {
            try
            {
                return vDocExperienciaEntidadBLL.ConsultarDocExperienciaEntidad(pIdDocAdjunto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Documentos adjuntos a información de experiencia
        /// </summary>
        /// <param name="pIdExpEntidad">Identificador de experiencia en una entidad Documentos adjuntos a información de experiencia</param>
        /// <param name="pDescripcion">Descripciòn en una entidad Documentos adjuntos a información de experiencia</param>
        /// <param name="pLinkDocumento">Liga del documento en una entidad Documentos adjuntos a información de experiencia</param>
        /// <returns>Lista de entidades Documentos adjuntos a información de experiencia</returns>
        public List<DocExperienciaEntidad> ConsultarDocExperienciaEntidads(int? pIdExpEntidad, String pDescripcion, String pLinkDocumento)
        {
            try
            {
                return vDocExperienciaEntidadBLL.ConsultarDocExperienciaEntidads(pIdExpEntidad, pDescripcion, pLinkDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region ExperienciaCodUNSPSCEntidad

        /// <summary>
        /// Inserta una nueva entidad Códigos UNSPSC de experiencia
        /// </summary>
        /// <param name="pExperienciaCodUNSPSCEntidad">Entidad Códigos UNSPSC de experiencia</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int InsertarExperienciaCodUNSPSCEntidad(ExperienciaCodUNSPSCEntidad pExperienciaCodUNSPSCEntidad)
        {
            try
            {
                return vExperienciaCodUNSPSCEntidadBLL.InsertarExperienciaCodUNSPSCEntidad(pExperienciaCodUNSPSCEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }




        /// <summary>
        /// Modifica una entidad Códigos UNSPSC de experiencia
        /// </summary>
        /// <param name="pExperienciaCodUNSPSCEntidad">Entidad Códigos UNSPSC de experiencia</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int ModificarExperienciaCodUNSPSCEntidad(ExperienciaCodUNSPSCEntidad pExperienciaCodUNSPSCEntidad)
        {
            try
            {
                return vExperienciaCodUNSPSCEntidadBLL.ModificarExperienciaCodUNSPSCEntidad(pExperienciaCodUNSPSCEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Códigos UNSPSC de experiencia
        /// </summary>
        /// <param name="pExperienciaCodUNSPSCEntidad">Entidad Códigos UNSPSC de experiencia</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int EliminarExperienciaCodUNSPSCEntidad(ExperienciaCodUNSPSCEntidad pExperienciaCodUNSPSCEntidad)
        {
            try
            {
                return vExperienciaCodUNSPSCEntidadBLL.EliminarExperienciaCodUNSPSCEntidad(pExperienciaCodUNSPSCEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }




        /// <summary>
        /// Consulta una entidad Códigos UNSPSC de experiencia
        /// </summary>
        /// <param name="pIdExpCOD">Identificador de una entidad Códigos UNSPSC de experiencia</param>
        /// <returns>Entidad Códigos UNSPSC de experiencia</returns>
        public ExperienciaCodUNSPSCEntidad ConsultarExperienciaCodUNSPSCEntidad(int pIdExpCOD)
        {
            try
            {
                return vExperienciaCodUNSPSCEntidadBLL.ConsultarExperienciaCodUNSPSCEntidad(pIdExpCOD);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Códigos UNSPSC de experiencia
        /// </summary>
        /// <param name="pIdExpEntidad">Identificador de una entidad Códigos UNSPSC de experiencia</param>
        /// <returns>Lista de entidades Códigos UNSPSC de experiencia</returns>
        public List<ExperienciaCodUNSPSCEntidad> ConsultarExperienciaCodUNSPSCEntidads(int? pIdExpEntidad)
        {
            try
            {
                return vExperienciaCodUNSPSCEntidadBLL.ConsultarExperienciaCodUNSPSCEntidads(pIdExpEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Acuerdos

        /// <summary>
        /// Inserta un nuevo Acuerdo
        /// </summary>
        /// <param name="pAcuerdos">Entidad Acuerdos</param>
        /// <returns>Identificador del Acuerdo en tabla</returns>
        public int InsertarAcuerdos(Acuerdos pAcuerdos)
        {
            try
            {
                return vAcuerdos.InsertarAcuerdos(pAcuerdos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta un Acuerdo
        /// </summary>
        /// <param name="pIdAcuerdo">Identificador del Acuerdo</param>
        /// <returns>Entidad Acuerdos</returns>
        public Acuerdos ConsultarAcuerdos(int pIdAcuerdo)
        {
            try
            {
                return vAcuerdos.ConsultarAcuerdos(pIdAcuerdo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de Acuerdos
        /// </summary>
        /// <param name="pNombre">Nombre del Acuerdo</param>
        /// <returns>Lista de Entidades Acuerdos</returns>
        public List<Acuerdos> ConsultarAcuerdoss(String pNombre)
        {
            try
            {
                return vAcuerdos.ConsultarAcuerdoss(pNombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta un Acuerdo Vigente
        /// </summary>
        /// <param></param>
        /// <returns>Entidad Acuerdos</returns>
        public Acuerdos ConsultarAcuerdoVigente()
        {
            try
            {
                return vAcuerdos.ConsultarAcuerdoVigente();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region ValidarTercero

        /// <summary>
        /// Inserta una nueva entidad validación de terceros
        /// </summary>
        /// <param name="pValidarTercero">Entidad validación de terceros</param>
        /// <param name="idEstadoTercero">Identificador del estado</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarValidarTercero(ValidarTercero pValidarTercero, int idEstadoTercero)
        {
            try
            {
                return vValidarTerceroBLL.InsertarValidarTercero(pValidarTercero, idEstadoTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad validación de terceros
        /// </summary>
        /// <param name="pIdValidarTercero">Identificador en una entidad validación de terceros</param>
        /// <returns>Entidad validación de terceros</returns>
        public ValidarTercero ConsultarValidarTercero(int pIdValidarTercero)
        {
            try
            {
                return vValidarTerceroBLL.ConsultarValidarTercero(pIdValidarTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad validación de terceros
        /// </summary>
        /// <param name="pIdTercero">Identificador de una entidad validación de terceros</param>
        /// <returns>Entidad validación de terceros</returns>
        public ValidarTercero ConsultarValidarTercero_Tercero(int pIdTercero)
        {
            try
            {
                return vValidarTerceroBLL.ConsultarValidarTercero_Tercero(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades validación de terceros
        /// </summary>
        /// <param name="pIdTercero">Identificador del tercero</param>
        /// <param name="pObservaciones">Observaciones en una entidad validación de terceros</param>
        /// <param name="pConfirmaYAprueba">Es Confirma ya aprueba valor en una entidad validación de terceros</param>
        /// <returns>Lista de entidades validación de terceros</returns>
        public List<ValidarTercero> ConsultarValidarTerceros(Int32 pIdTercero, String pObservaciones, Boolean? pConfirmaYAprueba)
        {
            try
            {
                return vValidarTerceroBLL.ConsultarValidarTerceros(pIdTercero, pObservaciones, pConfirmaYAprueba);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region ValidarInfoFinancieraEntidad

        /// <summary>
        /// Inserta una nueva entidad Validación de información financiera del proveedor
        /// </summary>
        /// <param name="pValidarInfoFinancieraEntidad">Entidad Validación de información financiera del proveedor</param>
        /// <param name="idEstadoInfoFinancieraEntidad">Identificador del estado en una entidad Validación de información financiera del proveedor</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarValidarInfoFinancieraEntidad(ValidarInfoFinancieraEntidad pValidarInfoFinancieraEntidad, int idEstadoInfoFinancieraEntidad)
        {
            try
            {
                return vValidarInfoFinancieraEntidadBLL.InsertarValidarInfoFinancieraEntidad(pValidarInfoFinancieraEntidad, idEstadoInfoFinancieraEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Validación de información financiera del proveedor
        /// </summary>
        /// <param name="pValidarInfoFinancieraEntidad">Entidad Validación de información financiera del proveedor</param>
        /// <param name="idEstadoInfoFinancieraEntidad">Identificador del estado en una entidad Validación de información financiera del proveedor</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int ModificarValidarInfoFinancieraEntidad(ValidarInfoFinancieraEntidad pValidarInfoFinancieraEntidad, int idEstadoInfoFinancieraEntidad)
        {
            try
            {
                return vValidarInfoFinancieraEntidadBLL.ModificarValidarInfoFinancieraEntidad(pValidarInfoFinancieraEntidad, idEstadoInfoFinancieraEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Validación de información financiera del proveedor
        /// </summary>
        /// <param name="pIdValidarInfoFinancieraEntidad">Identificador en una entidad Validación de información financiera del proveedor</param>
        /// <returns>Entidad Validación de información financiera del proveedor</returns>
        public ValidarInfoFinancieraEntidad ConsultarValidarInfoFinancieraEntidad(int pIdValidarInfoFinancieraEntidad)
        {
            try
            {
                return vValidarInfoFinancieraEntidadBLL.ConsultarValidarInfoFinancieraEntidad(pIdValidarInfoFinancieraEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Validación de información financiera del proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador de la entidad</param>
        /// <param name="pObservaciones">Observaciones en la entidad Validación de información financiera del proveedor</param>
        /// <param name="pConfirmaYAprueba">Es Confirma y aprueba valor en la entidad Validación de información financiera del proveedor</param>
        /// <returns>Lista de entidades Validación de información financiera del proveedor</returns>
        public List<ValidarInfoFinancieraEntidad> ConsultarValidarInfoFinancieraEntidads(Int32 pIdInfoFinancieraEntidad, String pObservaciones, Boolean? pConfirmaYAprueba)
        {
            try
            {
                return vValidarInfoFinancieraEntidadBLL.ConsultarValidarInfoFinancieraEntidads(pIdInfoFinancieraEntidad, pObservaciones, pConfirmaYAprueba);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ValidarInfoFinancieraEntidad> ConsultarValidarInfoFinancieraEntidadsResumen(Int32 pIdEntidad, String pObservaciones, Boolean? pConfirmaYAprueba)
        {
            try
            {
                return vValidarInfoFinancieraEntidadBLL.ConsultarValidarInfoFinancieraEntidadsResumen(pIdEntidad, pObservaciones, pConfirmaYAprueba);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Validación de información financiera del proveedor
        /// </summary>
        /// <param name="pIdInfoFin">Identificador de informaciòn financiera en una entidad Validación de información financiera del proveedor</param>
        /// <param name="pObservaciones">Observaciones en una entidad Validación de información financiera del proveedor</param>
        /// <param name="pConfirmaYAprueba">Es confirma y aprueba valor en una entidad Validación de información financiera del proveedor</param>
        /// <returns>Lista  de entidades Validación de información financiera del proveedor</returns>
        public ValidarInfoFinancieraEntidad ConsultarValidarInfoFinancieraEntidad_Ultima(Int32 pIdEntidad, int pIdVigencia)
        {
            try
            {
                return vValidarInfoFinancieraEntidadBLL.ConsultarValidarInfoFinancieraEntidad_Ultima(pIdEntidad, pIdVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region ValidarInfoExperienciaEntidad

        /// <summary>
        /// Inserta una nueva entidad Validación de experiencia
        /// </summary>
        /// <param name="pValidarInfoExperienciaEntidad">Entidad Validación de experiencia</param>
        /// <param name="idEstadoInfoExperienciaEntidad">Identificador del estado</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarValidarInfoExperienciaEntidad(ValidarInfoExperienciaEntidad pValidarInfoExperienciaEntidad, int idEstadoInfoExperienciaEntidad)
        {
            try
            {
                return this.vValidarInfoExperienciaEntidadBLL.InsertarValidarInfoExperienciaEntidad(pValidarInfoExperienciaEntidad, idEstadoInfoExperienciaEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Validación de experiencia
        /// </summary>
        /// <param name="pValidarInfoExperienciaEntidad">Entidad Validación de experiencia</param>
        /// <param name="idEstadoInfoExperienciaEntidad">Identificador del estado</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int ModificarValidarInfoExperienciaEntidad(ValidarInfoExperienciaEntidad pValidarInfoExperienciaEntidad, int idEstadoInfoExperienciaEntidad)
        {
            try
            {
                return this.vValidarInfoExperienciaEntidadBLL.ModificarValidarInfoExperienciaEntidad(pValidarInfoExperienciaEntidad, idEstadoInfoExperienciaEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Validación de experiencia
        /// </summary>
        /// <param name="pIdValidarInfoExperienciaEntidad">Identificador de una entidad Validación de experiencia</param>
        /// <returns>Entidad Validación de experiencia</returns>
        public ValidarInfoExperienciaEntidad ConsultarValidarInfoExperienciaEntidad(int pIdValidarInfoExperienciaEntidad)
        {
            try
            {
                return vValidarInfoExperienciaEntidadBLL.ConsultarValidarInfoExperienciaEntidad(pIdValidarInfoExperienciaEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Validación de experiencia
        /// </summary>
        /// <param name="pIdExpEntidad">Identificador de la entidad</param>
        /// <param name="pObservaciones">Observaciones en una entidad Validación de experiencia</param>
        /// <param name="pConfirmaYAprueba">Es confirma y aprueba valor en la entidad Validación de experiencia</param>
        /// <returns>Lista de entidades Validación de experiencia</returns>
        public List<ValidarInfoExperienciaEntidad> ConsultarValidarInfoExperienciaEntidads(Int32 pIdExpEntidad)
        {
            try
            {
                return vValidarInfoExperienciaEntidadBLL.ConsultarValidarInfoExperienciaEntidads(pIdExpEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Validación de experiencia
        /// </summary>
        /// <param name="pIdEntidad">Identificador de la entidad</param>
        /// <param name="pObservaciones">Observaciones en la entidad Validación de experiencia</param>
        /// <param name="pConfirmaYAprueba">Es confirma y aprueba valor en la entidad Validación de experiencia</param>
        /// <returns>Lista de entidades Validación de experiencia</returns>
        public List<ValidarInfoExperienciaEntidad> ConsultarValidarInfoExperienciaEntidadsResumen(Int32 pIdEntidad, String pObservaciones, Boolean? pConfirmaYAprueba)
        {
            try
            {
                return vValidarInfoExperienciaEntidadBLL.ConsultarValidarInfoExperienciaEntidadsResumen(pIdEntidad, pObservaciones, pConfirmaYAprueba);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Validación de experiencia
        /// </summary>
        /// <param name="pIdEntidad">Identificador de la entidad</param>
        /// <param name="pIdExpEntidad">Identificador de la experiencia en la entidad Validación de experiencia</param>
        /// <returns>Entidad Validación de experiencia</returns>
        public ValidarInfoExperienciaEntidad ConsultarValidarInfoExperienciaEntidad_Ultima(Int32 pIdEntidad, Int32 pIdExpEntidad)
        {
            try
            {
                return vValidarInfoExperienciaEntidadBLL.ConsultarValidarInfoExperienciaEntidad_Ultima(pIdEntidad, pIdExpEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region ValidarInfoDatosBasicosEntidad

        /// <summary>
        /// Insertar una entidad Validación de datos básicos del proveedor
        /// </summary>
        /// <param name="pValidarInfoDatosBasicosEntidad">Entidad validación de datos básicos del proveedor</param>
        /// <param name="idEstadoInfoDatosBasicosEntidad">Identificador de estado en una entidad validación de datos básicos del proveedor</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarValidarInfoDatosBasicosEntidad(ValidarInfoDatosBasicosEntidad pValidarInfoDatosBasicosEntidad, int idEstadoInfoDatosBasicosEntidad)
        {
            try
            {
                return this.vValidarInfoDatosBasicosEntidadBLL.InsertarValidarInfoDatosBasicosEntidad(pValidarInfoDatosBasicosEntidad, idEstadoInfoDatosBasicosEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad validación de datos básicos del proveedor
        /// </summary>
        /// <param name="pIdValidarInfoDatosBasicosEntidad">Identificador en una entidad validación de datos básicos del proveedor</param>
        /// <returns>Entidad validación de datos básicos del proveedor</returns>
        public ValidarInfoDatosBasicosEntidad ConsultarValidarInfoDatosBasicosEntidad(int pIdValidarInfoDatosBasicosEntidad)
        {
            try
            {
                return vValidarInfoDatosBasicosEntidadBLL.ConsultarValidarInfoDatosBasicosEntidad(pIdValidarInfoDatosBasicosEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades validación de datos básicos del proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador de entidad en una entidad validación de datos básicos del proveedor</param>
        /// <param name="pObservaciones">Observaciones en una entidad validación de datos básicos del proveedor</param>
        /// <param name="pConfirmaYAprueba">Es confirma y aprueba valor en la entidad validación de datos básicos del proveedor</param>
        /// <returns>Lista de entidades validación de datos básicos del proveedor</returns>
        public List<ValidarInfoDatosBasicosEntidad> ConsultarValidarInfoDatosBasicosEntidads(Int32 pIdEntidad, String pObservaciones, Boolean? pConfirmaYAprueba)
        {
            try
            {
                return vValidarInfoDatosBasicosEntidadBLL.ConsultarValidarInfoDatosBasicosEntidads(pIdEntidad, pObservaciones, pConfirmaYAprueba);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Consulta una lista de entidades validación de datos básicos del proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador en una entidad validación de datos básicos del proveedor</param>
        /// <param name="pObservaciones">Observaciones en una entidad validación de datos básicos del proveedor</param>
        /// <param name="pConfirmaYAprueba">Es confirma y aprueba valor en una entidad validación de datos básicos del proveedor</param>
        /// <returns>Lista de entidades validación de datos básicos del proveedor</returns>
        public List<ValidarInfoDatosBasicosEntidad> ConsultarValidarInfoDatosBasicosEntidadsResumen(Int32 pIdEntidad, String pObservaciones, Boolean? pConfirmaYAprueba)
        {
            try
            {
                return vValidarInfoDatosBasicosEntidadBLL.ConsultarValidarInfoDatosBasicosEntidadsResumen(pIdEntidad, pObservaciones, pConfirmaYAprueba);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Validación de datos básicos del proveedor
        /// </summary>
        /// <param name="pValidarInfoDatosBasicosEntidad">Entidad validación de datos básicos del proveedor</param>
        /// <param name="idEstadoInfoDatosBasicosEntidad">Identificador de estado en una entidad validación de datos básicos del proveedor</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int ModificarValidarInfoDatosBasicosEntidad(ValidarInfoDatosBasicosEntidad pValidarInfoDatosBasicosEntidad, int idEstadoInfoDatosBasicosEntidad)
        {
            try
            {
                return this.vValidarInfoDatosBasicosEntidadBLL.ModificarValidarInfoDatosBasicosEntidad(pValidarInfoDatosBasicosEntidad, idEstadoInfoDatosBasicosEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad validación de datos básicos del proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador en una entidad validación de datos básicos del proveedor</param>
        /// <returns>Entidad validación de datos básicos del proveedor</returns>
        public ValidarInfoDatosBasicosEntidad ConsultarValidarInfoDatosBasicosEntidad_Ultima(Int32 pIdEntidad)
        {
            try
            {
                return vValidarInfoDatosBasicosEntidadBLL.ConsultarValidarInfoDatosBasicosEntidad_Ultima(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica el ConfirmaYAprueba de la ultima revisión de datos básicos del proveedor
        /// </summary>
        /// <param name="pidEntidad">Id de la entidad Proveedor Oferente que se modifica</param>
        /// <returns>bool true si se ha ejecutado correctamente.</returns>
        public bool ModificarConfirmaYApreba_InfoDatosBasicosEntidad(int pIdEntidad)
        {
            try
            {
                return this.vValidarInfoDatosBasicosEntidadBLL.ModificarConfirmaYApreba_InfoDatosBasicosEntidad(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
        #region ValidarInfoIntegrantesEntidad

        /// <summary>
        /// Consulta una lista de entidades validación de datos de Integrantes del proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador en una entidad validación de datos de Integrantes del proveedor</param>
        /// <returns>Lista de entidades validación de datos De Integrantes del proveedor</returns>
        public List<ValidarInfoIntegrantesEntidad> ConsultarValidarInfoIntegrantesEntidadsResumen(Int32 pIdEntidad)
        {
            try
            {
                return vValidarInfoIntegrantesEntidadBLL.ConsultarValidarInfoIntegrantesEntidadsResumen(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region ValidarProveedor

        /// <summary>
        /// Consulta una lista de entidades Validaciòn de Informaciòn
        /// </summary>
        /// <param name="pIdEntidad">Identificador de la entidad</param>
        /// <returns>Lista de entidades Validaciòn de Informaciòn</returns>
        public List<ValidarInfo> ConsultarValidarModulosResumen(Int32 pIdEntidad)
        {
            try
            {
                return vValidarProveedorBLL.ConsultarValidarModulosResumen(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica el estado de una entidad Tercero
        /// </summary>
        /// <param name="pTercero">Entidad Tercero</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int ModificarTercero_EstadoTercero(Oferente.Entity.Tercero pTercero)
        {

            try
            {
                return vValidarTerceroBLL.ModificarTercero_EstadoTercero(pTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Verifica Mòdulo resumen
        /// </summary>
        /// <param name="pIdEntidad">Identificador de la entidad</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int FinalizarRevision(Int32 pIdEntidad)
        {
            try
            {
                return vValidarProveedorBLL.ValidarModulosResumen_FinalizarRevision(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// Modifica una entidad Información módulo financero por liberar
        /// </summary>
        /// <param name="pInfoFinancieraEntidad">Entidad Información módulo financero</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int ModificarInfoFinancieraEntidad_Liberar(InfoFinancieraEntidad vInfoFinanciera)
        {
            try
            {
                return vInfoFinancieraEntidadBLL.ModificarInfoFinancieraEntidad_Liberar(vInfoFinanciera);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Información Módulo Experiencia para liberar
        /// </summary>
        /// <param name="pInfoExperienciaEntidad">Entidad Información Módulo Experiencia</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int ModificarInfoExperienciaEntidad_Liberar(InfoExperienciaEntidad vInfoExperiencia)
        {
            try
            {
                return vInfoExperienciaEntidadBLL.ModificarInfoExperienciaEntidad_Liberar(vInfoExperiencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Revision

        /// <summary>
        /// Inserta una nueva entidad Revisión
        /// </summary>
        /// <param name="pRevision">Entidad Revisión</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarRevision(Revision pRevision)
        {
            try
            {
                return vRevisionBLL.InsertarRevision(pRevision);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Revisión
        /// </summary>
        /// <param name="pRevision">Entidad Revisión</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int ModificarRevision(Revision pRevision)
        {
            try
            {
                return vRevisionBLL.ModificarRevision(pRevision);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Revisión
        /// </summary>
        /// <param name="pRevision">Identificador de una entidad Revisión</param>
        /// <returns>Entidad Revisión</returns>
        public Revision ConsultarRevision(int pIdRevision)
        {
            try
            {
                return vRevisionBLL.ConsultarRevision(pIdRevision);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Revisión
        /// </summary>
        /// <param name="pIdEntidad">Identificador de una entidad Revisión</param>
        /// <param name="pComponente">Componente en una entidad Revisión</param>
        /// <returns>Entidad Revisión</returns>
        public Revision ConsultarUltimaRevision(Int32 pIdEntidad, String pComponente)
        {
            try
            {
                return vRevisionBLL.ConsultarUltimaRevision(pIdEntidad, pComponente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Revisión
        /// </summary>
        /// <param name="pIdRevision">Identificador de Revisiòn en una entidad Revisiòn</param>
        /// <param name="pIdEntidad">Identificador de entidad en una entidad Revisiòn</param>
        /// <param name="pComponente">Componente en una entidad Revisiòn</param>
        /// <param name="pNroRevision">Nùmero en una entidad Revisiòn</param>
        /// <returns>Lista de entidades Revisiòn</returns>
        public List<Revision> ConsultarRevisions(Int32? pIdRevision, Int32? pIdEntidad, String pComponente, Int16? pNroRevision)
        {
            try
            {
                return vRevisionBLL.ConsultarRevisions(pIdRevision, pIdEntidad, pComponente, pNroRevision);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region EntidadProvOferente
        /// <summary>
        /// Consulta una lista de entidades Estado Proveedor
        /// </summary>
        /// <param name="pEstado">Estado en una entidad Estado Proveedor</param>
        /// <returns>Lista de entidades Estado Proveedor</returns>
        public List<Icbf.Proveedor.Entity.EntidadProvOferente> ConsultarEstadoProveedor(Boolean? pEstado)
        {
            try
            {
                return vEntidadProvOferenteBLL.ConsultarEstadoProveedor(pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Finaliza una entidad Proveedores Oferente
        /// </summary>
        /// <param name="pIdEntidad"> Id Entidad Proveedores Oferente</param>
        public int FinalizaEntidadProvOferente(int pIdEntidad)
        {
            try
            {
                return vEntidadProvOferenteBLL.FinalizaEntidadProvOferente(pIdEntidad);
            }
            catch (UserInterfaceException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idproveedorContrato"></param>
        /// <returns></returns>
        public List<Tercero> ConsultarHistoricoRepresentanteLegal(int idproveedorContrato)
        {
            try
            {
                return vEntidadProvOferenteBLL.ConsultarHistoricoRepresentanteLegal(idproveedorContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        #endregion

        #region Integrantes
        public int InsertarIntegrantes(Integrantes pIntegrantes)
        {
            try
            {
                return vIntegrantesBLL.InsertarIntegrantes(pIntegrantes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad Integrantes
        /// </summary>
        /// <param name="pIntegrantes"></param>
        public int ModificarIntegrantes(Integrantes pIntegrantes)
        {
            try
            {
                return vIntegrantesBLL.ModificarIntegrantes(pIntegrantes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad Integrantes
        /// </summary>
        /// <param name="pIntegrantes"></param>
        public int EliminarIntegrantes(Integrantes pIntegrantes)
        {
            try
            {
                return vIntegrantesBLL.EliminarIntegrantes(pIntegrantes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad Integrantes
        /// </summary>
        /// <param name="pIdIntegrante"></param>
        public Integrantes ConsultarIntegrantes(int pIdIntegrante)
        {
            try
            {
                return vIntegrantesBLL.ConsultarIntegrantes(pIdIntegrante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad Integrantes
        /// </summary>
        /// <param name="pIdTipoPersona"></param>
        /// <param name="pIdTipoIdentificacionPersonaNatural"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <param name="pPorcentajeParticipacion"></param>
        /// <param name="pConfirmaCertificado"></param>
        /// <param name="pConfirmaPersona"></param>
        public List<Integrantes> ConsultarIntegrantess(int? pIdEntidad)
        {
            try
            {
                return vIntegrantesBLL.ConsultarIntegrantess(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Icbf.Proveedor.Entity.EntidadProvOferente ConsultarEntidadProv_SectorPrivado(String pTipoPersona, String pTipoidentificacion, String pIdentificacion, String pProveedor, String pEstado, String pUsuarioCrea)
        {
            try
            {
                return vIntegrantesBLL.ConsultarEntidadProvOferentes_SectorPrivado(pTipoPersona, pTipoidentificacion, pIdentificacion, pProveedor, pEstado, pUsuarioCrea);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {

                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método que valida el numero de integrantes registrados contra el numero de integrantes definido en datos básicos por el proveedor
        /// </summary>
        /// <param name="pIdEntidad"></param>
        /// <returns></returns>
        public Integrantes ValidaNumIntegrantes(int pIdEntidad)
        {
            try
            {
                return vIntegrantesBLL.ValidaNumIntegrantes(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {

                throw new GenericException(ex);
            }
        }
        #endregion

        #region EstadoProveedor
        ///// <summary>
        /// Consulta una entidad Estado Proveedor
        /// </summary>
        /// <param name="pDescripcion"></param>aram>
        /// <returns></returns>
        public Icbf.Proveedor.Entity.EstadoProveedor ConsultarEstadoProveedor(string pDescripcion)
        {
            try
            {
                return vEstadoProveedorBLL.ConsultarEstadoProveedor(pDescripcion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region ValidarInfoDatosBasicosEntidad  
        /// <summary>
        /// Consulta una lista de entidades validación de datos básicos del proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador de entidad en una entidad validación de datos básicos del proveedor</param>
        /// <param name="pObservaciones">Observaciones en una entidad validación de datos básicos del proveedor</param>
        /// <param name="pConfirmaYAprueba">Es confirma y aprueba valor en la entidad validación de datos básicos del proveedor</param>
        /// <returns>Lista de entidades validación de datos básicos del proveedor</returns>
        public List<ValidarInfoIntegrantesEntidad> ConsultarValidarInfoIntegrantesEntidad(Int32 pIdEntidad, String pObservaciones, Boolean? pConfirmaYAprueba)
        {
            try
            {
                return vValidarInfoIntegrantesEntidadBLL.ConsultarValidarInfoIntegrantesEntidad(pIdEntidad, pObservaciones, pConfirmaYAprueba);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad validación de datos básicos del proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador en una entidad validación de datos básicos del proveedor</param>
        /// <returns>Entidad validación de datos básicos del proveedor</returns>
        public ValidarInfoIntegrantesEntidad ConsultarValidarInfoIntegrantesEntidad_Ultima(Int32 pIdEntidad)
        {
            try
            {
                return vValidarInfoIntegrantesEntidadBLL.ConsultarValidarInfoIntegrantesEntidad_Ultima(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Insertar una entidad Validación de datos básicos del proveedor
        /// </summary>
        /// <param name="pValidarInfoIntegrantesEntidad">Entidad validación de datos básicos del proveedor</param>
        /// <param name="idEstadoInfoDatosBasicosEntidad">Identificador de estado en una entidad validación de datos básicos del proveedor</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarValidarInfoIntegrantesEntidad(ValidarInfoIntegrantesEntidad pValidarInfoIntegrantesEntidad, int idEstadoInfoDatosBasicosEntidad)
        {
            try
            {
                return this.vValidarInfoIntegrantesEntidadBLL.InsertarValidarInfoIntegrantesEntidad(pValidarInfoIntegrantesEntidad, idEstadoInfoDatosBasicosEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Validación de datos básicos del proveedor
        /// </summary>
        /// <param name="pValidarInfoIntegrantesEntidad">Entidad validación de datos básicos del proveedor</param>
        /// <param name="idEstadoInfoDatosBasicosEntidad">Identificador de estado en una entidad validación de datos básicos del proveedor</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int ModificarValidarInfoIntegrantesEntidad(ValidarInfoIntegrantesEntidad pValidarInfoIntegrantesEntidad, int idEstadoInfoDatosBasicosEntidad)
        {
            try
            {
                return this.vValidarInfoIntegrantesEntidadBLL.ModificarValidarInfoIntegrantesEntidad(pValidarInfoIntegrantesEntidad, idEstadoInfoDatosBasicosEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region ValidacionIntegrantesEntidad
        /// <summary>
        /// Modifica una ValidacionIntegrantesEntidad
        /// </summary>
        /// <param name="pEntidadProvOferente">ValidacionIntegrantesEntidad</param>
        /// <returns>Identificador de ValidacionIntegrantesEntidad en tabla</returns>
        public int ModificarValidacionIntegrantesEntidad_EstadoIntegrantes(ValidacionIntegrantesEntidad pValidacionIntegrantesEntidad)
        {
            try
            {
                return vValidacionIntegrantesEntidad.ModificarValidacionIntegrantesEntidad_EstadoIntegrantes(pValidacionIntegrantesEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// Consulta un ValidacionIntegrantesEntidad de una Entidad
        /// </summary>
        /// <param name="pIdEntidad">IdEntidad</param>
        /// <returns>ValidacionIntegrantesEntidad</returns>
        public ValidacionIntegrantesEntidad Consultar_ValidacionIntegrantesEntidad(int pIdEntidad)
        {
            try
            {
                return vValidacionIntegrantesEntidad.Consultar_ValidacionIntegrantesEntidad(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// Finaliza un ValidacionIntegrantesEntidad de una Entidad
        /// </summary>
        /// <param name="pIdEntidad">IdEntidad</param>
        /// <returns>int</returns>
        public int FinalizaValidacionIntegrantesEntidad(int pIdEntidad)
        {
            try
            {
                return vValidacionIntegrantesEntidad.FinalizaValidacionIntegrantesEntidad(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Formacion
        /// <summary>
        /// Método de inserción para la entidad Formacion
        /// </summary>
        /// <param name="pFormacion"></param>
        public int InsertarFormacion(Formacion pFormacion)
        {
            try
            {
                return vFormacionBLL.InsertarFormacion(pFormacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad Formacion
        /// </summary>
        /// <param name="pFormacion"></param>
        public int ModificarFormacion(Formacion pFormacion)
        {
            try
            {
                return vFormacionBLL.ModificarFormacion(pFormacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad Formacion
        /// </summary>
        /// <param name="pFormacion"></param>
        public int EliminarFormacion(Formacion pFormacion)
        {
            try
            {
                return vFormacionBLL.EliminarFormacion(pFormacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad Formacion
        /// </summary>
        /// <param name="pIdFormacion"></param>
        public Formacion ConsultarFormacion(int pIdFormacion)
        {
            try
            {
                return vFormacionBLL.ConsultarFormacion(pIdFormacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad Formacion
        /// </summary>
        /// <param name="pIdEntidad"></param>
        /// <param name="pCodigoModalidad"></param>
        /// <param name="pIdProfesion"></param>
        /// <param name="pEsFormacionPrincipal"></param>
        /// <param name="pEstado"></param>
        public List<Formacion> ConsultarFormacions(int? pIdEntidad, String pCodigoModalidad, String pIdProfesion, Boolean? pEsFormacionPrincipal, Boolean? pEstado)
        {
            try
            {
                return vFormacionBLL.ConsultarFormacions(pIdEntidad, pCodigoModalidad, pIdProfesion, pEsFormacionPrincipal, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        #endregion

        #region Reporte proveedores importados

        /// <summary>
        /// Consultar Municipios X departamento(s)
        /// </summary>
        /// <param name="pIdDepartamento"></param>
        /// <returns></returns>
        public List<ExperienciaMunicipio> ConsultarMunicipiosXDepto(string pIdDepto)
        {
            try
            {
                return vReporteImportado.ConsultarMunicipiosXDepto(pIdDepto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar datos de terceros importados y retorna un tru si hay datos y false si no.
        /// </summary>
        /// <param name="vReporte"></param>
        /// <returns></returns>
        public bool ConsultarDatosReporte(ReporteTercerosImportados vReporte)
        {
            try
            {
                return vReporteImportado.ConsultarDatosReporte(vReporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
    }
}
