using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Icbf.EstudioSectorCosto.Entity
{
    public class DiasEstimadosRealesPorModalidad : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdDiasEstimadosRealesPorModalidad
        {
            get;
            set;
        }
        public int IdModalidadSeleccion
        {
            get;
            set;
        }
        public int IdComplejidadIndicador
        {
            get;
            set;
        }
        public String Operador
        {
            get;
            set;
        }
        public int? Limite
        {
            get;
            set;
        }
        public int? DiasHabilesEntrePSeInicioEjecucion
        {
            get;
            set;
        }
        public int? DiasHabilesEntreComitePS
        {
            get;
            set;
        }
        public int? DiasHabilesEntreRadicacionContratoYComite
        {
            get;
            set;
        }
        public int? DiasHabilesEntreESyRadicacionContratos
        {
            get;
            set;
        }
        public int? DiasHabilesEntreFCTDefinitivaYES
        {
            get;
            set;
        }
        public int? DiasHabilesEntreRadicacionFCTyFCTDefinitiva
        {
            get;
            set;
        }
        public int Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public String NombreEstado
        {
            get;
            set;
        }
        public String Complejidad
        {
            get;
            set;
        }
        public String Modalidad
        {
            get;
            set;
        }
        public String LimiteGrilla
        {
            get;
            set;
        }
        public DiasEstimadosRealesPorModalidad()
        {
        }
    }
}
