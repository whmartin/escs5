﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.EstudioSectorCosto.Entity
{
    public class CierreEstudiosPorVigencia : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public Int32 IdCierreEstudiosPorVigencia
        {
            get;
            set;
        }
        public int AnioCierre
        {
            get;
            set;
        }
        public DateTime FechaEjecucion
        {
            get;
            set;
        }
        public String Descripcion
        {
            get;
            set;
        }        
        public String UsuarioCrea
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }        
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public CierreEstudiosPorVigencia()
        {
        }
    }
}
