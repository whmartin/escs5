﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.EstudioSectorCosto.Entity
{
    [Serializable]
    public class GestionesActividadesAdicionales : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public decimal IdGestionActividadAdicional { get; set; }

        public decimal IdGestionEstudio { get; set; }

        public int IdActividad { get; set; }
        public string Actividad { get; set; }

        public DateTime? FechaActividadAdicional
        {
            get;
            set;
        }
        public String Detalle
        {
            get;
            set;
        }
        public Boolean NoAplicaActividadAdicional
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }


        
    }
}
