using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.EstudioSectorCosto.Entity
{
    [Serializable]
    public class BitacoraAcciones : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public decimal IdBitacoraAcciones { get; set; }
        public decimal IdBitacoraEstudio { get; set; }
        public decimal IdEstudio
        {
            get;
            set;
        }
        public String NombreAbreviado
        {
            get;set;
        }
        public String UltimaAccion
        {
            get;
            set;
        }
        public String ProximaAccion
        {
            get;
            set;
        }
        public DateTime? FechaRegistro
        {
            get;
            set;
        }
        public DateTime? FechaModificacion
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime? FechaModifica
        {
            get;
            set;
        }
        public String UsuarioBitacora
        {
            get;
            set;
        }
        public BitacoraAcciones()
        {
        }
    }
}
