﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.EstudioSectorCosto.Entity
{
    public class InformacionProcesoSeleccion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public Int32 IdInformacionProcesoSeleccion
        {
            get;
            set;
        }
        public int IdConsecutivoEstudio
        {
            get;
            set;
        }
        public int? IdContrato
        {
            get;
            set;
        }
        public String EstadoDelProceso
        {
            get;
            set;
        }
        public Decimal ValorPresupuestadoInicial
        {
            get;
            set;
        }
        public Decimal ValorAdjudicado
        {
            get;
            set;
        }
        public String Observacion
        {
            get;
            set;
        }
        public String URLDelProceso
        {
            get;
            set;
        }

        #region Contrato
        public Int32 IdRegistroSolicitudEstudioSectoryCaso
        {
            get;
            set;
        }
        public String NumeroDelProceso
        {
            get;
            set;
        }
        public String ObjetoContrato
        {
            get;
            set;
        }
        public DateTime? FechaAdjudicacionDelProceso
        {
            get;
            set;
        }
        public String Contratista
        {
            get;
            set;
        }
        public String PlazoDeEjecucion
        {
            get;
            set;
        }
        public String ObservacionC
        {
            get;
            set;
        }
        public String URLDelProcesoC
        {
            get;
            set;
        }
        public Decimal ValorPresupuestadoInicialC
        {
            get;
            set;
        }
        public Decimal ValorAdjudicadoC
        {
            get;
            set;
        }
        #endregion

        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public InformacionProcesoSeleccion()
        {
        }
    }
}
