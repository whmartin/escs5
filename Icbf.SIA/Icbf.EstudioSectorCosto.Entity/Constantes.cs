﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.EstudioSectorCosto.Entity
{
    public static class Constantes
    {
        public const string ValorDefecto = "-1";        
        public const string Seleccione = "SELECCIONE";
        public const string Todos = "TODOS";
        public const string CodNo = "NO";
        public const string codSi = "SI";
        public static List<BaseDTO> LstSINO = new[]
        {
            new BaseDTO {Id="SI",Name="SI" },
            new BaseDTO {Id="NO",Name="NO" }
        }.ToList();

        public static List<BaseDTO> LstTipoDeValor = new[]
       {
            new BaseDTO {Id="VALOR UNITARIO",Name="VALOR UNITARIO" },
            new BaseDTO {Id="NO APLICA",Name="NO APLICA" },
            new BaseDTO {Id="VALOR TOTAL",Name="VALOR TOTAL" },
            new BaseDTO {Id="VALOR POR MACRO",Name="VALOR POR MACRO" }
        }.ToList();

        public static List<BaseDTO> LstCondicion= new[]
      {
            new BaseDTO {Id="MAYOR O IGUAL",Name="MAYOR O IGUAL" },
            new BaseDTO {Id="MENOR O IGUAL",Name="MENOR O IGUAL" },
            new BaseDTO {Id="IGUAL A",Name="IGUAL A" },
            new BaseDTO {Id="MENOR A",Name="MENOR A" },
            new BaseDTO {Id="MAYOR A",Name="MAYOR A" }
        }.ToList();

        public static string ViewStateIndicadores = "LstIndicadores";
        public static string ViewStateActividadesGestion = "ActividadesGestion";
        public static string ViewStateBitacoraAcciones = "LstBitacoraAcciones";
        public static string ViewStateConsultaDependencias = "LstConsultaDependencias";
        public static string ViewStateEliminarIndicadores = "LstEliminarIndicadores";
        public static string ViewStateEliminarBitacoraAciones = "LstEliminarBitacoraAcciones";
        public static string ViewStateConsultaEstudio = "LstCosultaResultado"; 
        public static string ViewStateSortDirection = "SortDirection";
        public static string ViewStateSortExpresion = "SortExpresion";
        public static string ViewStateConsultaGestion = "ViewStateConsultaGestion";
        public static string ViewStateDireccionSolicitante = "LstDireccionesSolicitantes";
        public static string SinOrden = "Default";


        public static string MayorOIgual = "Mayor o igual";
        public static string MenorOIgual = "Menor o igual";
        public static string ViewStateConsultaInformacion= "ViewStateConsultaInformacion";
        public static string ViewStateLstPACCO= "ViewStateLstPACCO";
        public static string ViewStateLstFeschasESC= "ViewStateLstFeschasESC";
        public static string ViewStateLstFechasReales= "ViewStateLstFechasReales";
    }
}
