using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.EstudioSectorCosto.Entity
{
    [Serializable]
    public class GestionesEstudioSectorCostos : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public decimal IdGestionEstudio
        {
            get;
            set;
        }

        public long IdRegistroSolicitudEstudioSectoryCaso
        {
            get;
            set;
        }
        public DateTime? FechaPreguntaOComentario
        {
            get;
            set;
        }
        public DateTime? FechaRespuesta
        {
            get;
            set;
        }
        public DateTime? UltimaFechaPreguntaOComentario
        {
            get;
            set;
        }
        public DateTime? UltimaFechaRespuesta
        {
            get;
            set;
        }
        public DateTime? FechaEntregaES_MC
        {
            get;
            set;
        }
        public Boolean NoAplicaFechaEntregaES_MC
        {
            get;
            set;
        }
        public DateTime? FechaEntregaFCTORequerimiento
        {
            get;
            set;
        }
        public Boolean NoAplicaFechaentregaFCTOrequerimiento
        {
            get;
            set;
        }
        public DateTime? FechaEnvioSDC
        {
            get;
            set;
        }
        public Boolean NoAplicaFechaEnvioSDC
        {
            get;
            set;
        }
        public DateTime? PlazoEntregaCotizaciones
        {
            get;
            set;
        }
        public Boolean NoAplicaPlazoEntregaCotizaciones
        {
            get;
            set;
        }
      
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }

        public string NombreAbreviado { get; set; }

        public string Actividad { get; set; }

        public bool NoAplica { get; set; }
        public long IdConsecutivoEstudio { get; set; }
        public DateTime? FechaPreguntaOComentarioDef { get; set; }
        public DateTime? FechaRespuestaDef { get; set; }
        public DateTime? FechaEntregaES_MCDef { get; set; }
        public DateTime? FechaEntregaFCTORequerimientoDef { get; set; }
        public DateTime? FechaEnvioSDCDef { get; set; }
        public DateTime? PlazoEntregaCotizacionesDef { get; set; }
        public string NumeroReproceso { get; set; }
        public GestionesEstudioSectorCostos()
        {
        }
    }
}
