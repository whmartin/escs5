using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.EstudioSectorCosto.Entity
{
    public class ResultadoEstudioSector : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public long IdConsecutivoResultadoEstudio
        {
            get;
            set;
        }
        public long IdConsecutivoEstudio
        {
            get;
            set;
        }
        public string DocumentosRevisadosNAS
        {
            get;
            set;
        }
        public DateTime? Fecha
        {
            get;
            set;
        }
        public int? IdRevisadoPor
        {
            get;
            set;
        }

        public string RevisadoPor
        {
            get;set;
        }
        public string Observacion
        {
            get;
            set;
        }
        public int? Consultados
        {
            get;
            set;
        }
        public int? Participantes
        {
            get;
            set;
        }
        public int? CotizacionesRecibidas
        {
            get;
            set;
        }
        public string RadicadoOficioEntregaESyC
        {
            get;
            set;
        }
        public DateTime? FechaDeEntregaESyC
        {
            get;
            set;
        }
        public string TipoDeValor
        {
            get;
            set;
        }
        public decimal? ValorPresupuestoESyC
        {
            get;
            set;
        }
        public string IndiceLiquidez
        {
            get;
            set;
        }
        public string IndiceLiquidezDesc
        {
            get;
            set;
        }
        public string NivelDeEndeudamiento
        {
            get;
            set;
        }
        public string NivelDeEndeudamientoDesc
        {
            get;
            set;
        }
        public string RazonDeCoberturaDeIntereses 
        {
            get;
            set;
        }
        public string RazonDeCoberturaDeInteresesDesc
        {
            get;
            set;
        }
        public int? CotizacionesParaPresupuesto
        {
            get;
            set;
        }
        public string CapitalDeTrabajo
        {
            get;
            set;
        }
        public string CapitalDeTrabajoDesc
        {
            get;
            set;
        }
        public string RentabilidadDelPatrimonio
        {
            get;
            set;
        }
        public string RentabilidadDelPatrimonioDesc
        {
            get;
            set;
        }
        public string RentabilidadDelActivo
        {
            get;
            set;
        }
        public string RentabilidadDelActivoDesc
        {
            get;
            set;
        }
        public decimal? TotalPatrimonio 
        {
            get;
            set;
        }
        public string TotalPatrimonioDesc
        {
            get;
            set;
        }
        public string KResidual
        {
            get;
            set;
        }
        public string Indicador
        {
            get;
            set;
        }
        public string Condicion
        {
            get;
            set;
        }
        public string Valor
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
       
    }
}
