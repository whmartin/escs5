﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.EstudioSectorCosto.Entity
{
    public class BaseDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
