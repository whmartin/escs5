using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Icbf.EstudioSectorCosto.Entity
{
    public class RangoDiasComplejidadPorModalidad : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdRangosDiasComplejidadPorModalidad
        {
            get;
            set;
        }
        public int IdModalidadSeleccion
        {
            get;
            set;
        }
        public String Operador
        {
            get;
            set;
        }
        public int? Limite
        {
            get;
            set;
        }
        public String LimiteGrilla
        {
            get;
            set;
        }
        public int IdComplejidadInterna
        {
            get;
            set;
        }
        public int IdComplejidadIndicador
        {
            get;
            set;
        }
        public int? DiasHabilesInternos
        {
            get;
            set;
        }
        public int? DiasHabilesCumplimientoIndicador
        {
            get;
            set;
        }
        public int Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public String NombreEstado
        {
            get;
            set;
        }
        public String NombreComplejidadInterna
        {
            get;
            set;
        }
        public String NombreComplejidadIndicador
        {
            get;
            set;
        }
        public String Modalidad
        {
            get;
            set;
        }
        public RangoDiasComplejidadPorModalidad()
        {
        }
    }
}
