using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Icbf.EstudioSectorCosto.Entity
{
    public class Complejidades : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdComplejidad
        {
            get;
            set;
        }
        public String Nombre
        {
            get;
            set;
        }
        public int TipoComplejidad
        {
            get;
            set;
        }
        public int Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public String NombreEstado
        {
            get;
            set;
        }
        public String NombreTipoComplejidad
        {
            get;
            set;
        }
        public String Descripcion
        {
            get;
            set;
        }
        public Complejidades()
        {
        }
    }
}
