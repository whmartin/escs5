﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.EstudioSectorCosto.Entity
{
    public  class TiempoEntreActividadesConsulta
    {
        public Decimal IdConsecutivoEstudio
        {
            get;
            set;
        }
        public Decimal IdTiempoEntreActividades
        {
            get;
            set;
        }
        public string FentregaES_ECTiemposEquipo
        {
            get;
            set;
        }

        public bool? NoAplicaFentregaES_ECTiemposEquipoR { get; set; }

        public string FEentregaES_ECTiemposIndicador
        {
            get;
            set;
        }

        public bool? NoAplicaFEentregaES_ECTiemposIndicadorR { get; set; }

        public string DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado
        {
            get;
            set;
        }

        public string DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor
        {
            get;
            set;
        }
        public bool? NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR { get; set; }

        public string DiasHabilesEntreFCTFinalYSDC
        {
            get;
            set;
        }
        public string DiasHabilesEntreFCTFinalYSDCColor
        {
            get;
            set;
        }
        public bool? NoAplicaDiasHabilesEntreFCTFinalYSDCR { get; set; }
        public string DiasHabilesEnESOCOSTEO
        {
            get;
            set;
        }
        public string DiasHabilesEnESOCOSTEOColor
        {
            get;
            set;
        }

        public bool? NoAplicaDiasHabilesEnESOCOSTEOR { get; set; }

        public bool? DevueltoDiasHabilesEnESOCOSTEOR { get; set; }
        public string DiashabilesParaDevolucion
        {
            get;
            set;
        }
        public string DiashabilesParaDevolucionColor
        {
            get;
            set;
        }
        public bool? NoAplicaDiasHabilesParaDevolucionR { get; set; }
      
       
        public string FEstimadaFCTPreliminarESC
        {
            get;
            set;
        }
        public string FRealFCTPreliminarESC
        {
            get;
            set;
        }
        public bool? NoAplicaFRealFCTPreliminarESC
        {
            get;
            set;
        }
        public string FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC
        {
            get;
            set;
        }
        public bool? NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC
        {
            get;
            set;
        }
        public string FEstimadaES_EC_ESC
        {
            get;
            set;
        }
        public bool? NoAplicaFEstimadaES_EC_ESC
        {
            get;
            set;
        }
        public string FEstimadaDocsRadicadosEnContratosESC
        {
            get;
            set;
        }
        public bool? NoAplicaFEstimadaDocsRadicadosEnContratosESC
        {
            get;
            set;
        }
        public string FEstimadaComitecontratacionESC
        {
            get;
            set;
        }
        public bool? NoAplicaFEstimadaComitecontratacionESC
        {
            get;
            set;
        }
        public string FEstimadaInicioDelPS_ESC
        {
            get;
            set;
        }
        public bool? NoAplicaFEstimadaInicioDelPS_ESC
        {
            get;
            set;
        }
        public string FEstimadaPresentacionPropuestasESC
        {
            get;
            set;
        }

        public bool? NoAplicaFEstimadaPresentacionPropuestasESC
        {
            get;
            set;
        }
        public string FEstimadaDeInicioDeEjecucionESC
        {
            get;
            set;
        }
        public string FRealFCTPreliminar
        {
            get;
            set;
        }
        public string FRealDocsRadicadosEnContratos
        {
            get;
            set;
        }
        public string FRealFCTDefinitivaParaMCFRealES_MCRevisado
        {
            get;
            set;
        }
        public string FRealComiteContratacion
        {
            get;
            set;
        }
        public string FRealES_EC
        {
            get;
            set;
        }
        public string FRealInicioPS
        {
            get;
            set;
        }
        public string FRealPresentacionPropuestas
        {
            get;
            set;
        }
        //////
        
        public string FEstimadaFCTPreliminarREGINICIAL
        {
            get;
            set;
        }
        public bool? NoAplicaFEstimadaFCTPreliminarREGINICIAL
        {
            get;
            set;
        }
        public string FEstimadaFCTPreliminar
        {
            get;
            set;
        }
        public bool? NoAplicaFEstimadaFCTPreliminar
        {
            get;
            set;
        }
        public string FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado
        {
            get;
            set;
        }
        public bool? NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado
        {
            get;
            set;
        }
        public string FEestimadaES_EC
        {
            get;
            set;
        }
        public bool? NoAplicaFEestimadaES_EC
        {
            get;
            set;
        }
        public string FEstimadaDocsRadicadosEnContratos
        {
            get;
            set;
        }
        public bool? NoAplicaFEstimadaDocsRadicadosEnContratos
        {
            get;
            set;
        }
        public string FEstimadaComiteContratacion
        {
            get;
            set;
        }
        public bool? NoAplicaFEstimadaComiteContratacion
        {
            get;
            set;
        }
        public string FEstimadaInicioDelPS
        {
            get;
            set;
        }
        public bool? NoAplicaFEstimadaInicioDelPS
        {
            get;
            set;
        }
        public string FEstimadaPresentacionPropuestas
        {
            get;
            set;
        }
        public bool? NoAplicaFEstimadaPresentacionPropuestas
        {
            get;
            set;
        }
        public string FEstimadaDeInicioDeEjecucion { get; set; }

        
        
    }
}
