﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Icbf.EstudioSectorCosto.Entity
{
    [Serializable]
    public class ConsultaDependencias : Icbf.Seguridad.Entity.EntityAuditoria
    {

        public decimal IdConsultaDependencia
        {
            get;
            set;
        }
        public String DireccionSolicitante
        {
            get;
            set;
        }

        public String Vigencia
        {
            get;
            set;
        }

        public String Objeto
        {
            get;
            set;
        }
        public DateTime? FechaSolicitudInicial
        {
            get;
            set;
        }
        public DateTime? FEstimadaFCTPreliminarREGINICIAL
        {
            get;
            set;
        }
        public DateTime? FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado
        {
            get;
            set;
        }
        public DateTime? FEestimadaES_EC
        {
            get;
            set;
        }
        public DateTime? FRealFCTDefinitivaParaMCFRealES_MCRevisado
        {
            get;
            set;
        }
        public DateTime? FechaDeEntregaESyC
        {
            get;
            set;
        }
    }
}
