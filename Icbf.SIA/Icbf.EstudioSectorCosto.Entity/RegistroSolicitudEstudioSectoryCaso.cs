using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Icbf.EstudioSectorCosto.Entity
{
    public class RegistroSolicitudEstudioSectoryCaso : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public Int32 IdRegistroSolicitudEstudioSectoryCaso
        {
            get;
            set;
        }
        public int AplicaPACCO
        {
            get;
            set;
        }
        public int ConsecutivoPACCO
        {
            get;
            set;
        }
        public int DireccionsolicitantePACCO
        {
            get;
            set;
        }
        public String ObjetoPACCO
        {
            get;
            set;
        }
        public int ModalidadPACCO
        {
            get;
            set;
        }
        public Decimal ValorPresupuestalPACCO
        {
            get;
            set;
        }
        public String VigenciaPACCO
        {
            get;
            set;
        }
        public int ConsecutivoEstudioRelacionado
        {
            get;
            set;
        }
        public DateTime FechaSolicitudInicial
        {
            get;
            set;
        }
        public String ActaCorreoNoRadicado
        {
            get;
            set;
        }
        public String NombreAbreviado
        {
            get;
            set;
        }
        public String NumeroReproceso
        {
            get;
            set;
        }
        public String Objeto
        {
            get;
            set;
        }
        public String VigenciasEjecucion
        {
            get;
            set;
        }
        public String ReprocesoVincula
        {
            get;
            set;
        }
        public String CuentaVigenciasFuturasPACCO
        {
            get;
            set;
        }
        public String AplicaProcesoSeleccion
        {
            get;
            set;
        }
        public int IdModalidadSeleccion
        {
            get;
            set;
        }
        public String ModalidadSeleccion
        {
            get;
            set;
        }
        public int IdTipoEstudio
        {
            get;
            set;
        }
        public int IdComplejidadInterna
        {
            get;
            set;
        }
        public int IdComplejidadIndicador
        {
            get;
            set;
        }
        public int IdResponsableES
        {
            get;
            set;
        }
        public int IdResponsableEC
        {
            get;
            set;
        }
        public String OrdenadorGasto
        {
            get;
            set;
        }
        public int IdEstadoSolicitud
        {
            get;
            set;
        }
        public String EstadoSolicitud
        {
            get;
            set;
        }
        public int IdMotivoSolicitud
        {
            get;
            set;
        }
        public int IdDireccionSolicitante
        {
            get;
            set;
        }
        public String DireccionSolicitante
        {
            get;
            set;
        }
        public int IdAreaSolicitante
        {
            get;
            set;
        }
        public String TipoValor
        {
            get;
            set;
        }
        public Decimal ValorPresupuestoEstimadoSolicitante
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public int AnioVigencia
        {
            get;
            set;
        }

        public int AnioCierre
        {
            get;
            set;
        }
        public string TipoEstudio
        {
            get;set;
        }
        public string ComplejidadInterna
        {
            get;set;
        }

        public string ComplejidadIndicador
        {
            get;set;
        }

        public string ResponsableES
        {
            get; set;
        }

        public string ResponsableEC
        {
            get; set;
        }

        public string AreaSolicitante
        {
            get; set;
        }

        public string MotivoSolicitud
        {
            get; set;
        }

        public string NombreDireccionSolicitantePACCO
        {
            get; set;
        }

        public string NombreModalidadPACCO
        {
            get; set;
        }

        public RegistroSolicitudEstudioSectoryCaso()
        {
        }
    }
}
