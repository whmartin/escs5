using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Icbf.EstudioSectorCosto.Entity
{
    public class ResponsablesESC : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdResponsable
        {
            get;
            set;
        }
        public String Responsable
        {
            get;
            set;
        }
        public int Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public String NombreEstado
        {
            get;
            set;
        }
        public ResponsablesESC()
        {
        }
    }
}
