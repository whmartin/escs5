﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.EstudioSectorCosto.Entity
{
    public class LogActualizacionMasiva : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public string UsuarioLog { get; set; }
        public string Fecha { get; set; }
        public string Hora { get; set; }
    }
}
