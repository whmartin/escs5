﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.EstudioSectorCosto.Entity
{
    public class BitacoraEstudio : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public decimal IdBitacoraEstudio { get; set; }
        public decimal IdConsecutivoEstudio { get; set; }

        public DateTime? FechaCrea { get; set; }
        public DateTime? FechaModifica { get; set; }

        public string UsuarioCrea { get; set; }
        public string UsuarioModifica { get; set; }

    }
}
