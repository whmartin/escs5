using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.EstudioSectorCosto.Entity
{
    [Serializable]
    public class ResultadoEstudioSectorConsulta : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public long IdConsecutivoEstudio
        {
            get;
            set;
        }
        public string NombreAbreviado
        {
            get;
            set;
        }
        public string Objeto
        {
            get;
            set;
        }
        public int IdResponsableES
        {
            get;
            set;
        }
        public int IdResponsableEC
        {
            get;
            set;
        }
        public int IdModalidadDeSeleccion
        {
            get;
            set;
        }
        public int DireccionSolicitante
        {
            get;
            set;
        }
        public int Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }

        public int ProveedoresConsultados
        {
            get;
            set;
        }
        public int ProveedoresParticipantes
        {
            get;
            set;
        }
        public int CotizacionesRecibidas
        {
            get;
            set;
        }
        public string Observacion
        {
            get;
            set;
        }

        public long IdResultadoEstudio
        {
            get;
            set;
        }
        public DateTime Fecha
        {
            get;
            set;
        }
        public DateTime FechaDeEntregaESyC
        {
            get;
            set;
        }
        

    }
}
