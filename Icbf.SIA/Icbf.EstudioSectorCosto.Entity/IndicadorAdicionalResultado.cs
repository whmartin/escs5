﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.EstudioSectorCosto.Entity
{
    [Serializable]
    public class IndicadorAdicionalResultado: Icbf.Seguridad.Entity.EntityAuditoria
    {
        public long IdIndicadoresAdicionalesResultado { get; set; }
        public long IdResultadoEstudio { get; set; }
        public string Indicador { get; set; }
        public string Condicion { get; set; }
        public string Valor { get; set; }
        public string IndicadorAdicionalDesc { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }


    }
}
