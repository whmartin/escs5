﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using Icbf.Utilities.Exceptions;

namespace Icbf.SEG.Business
{
    public class MembershipUserBLL
    {
        public MembershipUserBLL()
        {
        }
        public void UpdateMembershipUser(MembershipUser pUser)
        {
            try
            {
                Membership.UpdateUser(pUser);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public MembershipUser CreateMembershipUser(String pUserName, String pPassword)
        {
            try
            {
                return Membership.CreateUser(pUserName, pPassword);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public bool DeleteMembershipUser(String pUserName)
        {
            try
            {
                return Membership.DeleteUser(pUserName);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
