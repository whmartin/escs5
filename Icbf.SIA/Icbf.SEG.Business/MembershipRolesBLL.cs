﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using Icbf.Utilities.Exceptions;
using Icbf.Seguridad.Entity;

namespace Icbf.SEG.Business
{
    public class MembershipRolesBLL
    {
        public MembershipRolesBLL()
        {
        }
        public void CreateMembershipRol(String pRoleName)
        {
            try
            {
                Roles.CreateRole(pRoleName);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public bool DeleteMembershipRol(String pRoleName)
        {
            try
            {
                return Roles.DeleteRole(pRoleName, true);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public void AddUserToRole(String pUserName, String pRoleName)
        {
            try
            {
                Roles.AddUserToRole(pUserName, pRoleName);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public void RemoveUserFromRole(String pUserName, String pRoleName)
        {
            try
            {
                Roles.RemoveUserFromRole(pUserName, pRoleName);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Rol> FindRoles(String pRolName, Boolean? pEstado)
        {
            try
            {
                List<Rol> vListaRoles = new List<Rol>();
                string[] vRoles = Roles.GetAllRoles();
                foreach (string s in vRoles)
                {
                    vListaRoles.Add(new Rol { NombreRol=s, DescripcionRol=s, Estado = true});
                }
                return vListaRoles;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Rol FindRole(String pRolName)
        {
            try
            {
                Rol vRol = new Rol();
                string[] vRoles = Roles.GetAllRoles();
                if (vRoles.Length > 0)
                {
                    vRol.NombreRol = vRoles[0];
                    vRol.DescripcionRol = vRoles[0];
                    vRol.Estado = true;
                }
                return vRol;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
