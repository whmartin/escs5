﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class EstadoContrato : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdEstadoContrato
        {
            get;
            set;
        }
        public String CodEstado
        {
            get;
            set;
        }
        public String Descripcion
        {
            get;
            set;
        }

        public Boolean Inactivo
        {
            get; set;
        }

        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public EstadoContrato()
        {
        }
    }
}


