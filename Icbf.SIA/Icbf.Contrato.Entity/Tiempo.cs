﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class Tiempo : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int Dias { get; set; }
        public int Meses { get; set; }
        public int Anios { get; set; }
    }
}
