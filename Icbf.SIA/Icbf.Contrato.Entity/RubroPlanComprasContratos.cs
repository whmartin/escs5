﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class RubroPlanComprasContratos : Icbf.Seguridad.Entity.EntityAuditoria
    {

        public int IDRubroPlanComprasContrato { get; set; }
        public decimal? ValorRubroPresupuestal { get; set; }
        public int? IDPlanDeComprasContratos { get; set; }
        public string IDRubro { get; set; }
        public string IdPagosDetalle { get; set; }
        public int? IdReduccion { get; set; }
        public decimal? ValorReduccion { get; set; }
        public bool? IsReduccion { get; set; }
        public string UsuarioCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
        public bool? IsAdicion { get; set; }
        public int? IdAdicion { get; set; }
        public decimal? ValorAdicion { get; set; }

    }

    public class ProductoPlanComprasContratos : Icbf.Seguridad.Entity.EntityAuditoria
    {

        public int IDProductoPlanCompraContrato { get; set; }
        public string IDProducto { get; set; }
        public decimal? CantidadCupos { get; set; }
        public int? IDPlanDeComprasContratos { get; set; } 
        public decimal? ValorProducto { get; set; }
        public decimal? Tiempo { get; set; }      
        public string UnidadTiempo { get; set; }        
        public int? IdUnidadTiempo { get; set; }
        public string IdDetalleObjeto { get; set; }
        public decimal? ReduccionCantidadCupos { get; set; }
        public int? IdReduccion { get; set; }
        public decimal ? ValorReduccion { get; set; }
        public bool? IsReduccion { get; set; }
        public string UsuarioCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
        public bool? IsAdicion { get; set; }
        public int? IdAdicion { get; set; }
        public decimal? ValorAdicion { get; set; }

    }
}
