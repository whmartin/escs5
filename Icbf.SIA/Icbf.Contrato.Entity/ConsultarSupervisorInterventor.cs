using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para Supervisor interventor (consulta)
    /// </summary>
    public class ConsultarSupervisorInterventor : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad TipoSupervisorInterventor
        /// </summary>
        public int TipoSupervisorInterventor
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NumeroContrato
        /// </summary>
        public String NumeroContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NumeroIdentificacion
        /// </summary>
        public int NumeroIdentificacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreRazonSocialSupervisorInterventor
        /// </summary>
        public String NombreRazonSocialSupervisorInterventor
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NumeroIdentificacionDirectorInterventoria
        /// </summary>
        public int NumeroIdentificacionDirectorInterventoria
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreRazonSocialDirectorInterventoria
        /// </summary>
        public String NombreRazonSocialDirectorInterventoria
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public ConsultarSupervisorInterventor()
        {
        }
    }
}
