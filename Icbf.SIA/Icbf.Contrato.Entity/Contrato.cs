using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para contratos
    /// </summary>
    public class Contrato : Icbf.Seguridad.Entity.EntityAuditoria
    {
       /// <summary>
        /// Propiedad IdContrato
        /// </summary>
        public int IdContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdIDEmpleadosSolicitanteContrato
        /// </summary>
        public int? IdEmpleadoSolicitante
        {
            get;
            set;
        }
        public string NombreEmpleadoSolicitante { get; set; }
        /// <summary>
        /// Propiedad idRegionalEmpSol
        /// </summary>
        public int? IdRegionalEmpSol
        {
            get;
            set;
        }
        public string RegionalEmpleadoSolicitante { get; set; }
        /// <summary>
        /// Propiedad idDependenciaEmpSol
        /// </summary>
        public int? IdDependenciaEmpSol
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad idCargoEmpSol
        /// </summary>
        public int? IdCargoEmpSol
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdEstadoContrato
        /// </summary>
        public int? IdEstadoContrato
        {
            get;
            set;

        }
        /// <summary>
        /// Propiedad IdUsuario
        /// </summary>
        public int? IdUsuario
        {
            get;
            set;

        }
        /// <summary>
        /// Propiedad IdNumeroProceso
        /// </summary>
        public int? IdNumeroProceso
        {
            get;
            set;

        }
        /// <summary>
        /// Propiedad IdModalidadSeleccion
        /// </summary>
        public int? IdModalidadSeleccion
        {
            get;
            set;
        }
        
        /// <summary>
        /// Propiedad FechaAdjudicacionDelProceso
        /// </summary>
        public DateTime? FechaAdjudicacionProceso
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdCategoriaContrato
        /// </summary>
        public int? IdCategoriaContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoContrato
        /// </summary>
        public int? IdTipoContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdModalidadAcademica
        /// </summary>
        public String IdModalidadAcademica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdProfesion
        /// </summary>
        public String IdProfesion
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad ActaDeInicio
        /// </summary>
        public Boolean? ActaDeInicio
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad ManejaAporte
        /// </summary>
        public Boolean? ManejaAporte
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ManejaRecurso
        /// </summary>
        public Boolean? ManejaRecurso
        {
            get;
            set;
        }
        
        /// <summary>
        /// Propiedad IDRegimenContratacion
        /// </summary>
        public int? IdRegimenContratacion
        {
            get; 
            set;
        }
        /// <summary>
        /// Propiedad ObjetoContrato
        /// </summary>
        public String ObjetoContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad AlcanceObjetoContrato
        /// </summary>
        public String AlcanceObjetoContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ValorInicialContrato
        /// </summary>
        public Decimal? ValorInicialContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ValorFinalContrato
        /// </summary>
        public Decimal? ValorFinalContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaInicioEjecucion
        /// </summary>
        public DateTime? FechaInicioEjecucion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaInicFechaFinalizacionIniciaContratoioEjecucion
        /// </summary>
        public DateTime? FechaFinalizacionIniciaContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaFinalTerminacionContrato
        /// </summary>
        public DateTime? FechaFinalTerminacionContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaFinalTerminacion
        /// </summary>
        public DateTime? FechaFinalTerminacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ManejaVigenciaFuturas
        /// </summary>
        public Boolean? ManejaVigenciaFuturas
        {
            get; 
            set;
        }
        /// <summary>
        /// Propiedad IdVigenciaInicial
        /// </summary>
        public int? IdVigenciaInicial
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdVigenciaFinal
        /// </summary>
        public int? IdVigenciaFinal
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdFormaPago
        /// </summary>
        public int? IdFormaPago
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DatosAdicionaleslugarEjecución
        /// </summary>
        public String DatosAdicionaleslugarEjecucion
        {
            get;
            set;
        }
        
        /// <summary>
        /// Propiedad IDEmpleadoOrdenadorGasto
        /// </summary>
        public int? IdEmpleadoOrdenadorGasto
        {
            get; 
            set;
        }
        public string NombreEmpleadoOrdenadorGasto { get; set; }
        /// <summary>
        /// Propiedad idRegionalEmpOrdG
        /// </summary>
        public int? IdRegionalEmpOrdG
        {
            get;
            set;
        }
        public string RegionalEmpOrdG { get; set; }
        /// <summary>
        /// Propiedad idDependenciaEmpOrdG
        /// </summary>
        public int? IdDependenciaEmpOrdG
        {
            get;
            set;
        }
        public string DependenciaEmpOrdG { get; set; }
        /// <summary>
        /// Propiedad idCargoEmpOrdG
        /// </summary>
        public int? IdCargoEmpOrdG
        {
            get;
            set;
        }
        public string CargoEmpOrdG { get; set; }

        /// <summary>
        /// Propiedad IDContratoAsociado
        /// </summary>
        public int? IdContratoAsociado
        {
            get; 
            set;
        }
        /// <summary>
        /// Propiedad ConvenioMarco
        /// </summary>
        public Boolean ConvenioMarco
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ConvenioAdhesion
        /// </summary>
        public Boolean ConvenioAdhesion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FK_IdContrato
        /// </summary>
        public int? FK_IdContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NumeroContrato
        /// </summary>
        public String NumeroContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdRegionalContrato
        /// </summary>
        public int? IdRegionalContrato
        {
            get; 
            set;
        }
        /// <summary>
        /// Propiedad FechaSuscripcionContrato
        /// </summary>
        public DateTime? FechaSuscripcionContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ConsecutivoSuscrito
        /// </summary>
        public String ConsecutivoSuscrito
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDUsuarioSuscribe
        /// </summary>
        public int? IDUsuarioSuscribe
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Suscrito
        /// </summary>
        public Boolean Suscrito
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ConsecutivoPlanCompasAsociado
        /// </summary>
        public int? ConsecutivoPlanComprasAsociado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }

        ///
        /// 
        /// hasta aqui son los que aparecen en el modelo
        /// 
        /// 

        /// <summary>
        /// Propiedad ValorInicialContrato
        /// </summary>
        public Decimal? ValorTotalCDP
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad ValorInicialContrato
        /// </summary>
        public Decimal? ValorTotalAportesICBF
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad ValorInicialContrato
        /// </summary>
        public Decimal? ValorTotalAportesContratista
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad ValorInicialContrato
        /// </summary>
        public Decimal? ValorTotalContratosAdheridos
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaSuscripcion
        /// </summary>
        public DateTime FechaSuscripcion { get; set; }
        /// <summary>
        /// Propiedad FechaRegistro
        /// </summary>
        public DateTime FechaRegistro{get;set;}
        /// <summary>
        /// Propiedad NumeroProceso
        /// </summary>
        public String NumeroProceso{get;set;}
        ///////////// <summary>
        ///////////// Propiedad FechaAdjudicacion
        ///////////// </summary>
        public DateTime? FechaAdjudicacion { get; set; }
        /// <summary>
        /// Propiedad IdModalidad
        /// </summary>
        public int? IdModalidad{get;set;}
        /// <summary>
        /// Propiedad IdCodigoModalidad
        /// </summary>
        public int? IdCodigoModalidad { get; set; }
        ///////////// <summary>
        ///////////// Propiedad IdCodigoProfesion
        ///////////// </summary>
        public int? IdCodigoProfesion { get; set; }
        /// <summary>
        /// Propiedad IdNombreProfesion
        /// </summary>
        public int? IdNombreProfesion { get; set; }
        ///////////// <summary>
        ///////////// Propiedad RequiereActa
        ///////////// </summary>
        public Boolean RequiereActa { get; set; }
        ///////////// <summary>
        ///////////// Propiedad ManejaAportes
        ///////////// </summary>
        public Boolean ManejaAportes { get; set; }
        ///////////// <summary>
        ///////////// Propiedad ManejaRecursos
        ///////////// </summary>
        public Boolean ManejaRecursos { get; set; }
        /// <summary>
        /// Propiedad ManejaVigenciasFuturas
        /// </summary>
        public Boolean ManejaVigenciasFuturas { get; set; }
        /// <summary>
        /// Propiedad CodigoRegional
        /// </summary>
        public int? CodigoRegional{get;set;}
        /// <summary>
        /// Propiedad NombreSolicitante
        /// </summary>
        public String NombreSolicitante{get;set;}
        /// <summary>
        /// Propiedad DependenciaSolicitante
        /// </summary>
        public String DependenciaSolicitante{get;set;}
        /// <summary>
        /// Propiedad CargoSolicitante
        /// </summary>
        public String CargoSolicitante { get; set; }
        /// <summary>
        /// Propiedad ValorTotalAdiciones
        /// </summary>
        public Decimal ValorTotalAdiciones{get;set;}
        /// <summary>
        /// Propiedad ValorAportesICBF
        /// </summary>
        public Decimal ValorAportesICBF{get;set;}
        /// <summary>
        /// Propiedad ValorAportesOperador
        /// </summary>
        public Decimal ValorAportesOperador{get;set;}
        /// <summary>
        /// Propiedad ValorTotalReduccion
        /// </summary>
        public Decimal ValorTotalReduccion{get;set;}
        /// <summary>
        /// Propiedad JustificacionAdicionSuperior50porc
        /// </summary>
        public String JustificacionAdicionSuperior50porc { get; set; }
        /// <summary>
        /// Propiedad PlazoInicial
        /// </summary>
        public int PlazoInicial{get;set;}
        /// <summary>
        /// Propiedad FechaInicialTerminacion
        /// </summary>
        public DateTime? FechaInicialTerminacion { get; set; }
        /// <summary>
        /// Propiedad FechaProyectadaLiquidacion
        /// </summary>
        public DateTime FechaProyectadaLiquidacion{get;set;}
        /// <summary>
        /// Propiedad FechaAnulacion
        /// </summary>
        public DateTime? FechaAnulacion { get; set; }
        /// <summary>
        /// Propiedad Prorrogas
        /// </summary>
        public int Prorrogas{get;set;}
        /// <summary>
        /// Propiedad PlazoTotal
        /// </summary>
        public int PlazoTotal{get;set;}
        /// <summary>
        /// Propiedad FechaFirmaActaInicio
        /// </summary>
        public DateTime FechaFirmaActaInicio{get;set;}
        /// <summary>
        /// Propiedad VigenciaFiscalInicial
        /// </summary>
        public int VigenciaFiscalInicial{get;set;}
        /// <summary>
        /// Propiedad VigenciaFiscalFinal
        /// </summary>
        public int VigenciaFiscalFinal{get;set;}
        /// <summary>
        /// Propiedad IdUnidadEjecucion
        /// </summary>
        public int IdUnidadEjecucion{get;set;}
        /// <summary>
        /// Propiedad IdLugarEjecucion
        /// </summary>
        public int IdLugarEjecucion{get;set;}
        /// <summary>
        /// Propiedad DatosAdicionales
        /// </summary>
        public String DatosAdicionales{get;set;}
        /// <summary>
        /// Propiedad IdTipoDocumentoContratista
        /// </summary>
        public String IdTipoDocumentoContratista{get;set;}
        /// <summary>
        /// Propiedad IdentificacionContratista
        /// </summary>
        public String IdentificacionContratista{get;set;}
        /// <summary>
        /// Propiedad NombreContratista
        /// </summary>
        public String NombreContratista{get;set;}
        /// <summary>
        /// Propiedad IdTipoEntidad
        /// </summary>
        public int IdTipoEntidad{get;set;}
        /// <summary>
        /// Propiedad ClaseContrato
        /// </summary>
        public string ClaseContrato { get; set; }
        /// <summary>
        /// Propiedad Consecutivo
        /// </summary>
        public string Consecutivo { get; set; }
        /// <summary>
        /// Propiedad AfectaPlanCompras
        /// </summary>
        public bool AfectaPlanCompras { get; set; }
        /// <summary>
        /// Propiedad IdSolicitante
        /// </summary>
        public int? IdSolicitante { get; set; }
        /// <summary>
        /// Propiedad IdProducto
        /// </summary>
        public int? IdProducto { get; set; }
        /// <summary>
        /// Propiedad FechaLiquidacion
        /// </summary>
        public DateTime? FechaLiquidacion { get; set; }
        /// <summary>
        /// Propiedad NumeroDocumentoVigenciaFutura
        /// </summary>
        public int? NumeroDocumentoVigenciaFutura { get; set; }
        /// <summary>
        /// Propiedad RequiereGarantias
        /// </summary>
        public Boolean? RequiereGarantias { get; set; }

        public string AcnoVigencia { get; set; }

        public string AcnoVigenciaFinal { get; set; }
        public string NombreRegional { get; set; }
        public string NombreCategoriaContrato { get; set; }
        public string NombreTipoContrato { get; set; }

        public string NombreModalidadSeleccion { get; set; }
        public string EstadoContrato { get; set; }
        public string NombreEstadoContrato { get; set; }
        public DateTime? FechaActaInicio { get; set; }

        public Boolean RequiereGarantia { get; set; }

        public Boolean AportesEspecie { get; set; }

        public int ValorAnticipo { get; set; }

        public int PorcentajeAnticipo { get; set; }

        public int? IdTipoPago { get; set; }

        public string NumeroContratoMigrado { get; set; }

        public Boolean EsContSinValor { get; set; }

        public string NumeroDocumentoIdentificacion { get; set; }
        public string TipoDocumentoIdentificacion { get; set; }
        public Boolean ? EsFechaFinalCalculada { get; set; }
        public int ? DiasFechaFinalCalculada { get; set; }
        public int ? MesesFechaFinalCalculada { get; set; }

        public int? IdCodigoSECOP { get; set; }
        public string VinculoSECOP { get; set; }

        public string FechasSuscripcionContratoView 
        { 
            get
            {
                string result = string.Empty;

                if (FechaSuscripcionContrato.HasValue)
                 result =  FechaSuscripcionContrato.Value.ToShortDateString();

                return result;
            } 
        }

        public decimal ValorAportesInicialDineroICBF { get; set; }

        public decimal ValorVigenciasFuturas { get; set; }

        public Contrato()
        {
        }
    }
}
