﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class ConsultarSuperInterContrato : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IDSupervisorIntervContrato
        {
            get;
            set;
        }
        public int IDSupervisorInterventor
        {
            get;
            set;
        }
        public String SupervisorInterventor
        {
            get;
            set;
        }
        public String NumeroContrato
        {
            get;
            set;
        }
        public String TipoContrato
        {
            get;
            set;
        }
        public String EstadoContrato
        {
            get;
            set;
        }
        public String ObjetoContrato
        {
            get;
            set;
        }
        public String NumeroIdentificacion
        {
            get;
            set;
        }
        public String TipoIdentificacion
        {
            get;
            set;
        }
        public String RazonSocial
        {
            get;
            set;
        }
        public String NombreSuperInterv
        {
            get;
            set;
        }
        public String NombreRazonSocialSuperInterv
        {
            get;
            set;
        }
        public String NumIdentifDirInterventoria
        {
            get;
            set;
        }
        public String NombreDirInterventoria
        {
            get;
            set;
        }
        
        public String RegionalContrato
        {
            get;
            set;
        }
        public DateTime? FechaSuscripcion
        {
            get;
            set;
        }
        public DateTime? FechaTerminacion
        {
            get;
            set;
        }
        public DateTime? FechaInicioSuperInterv
        {
            get;
            set;
        }
        public DateTime? FechaFinalSuperInterv
        {
            get;
            set;
        }
        public DateTime? FechaModSuperInterv
        {
            get;
            set;
        }
        public String NumeroContratoInterventoria
        {
            get;
            set;
        }
        public String TelefonoInterventoria
        {
            get;
            set;
        }
        public String TelCelularInterventoria
        {
            get;
            set;
        }
        public bool Inactivo
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime? FechaModifica
        {
            get;
            set;
        }

        public string CodTipoSuperInter { get; set; }

        public ConsultarSuperInterContrato()
        {
        }
    }
}

