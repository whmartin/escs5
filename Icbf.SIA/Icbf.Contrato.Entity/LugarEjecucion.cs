using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para Lugar Ejecución
    /// </summary>
    [Serializable]
    public class LugarEjecucion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IDDepartamento 
        /// </summary>
        public int IdContratoLugarEjecucion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public Int32 IDDepartamento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDMunicipio
        /// </summary>
        public Int32? IDMunicipio
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public LugarEjecucion()
        {
        }
    }
}
