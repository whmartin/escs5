using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class TipoModificacionBasica : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdTipoModificacionBasica
        {
            get;
            set;
        }
        public String Descripcion
        {
            get;
            set;
        }
        public bool RequiereModificacion
        {
            get;
            set;
        }
        public bool Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public TipoModificacionBasica()
        {
        }
    }
}
