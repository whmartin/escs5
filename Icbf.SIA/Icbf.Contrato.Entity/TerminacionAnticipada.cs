﻿using Icbf.Seguridad.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class TerminacionAnticipada : EntityAuditoria
    {
        public int IdTerminacionAnticipada { get; set; }
        public Int64 IDDetalleConsModeContractual { get; set; }
        public DateTime FechaRadicacion { get; set; }
        public string FechaRadicacionView
        {
            get
            {
                string result = string.Empty;
                if (FechaRadicacion != null)
                    result = FechaRadicacion.ToShortDateString();
                return result;
            }
        }
        public string NumeroRadicacion { get; set; }
        public DateTime FechaTerminacionAnticipada { get; set; }
        public string FechaTerminacionAnticipadaView
        {
            get
            {
                string result = null;
                if (FechaTerminacionAnticipada != null)
                    result = FechaTerminacionAnticipada.ToShortDateString();
                return result;
            }
        }
        public string Consideraciones { get; set; }
        public string TipoTerminacion { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
        public bool EsUnilateral { get; set; }
    }
}
