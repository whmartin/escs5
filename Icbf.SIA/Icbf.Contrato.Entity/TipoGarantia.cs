using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class TipoGarantia : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad  Tipo Garant�a
        /// </summary>
        public int IdTipoGarantia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreTipoGarantia
        /// </summary>
        public String NombreTipoGarantia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad EstadoString
        /// </summary>
        public String EstadoString
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public TipoGarantia()
        {
        }
    }
}
