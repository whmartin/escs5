using System;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class DirectorInterventoria : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IDContratoInterventoria
        {
            get;
            set;
        }
        public int IDDirectorInterventoria
        {
            get;
            set;
        }
        public int? IdTipoIdentificacion
        {
            get;
            set;
        }        
        public String NumeroIdentificacion
        {
            get;
            set;
        }
        public String PrimerNombre
        {
            get;
            set;
        }
        public String SegundoNombre
        {
            get;
            set;
        }
        public String PrimerApellido
        {
            get;
            set;
        }
        public String SegundoApellido
        {
            get;
            set;
        }
        public String Celular
        {
            get;
            set;
        }
        public String Telefono
        {
            get;
            set;
        }
        public String CorreoElectronico
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public DirectorInterventoria()
        {
        }
    }
}
