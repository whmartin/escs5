using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para el registro tipo cuenta entidad financiera
    /// </summary>
    public class TipoCuentaEntFin : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdTipoCta
        {
            get;
            set;
        }
        public String CodTipoCta
        {
            get;
            set;
        }
        public String Descripcion
        {
            get;
            set;
        }
        public Boolean Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public TipoCuentaEntFin()
        {
        }
    }
}
