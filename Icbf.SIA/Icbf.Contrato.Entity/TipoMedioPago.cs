using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para Tipo medio de pago
    /// </summary>
    public class TipoMedioPago : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdTipoMedioPago
        {
            get;
            set;
        }
        public String CodMedioPago
        {
            get;
            set;
        }
        public String DescTipoMedioPago
        {
            get;
            set;
        }
        public Boolean Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public TipoMedioPago()
        {
        }
    }
}
