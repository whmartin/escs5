using System;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class VigenciaFuturas : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IDVigenciaFuturas
        {
            get;
            set;
        }
        public String NumeroRadicado
        {
            get;
            set;
        }
        public DateTime FechaExpedicion
        {
            get;
            set;
        }
        public int IdContrato
        {
            get;
            set;
        }
        public Decimal ValorVigenciaFutura
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public int AnioVigencia
        {
            get;
            set;
        }
        public int Estado
        {
            get;
            set;
        }
        public VigenciaFuturas()
        {
        }
    }
}
