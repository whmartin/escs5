using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para registro de Cargos del ICBF
    /// </summary>
    public class CargosICBF : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdCargo
        {
            get;
            set;
        }
        public String CodCargo
        {
            get;
            set;
        }
        public String Nombre
        {
            get;
            set;
        }
        public Boolean Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public CargosICBF()
        {
        }
    }
}
