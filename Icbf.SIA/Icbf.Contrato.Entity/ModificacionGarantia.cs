using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class ModificacionGarantia : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdModificacionGarantia
        {
            get;
            set;
        }
        public int IdContrato
        {
            get;
            set;
        }
        public int NumeroDocumento
        {
            get;
            set;
        }
        public DateTime FechaRegistro
        {
            get;
            set;
        }
        public String TipoModificacion
        {
            get;
            set;
        }
        public String NumeroGarantia
        {
            get;
            set;
        }
        public int IdAmparo
        {
            get;
            set;
        }
        public String TipoAmparo
        {
            get;
            set;
        }
        public DateTime FechaVigenciaDesde
        {
            get;
            set;
        }
        public DateTime FechaVigenciaHasta
        {
            get;
            set;
        }
        public decimal CalculoValorAsegurado
        {
            get;
            set;
        }
        public String TipoCalculo
        {
            get;
            set;
        }
        public decimal ValorAdicion
        {
            get;
            set;
        }
        public decimal ValorTotalReduccion
        {
            get;
            set;
        }
        public decimal ValorAsegurado
        {
            get;
            set;
        }
        public String ObservacionesModificacion
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public ModificacionGarantia()
        {
        }
    }
}
