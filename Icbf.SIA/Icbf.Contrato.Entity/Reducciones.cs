using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class Reducciones : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdReduccion
        {
            get;
            set;
        }
        public decimal ValorReduccion
        {
            get;
            set;
        }
        public DateTime FechaReduccion
        {
            get;
            set;
        }
        public int IDDetalleConsModContractual
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public String Justificacion
        {
            get;
            set;
        }
        public bool SumaValor
        {
            get;
            set;
        }

        public string  FechaReduccionView
        {
            get
            {
                string result = string.Empty;

                if (FechaReduccion != null)
                {
                    result = FechaReduccion.Date.ToShortDateString();
                }
                return result; 
            }
        }
        

        public Reducciones()
        {
        }
    }

    public class ReduccionContrato : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public string NumeroContrato { get; set; }
        public string NombreRegional { get; set; }
        public DateTime FechaInicioContrato { get; set; }
        public DateTime FechaFinalizacionInicialContrato { get; set; }
        public DateTime FechaFinalTerminacionContrato { get; set; }
        public string ObjetoContrato { get; set; }
        public string AlcanceObjeto { get; set; }
        public decimal ValorInicial { get; set; }
        public decimal ValorFinal { get; set; }
        public string PrimerNombreSupervisor { get; set; }
        public string SegundoNombreSupervisor { get; set; }
        public string PrimerApellidoSupervisor { get; set; }
        public string SegundoApellidoSupervisor { get; set; }        

    }

    [Serializable]
    public class ProductosReducion
    {
        public int CantidadCupos;
        public int NumeroConsecutivo;
        public int CodigoProducto;
        public string NombreProducto;
        public string TipoProducto;
        public int ValorUnitario;
        public int ValorTotal;
        public int Tiempo;
        public string UnidadTiempo;
        public string UnidadMedida;


        	 
    }
}
