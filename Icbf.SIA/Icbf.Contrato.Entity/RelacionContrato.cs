﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para Relación Contrato
    /// </summary>
    [Serializable]
    public class RelacionContrato: Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdContratoMaestro
        /// </summary>
        public int IdContratoMaestro { get; set; }
        /// <summary>
        /// Propiedad IdContratoAsociado
        /// </summary>
        public int IdContratoAsociado { get; set; }
        /// <summary>
        /// Propiedad FechaAsociacion
        /// </summary>
        public DateTime? FechaAsociacion { get; set; }
        /// <summary>
        /// Propiedad IdPersonaAbogado
        /// </summary>
        public int? IdPersonaAbogado { get; set; }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }

        public RelacionContrato()
        { }


    }
}
