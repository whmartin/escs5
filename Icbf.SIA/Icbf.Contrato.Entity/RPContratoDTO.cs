﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class RPContratoDTO
    {

        public int IdRPContrato
        {
            get;
            set;
        }
        public int IdContrato
        {
            get;
            set;
        }
        public int IdRP
        {
            get;
            set;
        }

        public Decimal ValorRP
        {
            get;
            set;
        }

        public DateTime? FechaRP
        {
            get;
            set;
        }
        public DateTime? FechaExpedicionRP
        {
            get;
            set;
        }
        public int IdVigenciaFiscal
        {
            get;
            set;
        }

        public int VigenciaFiscal
        {
            get;
            set;
        }

        public int IdRegional
        {
            get;
            set;
        }

        public String CodigoRegional
        {
            get;
            set;
        }

        public String NombreRegional
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public string NumeroRP { get; set; }

    }
}
