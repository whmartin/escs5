﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class ProveedoresContratos
    {
        public int IdProveedoresContratos
        {
            get;
            set;
        }
        public int IdProveedores
        {
            get;
            set;
        }
        public int IdContrato
        {
            get;
            set;
        }
        public int? IdFormaPago
        {
            get;
            set;
        }

        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public ProveedoresContratos()
        {
        }

    }
}
