using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para Cl�usla contratos
    /// </summary>
    public class ClausulaContrato : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdClausulaContrato
        /// </summary>
        public int IdClausulaContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreClausulaContrato
        /// </summary>
        public String NombreClausulaContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoClausula
        /// </summary>
        public int IdTipoClausula
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Contenido
        /// </summary>
        public String Contenido
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoContrato
        /// </summary>
        public int IdTipoContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public ClausulaContrato()
        {
        }
    }
}
