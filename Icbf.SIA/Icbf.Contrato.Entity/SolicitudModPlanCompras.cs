﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class SolicitudModPlanCompras : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdSolicitudModPlanCompra
        {
            get;
            set;
        }
        public String Justificacion
        {
            get;
            set;
        }
        public int NumeroSolicitud
        {
            get;
            set;
        }
        public DateTime FechaSolicitud
        {
            get;
            set;
        }
        public String IdEstadoSolicitud
        {
            get;
            set;
        }
        public string EstadoSolicitud
        {
            get;
            set;
        }
        public int IdUsuarioSolicitud
        {
            get;
            set;
        }
        public string UsuarioSolicitud
        {
            get;
            set;
        }
        public int IdPlanDeCompras
        {
            get;
            set;
        }
        public int NumeroConsecPlanDeCompras
        {
            get;
            set;
        }
        public int IdVigencia
        {
            get;
            set;
        }
        public string Vigencia
        {
            get;
            set;
        }
        public int IdContrato
        {
            get;
            set;
        }
        public string NumeroContrato
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public SolicitudModPlanCompras()
        {
        }
    }
}

