﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class DocumentosLiquidacion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdDocumentoLiquidacion
        {
            get;
            set;
        }
        
        public Boolean Estado
        {
            get;
            set;
        }
        public String NombreDocumento
        {
            get;
            set;
        }

        public String DescripcionDocumento
        {
            get;
            set;
        }

        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
    }
}
