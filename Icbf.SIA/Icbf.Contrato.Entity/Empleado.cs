﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para Empleado
    /// </summary>
    public class Empleado : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdTipoIdentificacion
        /// </summary>
        public string IdTipoIdentificacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad TipoIdentificacion
        /// </summary>
        public string TipoIdentificacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NumeroIdentificación
        /// </summary>
        public string NumeroIdentificacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoVinculacionContractual
        /// </summary>
        public string IdTipoVinculacionContractual
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad TipoVinculacionContractual
        /// </summary>
        public string TipoVinculacionContractual
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreEmpleado
        /// </summary>
        public string NombreEmpleado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdRegional
        /// </summary>
        public string IdRegional
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdRegional
        /// </summary>
        public string CodigoRegional
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Regional
        /// </summary>
        public string Regional
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdDependencia
        /// </summary>
        public string IdDependencia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Dependencia
        /// </summary>
        public string Dependencia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdCargo
        /// </summary>
        public string IdCargo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Cargo
        /// </summary>
        public string Cargo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PrimerNombre
        /// </summary>
        public string PrimerNombre
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad SegundoNombre
        /// </summary>
        public string SegundoNombre
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PrimerApellido
        /// </summary>
        public string PrimerApellido
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad SegundoApellido
        /// </summary>
        public string SegundoApellido
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Direccion
        /// </summary>
        public string Direccion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Telefono
        /// </summary>
        public string Telefono
        {
            get;
            set;
        }
    }
}
