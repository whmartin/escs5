using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class ConsModContractual : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IDCosModContractual
        {
            get;
            set;
        }
        public String NumeroDoc
        {
            get;
            set;
        }
        public String Justificacion
        {
            get;
            set;
        }
        public int IDContrato
        {
            get;
            set;
        }
        public String ConsecutivoSuscrito
        {
            get;
            set;
        }
        public Boolean Suscrito
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }

        public int IdConsModContractualesEstado { get; set; }

        public int IDTipoModificacionContractual { get; set; }

	  public String ConsModContractualesEstado_Descripcion   {get;set;}
	  public String TipoModificacionContractual_Codigo       {get;set;}
	  public String TipoModificacionContractual_Descripcion	{ get;set;} 
	  public String UsuarioAbogado  { get;set;}

      public DateTime FechaSuscripcion { get; set; }

      public DateTime FechaSolicitud { get; set; }

        public ConsModContractual()
        {
        }
    }
}
