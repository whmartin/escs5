﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class ContratoModificacionDetalle
    {
        public int IdContrato { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
        public int IdEstadoContrato { get; set; }
        public string EstadoContrato { get; set; }
        public string CodigoEstadoContrato { get; set; }
        public int IdRegional { get; set; }
        public string Regional { get; set; }
        public int IdModalidadSeleccion { get; set; }
        public string ModalidadSeleccion { get; set; }
        public int IdCategoriaContrato { get; set; }
        public string CategoriaContrato { get; set; }
        public int IdTipoContrato { get; set; }
        public string TipoContrato { get; set; }
        public string CodigoTipoContrato { get; set; }
        public string NumeroContrato { get; set; }
        public bool? EsContratoMarco { get; set; }
        public bool? EsContratoConvenioAdhesion { get; set; }
        public string IdModalidadAcademica { get; set; }
        public string IdProfesion { get; set; }
        public bool RequiereActaInicio { get; set; }
        public bool RequiereGarantia { get; set; }
        public bool ManejaCofinanciacion { get; set; }
        public bool ManejaRecursosICBF { get; set; }
        public bool ManejaRecursosEspecieICBF { get; set; }
        public bool ManejaVigenciasFuturas { get; set; }

        public int? IdentificacionOrdenadorGasto { get; set; }
        public int? IdRegionalOrdenadorGasto { get; set; }
        public int? IdDependenciaOrdenadorGasto { get; set; }
        public int? IdCargoOrdenadorGasto { get; set; }

        public int? IdentificacionSolicitante { get; set; }
        public int? IdRegionalSolicitante { get; set; }
        public int? IdDependenciaSolicitante { get; set; }
        public int? IdCargoSolicitante { get; set; }

        public DateTime? FechaInicioContrato { get; set; }
        public DateTime? FechaFinalizaContrato { get; set; }
        public DateTime? FechaFinalizaContratoInicial { get; set; }
        public DateTime? FechaSuscripcionContrato { get; set; }

        public int IdVigenciaInicial { get; set; }
        public int IdVigenciaFinal { get; set; }

        public decimal? ValorContrato { get; set; }
        public decimal? ValorContratoFinal { get; set; }
        public decimal? ValorAporteDinero { get; set; }

        public string Objeto { get; set; }
        public string Alcance { get; set; }

        public int? IdPlanCompras { get; set; }

        public int VigenciaInicial { get; set; }
        public int VigenciaFinal { get; set; }
        public bool? EsFechaFinalCalculada { get; set; }

        public int TieneProrrogas { get; set; }
        public int TieneReducciones { get; set; }
        public int TieneAdiciones { get; set; }
        public int TieneCesiones { get; set; }

        public int TieneCambioSupervisor { get; set; }

        public int? IdNumeroProceso { get; set; }
        public string NumeroProceso { get; set; }
        public DateTime? FechaProceso { get; set; }

        public int IdRegimenContratacion { get; set; }

        public DateTime? FechaActaInicio { get; set; }
    }

    public class ContratoModificacionEditar
    {
        public int IdContrato { get; set; }
        public int IdModalidadSeleccion { get; set; }
        public int IdCategoriaContrato { get; set; }
        public int IdTipoContrato { get; set; }
        public string IdModalidadAcademica { get; set; }
        public string IdProfesion { get; set; }
        public string UsuarioModifica { get; set; }
        public decimal ValorAporteDinero { get; set; }
        public string CodigoEstadoContrato { get; set; }
        public DateTime FechaSuscripcion { get; set; }
        public bool RequiereGarantia { get; set; }
        public bool RequiereActaInicio { get; set; }
        public bool EsFechaCalculada { get; set; }
        public DateTime FechaInicioEjecuccion { get; set; }
        public DateTime FechaFinalEjecuccion { get; set; }
        public int IdVigenciaInicial { get; set; }
        public int IdVigenciaFinal { get; set; }
        public int ? IdNumeroProceso { get; set; }
        public DateTime ? FechaProceso { get; set; }
        public bool ManejaCofinanciacion { get; set; }
        public bool ManejaRecursosEspecieICBF { get; set; }
        public bool ManejaVigenciasFuturas { get; set; }
        public int IdRegimenContratacion { get; set; }
        public int ? DiasCalculados { get; set; }
        public int ? MesesCalculados { get; set; }
        public DateTime? FechaActaInicio { get; set; }
    }
}
