﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class EstadoSolcitudModPlanCompras : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public String IdEstadoSolicitud
        {
            get;
            set;
        }
        public String CodEstado
        {
            get;
            set;
        }
        public String Descripcion
        {
            get;
            set;
        }
        public Boolean Activo
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public EstadoSolcitudModPlanCompras()
        {
        }
    }
}

