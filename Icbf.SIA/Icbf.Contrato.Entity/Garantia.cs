using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class Garantia : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IDGarantia
        {
            get;
            set;
        }
        public int IDTipoGarantia
        {
            get;
            set;
        }
        public String NombreTipoGarantia
        {
            get;
            set;
        }
        public String NumeroGarantia
        {
            get;
            set;
        }
        public DateTime? FechaAprobacionGarantia
        {
            get;
            set;
        }
        public DateTime? FechaCertificacionGarantia
        {
            get;
            set;
        }
        public DateTime? FechaDevolucion
        {
            get;
            set;
        }
        public String MotivoDevolucion
        {
            get;
            set;
        }
        public int IDUsuarioAprobacion
        {
            get;
            set;
        }
        public int IDUsuarioDevolucion
        {
            get;
            set;
        }
        public int IdContrato
        {
            get;
            set;
        }
        public Boolean BeneficiarioICBF
        {
            get;
            set;
        }
        public String DescripcionBeneficiarios
        {
            get;
            set;
        }
        public DateTime? FechaInicioGarantia
        {
            get;
            set;
        }
        public string FechaInicioGarantiaView
        {
            get
            {
                string result = string.Empty;
                if (FechaInicioGarantia.HasValue)
                    result = FechaInicioGarantia.Value.ToShortDateString();
                return result;
            }
        }
        public DateTime? FechaExpedicionGarantia
        {
            get;
            set;
        }
        public DateTime? FechaVencimientoInicialGarantia
        {
            get;
            set;
        }
        public DateTime? FechaVencimientoFinalGarantia
        {
            get;
            set;
        }
        public string FechaVencimientoFinalGarantiaView
        {
            get
            {
                string result = string.Empty;
                if (FechaVencimientoFinalGarantia.HasValue)
                    result = FechaVencimientoFinalGarantia.Value.ToShortDateString();
                return result;
            }
        }

        public string FechaAprobacionGarantiaView
        {
            get
            {
                string result = string.Empty;
                if(FechaAprobacionGarantia.HasValue)
                    result = FechaAprobacionGarantia.Value.ToShortDateString();
                return result;
            }
        }


        public DateTime? FechaReciboGarantia
        {
            get;
            set;
        }
        public String ValorGarantia
        {
            get;
            set;
        }
        public Boolean? Anexos
        {
            get;
            set;
        }
        public String ObservacionesAnexos
        {
            get;
            set;
        }
        //public int EntidadProvOferenteAseguradora
        //{
        //    get;
        //    set;
        //}
        public int IdTercero
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public Boolean Estado
        {
            get;
            set;
        }
        public int IDSucursalAseguradoraContrato
        {
            get;
            set;
        }
        public int IDEstadosGarantias
        {
            get;
            set;
        }
        public String CodigoEstadoGarantia
        {
            get;
            set;
        }
        public String DescripcionEstadoGarantia
        {
            get;
            set;
        }
        public bool EsGarantiaModificada { get; set; }

        public int? IdTipoModificacion { get; set; }
        public String CodigoDescripcionEstadoGarantia
        {
            get;
            set;
        }

        public String Zona
        {
            get;
            set;
        }
        public Boolean BeneficiarioOTROS { get; set; }

        public string OrigenAprobación
        {
            get
            {
                string result = string.Empty;

                if (CodigoEstadoGarantia == "006")
                    result = "Regístro";
                else if (CodigoEstadoGarantia == "007")
                    result = "Modificación";

                return result;
            }
        }

        public string DescripcionModificacionGarantia { get; set; }

        public string NumeroModificacion { get; set; }

        public DateTime? FechaModificacionGarantia { get; set; }

        public string EntidadAseguradora { get; set; }

        public string ValorSIRECI { get; set; }

        public string NumeroContrato { get; set; }

        public string DescEstadoContrato { get; set; }

        public DateTime? FechaSuscripcionContrato { get; set; }

        public string NombreSupervisor { get; set; }

        public Garantia()
        {
        }
    }
}
