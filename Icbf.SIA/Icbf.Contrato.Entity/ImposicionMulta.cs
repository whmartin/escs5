﻿using Icbf.Seguridad.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class ImposicionMulta : EntityAuditoria
    {
        public int IdImposicionMulta { get; set; }
        public Int64 IDDetalleConsModContractual { get; set; }
        public string Razones { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
        public DateTime ?FechaEnvioProcesoSancionatorio {get; set;}
        public string NumeroRadicadoCitacion{ get; set; }
        public DateTime ?FechaCelebracionAudiencia{ get; set; }
        public string InformeEjecutivo{ get; set; }
        public string EstadoAudiencia{ get; set; }
        public string MotivoEstado{ get; set; }
        public string NumeroResolucion{ get; set; }
        public DateTime? FechaResolucion{ get; set; }
        public string Resuleve{ get; set; }
        public string TipoResuelve{ get; set; }
        public decimal? ValorSancion{ get; set; }
        public bool ? InterponeRecurso{ get; set; }
        public string DesicionRecurso{ get; set; }
        public DateTime? FechaEjecutoriaResolucion{ get; set; }
        public DateTime? FechaPublicacionSECOP{ get; set; }
        public DateTime? FechaEnvioCamaraComercio  { get; set; }
        public DateTime? FechaComunicacionProcuraduria { get; set; }
        public decimal? ValorSancionModificacion { get; set; }
        public string TipoSancion { get; set; }
        public bool? Aprobado { get; set; }
    }

    public class ImposicionMultaHistorico
    {
        public int IdHistoricoProcesoImpMultas { get; set; }
        public DateTime Fecha { get; set; }
        public string Motivo { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
    }
}
