using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class Prorrogas : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdProrroga
        {
            get;
            set;
        }
        public DateTime FechaInicio
        {
            get;
            set;
        }
        public DateTime FechaFin
        {
            get;
            set;
        }
        public int Dias
        {
            get;
            set;
        }
        public int Meses
        {
            get;
            set;
        }
        public int Años
        {
            get;
            set;
        }
        public String Justificacion
        {
            get;
            set;
        }
        public int IdDetalleConsModContractual
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public string FechaInicioView 
        { 
            get
            {
                string result = string.Empty;
                if(FechaInicio != null)
                result = FechaInicio.Date.ToShortDateString();
                return result;
            }
        }
        public string FechaFinView
        {
            get
            {
                string result = string.Empty;
                if (FechaFin != null)
                    result = FechaFin.Date.ToShortDateString();
                return result;
            }
        }
        public int Anios { get { return Años; } }
        public Prorrogas()
        {
        }
    }

    public class ProrrogasContrato : Icbf.Seguridad.Entity.EntityAuditoria
    {
       public int IdContrato { get; set; }
       public string NumeroContrato { get; set; }
       public string NombreRegional { get; set; }
       public DateTime FechaInicio { get; set; }
       public DateTime FechaFinalizacion { get; set; }
       public string ObjetoDelContrato { get; set; }
       public string AlcanceContrato { get; set; }
       public decimal ValorInicialContrato { get; set; }
       public decimal ValorFinalContrato { get; set; }
    }
}
