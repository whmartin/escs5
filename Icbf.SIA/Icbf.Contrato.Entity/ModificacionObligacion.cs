using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class ModificacionObligacion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdModObligacion
        {
            get;
            set;
        }
        public int IDDetalleConsModContractual
        {
            get;
            set;
        }
        public String ClausulaContrato
        {
            get;
            set;
        }
        public String DescripcionModificacion
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public ModificacionObligacion()
        {
        }
    }
}
