using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class PlanComprasProductos : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int? IdProductoPlanCompraContrato { get; set; }
        public int? NumeroConsecutivoPlanCompras
        {
            get;
            set;
        }
        public String CodigoProducto
        {
            get;
            set;
        }
        public String NombreProducto
        {
            get;
            set;
        }
        public String TipoProducto
        {
            get;
            set;
        }
        public Decimal? CantidadCupos
        {
            get;
            set;
        }
        public Decimal? ValorUnitario
        {
            get;
            set;
        }
        public Decimal? Tiempo
        {
            get;
            set;
        }
        public Decimal? ValorTotal
        {
            get;
            set;
        }
        public String IdUnidadTiempo
        {
            get;
            set;
        }
        public String UnidadTiempo
        {
            get;
            set;
        }
        public String UnidadMedida
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public PlanComprasProductos()
        {
        }

        public String IdDetalleObjeto { get; set; }

        public int? IdProducto { get; set; }

        public int? IdPlanDeComprasContratos { get; set; }

        public System.Data.DataTable DtDatosProductos { get; set; }

        public System.Data.DataTable DtDatosRubros { get; set; }

        public int? IdContrato { get; set; }

        public int? Vigencia { get; set; }

        public int? IdUsuario { get; set; }

        public bool? EsAdicion { get; set; }

        public int? IdAdicion { get; set; }

        public bool? IsReduccion { get; set; }

        public int? IdReduccion { get; set; }

        public decimal ValorReduccion { get; set; }

        public decimal ValorAdiccion { get; set; }

        public int IdDetConsModContractual { get; set; }
        public int? IdVigenciaFutura { get; set; }
        public bool? esVigenciaFutura { get; set; }
        public string Objeto { get; set; }
        public string Alcance { get; set; }
    }

    public class ValidacionProductoOriginal
    {
        public bool EsproductoOriginal { get; set; }
        public bool SufrioModificaciones { get; set; }
        public bool EsModificacionProceso { get; set; }
        public bool EsModificacionUltima { get; set; }
    }

    public class ValidacionRubroOriginal
    {
        public bool EsproductoOriginal { get; set; }
        public bool SufrioModificaciones { get; set; }
        public bool EsModificacionProceso { get; set; }
        public bool EsModificacionUltima { get; set; }
    }

    public class PlanComprasProductosValidar
    {
        public int idPlanCompras { get; set; }
        public int idContrato { get; set; }
        public int IdPlanComprasProducto { get; set; }
        public string IdDetalleProducto { get; set; }
        public int Vigencia { get; set; }
    }

    public class PlanComprasProductosActualizar
    {
        public int IdPlanComprasProducto { get; set; }
        public string UnidadMedida { get; set; }
        public string UnidadTiempo { get; set; }
        public string DescripcionProducto { get; set; }
        public string IdDetalleProducto { get; set; }
    }
}
