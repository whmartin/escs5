using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para Unidad Medida
    /// </summary>
    public class UnidadMedida : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdNumeroContrato
        /// </summary>
        public int IdNumeroContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NumeroContrato
        /// </summary>
        public String NumeroContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaInicioEjecuciónContrato
        /// </summary>
        public DateTime FechaInicioEjecuciónContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaTerminacionInicialContrato
        /// </summary>
        public DateTime FechaTerminacionInicialContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaFinalTerminacionContrato
        /// </summary>
        public DateTime FechaFinalTerminacionContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public UnidadMedida()
        {
        }
    }
}
