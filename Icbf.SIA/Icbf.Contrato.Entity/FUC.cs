using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class FUC : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public Int64 Id
        {
            get;
            set;
        }
        public String Regional
        {
            get;
            set;
        }
        public String CodRegional
        {
            get;
            set;
        }
        public String NumeroContrato
        {
            get;
            set;
        }
        public String FechaSuscripcion
        {
            get;
            set;
        }
        public String Modalidad
        {
            get;
            set;
        }
        public String IdModalidad 
        {
            get;
            set;
        }
        public String ProcesoSeleccion
        {
            get;
            set;
        }
        public String CategoriaContrato
        {
            get;
            set;
        }
        public String IdCategoriaContrato
        {
            get;
            set;
        }
        public String ClaseContrato
        {
            get;
            set;
        }
        public String IdAreaSolicita
        {
            get;
            set;
        }
        public String Area
        {
            get;
            set;
        }
        public String IdNaturaleza
        {
            get;
            set;
        }
        public String Naturaleza
        {
            get;
            set;
        }
        public String ContratistaTipoIdentificacion
        {
            get;
            set;
        }
        public String ContratistaIdentificacion
        {
            get;
            set;
        }
        public String ContratistaDigVerifi
        {
            get;
            set;
        }
        public String ContratistaNombre
        {
            get;
            set;
        }
        public String ContratistaProfesion
        {
            get;
            set;
        }
        public String ContratistaDireccion
        {
            get;
            set;
        }
        public String ContratistaTelefono
        {
            get;
            set;
        }
        public String ContratistaLugarUbicacion
        {
            get;
            set;
        } 
        public String IdContratistaLugarUbicacion
        {
            get;
            set;
        }
        public String IdDepartamentoLugarUbicacion
        {
            get;
            set;
        }
        public String IdTipoActividad
        {
            get;
            set;
        }
        public String TipoActividad
        {
            get;
            set;
        }
        public String IdTipoOrganizacion
        {
            get;
            set;
        }
        public String TipoOrganizacion
        {
            get;
            set;
        }
        public String ContratistaRepresentanteLegal
        {
            get;
            set;
        }
        public String ContratistaRepresentanteIdentificacion
        {
            get;
            set;
        }
        public String IntegrantesConsorcioUnionTemp1TipoIdentificacion
        {
            get;
            set;
        }  
       
        public String IntegrantesConsorcioUnionTemp1Nombre
        {
            get;
            set;
        }
        public String IntegrantesConsorcioUnionTemp1DigitoVerificacion
        {
            get;
            set;
        }
        public String IntegrantesConsorcioUnionTemp1PorcentajeParticipacion
        {
            get;
            set;
        }
        public String IntegrantesConsorcioUnionTemp2TipoIdentificacion
        {
            get;
            set;
        }
        public String IntegrantesConsorcioUnionTemp2Nombre
        {
            get;
            set;
        }
        public String IntegrantesConsorcioUnionTemp2DigitoVerificacion
        {
            get;
            set;
        }
        public String IntegrantesConsorcioUnionTemp2PorcentajeParticipacion
        {
            get;
            set;
        }
        public String IntegrantesConsorcioUnionTemp3TipoIdentificacion
        {
            get;
            set;
        }
        public String IntegrantesConsorcioUnionTemp3Nombre
        {
            get;
            set;
        }
        public String IntegrantesConsorcioUnionTemp3DigitoVerificacion
        {
            get;
            set;
        }
        public String IntegrantesConsorcioUnionTemp3PorcentajeParticipacion
        {
            get;
            set;
        }
        public String IntegrantesConsorcioUnionTemp4TipoIdentificacion
        {
            get;
            set;
        }
        public String IntegrantesConsorcioUnionTemp4Nombre
        {
            get;
            set;
        }
        public String IntegrantesConsorcioUnionTemp4DigitoVerificacion
        {
            get;
            set;
        }
        public String IntegrantesConsorcioUnionTemp4PorcentajeParticipacion
        {
            get;
            set;
        }
        public String IntegrantesConsorcioUnionTemp5TipoIdentificacion
        {
            get;
            set;
        }
        public String IntegrantesConsorcioUnionTemp5Nombre
        {
            get;
            set;
        }
        public String IntegrantesConsorcioUnionTemp5DigitoVerificacion
        {
            get;
            set;
        }
        public String IntegrantesConsorcioUnionTemp5PorcentajeParticipacion
        {
            get;
            set;
        }
        public String AfectacionRecurso
        {
            get;
            set;
        }
        public String IdCodigoSecop
        {
            get;
            set;
        }
        public String CodigoSecop
        {
            get;
            set;
        }
        public String ObjectoContrato
        {
            get;
            set;
        }
        public String LugarEjecucion
        {
            get;
            set;
        }
        public String idDptoLugarEjecucion
        {
            get;
            set;
        }
        public String FechaInicio
        {
            get;
            set;
        }
        public String FechaTerminacion
        {
            get;
            set;
        }
        public String PlazoInicial
        {
            get;
            set;
        }
        public String CDPNumero
        {
            get;
            set;
        }
        public String CDPFecha
        {
            get;
            set;
        }
        public String CDPValor
        {
            get;
            set;
        }
        public String RPNumero
        {
            get;
            set;
        }
        public String RPFecha
        {
            get;
            set;
        }
        public String RPValor
        {
            get;
            set;
        }
        public String RPRubro1
        {
            get;
            set;
        }
        public String RPRubro2
        {
            get;
            set;
        }
        public String RPRubro3
        {
            get;
            set;
        }
        public String ValorInicialContrato
        {
            get;
            set;
        }
        public String FormaPago
        {
            get;
            set;
        }
        public String Anticipo
        {
            get;
            set;
        }
        public String ValorAnticipado
        {
            get;
            set;
        }
        public String Confinanciador1IdPersona
        {
            get;
            set;
        }
        public String Confinanciador1TipoPersona
        {
            get;
            set;
        }
        public String Confinanciador1TipoIdentificacion
        {
            get;
            set;
        }
        public String Confinanciador1NumeroIdentificacion
        {
            get;
            set;
        }
        public String Confinanciador1NombreRazonSocial
        {
            get;
            set;
        }
        public String Confinanciador1Valor
        {
            get;
            set;
        }
        public String Confinanciador2IdPersona
        {
            get;
            set;
        }
        public String Confinanciador2TipoPersona
        {
            get;
            set;
        }
        public String Confinanciador2TipoIdentificacion
        {
            get;
            set;
        }
        public String Confinanciador2NumeroIdentificacion
        {
            get;
            set;
        }
        public String Confinanciador2NombreRazonSocial
        {
            get;
            set;
        }
        public String Confinanciador2Valor
        {
            get;
            set;
        }
        public String Confinanciador3IdPersona
        {
            get;
            set;
        }
        public String Confinanciador3TipoPersona
        {
            get;
            set;
        }
        public String Confinanciador3TipoIdentificacion
        {
            get;
            set;
        }
        public String Confinanciador3NumeroIdentificacion
        {
            get;
            set;
        }
        public String Confinanciador3NombreRazonSocial
        {
            get;
            set;
        }
        public String Confinanciador3Valor
        {
            get;
            set;
        }
        public String Confinanciador4IdPersona
        {
            get;
            set;
        }
        public String Confinanciador4TipoPersona
        {
            get;
            set;
        }
        public String Confinanciador4TipoIdentificacion
        {
            get;
            set;
        }
        public String Confinanciador4NumeroIdentificacion
        {
            get;
            set;
        }
        public String Confinanciador4NombreRazonSocial
        {
            get;
            set;
        }
        public String Confinanciador4Valor
        {
            get;
            set;
        }
        public String Confinanciador5IdPersona
        {
            get;
            set;
        }
        public String Confinanciador5TipoPersona
        {
            get;
            set;
        }
        public String Confinanciador5TipoIdentificacion
        {
            get;
            set;
        }
        public String Confinanciador5NumeroIdentificacion
        {
            get;
            set;
        }
        public String Confinanciador5NombreRazonSocial
        {
            get;
            set;
        }
        public String Confinanciador5Valor
        {
            get;
            set;
        }
        public String ValorTotalCofinanciacion
        {
            get;
            set;
        }
        public String VigenciaFuturaNumero
        {
            get;
            set;
        }
        public String VigenciaFuturaValor
        {
            get;
            set;
        }
        public String ValorTotalInicialContrato
        {
            get;
            set;
        }
        public String ValorInicialContratoIncluidaCofinanciacion
        {
            get;
            set;
        }
        public String FechaAprobacionGarantias
        {
            get;
            set;
        }
        public String TipoGarantia
        {
            get;
            set;
        }
        public String EntidadAseguradoraNumeroIdentificacion
        {
            get;
            set;
        }
        public String EntidadAseguradoraGarantias
        {
            get;
            set;
        }
        public String EntidadAseguradoraTipoGarantia
        {
            get;
            set;
        }
        public String IdRiesgos
        {
            get;
            set;
        }
        public String GarantiasRiesgosAsegurados
        {
            get;
            set;
        }
        public String GarantiasPorcentajeAsegurado
        {
            get;
            set;
        }
        public String GarantiasValorTotalAsegurado
        {
            get;
            set;
        }
        public String IdArea
        {
            get;
            set;
        }
        public String Supervisor
        {
            get;
            set;
        }
        public String SupervisorArea
        {
            get;
            set;
        }
        public String SupervisorCargo
        {
            get;
            set;
        }
        public String SupervisorNombre
        {
            get;
            set;
        }
        public String SupervisorIdentificacion
        {
            get;
            set;
        }
        public String IdTipoSupervisor
        {
            get;
            set;
        }
        public String TipoSupervisor
        {
            get;
            set;
        }
        public String IdentificacionGasto
        {
            get;
            set;
        }
        public String NombreOrdenadorGasto
        {
            get;
            set;
        }
        public String IdOrdenadorGasto
        {
            get;
            set;
        }
        public String CargoOrdenadorGasto
        {
            get;
            set;
        }
        public String Prorroga1Fecha
        {
            get;
            set;
        }
        public String Prorroga1Plazo
        {
            get;
            set;
        }
        public String Prorroga2Fecha
        {
            get;
            set;
        }
        public String Prorroga2Plazo
        {
            get;
            set;
        }
        public String Prorroga3Fecha
        {
            get;
            set;
        }
        public String Prorroga3Plazo
        {
            get;
            set;
        }
        public String Prorroga4Fecha
        {
            get;
            set;
        }
        public String Prorroga4Plazo
        {
            get;
            set;
        }
        public String Prorroga5Fecha
        {
            get;
            set;
        }
        public String Prorroga5Plazo
        {
            get;
            set;
        }
        public String ProrrogaTotalDias
        {
            get;
            set;
        }
        public String DisminucionDias
        {
            get;
            set;
        }
        public String FechaTerminacionAnticipada
        {
            get;
            set;
        }
        public String PlazoTotalContrato
        {
            get;
            set;
        }
        public String FechaTerminacionFinal
        {
            get;
            set;
        }
        public String Adicion1FechaSuscripcion
        {
            get;
            set;
        }
        public String Adicion1RP
        {
            get;
            set;
        }
        public String Adicion1Valor
        {
            get;
            set;
        }
        public String Adicion2FechaSuscripcion
        {
            get;
            set;
        }
        public String Adicion2RP
        {
            get;
            set;
        }
        public String Adicion2Valor
        {
            get;
            set;
        }
        public String Adicion3FechaSuscripcion
        {
            get;
            set;
        }
        public String Adicion3RP
        {
            get;
            set;
        }
        public String Adicion3Valor
        {
            get;
            set;
        }
        public String Adicion4FechaSuscripcion
        {
            get;
            set;
        }
        public String Adicion4RP
        {
            get;
            set;
        }
        public String Adicion4Valor
        {
            get;
            set;
        }
        public String Adicion5FechaSuscripcion
        {
            get;
            set;
        }
        public String Adicion5RP
        {
            get;
            set;
        }
        public String Adicion5Valor
        {
            get;
            set;
        }
        public String ValorTotalAdicion
        {
            get;
            set;
        }
        public String DisminucionValor
        {
            get;
            set;
        }
        public String ValorFinalContrato
        {
            get;
            set;
        }
        public String ValorFinalContratoCofinanciacion
        {
            get;
            set;
        }
        public String DesembolsoEfectuado
        {
            get;
            set;
        }
        public String PorcentajeAvanceFisico
        {
            get;
            set;
        }
        public String PorcentajeAvanceFisicoReal
        {
            get;
            set;
        }
        public String PorcentajeAvancePresupuestado
        {
            get;
            set;
        }
        public String PorcentajeAvancePresupuestadoReal
        {
            get;
            set;
        }
        public String FechaLiquidacion
        {
            get;
            set;
        }
        public String EstadoContrato
        {
            get;
            set;
        }
        public String Cesionario1Identificacion
        {
            get;
            set;
        }
        public String Cesionario1TipoIdentificacion
        {
            get;
            set;
        }
        public String Cesionario1NombreRazonSocial
        {
            get;
            set;
        }
        public String Cesionario2Identificacion
        {
            get;
            set;
        }
        public String Cesionario2TipoIdentificacion
        {
            get;
            set;
        }
        public String Cesionario2NombreRazonSocial
        {
            get;
            set;
        }
        public String Cesionario3Identificacion
        {
            get;
            set;
        }
        public String Cesionario3TipoIdentificacion
        {
            get;
            set;
        }
        public String Cesionario3NombreRazonSocial
        {
            get;
            set;
        }
        public String Cesionario4Identificacion
        {
            get;
            set;
        }
        public String Cesionario4TipoIdentificacion
        {
            get;
            set;
        }
        public String Cesionario4NombreRazonSocial
        {
            get;
            set;
        }
        public String Cesionario5Identificacion
        {
            get;
            set;
        }
        public String Cesionario5TipoIdentificacion
        {
            get;
            set;
        }
        public String Cesionario5NombreRazonSocial
        {
            get;
            set;
        }
        public String FechaInicioSuspencion
        {
            get;
            set;
        }
        public String FechaTerminacionSuspension
        {
            get;
            set;
        }
        public String Observaciones
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public String FechaCrea
        {
            get;
            set;
        }
        public String FechaModifica
        {
            get;
            set;
        }
        public FUC()
        {
        }
    }
}
