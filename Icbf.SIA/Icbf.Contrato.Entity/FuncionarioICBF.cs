using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para el registro funcionarios ICBF
    /// </summary>
    public class FuncionarioICBF : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdFuncionario
        {
            get;
            set;
        }
        public int? IdDependencia
        {
            get;
            set;
        }
        public int IdRegionalPCI
        {
            get;
            set;
        }

        public String NombreRegional
        {
            get;
            set;
        }

        public int? IdTipoVinculacon
        {
            get;
            set;
        }
        public int? IdTercero
        {
            get;
            set;
        }
        public int IdUsuario
        {
            get;
            set;
        }
        public String NumeroIdentificacion
        {
            get;
            set;
        }
        public String PrimerNombre
        {
            get;
            set;
        }
        public String SegundoNombre
        {
            get;
            set;
        }
        public String PrimerApellido
        {
            get;
            set;
        }
        public String SegundoApellido
        {
            get;
            set;
        }
        public String Email
        {
            get;
            set;
        }
        public String Resolucion
        {
            get;
            set;
        }
        public String NoContarto
        {
            get;
            set;
        }
        public String NoTelefonoCelular
        {
            get;
            set;
        }
        public String NoTelefonoFijo
        {
            get;
            set;
        }
        public Boolean Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public FuncionarioICBF()
        {
        }
    }
}
