﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class GarantiaModificacionContrato : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdContrato { get; set; }
        public string NumeroContrato { get; set; }
        public int VigenciaFiscalInicial { get; set; }
        public int VigenciaFiscalFinal { get; set; }
        public string NombreRegional { get; set; }
        public string NombreTipoContrato { get; set; }
        public string CategoriaContrato { get; set; }
    }

    public class GarantiaHistorico : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IDGarantiaHistorico { get; set; }
        public int IDGarantia
        {
            get;
            set;
        }
        public int IDTipoGarantia
        {
            get;
            set;
        }
        public String NombreTipoGarantia
        {
            get;
            set;
        }
        public String NumeroGarantia
        {
            get;
            set;
        }
        public DateTime? FechaAprobacionGarantia
        {
            get;
            set;
        }
        public DateTime? FechaCertificacionGarantia
        {
            get;
            set;
        }
        public DateTime? FechaDevolucion
        {
            get;
            set;
        }
        public String MotivoDevolucion
        {
            get;
            set;
        }
        public int IDUsuarioAprobacion
        {
            get;
            set;
        }
        public int IDUsuarioDevolucion
        {
            get;
            set;
        }
        public int IdContrato
        {
            get;
            set;
        }
        public Boolean BeneficiarioICBF
        {
            get;
            set;
        }
        public String DescripcionBeneficiarios
        {
            get;
            set;
        }
        public DateTime? FechaInicioGarantia
        {
            get;
            set;
        }
        public DateTime? FechaExpedicionGarantia
        {
            get;
            set;
        }
        public DateTime? FechaVencimientoInicialGarantia
        {
            get;
            set;
        }
        public DateTime? FechaVencimientoFinalGarantia
        {
            get;
            set;
        }
        public DateTime? FechaReciboGarantia
        {
            get;
            set;
        }
        public String ValorGarantia
        {
            get;
            set;
        }
        public Boolean? Anexos
        {
            get;
            set;
        }
        public String ObservacionesAnexos
        {
            get;
            set;
        }
        public int EntidadProvOferenteAseguradora
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public Boolean Estado
        {
            get;
            set;
        }
        public int IDSucursalAseguradoraContrato
        {
            get;
            set;
        }
        public int IDEstadosGarantias
        {
            get;
            set;
        }
        public String CodigoEstadoGarantia
        {
            get;
            set;
        }
        public String DescripcionEstadoGarantia
        {
            get;
            set;
        }
        public String CodigoDescripcionEstadoGarantia
        {
            get;
            set;
        }
        public String Zona
        {
            get;
            set;
        }
        public Boolean BeneficiarioOTROS { get; set; }
        public string TipoModificacionContractual { get; set; }
        public DateTime? FechaModificacionContractual { get; set; }
        public GarantiaHistorico()
        {
        }
    }
}

