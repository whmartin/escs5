using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class Suspensiones : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdSuspension
        {
            get;
            set;
        }
        public int IDContrato
        {
            get;
            set;
        }
        public int IDCosModContractual
        {
            get;
            set;
        }
        public DateTime FechaInicio
        {
            get;
            set;
        }
        public string FechaInicioView
        {
            get
            {
                string result = string.Empty;
                if (FechaInicio != DateTime.MinValue)
                    result = FechaInicio.Date.ToShortDateString();
                return result;
            }
        }
        public DateTime ReinicioContrato
        {
            get;
            set;
        }
        public DateTime FechaFin
        {
            get;
            set;
        }
        public string FechaFinView
        {
            get
            {
                string result = string.Empty;
                if (FechaFin != DateTime.MinValue)
                    result = FechaFin.Date.ToShortDateString();
                return result;
            }
        }
        public int IDDetalleConsModContractual
        {
            get;
            set;
        }
        public int Suspensionyear
        {
            get;
            set;
        }
        public int SuspensionMeses
        {
            get;
            set;
        }
        public int SuspensionDias
        {
            get;
            set;
        }
        public int ValorConmutado
        {
            get;
            set;
        }

        public Decimal ValorSuspencion
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set; 
        }
        public DateTime FechaSolicitud
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public Suspensiones()
        {
        }
    }
}
