using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class Cesiones : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdCesion
        {
            get;
            set;
        }
        public DateTime FechaCesion
        {
            get;
            set;
        }
        public String Justificacion
        {
            get;
            set;
        }
        public int Estado
        {
            get;
            set;
        }
        public int IDDetalleConsModContractual
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public int IdProveedoresContratos
        {
            get;
            set;
        }
        public int IdEntidad
        {
            get;
            set;
        }
        public int IdTercero
        {
            get;
            set;
        }
        public String Proveedor
        {
            get;
            set;
        }
        public String TipoIdentificacion
        {
            get;
            set;
        }
        public String NumeroIdentificacion
        {
            get;
            set;
        }
        public String TipoPersonaNombre
        {
            get;
            set;
        }
        public String TipoDocumentoRepresentanteLegal
        {
            get;
            set;
        }
        public String NumeroIDRepresentanteLegal
        {
            get;
            set;
        }
        public String RepresentanteLegal
        {
            get;
            set;
        }
        public String PorcentajeParticipacion
        {
            get;
            set;
        }
        public String NumeroIdentificacionIntegrante
        {
            get;
            set;
        }
        public decimal ValorEjecutado { get; set; }
        public decimal ValorCesion { get; set; }

        public int IdRP { get; set; }
        public DateTime FechaSuscripcion { get; set; }

        public int IDCosModContractual { get; set; }
        public Cesiones()
        {
        }
    }
}
