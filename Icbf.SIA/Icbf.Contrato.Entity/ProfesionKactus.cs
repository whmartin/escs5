﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para Profesiones Kactus(Gestion humana)
    /// </summary>
    public class ProfesionKactus
    {
        /// <summary>
        /// Propiedad Id de la profesion de Kactus
        /// </summary>
        public int Id
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Codigo de la profesion de Kactus
        /// </summary>
        public string Codigo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Descripcion de la profesion de Kactus
        /// </summary>
        public string Descripcion
        {
            get;
            set;
        }
        public ProfesionKactus()
        { 
        
        }
    }
}
