﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class PlanDeComprasContratos : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IDPlanDeComprasContratos
        {
            get;
            set;
        }
        public int NumeroConsecutivoPlanCompras
        {
            get;
            set;
        }
        public int IdContrato
        {
            get;
            set;
        }
        public int IDPlanDeCompras
        {
            get;
            set;
        }
        public int Vigencia
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public decimal ? ValorTotal { get; set; }
        public int IdAdicion { get; set; }
        public bool EsAdicion { get; set; }
        public String CodigoRegional { get; set; }
        public PlanDeComprasContratos()
        {
        }
    }

    public class PlanComprasMigracion
    {
        public int Consecutivo { get; set; }
        public int Vigencia { get; set; }
        public decimal ValorTotal { get; set; }
        public DataTable Productos { get; set; }
        public DataTable Rubros { get; set; }
    }
}
