using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para gestión de obligación
    /// </summary>
    public class GestionarObligacion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdGestionObligacion
        /// </summary>
        public int IdGestionObligacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdGestionClausula
        /// </summary>
        public int IdGestionClausula
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreObligacion
        /// </summary>
        public String NombreObligacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoObligacion
        /// </summary>
        public int IdTipoObligacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Orden
        /// </summary>
        public String Orden
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DescripcionObligacion
        /// </summary>
        public String DescripcionObligacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdObligacion
        /// </summary>
        public int IdObligacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreTipoObligacion
        /// </summary>
        public String NombreTipoObligacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Descripcion
        /// </summary>
        public String Descripcion
        {
            get;
            set;
        }
        public GestionarObligacion()
        {
        }
    }
}
