﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class ContratosCDPDTO
    {
        /// <summary> 
        /// Propiedad IdContratosCDP
        /// </summary>
        public int IdContratosCDP
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdCDP
        /// </summary>
        public int IdCDP
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdContrato
        /// </summary>
        public int IdContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Regional
        /// </summary>
        public String Regional
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Area
        /// </summary>
        public String Area
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NumeroCDP
        /// </summary>
        public String NumeroCDP
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCDP
        /// </summary>
        public DateTime? FechaCDP
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ValorCDP
        /// </summary>
        public Decimal? ValorCDP
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoRubro
        /// </summary>
        public String CodigoRubro
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad RubroPresupuestal
        /// </summary>
        public String RubroPresupuestal
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad TipoFuenteFinanciamiento
        /// </summary>
        public String TipoFuenteFinanciamiento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad RecursoPresupuestal
        /// </summary>
        public String RecursoPresupuestal
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DependenciaAfectacionGastos
        /// </summary>
        public String DependenciaAfectacionGastos
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad TipoDocumentoSoporte
        /// </summary>
        public String TipoDocumentoSoporte
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad TipoSituacionFondos
        /// </summary>
        public String TipoSituacionFondos
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DescripcionRubro
        /// </summary>
        public String DescripcionRubro
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ConsecutivoPlanCompras
        /// </summary>
        public int? ConsecutivoPlanCompras
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        public int IdAdicion { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool EsAdicion { get; set; }
        /// <summary>
        /// Propiedad ValorCDP
        /// </summary>
        public Decimal? ValorRubro
        {
            get;
            set;
        }
    }
}
