using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class AmparosGarantias : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IDAmparosGarantias
        {
            get;
            set;
        }
        public int IDGarantia
        {
            get;
            set;
        }
        public int IdTipoAmparo
        {
            get;
            set;
        }
        public DateTime FechaVigenciaDesde
        {
            get;
            set;
        }
        public DateTime FechaVigenciaHasta
        {
            get;
            set;
        }
        public int IDUnidadCalculo
        {
            get;
            set;
        }
        public Decimal ValorCalculoAsegurado
        {
            get;
            set;
        }
        public Decimal ValorAsegurado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }

        public string valorSIRECI { get; set; }

        public string NombreTipoAmparo { get; set; }
        public string NombreUnidadCalculo { get; set; }

        public AmparosGarantias()
        {
        }
    }
}
