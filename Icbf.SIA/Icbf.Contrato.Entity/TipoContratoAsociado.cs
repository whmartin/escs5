﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para contratos
    /// </summary>
    public class TipoContratoAsociado : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdTipoContratoAsociado
        /// </summary>
        public int IdContratoAsociado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodContratoAsociado
        /// </summary>
        public String CodContratoAsociado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Descripcion
        /// </summary>
        public String Descripcion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Inactivo
        /// </summary>
        public bool Inactivo
        {
            get;
            set;
        }
    }
}
