using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para información Presupuestal RP
    /// </summary>
    public class InformacionPresupuestalRP : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdInformacionPresupuestalRP
        /// </summary>
        public int IdInformacionPresupuestalRP
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaSolicitudRP
        /// </summary>
        public DateTime FechaSolicitudRP
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NumeroRP
        /// </summary>
        public String NumeroRP
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ValorRP
        /// </summary>
        public Decimal ValorRP
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaExpedicionRP
        /// </summary>
        public DateTime FechaExpedicionRP
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public InformacionPresupuestalRP()
        {
        }
    }
}
