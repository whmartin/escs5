﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class PlanDeCompras : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdPlanDeCompras
        {
            get;
            set;
        }
        public int NumeroConsecutivo
        {
            get;
            set;
        }
        public int IdContrato
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public PlanDeCompras()
        {
        }
    }
}

