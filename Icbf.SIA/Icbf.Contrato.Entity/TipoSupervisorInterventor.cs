﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class TipoSupervisorInterventor : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IDSupervisorInterventor
        {
            get;
            set;
        }
        public String Codigo
        {
            get;
            set;
        }
        public String Descripcion
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public TipoSupervisorInterventor()
        {
        }
    }
}

