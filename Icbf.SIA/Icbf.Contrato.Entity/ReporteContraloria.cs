﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class ReporteContraloria
    {
        public int IdContrato { get; set; }
        public int CantidadVeces { get; set; }
        public string NumeroContrato { get; set; }
        public string NombreRegional { get; set; }
        public string CodigoRegional { get; set; }
        public DateTime FechaSuscripcion { get; set; }
        public DateTime FechaInicioContrato { get; set; }
        public DateTime FechaFinalizacion { get; set; }
        public DateTime FechaLiquidacion { get; set; }
        public string ObjetoContrato { get; set; }
        public string ModalidadSeleccion { get; set; }
        public string ClaseContrato { get; set; }
        public string ClaseContratoOriginal { get; set; }
        public string CodigoSecop { get; set; }
        public decimal ValorInicialDinero { get; set; }
        public decimal ValorInicial { get; set; }
        public int DiasTotales { get; set; }
        public int DiasProrrogados { get; set; }
        public decimal ValorAdicionado { get; set; }
        public string EsContrato { get; set; }
        public string Naturaleza { get; set; }
        public string TipoIdentificacion { get; set; }
        public string NoIdentificacionContratista { get; set; }
        public string NombresRazonSocialContratista { get; set; }
        public string FormaPago { get; set; }
        public decimal PorcentajeAnticipo { get; set; }
        public string IdentificacionSupervisor { get; set; }
        public string NombresSupervisor { get; set; }
        public string TipoIdentificacionSupervisor { get; set; }
        public string TipoIdentificacionInterventor { get; set; }
        public string IdentificacionInterventor { get; set; }
        public string NombreInterventor { get; set; }
        public DateTime ? FechaTerminacionAnticipada { get; set; }
        public decimal ValorFinal { get; set; }
        public decimal ValorProgramado { get; set; }
    }

    public class ReporteContraloriaUnionesConsorcios
    {
        public int IdContrato { get; set; }
        public string NumeroContrato { get; set; }
        public string NombreRegional { get; set; }
        public string CodigoRegional { get; set; }
        public DateTime FechaSuscripcion { get; set; }

        public string CodigoNaturaleza { get; set; }
        public string Naturaleza { get; set; }
        public string NoIdentificacionContratista { get; set; }
        public string NombresRazonSocialContratista { get; set; }

        public string NaturalezaIntegrante { get; set; }
        public string TipoIdentificacionIntegrante { get; set; }
        public string CodigoIdentificacionIntegrante { get; set; }
        public string NoIdentificacionIntegrante { get; set; }
        public string NombresRazonSocialIntegrante { get; set; }        
    }

    public enum ReporteContraloraTrimestre
    {
        Primero = 1,
        Segundo = 2,
        Tercero = 3,
        Cuarto = 4
    }
}
