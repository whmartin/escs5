﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class SupervisorInterContrato : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IDSupervisorIntervContrato
        {
            get;
            set;
        }
        public int IDSupervisorAnterior { get; set; }

        public DateTime FechaInicio
        {
            get;
            set;
        }

        public string FechaInicioString
        {
            get 
            {
                string result = string.Empty;

                if(FechaInicio != new DateTime(1900,1,1))
                return FechaInicio.ToShortDateString(); 

                return result;
            }
        }

        public DateTime FechaFin
        {
            get;
            set;
        }

        public string FechaFinString
        {
            get { return FechaFin.ToShortDateString(); }
        }

        public Boolean Inactivo
        {
            get;
            set;
        } 
        public String Identificacion
        {
            get;
            set;
        }
        public String NombreCompletoSuperInterventor
        {
            get;
            set;
        }
        public String TipoVinculacionContractual
        {
            get;
            set;
        }
        public String NombreCompleto
        {
            get;
            set;
        } 
        public String TipoIdentificacion
        {
            get;
            set;
        }
        public int? IDTipoSuperInter
        {
            get;
            set;
        }
        public int? IdNumeroContratoInterventoria
        {
            get;
            set;
        }
        public String NumeroContratoInterventoria
        {
            get;
            set;
        }
        public int? IDProveedoresInterventor
        {
            get;
            set;
        }
        public int? IDEmpleadosSupervisor
        {
            get;
            set;
        }
        public int? IdContrato
        {
            get;
            set;
        }
        public int? IDDirectorInterventoria
        {
            get;
            set;
        }
        public Supervisor SupervisorInterventor
        {
            get;
            set;
        }
        public String EtQSupervisorInterventor
        {
            get;
            set;
        }
        public String EtQInternoExterno
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }

        public int IdTipoPersona { get; set; }
        public string TipoPersona { get; set; }
        public int? IdRol { get; set; }
        public string NombreRol { get; set; }

        public SupervisorInterContrato()
        {
            SupervisorInterventor = new Supervisor();
        }
    }
}

