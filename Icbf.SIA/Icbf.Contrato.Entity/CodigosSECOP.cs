﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class CodigosSECOP : Icbf.Seguridad.Entity.EntityAuditoria
    {

        public int IdCodigoSECOP
        {
            get;
            set;
        }

        public Boolean Estado
        {
            get;
            set;
        }
        public String CodigoSECOP
        {
            get;
            set;
        }

        public String Descripcion
        {
            get;
            set;
        }

        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
    }
}
