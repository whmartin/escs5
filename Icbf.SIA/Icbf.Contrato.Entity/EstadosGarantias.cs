using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class EstadosGarantias : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IDEstadosGarantias
        {
            get;
            set;
        }
        public String CodigoEstadoGarantia
        {
            get;
            set;
        }
        public String DescripcionEstadoGarantia
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public EstadosGarantias()
        {
        }
    }
}
