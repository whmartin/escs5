﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class InfoContratoPaccoDTO
    {
       public int IdContrato { get; set; }
       public int Consecutivo { get; set; }  
       public int Vigencia { get; set; }  
       public string CodRegional { get; set; }
       public string NombreRegional { get; set; }
       public string EstadoContrato { get; set; }
       public string NumeroDocumento { get; set; }
       public string TipoDocumento { get; set; }
       public DateTime FechaContrato { get; set; }
       public decimal ValorInicialContrato { get; set; }
       public decimal ValorFinalContrato { get; set; }
       public decimal ValorContrato { get; set; }
       public string IdModalidad { get; set; }
       public string NombreModalidad { get; set; }
       public string TipoPersona { get; set; }
       public string Tercero { get; set; }
       public string TerceroDescripcion { get; set; }
       public string TipoContrato { get; set; }
       public DateTime FechaFinalizacionInicialContrato { get; set; }
       public DateTime FechaFinalTerminacionContrato { get; set; }
       public List<InfoContratoCDPDTO> CDP { get; set; }
       public List<InfoContratoRPDTO>  RP { get; set; }
       public List<InfoContratoVFDTO> VigenciasFuturas { get; set; }
    }

    public class InfoContratoDTO
    {
        public int IdContrato { get; set; }
        public string ConsecutivoContrato { get; set; }
        public DateTime FechaSuscripcionContrato { get; set; }
        public int ConsecutivoPlanCompras { get; set; }
        public int VigenciaInicial { get; set; }
        public int VigenciaFinal { get; set; }
        public string CodRegional { get; set; }
        public string NombreRegional { get; set; }
        public string EstadoContrato { get; set; }
        public decimal ValorInicialContrato { get; set; }
        public decimal ValorFinalContrato { get; set; }
        public string CodigoModalidad { get; set; }
        public string NombreModalidad { get; set; }
        public string CodigoTipoContrato { get; set; }
        public string NombreTipoContrato { get; set; }
        public string CodigoCategoriaContrato { get; set; }
        public string NombreCategoriaContrato { get; set; }
        public DateTime FechaInicioEjecucion { get; set; }
        public DateTime FechaFinalizacionInicialContrato { get; set; }
        public DateTime FechaFinalTerminacionContrato { get; set; }
        public string PlazoEjecuccion { get; set; }
        public List<InfoContratoCDPDTO> CDP { get; set; }
        public List<InfoContratoRPDTO> RP { get; set; }
        public List<InfoContratoVFDTO> VigenciasFuturas { get; set; }
        public List<InfoRubroPlanComprasDTO> RubrosPlanCompras { get; set; }
        public string RubrosPlanComprasConcatenados { get; set; }
        public List<InfoProductoPlanComprasDTO> ProductosPlanCompras { get; set; }
        public List<InfoProveedoresDTO> Contratistas { get; set; }
        public List<InfoSupervisorDTO> Supervisores { get; set; }
        public bool EsLugarEjeccucionNacional { get; set; }
        public List<InfoLugarEjeccuionDTO> LugarEjeccuion { get; set; }
    }

    public class InfoContratoVFDTO
    {
        public string Numero { get; set; }
        public int Vigencia { get; set; }
        public decimal Valor { get; set; }
    }

    public class InfoLugarEjeccuionDTO
    {
        public String Municipio { get; set; }
        public string CodigoMunicipio { get; set; }
        public string Departamento { get; set; }
        public string CodigoDepartamento { get; set; }
    }

    public class InfoContratoCDPDTO
    {
        public string CDP { get; set; }
        public DateTime? Fecha { get; set; }
    }

    public class InfoContratoRPDTO
    {
        public string RP { get; set; }
        public DateTime? Fecha { get; set; }
    }

    public class InfoRubroPlanComprasDTO
    {
        public string Rubro { get; set; }
        public decimal ? Valor { get; set; }
    }

    public class InfoProductoPlanComprasDTO
    {
        public string IdProducto { get; set; }
        public decimal ? Tiempo { get; set; }
        public decimal ? Valor { get; set; }
        public decimal ? Cantidad { get; set; }
    }

    public class InfoProveedoresDTO
    {
        public string NumeroDocumento { get; set; }
        public string TipoDocumento { get; set; }
        public string TipoPersona { get; set; }
        public string Tercero { get; set; }
        public string TerceroDescripcion { get; set; }
        public string NombresRLegal { get; set; }
        public string NumeroRLegal { get; set; }
        public string TipoDocumentoRLegal { get; set; }
    }

    public class InfoSupervisorDTO
    {
        public String Nombres { get; set; }
        public string TipoIdentificacion { get; set; }
        public string Identificacion { get; set; }
    }
}
