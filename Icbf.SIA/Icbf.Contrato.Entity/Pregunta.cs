using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class Pregunta : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdPregunta
        {
            get;
            set;
        }
        public String DescripcionPregunta
        {
            get;
            set;
        }
        public int IdTipoContrato
        {
            get;
            set;
        }
        public int IdComponente
        {
            get;
            set;
        }
        public int IdSubComponente
        {
            get;
            set;
        }
        public int IdCategoriaContrato
        {
            get;
            set;
        }
        public int RequiereDocumento
        {
            get;
            set;
        }
        public int Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public Pregunta()
        {
        }
    }
}
