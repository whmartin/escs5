using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para Tipo Amparo
    /// </summary>
    public class TipoAmparo : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdTipoAmparo
        /// </summary>
        public int IdTipoAmparo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreTipoAmparo
        /// </summary>
        public String NombreTipoAmparo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoGarantia
        /// </summary>
        public int IdTipoGarantia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }

        public TipoAmparo()
        {
        }
    }
}
