using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para Relaci�n de supervisor / interventor
    /// </summary>
    public class RelacionarSupervisorInterventor : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IDSupervisorInterv
        /// </summary>
        public int IDSupervisorInterv
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDContratoSupervisa
        /// </summary>
        public int IDContratoSupervisa
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad OrigenTipoSupervisor
        /// </summary>
        public bool OrigenTipoSupervisor
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDTipoSupvInterventor
        /// </summary>
        public int IDTipoSupvInterventor
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDTerceroExterno
        /// </summary>
        public int? IDTerceroExterno
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDFuncionarioInterno
        /// </summary>
        public int? IDFuncionarioInterno
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDContratoInterventoria
        /// </summary>
        public int? IDContratoInterventoria
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad TipoVinculacion
        /// </summary>
        public bool TipoVinculacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaInicia
        /// </summary>
        public DateTime FechaInicia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaFinaliza
        /// </summary>
        public DateTime FechaFinaliza
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public bool Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModificaInterventor
        /// </summary>
        public DateTime FechaModificaInterventor
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public string UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public string UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDDirectorInterventoria
        /// </summary>
        public int? IDDirectorInterventoria { get;set;}
       
        public RelacionarSupervisorInterventor()
        {
        }
    }
}
