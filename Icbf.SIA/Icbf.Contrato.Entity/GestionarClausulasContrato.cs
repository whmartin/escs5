using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para Gesti�n de cl�usulas de contrato
    /// </summary>
    public class GestionarClausulasContrato : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdGestionClausula
        /// </summary>
        public int IdGestionClausula
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdContrato
        /// </summary>
        public String IdContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreClausula
        /// </summary>
        public String NombreClausula
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad TipoClausula
        /// </summary>
        public int TipoClausula
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Orden
        /// </summary>
        public String Orden
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DescripcionClausula
        /// </summary>
        public String DescripcionClausula
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad OrdenNumero
        /// </summary>
        public int OrdenNumero
        {
            get;
            set;
        }
        public GestionarClausulasContrato()
        {
        }
    }
}
