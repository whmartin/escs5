using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class Componentes : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdComponente
        {
            get;
            set;
        }

        public String Nombre
        {
            get;
            set;
        }
        public int Estado
        {
            get;
            set;
        }
        public Componentes()
        {
        }
    }
}
