using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class ContratistaGarantias : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IDContratista_Garantias
        {
            get;
            set;
        }
        public int IDGarantia
        {
            get;
            set;
        }
        public int IDEntidadProvOferente
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }

        public string TipoPersonaNombre { get; set; }
        public string TipoIdentificacion { get; set; }
        public string NumeroIdentificacion { get; set; }
        public string Proveedor { get; set; }

        public ContratistaGarantias()
        {
        }
    }
}
