using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para  Tipo contrato
    /// </summary>
    public class TipoContrato : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdTipoContrato
        /// </summary>
        public int IdTipoContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoTipoContrato
        /// </summary>
        public String CodigoTipoContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreTipoContrato
        /// </summary>
        public String NombreTipoContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdCategoriaContrato
        /// </summary>
        public int IdCategoriaContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ActaInicio
        /// </summary>
        public Boolean ActaInicio
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad AporteCofinaciacion
        /// </summary>
        public Boolean AporteCofinaciacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad RecursoFinanciero
        /// </summary>
        public Boolean RecursoFinanciero
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad RegimenContrato
        /// </summary>
        public int RegimenContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DescripcionTipoContrato
        /// </summary>
        public String DescripcionTipoContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }

        public TipoContrato()
        {
        }
    }
}
