using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para Información Presupuestal
    /// </summary>
    public class InformacionPresupuestal : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdInformacionPresupuestal
        /// </summary>
        public int IdInformacionPresupuestal
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NumeroCDP
        /// </summary>
        public String NumeroCDP
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ValorCDP
        /// </summary>
        public Decimal ValorCDP
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaExpedicionCDP
        /// </summary>
        public DateTime FechaExpedicionCDP
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public InformacionPresupuestal()
        {
        }
    }
}
