﻿using Icbf.Seguridad.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class LiquidacionContrato : EntityAuditoria
    {
        public int IdLiquidacionContrato { get; set; }
        public Int64 IdDetConsModContractual { get; set; }
        public DateTime FechaRaquidacion { get; set; }
        public string FechaRadicacionView
        {
            get
            {
                string result = string.Empty;
                if(FechaRaquidacion != null)
                result = FechaRaquidacion.ToShortDateString();
                return result;
            }
        }
        public string NombreSupervisor { get; set; }
        public string DependenciaSueprvisor { get; set; }
        public string CargoSupervisor { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
        public string TipodeLiquidacion { get; set; }
        public decimal ValorEjecutado { get; set; }
        public decimal ValorLiberado { get; set; }
        public string JustificacionLiberacion { get; set; }
    }
}
