﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    /// 
    public class CargueContratoMasivo
    {
        public int IdCargueContratoMasivo { get; set; }
        public string Usuario { get; set; }
        public DateTime Fecha { get; set; }
        public string Descripcion { get; set; }
        public int TotalRegistros { get; set; }
        public int RegistrosCargados { get; set; }
        public int RegistrosErrados { get; set; }
        public int IdRegional { get; set; }
        public List<itemCargueError> ListaRegistroErrados { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class itemCargueError
    {
        public int Fila { get; set; }
        public string Regional { get; set; }
        public string CodigoRegional { get; set; }
        public string NumeroContrato { get; set; }
        public string Observaciones { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class itemCargueArchivo
    {
        public string CodigoRegional { get; set; }
        public string ConsecutivoContrato { get; set; }
        public DateTime FechaSuscripcion { get; set; }
        public string CategoriaContrato { get; set; }
        public string ModalidadSeleccion { get; set; }
        public string NumeroProceso { get; set; }
        public DateTime ? FechaAdjudicacionProceso { get; set; }
        public string TipoContrato { get; set; }
        public string TipoPago { get; set; }
        public string CedulaSolicitante { get; set; }
        public string AreaSolicitante { get; set; }
        public string TipoIdentificacionContratista { get; set; }
        public string NumeroIdentificacionContratista { get; set; }
        public string ModalidadAcademica { get; set; }
        public string Profesion { get; set; }
        public string AfectacionRecurso { get; set; }
        public string CodigoSecop { get; set; }
        public string ObjetoContrato { get; set; }
        public string LugarEjeccucion { get; set; }
        public DateTime FechaInicioEjecucion { get; set; }
        public DateTime FechaFinEjecuccion { get; set; }
        public int ConsecutivoPlanCompras { get; set; }
        public int CDP { get; set; }
        public int RP { get; set; }
        public decimal ValorIncialContrato { get; set; }
        public string Anticipo { get; set; }
        public decimal ? ValorAnticipo { get; set; }
        public string cofinaciacionEntidad1 { get; set; }
        public decimal ? ValorCofinanciacion1 { get; set; }
        public string cofinaciacionEntidad2 { get; set; }
        public decimal ? ValorCofinanciacion2 { get; set; }
        public string VigenciaFutura { get; set; }
        public decimal ? ValorVigenciaFutura { get; set; }
        public DateTime FechaGarantia { get; set; }
        public string TipoGarantia { get; set; }
        public string Aseguradora { get; set; }
        public string RiesgosGarantias { get; set; }
        public decimal ValorAseguradoGarantia { get; set; }
        public string PorcentajeGarantia { get; set; }
        public string AreaSupervisor { get; set; }
        public string CargoSupervisor { get; set; }
        public string IdentificacionSupervisor { get; set; }
        public string CargoOrdenador { get; set; }
        public string IdentificacionOrdenador { get; set; }
        public DateTime ? FechaProrroga1 { get; set; }
        public DateTime ? FechaProrroga2 { get; set; }
        public DateTime ? FechaProrroga3 { get; set; }
        public DateTime fechaTerminacionFinal { get; set; }
        public int ? RPAdicion1 { get; set; }
        public DateTime ? FechaAdicion1 { get; set; }
        public Decimal ? ValorAdicion1 { get; set; }
        public int? RPAdicion2 { get; set; }
        public DateTime? FechaAdicion2 { get; set; }
        public Decimal? ValorAdicion2 { get; set; }
        public int? RPAdicion3 { get; set; }
        public DateTime? FechaAdicion3 { get; set; }
        public Decimal? ValorAdicion3 { get; set; }
        public int? RPAdicion4 { get; set; }
        public DateTime? FechaAdicion4 { get; set; }
        public Decimal? ValorAdicion4 { get; set; }
        public decimal ? ValorDisminucion { get; set; }
        public decimal ValorFinalContrato { get; set; }
        public DateTime ? FechaLiquidacion { get; set; }
        public string Estado { get; set; }
        public int ? DiasTerminacionAnticipada { get; set; }
        public int Fila { get; set; }
        public string UsuarioCrea { get; set; }
    }
}
