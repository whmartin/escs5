using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class DetalleConsModContractual : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IDDetalleConsModContractual
        {
            get;
            set;
        }
        public int IDTipoModificacionContractual
        {
            get;
            set;
        }
        public int IDCosModContractual
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public DetalleConsModContractual()
        {
        }
    }
}
