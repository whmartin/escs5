using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para Relaci�n de contratistas
    /// </summary>
    public class RelacionarContratistas : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdContratistaContrato
        /// </summary>
        public int IdContratistaContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdContrato
        /// </summary>
        public int IdContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NumeroIdentificacion
        /// </summary>
        public long NumeroIdentificacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ClaseEntidad
        /// </summary>
        public String ClaseEntidad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PorcentajeParticipacion
        /// </summary>
        public int PorcentajeParticipacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad EstadoIntegrante
        /// </summary>
        public Boolean EstadoIntegrante
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NumeroIdentificacionRepresentanteLegal
        /// </summary>
        public long NumeroIdentificacionRepresentanteLegal
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }

        public RelacionarContratistas()
        {
        }
    }
}
