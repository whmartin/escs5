using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class Adiciones : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdAdicion
        {
            get;
            set;
        }
        public  decimal ValorAdicion
        {
            get;
            set;
        }
        public DateTime FechaSubscripcion { get; set; }
        public String JustificacionAdicion
        {
            get;
            set;
        }
        public int Estado
        {
            get;
            set;
        }
        public DateTime FechaAdicion
        {
            get;
            set;
        }
        public int IDDetalleConsModContractual
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public bool SumaValor { get; set; }
        public string FechaAdicionView
        {   get 
            {
                string result = string.Empty;

                if (FechaAdicion != null)
                    result = FechaAdicion.Date.ToShortDateString();

                return result;
            } 
        }

        public string FechaSubscripcionView
        {
            get
            {
                string result = string.Empty;

                if (FechaSubscripcion != null)
                    result = FechaSubscripcion.Date.ToShortDateString();

                return result;
            }
        }
        public int IdCDP { get; set; }
        public string NumeroRP { get; set; }
        public decimal ValorCDP { get; set; }
        public decimal ValorRP { get; set; }
        public Adiciones()
        {
        }
    }
}
