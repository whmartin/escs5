using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class LugarEjecucionContrato : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdLugarEjecucionContratos
        {
            get;
            set;
        }
        public int IdContrato
        {
            get;
            set;
        }
        public int? IdDepartamento
        {
            get;
            set;
        }
        public String Departamento
        {
            get;
            set;
        }
        public string CodigoDepartamento { get; set; }
        public int? IdMunicipio
        {
            get;
            set;
        }
        public String Municipio
        {
            get;
            set;
        }
        public string CodigoMunicipio { get; set; }
        public int? IdRegional
        {
            get;
            set;
        }
        public string IdsRegionales
        {
            get;
            set;
        }
        public Boolean? NivelNacional
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public string DatosAdicionales { get; set; }

        public bool EsNivelNacional { get; set; }
        public LugarEjecucionContrato()
        {
        }

        public bool Historico { get; set; }
    }
}
