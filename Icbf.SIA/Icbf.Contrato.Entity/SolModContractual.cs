﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class SolModContractualContrato : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdContrato { get; set; } 
        public string NumeroContrato { get; set; }
        public int VigenciaFiscalInicial { get; set; }
        public int VigenciaFiscalFinal { get; set; }
        public string NombreRegional { get; set; }  
        public string  DependenciaSolicitante { get; set; }
        public string IdTipoDocumentoContratista { get; set; }
        public string IdentificacionContratista { get; set; }
        public string NombreContratista { get; set; }
        public string NombreTipoContrato { get; set; }
        public string CodDocumentoContratista { get; set; }
        public string TipoPersona { get; set; }
        public string Regional { get; set; }
        public string ModalidadSeleccion { get; set; }
        public string CategoriaContrato { get; set; }
        public string TipoContrato { get; set; }
    }

    public class SolModContractual : Icbf.Seguridad.Entity.EntityAuditoria
    {
       public int IDCosModContractual { get; set; } 
       public string NumeroDoc { get; set; }
       public string Justificacion { get; set; }
       public string JustificacionRechazo { get; set; }
       public string JustificacionDevolucion { get; set; }
       public int IdContrato { get; set; }
       public string NumeroContrato { get; set; }
       public string ConsecutivoSuscrito { get; set; }
       public bool Suscrito { get; set; }
       public string Estado { get; set; }
       public string UsuarioCrea {get; set;}
       public DateTime FechaCrea {get; set;}
       public string  UsuarioModifica {get; set;}
       public DateTime FechaModifica {get; set;}
       public string TipoModificacion { get; set; }
       public int IDTipoModificacionContractual { get; set; }
       public DateTime FechaDevolucion { get; set; }
       public string RazonDevolucion { get; set; }
       public int IdAdicion { get; set; }
       public int IdCesion { get; set; }
       public int IdProcesoImpMultas { get; set; }
       public DateTime FechaSuscripcion { get; set; }
       public string UsuarioAsignado { get; set; }

        public DateTime FechaSolicitud { get; set; }

        public string Codigo { get; set; }

    }

    public class SolModContractualDetalle : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IDDetalleConsModContractual { get; set; }
        public int IdConsModContractual { get; set; }
        public string TipoModificacion { get; set; }
        public DateTime Fecha { get; set; }
        public string FechaString 
        { 
            get
            {
                return Fecha.Year + "-" + Fecha.Month + "-" + Fecha.Day;
            }
        }
    }

    public class SolModContractualValidacion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public string Tipo { get; set; }
        public string Descripcion { get; set; }
    }

    public class SolModContractualHistorico
    {
        public int IdConsModContractualEstado { get; set; }
        public int IdConsModContractual { get; set; }
        public string Justificacion { get; set; }
        public String Estado { get; set; }
        public String UsuarioCrea { get; set; }
        public DateTime FechaCreacion { get; set; }
        public String FechaString { get { return FechaCreacion.Year + "-" + FechaCreacion.Month + "-" + FechaCreacion.Day; } }

    }
    public class HistoricoSolicitudesEliminadas : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public DateTime FechaEliminacion { get; set; }
        public string TipoModificacion { get; set; }
        public string NombreRegional { get; set; }
        public int IdRegional { get; set; }
        public string NumeroContrato { get; set; }
        public string JustificacionEliminacion { get; set; }
        public int NumeroSolicitud { get; set; }
        public int IdConsModContractualesEstado { get; set; }
        public String UsuarioCrea { get; set; }
        public int IdConsModContractual { get; set; }
        public int IdHistoricoSolicitudEliminada { get; set; }
        public int IdContrato { get; set; }
        public string NombreDependenciaSolicitante { get; set; }

    }

    public class HistoricoSolicitudesContrato : Icbf.Seguridad.Entity.EntityAuditoria
    {
       
        public string TipoModificacion { get; set; }           
        public int IdContrato { get; set; }
        public int NumeroModificacion { get; set; }
        public int IdCosModContractual { get; set; }
        public string  EstadoModificacion { get; set; }
        public DateTime? FechaSolicitud { get; set; }
        public DateTime? FechaSuscripcion { get; set; }
        public DateTime? FechaModificacion { get; set; }


    }
}
