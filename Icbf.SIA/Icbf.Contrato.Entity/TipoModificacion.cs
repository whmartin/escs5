using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class TipoModificacion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdTipoModificacionBasica
        {
            get;
            set;
        }
        public string Codigo { get; set; }
        public bool EsPorDefecto { get; set; }
        public String Descripcion
        {
            get;
            set;
        }
        public bool RequiereModificacion
        {
            get;
            set;
        }
        public bool Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }

        public bool GeneraNuevaGarantia { get; set; }

        public string EstadoToString
        {
            get
            {
                return Estado == true ? "Activo" : "Inactivo";
            }
        }

        public string RequiereModificacionToString
        {
            get
            {
                return RequiereModificacion == true ? "SI" : "NO";
            }
        }

        public string GeneraNuevaGarantiaToString
        {
            get
            {
                return GeneraNuevaGarantia == true ? "SI" : "NO";
            }
        }

        public string IdsAsociados { get; set; }

        public TipoModificacion()
        {
        }
    }
}
