using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Realiza el registro de la información del Registro Presupuestal relacionada a un contrato
    /// </summary>
    public class RPContrato : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdRPContrato
        {
            get;
            set;
        }
        public int IdContrato
        {
            get;
            set;
        }
        public int IdRP
        {
            get;
            set;
        }

        public Decimal ValorRP
        {
            get; set;
        }

        public Decimal ValorActualRP
        {
            get;
            set;
        }

        public Decimal ValorInicialRP
        {
            get;
            set;
        }

        public DateTime? FechaRP
        {
            get;
            set;
        }
        public DateTime? FechaExpedicionRP
        {
            get;
            set;
        }
        public int IdVigenciaFiscal
        {
            get;
            set;
        }

        public int VigenciaFiscal
        {
            get;
            set;
        }

        public int IdRegional
        {
            get;
            set;
        }

        public String CodigoRegional
        {
            get;
            set;
        }

        public String NombreRegional
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public string NumeroRP { get; set; }
        public RPContrato()
        {
        }
        public bool EsCesion { get; set; }
        public string EsCesionString 
        { 
            get
            {
                string result = "No";

                if (EsCesion)
                    result = "Si";

                return result;
            }
        }
        public DateTime? FechaFinalizacion { get; set; }
        public int IdVigenciaFutura { get; set; }       
        public bool EsVigenciaFutura { get; set; }

        public bool EsEnLinea { get; set; }
    }
}
