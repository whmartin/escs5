﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class RegistrosReporteFUC
    {
        public IEnumerable<ReporteFUC> misDatos { get; set; }

        public IEnumerable<ReporteFUC> ObtenerDatosReporte()
        {
            return misDatos;

        }
    }

    public class ReporteFUC
    {
        public int idContrato { get; set; }
        public int VIgenciaInicial { get; set; }
        public string Regional { get; set; }
        public string CodigoRegional { get; set; }
        public string NumeroPACCO { get; set; }
        public string Rubro { get; set; }
        public string NumeroContrato { get; set; }
        public string FechaSuscripcion { get; set; }
        public string ModalidadSeleccion { get; set; }
        public string CategoriaContato { get; set; }
        public string CodigoTipoPersona { get; set; }
        public string TipoContrato { get; set; }
        public string TipoIdentificacion { get; set; }
        public string NumeroIdentificacion { get; set; }
        public string DV { get; set; }
        public string NombreRazonSocial { get; set; }
        public string ProfesionContratista { get; set; }
        public string GeneroContratista { get; set; }
        public string DireccionContratista { get; set; }
        public string TelefonoContratista { get; set; }
        public string LugarUbicacionContratista { get; set; }
        public string TipoOrganizacion { get; set; }
        public string NombreRepresentantelegal { get; set; }
        public string IdentificacionRepresentantelegal { get; set; }

        public string AfectacionRecurso { get; set; }
        public string ObjetoContrato { get; set; }
        public string LugarEjecucionContrato { get; set; }
        public string FechaInicioContrato { get; set; }
        public string FechaTerminacionIncial { get; set; }
        public int DiasIniciales { get; set; }
        public string Anticipos { get; set; }
        public string PorcentajeAnticipo { get; set; }
        public string ValorAnticipo { get; set; }
        public string NumeroVigenciaFutura { get; set; }
        public string ValorVigenciaFutura { get; set; }

        public string SupervisorArea { get; set; }
        public string SupervisorNombreCompleto { get; set; }
        public string SupervisorRol { get; set; }
        public string SupervisorNumeroDocumentoIdentificacion { get; set; }

        public string OrdenadorGastoArea { get; set; }
        public string OrdenadorGastoNombreCompleto { get; set; }
        public string OrdenadorGastoDocumentoIdentificacion { get; set; }

        public string AportesDinero { get; set; }
        public string AportesEspecie { get; set; }

        public string Cofinanciador { get; set; }
        public string CofinanciadorIdentificacion { get; set; }
        public string CofinanciacionValor { get; set; }

        public string Cofinanciador1 { get; set; }
        public string CofinanciadorIdentificacion1 { get; set; }
        public string CofinanciacionValor1 { get; set; }

        public string Cofinanciador2 { get; set; }
        public string CofinanciadorIdentificacion2 { get; set; }
        public string CofinanciacionValor2 { get; set; }

        public string ValorTotalICBF { get; set; }
        public string ValorTotalCofinanciacion { get; set; }
        public string ValorTotalContratoImcial { get; set; }

        #region Consocio Unión Temporal

        public string IntegranteConsorcionUnionTemporalNombre { get; set; }
        public string IntegranteConsorcionUnionTemporalIdentificacion { get; set; }
        public string IntegranteConsorcionUnionTemporalDV { get; set; }
        public string IntegranteConsorcionUnionTemporalNombreRL { get; set; }
        public string IntegranteConsorcionUnionTemporalCedulaRL { get; set; }
        public string IntegranteConsorcionUnionTemporalPorcentaje { get; set; }

        public string IntegranteConsorcionUnionTemporalNombre1 { get; set; }
        public string IntegranteConsorcionUnionTemporalIdentificacion1 { get; set; }
        public string IntegranteConsorcionUnionTemporalDV1 { get; set; }
        public string IntegranteConsorcionUnionTemporalNombreRL1 { get; set; }
        public string IntegranteConsorcionUnionTemporalCedulaRL1 { get; set; }
        public string IntegranteConsorcionUnionTemporalPorcentaje1 { get; set; }

        public string IntegranteConsorcionUnionTemporalNombre2 { get; set; }
        public string IntegranteConsorcionUnionTemporalIdentificacion2 { get; set; }
        public string IntegranteConsorcionUnionTemporalDV2 { get; set; }
        public string IntegranteConsorcionUnionTemporalNombreRL2 { get; set; }
        public string IntegranteConsorcionUnionTemporalCedulaRL2 { get; set; }
        public string IntegranteConsorcionUnionTemporalPorcentaje2 { get; set; }

        #endregion

        # region  cdp

        public string NumeroCDP { get; set; }
        public string FechaCDP { get; set; }
        public string ValorCDP { get; set; }

        #endregion

        # region  rp

        public string NumeroRP { get; set; }
        public string FechaRP { get; set; }
        public string ValorRP { get; set; }
        #endregion

        #region Garantias

        public string FechaGarantia { get; set; }
        public string TipoGarantia { get; set; }
        public string EntidadAseguradora { get; set; }
        public string Porcentaje { get; set; }
        public string ValorAsegurado { get; set; }

        #endregion

        #region Prorrogas

        public string FechaProrroga { get; set; }
        public int PlazoProrroga { get; set; }
        public string FechaProrroga1 { get; set; }
        public int PlazoProrroga1 { get; set; }
        public string FechaProrroga2 { get; set; }
        public int PlazoProrroga2 { get; set; }

        public int TotalDiasProrroga { get; set; }

        #endregion

        public int DiasFinales { get; set; }

        public int DisminusionDiasTerminacionAnticipada { get; set; }

        public string FechaFinalTerminacion { get; set; }

        #region adiciones

        public string FechaAdicion { get; set; }
        public string NumeroRPAdicion { get; set; }
        public string ValorAdicion { get; set; }

        public string FechaAdicion1 { get; set; }
        public string NumeroRPAdicion1 { get; set; }
        public string ValorAdicion1 { get; set; }

        public string FechaAdicion2 { get; set; }
        public string NumeroRPAdicion2 { get; set; }
        public string ValorAdicion2 { get; set; }

        public string FechaAdicion3 { get; set; }
        public string NumeroRPAdicion3 { get; set; }
        public string ValorAdicion3 { get; set; }

        public string TotalAdicion { get; set; }
        public string ReduccionValor { get; set; }

        #endregion

        public string ValorTotalContrato { get; set; }

        public string EstadoContrato { get; set; }

        public string CodigoSECOP { get; set; }

        public string NumeroProceso { get; set; }

        public string FechaAdjudicacionProceso { get; set; }

        public string Riesgo1 { get; set; }
        public string RiesgoValor1 { get; set; }
        public string RiesgoPorcentaje { get; set; }

        public string UrlSECOP { get; set; }

        public string TieneCesion { get; set; }

        public string ContratistaCedente1 { get; set; }
        public string ContratistaCedente2 { get; set; }
        public string ContratistaCedente3 { get; set; }
        public string RPCesion1 { get; set; }

        public string RPCesion2 { get; set; }
        public string RPCesion3 { get; set; }

        public string FechaSuscripcionCesion1 { get; set; }

        public string FechaSuscripcionCesion2 { get; set; }
        public string FechaSuscripcionCesion3 { get; set; }
        public string NumeroIdentificacion1 { get; set; }
        public string NumeroIdentificacion2 { get; set; }
        public string NumeroIdentificacion3 { get; set; }

    }
}
