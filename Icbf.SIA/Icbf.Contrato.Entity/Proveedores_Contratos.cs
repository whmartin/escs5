﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class Proveedores_Contratos : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdProveedoresContratos
        {
            get;
            set;
        }
        public int IdProveedores
        {
            get;
            set;
        }
        public int? IdContrato
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad 
        /// </summary>
        public int IdEntidad
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad 
        /// </summary>
        public String ConsecutivoInterno
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad 
        /// </summary>
        public int IdTercero
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad 
        /// </summary>
        public String ObserValidador
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad 
        /// </summary>
        public String Proveedor
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad 
        /// </summary>
        public String TipoIdentificacion
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad 
        /// </summary>
        public String NumeroIdentificacion
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad 
        /// </summary>
        public int IdTipoPersona
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad CodigoTipoPersona
        /// </summary>
        public String CodigoTipoPersona
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad 
        /// </summary>
        public String TipoPersonaNombre
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad 
        /// </summary>
        public String Estado
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad EstadoProveedor
        /// </summary>
        public String EstadoProveedor
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad TipoDocumentoRepresentanteLegal
        /// </summary>
        public String TipoDocumentoRepresentanteLegal
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad NumeroIdRepresentanteLegal
        /// </summary>
        public String NumeroIDRepresentanteLegal
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad RepresentanteLegal
        /// </summary>
        public String RepresentanteLegal
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad PorcentajeParticipacion
        /// </summary>
        public String PorcentajeParticipacion
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad NumeroIdentificacionIntegrante
        /// </summary>
        public String NumeroIdentificacionIntegrante
        {
            get;
            set;
        }

        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public int? idCesion
        {
            get;
            set;
        }


        public bool esCessio
        {
            get;
            set;
        }
        public bool EsCesion
        {
            get;
            set;
        }
        public Proveedores_Contratos()
        {
        }

        public string Tipo
        {
            get
            {
                return EsCesion == true ? "Cesionario" : "Cedente";

            }
        }

        public int? idProveedorPadre { get; set; }
    }


    public class IntegrantesUTConsorcio
    {
        public int IdContratosMigrados { get; set; }
        public int IdTipoPersona { get; set; }
        public string TipoIdentificacion { get; set; }
        public string NumeroIdentificacion { get; set; }
        public string NombreRazonSocial { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Ubicacion { get; set; }
        public string CorreoElectronico { get; set; }
        public string PorcentajeParticipacion { get; set; }
        public string NumeroIdentificacionRepresentanteLegal { get; set; }
        public string NombreRepresentanteLegal { get; set; }
    }
}
