using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Guarda la información relacionada con la forma de pago
    /// </summary>
    public class FormaPago : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdFormaPago
        {
            get;
            set;
        }
        public int IdProveedores
        {
            get;
            set;
        }
        public int? IdMedioPago
        {
            get;
            set;
        }
        public int? IdEntidadFinanciera
        {
            get;
            set;
        }
        public int? TipoCuentaBancaria
        {
            get;
            set;
        }
        public String NumeroCuentaBancaria
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public String NombreTipoCuenta
        {
            get;
            set;
        }
        public String NombreEntidadFinanciera
        {
            get;
            set;
        }
        public String NombreTipoMedioPago
        {
            get;
            set;
        }
        public FormaPago()
        {
        }
    }
}
