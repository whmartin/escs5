using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para Secuencia N�mero proceso
    /// </summary>
    public class SecuenciaNumeroProceso : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad NumeroProceso
        /// </summary>
        public string NumeroProceso 
        { 
            get; 
            set; 
        }
        /// <summary>
        /// Propiedad CodigoRegional
        /// </summary>
        public int CodigoRegional
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad AnoVigencia
        /// </summary>
        public int AnoVigencia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Sigla
        /// </summary>
        public string Sigla
        {

            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }

        public SecuenciaNumeroProceso()
        {
        }
    }
}
