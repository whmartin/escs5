using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Este modulo nos ayuda a generar las certificaciones de los contratos
    /// </summary>
    public class CertificacionesContratos : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IDCertificacion
        {
            get;
            set;
        }
        public int IDContrato
        {
            get;
            set;
        }

        public string NumeroContrato { get; set; }
        public DateTime FechaCertificacion
        {
            get;
            set;
        }
        public Boolean Estado
        {
            get;
            set;
        }

        public string StrEstado
        {
            get;
            set;
        }

        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }

        public String DescRegional
        {
            get;
            set;
        }

        public DateTime FechaInicioEjecucion
        {
            get;
            set; 
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }

        public decimal ValorInicialContrato
        {
            get; 
            set;
        }

        public string DescEstado
        {
            get; 
            set;
        }

        public decimal IdArchivo
        {
            get; set;
        }

        public String NombreArchivo
        {
            get;
            set;
        }
        public String NombreArchivoOri
        {
            get;
            set;
        }
        public CertificacionesContratos()
        {
        }
    }
}
