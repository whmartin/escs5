using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para el registro del funcionario cargos del icbf
    /// </summary>
    public class FuncionarioCargosICBF : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdFuncCargo
        {
            get;
            set;
        }
        public int IdCargo
        {
            get;
            set;
        }

        public String Nombre
        {
            get;
            set;
        }

        public int IdFuncionario
        {
            get;
            set;
        }

        public String NombreFuncionario
        {
            get;
            set;
        }

        public String ResolucionNombramiento
        {
            get;
            set;
        }
        public Boolean Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public FuncionarioCargosICBF()
        {
        }
    }
}
