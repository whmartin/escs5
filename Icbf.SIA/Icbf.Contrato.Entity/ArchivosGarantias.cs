using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class ArchivosGarantias : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IDArchivosGarantias
        {
            get;
            set;
        }
        public int IDArchivo
        {
            get;
            set;
        }
        public int IDGarantia
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }

        public string NombreArchivo { get; set; }
        public string NombreArchivoOri { get; set; }

        public ArchivosGarantias()
        {
        }
    }
}
