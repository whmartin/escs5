﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class Supervisor : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdSupervisor
        {
            get;
            set;
        }
        public String IdTipoIdentificacion
        {
            get;
            set;
        }
        public String ID_Ident
        {
            get;
            set;
        }
        
        public String TipoIdentificacion
        {
            get;
            set;
        }
        public String TipoVinculacionContractual
        {
            get;
            set;
        }
        public String NumeroIdentificacion
        {
            get;
            set;
        }

        public String IdRegional
        {
            get;
            set;
        }
        public String Regional
        {
            get;
            set;
        }

        public String NombreCompleto
        {
            get;
            set;
        }
        public String Dependencia
        {
            get;
            set;
        }
        public String Direccion
        {
            get;
            set;
        }
        public String Telefono
        {
            get;
            set;
        }
        public String Cargo
        {
            get;
            set;
        }
        public String CorreoElectronico
        {
            get;
            set;
        }
        public String Celular
        {
            get;
            set;
        }
        public String PrimerNombre
        {
            get;
            set;
        }
        public String SegundoNombre
        {
            get;
            set;
        }
        public String PrimerApellido
        {
            get;
            set;
        }
        public String SegundoApellido
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public string NombreRol { get; set; }
        public Supervisor()
        {
        }
    }
}


