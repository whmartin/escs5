﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
   public class GetDetalleProductosServicioWSPacco
    {

        public decimal? cantidad { get; set; }
        public string codigo_producto { get; set; }
        public int? consecutivo { get; set; }
        public long iddetalleobjetocontractual { get; set; }
        public string nombre_producto { get; set; }
        public decimal? tiempo { get; set; }
        public string tipoProducto { get; set; }
        public string tipoProductoView { get; set; }
        public int? tipotiempo { get; set; }
        public string tipotiempoView { get; set; }
        public string unidad_medida { get; set; }
        public decimal? valor_total { get; set; }
        public decimal? valor_unitario { get; set; }
        public string Modificacion { get; set; }
    }


   public class GetDetalleRubrosServicioWSPacco
   {
       public string codigo_rubro { get; set; }
       public int? consecutivo { get; set; }
       public long idpagosdetalle { get; set; }
       public string NombreRubro { get; set; }
       public decimal? total_rubro { get; set; }
       public string Modificacion { get; set; }
   }
}
