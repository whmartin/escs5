using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class SubComponente : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdSubComponente
        {
            get;
            set;
        }
        public String NombreSubComponente
        {
            get;
            set;
        }
        public int IdComponente
        {
            get;
            set;
        }
        public int Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public SubComponente()
        {
        }
    }
}
