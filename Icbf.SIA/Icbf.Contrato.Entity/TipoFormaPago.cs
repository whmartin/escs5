﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para el registro de tipo forma pago
    /// </summary>
    public class TipoFormaPago : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdTipoFormaPago
        /// </summary>
        public int IdTipoFormaPago
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoTipoFormaPago
        /// </summary>
        public String CodigoTipoFormaPago
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreTipoFormaPago
        /// </summary>
        public String NombreTipoFormaPago
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Descripcion
        /// </summary>
        public String Descripcion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public bool Estado
        {
            get;
            set;
        } 
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public string EstadoString
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
    }
}
