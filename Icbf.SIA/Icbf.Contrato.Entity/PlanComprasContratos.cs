using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class PlanComprasContratos : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int NumeroConsecutivoPlanCompras
        {
            get;
            set;
        }

        public int Vigencia
        {
            get;
            set;
        }
        public int IdContrato
        {
            get;
            set;
        }
        public int IdPlanDeComprasContratos
        {
            get;
            set;
        }
        public String UsuarioDuenoPlanCompras
        {
            get;
            set;
        }
        public int IdEstadoPlanCompras
        {
            get;
            set;
        }
        public String IdAlcance
        {
            get;
            set;
        }
        public Decimal Valor
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public PlanComprasContratos()
        {
        }

        public String CodigoRegional { get; set; }

        public int IdValoresContrato { get; set; }

        public String Desde { get; set; }

        public String Hasta { get; set; }

        public bool EsAdicion { get; set; }
        public int? IdVigenciaFutura { get; set; }
        public bool? EsVigenciaFutura { get; set; }

        public int ? IdSolicitudContrato { get; set; }
    }
}
