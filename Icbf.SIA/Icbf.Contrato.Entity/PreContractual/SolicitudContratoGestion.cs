﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity.PreContractual
{
    public class SolicitudContratoGestion
    {
        public int IdSolicitudContratoGestion { get; set; }
        public int IdSolicitudContrato { get; set; }
        public int IdSolicitudContratoEstado { get; set; }
        public string EstadoSolicitud { get; set; }
        public string Usuario { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string Observaciones { get; set; }
    }
}
