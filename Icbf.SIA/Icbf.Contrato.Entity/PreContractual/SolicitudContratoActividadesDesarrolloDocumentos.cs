﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity.PreContractual
{
    public class SolicitudContratoActividadesDesarrolloDocumentos
    {
        public int IdDocumento { get; set; }
        public int IdDesarrolloActividad { get; set; }
        public string NombreArchivo { get; set; }
        public string NombreOriginal { get; set; }
        public string RutaArchivo { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
    }
}
