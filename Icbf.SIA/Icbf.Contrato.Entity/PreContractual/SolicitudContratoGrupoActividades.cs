﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity.PreContractual
{
    public class SolicitudContratoGrupoActividades
    {
        public int IdGrupoActividades { get; set; }
        public string Nombre { get; set; }
        public int IdModalidadSeleccion { get; set; }
        public string ModalidadSeleccion { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
        public bool TieneActividades { get; set; }
        public string Descripcion { get; set; }

        public bool Estado { get; set; }
    }
}
