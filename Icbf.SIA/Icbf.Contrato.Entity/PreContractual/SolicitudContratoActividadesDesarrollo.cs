﻿using System;

namespace Icbf.Contrato.Entity.PreContractual
{
    public class SolicitudContratoActividadesDesarrollo
    {
        public int IdSolicitud { get; set; }
        public int  IdDesarrolloActividad { get; set; }
        public int  IdActivdad { get; set; }
        public DateTime ? FechaInicial { get; set; }
        public DateTime ? FechaFinalizacion { get; set; }
        public string Observaciones { get; set; }
        public bool ? Finalizada { get; set; }
        public string  FinalizadaView
        {
            get
            {
                string result = string.Empty;

                if(!Finalizada.HasValue)
                result = "Sin Desarrollar";
                else if(Finalizada.Value)
                result = "Finalizada";
                else
                result = "En Curso";

                return result;
            }
        }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
        public bool AplicaPublicacion { get; set; }
        public DateTime ? FechaPublicacion { get; set; }
        public string Nombre { get; set; }
        public bool EsPadre { get; set; }
        public int Cantidad { get; set; }
        public string ModalidadSeleccion { get; set; }
    }

    public class SolicitudContratoActividadesDesarrolloDetalle
    {
        public int IdSolicitud { get; set; }
        public int ? IdDesarrolloActividad { get; set; }
        public int IdActivdad { get; set; }
        public DateTime ? FechaInicial { get; set; }
        public DateTime ? FechaFinalizacion { get; set; }
        public string Observaciones { get; set; }
        public bool? Finalizada { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
        public bool AplicaPublicacion { get; set; }
        public DateTime ? FechaPublicacion { get; set; }
        public string NombreActividad { get; set; }
        public int IdModalidadSeleccion { get; set; }
        public string NumeroRadico { get; set; }
        public string EstadoSolicitud { get; set; }
        public string NombreActividadPadre { get; set; }
    }
}
