﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity.PreContractual
{
    public class SolicitudContratoDocumentos
    {
        public int IdSolicitudContratoDocumento { get; set; }
        public int IdSolicitudContrato { get; set; }
        public int IdTipoDocumento { get; set; }
        public string TipoDocumento { get; set; }
        public string NombreArchivo { get; set; }
        public string RutaArchivo { get; set; }
        public bool Aprobado { get; set; }
        public string AprobadoView 
        { 
            get
            {
                string result = string.Empty;

                if (Aprobado)
                    result = "Si";
                else
                    result = "No";

                return result;
            }
        }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
    }
}
