﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity.PreContractual
{
    public class SolicitudContrato
    {
        public int IdSolicitud { get; set; }
        public int IdContrato { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
        public int IdEstadoSolicitud { get; set; }
        public string EstadoSolicitud { get; set; }
        public string CodigoEstadoSolicitud { get; set; }
        public int IdRegional { get; set; }
        public string Regional { get; set; }
        public int IdModalidadSeleccion { get; set; }
        public string ModalidadSeleccion { get; set; }
        public int IdCategoriaContrato { get; set; }
        public string CategoriaContrato { get; set; }
        public int IdTipoContrato { get; set; }
        public string TipoContrato { get; set; }
        public string CodigoTipoContrato { get; set; }
        public string NumeroRadicado { get; set; }
        public DateTime FechaMaximaTramite { get; set; }
        public bool ? EsContratoMarco { get; set; }
        public bool ? EsContratoConvenioAdhesion { get; set; }
        public string  IdModalidadAcademica { get; set; }
        public string IdProfesion { get; set; }
        public bool RequiereActaInicio {get;set;}
        public bool RequiereGarantia{get;set;}
        public bool ManejaCofinanciacion { get; set; }
        public bool ManejaRecursosICBF { get; set; }
        public bool ManejaRecursosEspecieICBF { get; set; }
        public bool ManejaVigenciasFuturas { get; set; }

        public int ? IdentificacionOrdenadorGasto { get; set; }
        public string NombreOrdenadorGasto { get; set; }
        public int ? IdRegionalOrdenadorGasto { get; set; }
        public string RegionalOrdenadorGasto { get; set; }
        public int ? IdDependenciaOrdenadorGasto { get; set; }
        public string DependenciaOrdenadorGasto { get; set; }
        public int ? IdCargoOrdenadorGasto { get; set; }
        public string CargoOrdenadorGasto { get; set; }

        public int ? IdentificacionSolicitante { get; set; }
        public string NombreSolicitante { get; set; }
        public int ? IdRegionalSolicitante { get; set; }
        public string RegionalSolicitante { get; set; }
        public int ? IdDependenciaSolicitante { get; set; }
        public string DependenciaSolicitante { get; set; }
        public int ? IdCargoSolicitante { get; set; }
        public string CargoSolicitante { get; set; }

        public DateTime ? FechaInicioContrato { get; set; }
        public DateTime ? FechaFinalizaContrato { get; set; }
        public int IdVigenciaInicial { get; set; }
        public int IdVigenciaFinal { get; set; }

        public decimal? ValorContrato { get; set; }
        public decimal? ValorContratoFinal { get; set; }
        public string Objeto { get; set; }
        public string Alcance { get; set; }

        public string UsuarioArea { get; set; }
        public string UsuarioRevision { get; set; }

        public int ? IdPlanCompras { get; set; }

        public int VigenciaInicial { get; set; }
        public int VigenciaFinal { get; set; }

        public string EstadoView
        {
            get
            {
                string result = this.EstadoSolicitud;

                if(result == "Enviado" && string.IsNullOrEmpty(this.UsuarioRevision))
                    result = "Sin Asignar";
                else if(result == "Enviado")
                    result = "Asignada";

                return result;
            }
        }

    }
}
