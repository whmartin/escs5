﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity.PreContractual
{
    public class TiposDocumento
    {
        public int IdTipoDocumento { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
        public bool Estado { get; set; }
        public string ModalidadesSeleccion { get; set; }
        public List<string> ModalidadesSeleccionList 
        {
            get
            {
                List<string> result = new List<string>();

                if (! string.IsNullOrEmpty(ModalidadesSeleccion))
                {
                    var misModalidades = ModalidadesSeleccion.Split(',');

                    if (misModalidades != null && misModalidades.Count() > 0)
                    {
                         foreach(var itemMod in misModalidades)
                        result.Add(itemMod.Trim().TrimStart().TrimEnd());
                    }
                }

                return result;
            }
            set
            {
                string result = string.Empty;

                if (value != null && value.Count > 0)
                {
                    foreach (var itemModalidad in value)
                        result += string.Format("{0},",itemModalidad);

                    if (result.Length > 0)
                        ModalidadesSeleccion = result;
                }
            }
        }

        public string EstadoView
        {
            get
            {
                return Estado ? "Activo" : "Inactivo";
            }
        }

        public string ModalidadesSeleccionView
        {
            get
            {
                return ModalidadesSeleccion != null ? ModalidadesSeleccion.Replace(",","<br />"): string.Empty;
            }
        }
    }
}
