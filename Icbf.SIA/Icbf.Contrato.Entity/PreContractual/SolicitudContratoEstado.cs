﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity.PreContractual
{
    public enum SolicitudContratoEstados
    {
        REG,
        REP,
        GES,
        FIN
    }

    public class SolicitudContratoEstado
    {
        public int IdEstadoSolicitud { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
        public string Nombre { get; set; }
        public string Codigo { get; set; }
        public bool Estado { get; set; }
    }

    public class SolicitudContratoCambioEstado
    {
        public int IdSolicitudContrato { get; set; }
        public string Observaciones { get; set; }
        public DateTime ? Fecha { get; set; }
        public string Codigo { get; set; }
        public string Usuario { get; set; }
        public string NumeroRadicado { get; set; }
        public string IdsDocumentosAprobados { get; set; }
    }
}
