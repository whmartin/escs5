﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity.PreContractual
{
    public class ParametrosModuloPrecontractual
    {
        public int IdParametro { get; set; }
        public string Nombre { get; set; }
        public string Valor { get; set; }
    }
}
