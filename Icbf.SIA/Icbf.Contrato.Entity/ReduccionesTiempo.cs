﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class ReduccionesTiempo : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdReduccionTiempo
        {
            get;
            set;
        }
       
        public DateTime FechaReduccion
        {
            get;
            set;
        }       
        public String Justificacion
        {
            get;
            set;
        }
        public int IdDetalleConsModContractual
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }

        public int Dias { get; set; }
        public int Meses { get; set; }
        public int Anios { get; set; }

    }
}
