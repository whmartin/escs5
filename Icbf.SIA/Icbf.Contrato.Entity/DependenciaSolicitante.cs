﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class DependenciaSolicitante
    {
        public int IdDependenciaSolicitante
        {
            get;
            set;
        }
        public String NombreDependenciaSolicitante
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public String CodigoDescripcion
        {
            get;
            set;
        }
        public DependenciaSolicitante()
        {
        }
    }
}
