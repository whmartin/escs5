using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Muestra la información relacionada con el certificado de diaponibilidad presupuestal
    /// </summary>
    public class RegistroInformacionPresupuestal : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdEtlCDP
        {
            get;
            set;
        }
        public int VigenciaFiscal
        {
            get;
            set;
        }
        public int RegionalICBF
        {
            get;
            set;
        }
        public String NumeroCDP
        {
            get;
            set;
        }
        public String Area
        {
            get;
            set;
        }
        public Decimal ValorCDP
        {
            get;
            set;
        }
        public Decimal ValorTotalHasta
        {
            get;
            set;
        }
        public DateTime FechaCDP
        {
            get;
            set;
        }
        public String DependenciaAfectacionGastos
        {
            get;
            set;
        }
        public String TipoFuenteFinanciamiento
        {
            get;
            set;
        }

        public String PosicionCatalogoGastos
        {
            get;
            set;
        }

        public String RubroPresupuestal
        {
            get;
            set;
        }

        public String RecursoPresupuestal
        {
            get;
            set;
        }

        public String TipoDocumentoSoporte
        {
            get;
            set;
        }

        public String TipoSituacionFondos
        {
            get;
            set;
        }
        
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public String NombreRegionalICBF
        {
            get;
            set;
        }

        public int IdContrato { get; set; }
        public string NumeroContrato { get; set; }
        public string CodigoRubro { get; set; }
        public RegistroInformacionPresupuestal()
        {
        }
    }




}
