﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class SupervisorTipoIdentificacion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public String ID_Ident
        {
            get;
            set;
        }
        public String desc_ide
        {
            get;
            set;
        }
        public SupervisorTipoIdentificacion()
        { }
    }
}
