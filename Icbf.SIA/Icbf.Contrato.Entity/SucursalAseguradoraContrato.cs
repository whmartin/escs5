using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class SucursalAseguradoraContrato : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IDSucursalAseguradoraContrato
        {
            get;
            set;
        }
        public int IDDepartamento
        {
            get;
            set;
        }
        public int IDMunicipio
        {
            get;
            set;
        }
        public String Nombre
        {
            get;
            set;
        }
        public int IDZona
        {
            get;
            set;
        }
        public String DireccionNotificacion
        {
            get;
            set;
        }
        public String CorreoElectronico
        {
            get;
            set;
        }
        public String Indicativo
        {
            get;
            set;
        }
        public String Telefono
        {
            get;
            set;
        }
        public String Extension
        {
            get;
            set;
        }
        public String Celular
        {
            get;
            set;
        }
        //public int IDEntidadProvOferente
        //{
        //    get;
        //    set;
        //}
        public int IdTercero
        {
            get;
            set;
        }
        public int CodigoSucursal
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public SucursalAseguradoraContrato()
        {
        }
    }
}
