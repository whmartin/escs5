using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para Modalidad Selecci�n
    /// </summary>
    public class ModalidadSeleccion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdModalidad
        /// </summary>
        public int IdModalidad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoModalidad
        /// </summary>
        public String CodigoModalidad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Nombre
        /// </summary>
        public String Nombre
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Sigla
        /// </summary>
        public String Sigla
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad EstadoString
        /// </summary>
        public String EstadoString
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public ModalidadSeleccion()
        {
        }
    }
}
