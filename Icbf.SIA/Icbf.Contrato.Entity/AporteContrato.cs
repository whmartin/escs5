using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class AporteContrato : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdAporteContrato
        {
            get;
            set;
        }
        public Boolean AportanteICBF
        {
            get;
            set;
        }
        public string Procedencia
        {
            get
            {
                string result = string.Empty;

                if (AportanteICBF)
                {
                    result = "ICBF";
                }
                else
                    result = "Cofinanciación Contratista";

                return result;
            }

        }
        public String NumeroIdentificacionICBF
        {
            get;
            set;
        }
        public Decimal ValorAporte
        {
            get;
            set;
        }
        public Decimal ValorAporteE
        {
            get;
            set;
        }

        public String DescripcionAporte
        {
            get;
            set;
        }
        public Boolean AporteEnDinero
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad TipoAporte
        /// </summary>
        public String TipoAporte
        {
            get;
            set;
        }
        public int IdContrato
        {
            get;
            set;
        }
        public int? IDEntidadProvOferente
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NumeroIdentificacionContratista
        /// </summary>
        public String NumeroIdentificacionContratista
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad InformacionAportante
        /// </summary>
        public String InformacionAportante
        {
            get;
            set;
        }
        public Boolean Estado
        {
            get;
            set;
        }
        public DateTime? FechaRP
        {
            get;
            set;
        }
        public String NumeroRP
        {
            get;
            set;
        }

        public String NombreTipoDocumento
        {
            get;
            set;
        }
        public String CodigoTipoDocumento
        {
            get;
            set;
        }
        public int? IdTipoDocumento
        {
            get;
            set;
        }

        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public Boolean EsAdicion { get; set; }
        public int IdAdicion { get; set; }
        public Boolean Esreduccion { get; set; }
        public int IdReduccion { get; set; }

        public string TipoModificacion
        {
                get
               {
                string result = string.Empty;

                if (!Esreduccion && !EsAdicion)
                    result = "Valor Orginal";
                else if (Esreduccion)
                    result = "Valor Reducido";
                else if (EsAdicion)
                    result = "Valor Adicionado";

                    return result;
                }
        }


        public string TipoAportante
        {
            get
            {
                string result = string.Empty;

                if (AportanteICBF)
                    result = "ICBF";
                else
                    result = "Contratista";

                return result;
            }
        }

        public AporteContrato()
        {
        }
    }
}
