﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class Proveedores_Contratista : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdProveedoresContratista
        {
            get;
            set;
        }
        public int IdEntidad
        {
            get;
            set;
        }
        public int IdTercero
        {
            get;
            set;
        }
        public String NombreTipoPersona
        {
            get;
            set;
        }
        public String CodDocumento
        {
            get;
            set;
        }
        public String NumeroIdentificacion
        {
            get;
            set;
        }
        public String Razonsocial
        {
            get;
            set;
        }
        public String NumeroidentificacionRepLegal
        {
            get;
            set;
        }
        public String RazonsocialRepLegal
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public Proveedores_Contratista()
        {
        }
    }
}
