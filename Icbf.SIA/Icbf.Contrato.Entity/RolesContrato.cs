﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class RolesContrato : Icbf.Seguridad.Entity.EntityAuditoria
    {

        public int IdRol
        {
            get;
            set;
        }

        public Boolean Estado
        {
            get;
            set;
        }
        public String NombreRol
        {
            get;
            set;
        }

        public String Descripcion
        {
            get;
            set;
        }

        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
    }
}
