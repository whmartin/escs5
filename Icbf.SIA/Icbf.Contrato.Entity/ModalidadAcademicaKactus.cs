﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para Modalidad Académica Kactus(Gestion humana)
    /// </summary>
    public class ModalidadAcademicaKactus : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad Id de la modalidad academica de Kactus
        /// </summary>
        public int Id
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Codigo de la modalidad academica de Kactus
        /// </summary>
        public string Codigo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Descripcion de la modalidad academica de Kactus
        /// </summary>
        public string Descripcion
        {
            get;
            set;
        }
        public ModalidadAcademicaKactus()
        { 
        
        }
    }
}
