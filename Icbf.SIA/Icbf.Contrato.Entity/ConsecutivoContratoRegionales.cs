﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class ConsecutivoContratoRegionales : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IDConsecutivoContratoRegional
        {
            get;
            set;
        }
        public int IdRegional
        {
            get;
            set;
        }
        public string Regional
        {
            get;
            set;
        } 
        public int IdVigencia
        {
            get;
            set;
        }
        public string Vigencia
        {
            get;
            set;
        }
        public Decimal Consecutivo
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public ConsecutivoContratoRegionales()
        {
        }
    }
}

