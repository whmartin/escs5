﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class SolicitudCDP
    {
        public System.Data.DataTable SolicitidesCDP { get; set; }
    }


    public class TrazabilidadSolicitudCDP
    {
        public int IdCargue { get; set; }
        public DateTime Fecha { get; set; }
        public string NombreArchivo { get; set; }
        public string FechaView 
        { 
            get
            {
                return Fecha.ToShortDateString();
            }
        }
    }
}
