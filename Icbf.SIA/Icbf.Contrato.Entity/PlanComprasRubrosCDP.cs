using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class PlanComprasRubrosCDP : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int? IdRubroPlanComprasContrato { get; set; }

        public int? NumeroConsecutivoPlanCompras
        {
            get;
            set;
        }
        public String CodigoRubro
        {
            get;
            set;
        }
        public String DescripcionRubro
        {
            get;
            set;
        }
        public Decimal? ValorRubro
        {
            get;
            set;
        }
        public Decimal? ValorRubroPACCO
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public PlanComprasRubrosCDP()
        {
        }

        public String RecursoPresupuestal { get; set; }
        public String IdPagosDetalle { get; set; }
        
        public int? IdPlanDeComprasContratos { get; set; }

        public int? IdRubro { get; set; }

        public bool IsReduccion { get; set; }

        public bool EsAdiccion { get; set; }
    }
}
