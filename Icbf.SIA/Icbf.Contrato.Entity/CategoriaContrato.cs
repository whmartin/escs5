using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para Categor�a Contratos
    /// </summary>
    public class CategoriaContrato : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdCategoriaContrato
        /// </summary>
        public int IdCategoriaContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoCategoriaContrato
        /// </summary>
        public String CodigoCategoriaContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreCategoriaContrato
        /// </summary>
        public String NombreCategoriaContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Descripcion
        /// </summary>
        public String Descripcion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad EstadoString
        /// </summary>
        public String EstadoString
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public CategoriaContrato()
        {
        }
    }
}
