using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Modulo para almacenar las obligaciones de los contratos
    /// </summary>
    public class ObligacionesContratoReporte : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IDObligacionContrato
        {
            get;
            set;
        }
        public int IDContrato
        {
            get;
            set;
        }
        public string DescripcionObligacion
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public int IDObligacionSigepcyp
        {
            get;
            set;
        }
        public ObligacionesContratoReporte()
        {
        }
    }
}
