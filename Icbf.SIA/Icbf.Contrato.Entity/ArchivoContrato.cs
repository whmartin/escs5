using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para registro de archivos
    /// </summary>
    public class ArchivoContrato : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdArchivoContrato
        {
            get;
            set;
        }
        public decimal IdArchivo
        {
            get;
            set;
        }

        public int IdContrato
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }

        /// <summary>
        /// Hasta aqui archivos que estan en el modelo
        /// </summary>


        public int IdUsuario
        {
            get;
            set;
        }
        public String NombreUsuario
        {
            get;
            set;
        }
        public int IdFormatoArchivo
        {
            get;
            set;
        }
        public int IdModalidad
        {
            get;
            set;
        }
        public DateTime? FechaRegistro
        {
            get;
            set;
        }
        public String NombreArchivo
        {
            get;
            set;
        }
        public String ServidorFTP
        {
            get;
            set;
        }
        public String NombreEstructura
        {
            get;
            set;
        }
        public String Estado
        {
            get;
            set;
        }
        public String ResumenCarga
        {
            get;
            set;
        }
        public string NombreArchivoOri
        {
            get; set;
        }
        public string TipoEstructura
        {
            get;
            set;
        }

        public string NombreTabla
        {
            get;
            set;
        }
        public string IdTabla
        {
            get;
            set;
        }

        public string Extension { get; set; }

        public ArchivoContrato()
        {
        }
    }
}
