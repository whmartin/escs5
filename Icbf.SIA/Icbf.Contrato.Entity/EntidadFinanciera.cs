using System;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// Clase entidad para el registro de entidades financieras
    /// </summary>
    public class EntidadFinanciera : Icbf.Seguridad.Entity.EntityAuditoria
    {   
        public const string cEsPagoPorOrdenDePago = "EsPagoPorOrdenDePago";

        public const string cEsPagoPorOrdenBancaria = "EsPagoPorOrdenBancaria";

        public int IdEntidadFinanciera
        {
            get;
            set;
        }
        public String CodigoEntFin
        {
            get;
            set;
        }
        public String NombreEntFin
        {
            get;
            set;
        }
        public Boolean Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }

        public Boolean? EsPagoPorOrdenDePago
        {
            get;
            set;
        }

        public Boolean? EsPagoPorOrdenBancaria
        {
            get;
            set;
        }

        public EntidadFinanciera()
        {
        }
    }
}
