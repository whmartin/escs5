﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class Interventor : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdInterventor
        {
            get;
            set;
        }
        public int IdTipoPersona
        {
            get;
            set;
        }
        public String TipoPersona
        {
            get;
            set;
        }
        public int IdTipoIdentificacion
        {
            get;
            set;
        }
        public String TipoIdentificacion
        {
            get;
            set;
        }
        public String NumeroDocumento
        {
            get;
            set;
        }
        public String NombreRazonSocial
        {
            get;
            set;
        }
        public String PrimerNombre
        {
            get;
            set;
        }
        public String SegundoNombre
        {
            get;
            set;
        }
        public String PrimerApellido
        {
            get;
            set;
        }
        public String SegundoApellido
        {
            get;
            set;
        }
        public String RazonSocial
        {
            get;
            set;
        }
        public String CorreoElectronico
        {
            get;
            set;
        }
        public String Direccion
        {
            get;
            set;
        }
        public String Telefono
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public Interventor()
        {
        }
    }
}


