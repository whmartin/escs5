﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Entity
{
    public class ContratistaMigrados : Icbf.Seguridad.Entity.EntityAuditoria
    {

        public int IdContratosMigradosContratista { get; set; }
        public int IdTipoPersona { get; set; }
        public int IdTipoIdentificacion { get; set; }
        public string NumeroIdentificacion { get; set; }
        public string NombreRazonSocial { get; set; }
        public string Genero { get; set; }
        public string Direccion  { get; set; }
        public string Telefono { get; set; }
        public string CorreoElectronico { get; set; }
        public string Profesion { get; set; }
        public string Ubicacion { get; set; }
        public string Otro { get; set; }

        public string IdentificacionRepresentanteLegal { get; set; }
        public string NombreRepresentanteLegal { get; set; }

        public string TipoPersona { get; set; }
        public string TipoIdentificacion { get; set; }

    }

    public class ContratoMigrado : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdContratoEnContratos { get; set; }
        public string TipoGarantia { get; set; }
        public DateTime FechaAprobacionGarantia { get; set; }
        public decimal ValorTotalAsegurado { get; set; }
        public string NombreEntidadAseguradora { get; set; }
        public string NitEntidadAseguradora { get; set; }
        public decimal PorcentajeAsegurado { get; set; }

    }


}
