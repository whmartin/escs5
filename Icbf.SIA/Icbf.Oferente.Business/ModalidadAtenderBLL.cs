using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class ModalidadAtenderBLL
    {
        private ModalidadAtenderDAL vModalidadAtenderDAL;
        public ModalidadAtenderBLL()
        {
            vModalidadAtenderDAL = new ModalidadAtenderDAL();
        }
        public int InsertarModalidadAtender(ModalidadAtender pModalidadAtender)
        {
            try
            {
                return vModalidadAtenderDAL.InsertarModalidadAtender(pModalidadAtender);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarModalidadAtender(ModalidadAtender pModalidadAtender)
        {
            try
            {
                return vModalidadAtenderDAL.ModificarModalidadAtender(pModalidadAtender);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarModalidadAtender(ModalidadAtender pModalidadAtender)
        {
            try
            {
                return vModalidadAtenderDAL.EliminarModalidadAtender(pModalidadAtender);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public ModalidadAtender ConsultarModalidadAtender(int pIdModalidadAtender)
        {
            try
            {
                return vModalidadAtenderDAL.ConsultarModalidadAtender(pIdModalidadAtender);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ModalidadAtender> ConsultarModalidadAtenders(String pCodigoModalidadAtender, String pNombreModalidadAtender, Boolean? pEstado)
        {
            try
            {
                return vModalidadAtenderDAL.ConsultarModalidadAtenders(pCodigoModalidadAtender, pNombreModalidadAtender, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
