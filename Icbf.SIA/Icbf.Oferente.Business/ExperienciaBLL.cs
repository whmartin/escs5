using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class ExperienciaBLL
    {
        private ExperienciaDAL vExperienciaDAL;
        public ExperienciaBLL()
        {
            vExperienciaDAL = new ExperienciaDAL();
        }
        /// <summary>
        /// Realiza una consulta del salario m�nimo espec�fico de un a�o
        /// </summary>
        /// <param name="pAno"></param>
        /// <returns></returns>
        public SalarioMinimo ConsultarSalarioMinimo(int pAno, string descripcion = null)
        {
            try
            {
                return vExperienciaDAL.ConsultarSalarioMinimo(pAno, descripcion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Consultar Experiencia Departamento
        /// </summary>
        /// <returns></returns>
        public List<ExperienciaDepartamento> ConsultarExperienciaDepartamento()
        {
            try
            {
                return vExperienciaDAL.ConsultarExperienciaDepartamento();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Consultar Experiencia Municipio
        /// </summary>
        /// <param name="pIdDepartamento"></param>
        /// <returns></returns>
        public List<ExperienciaMunicipio> ConsultarExperienciaMunicipio(int pIdDepartamento)
        {
            try
            {
                return vExperienciaDAL.ConsultarExperienciaMunicipio(pIdDepartamento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

       
    }
}
