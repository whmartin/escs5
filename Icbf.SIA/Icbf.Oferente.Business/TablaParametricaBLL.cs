using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class TablaParametricaBLL
    {
        private TablaParametricaDAL vTablaParametricaDAL;
        public TablaParametricaBLL()
        {
            vTablaParametricaDAL = new TablaParametricaDAL();
        }
        public int InsertarTablaParametrica(TablaParametrica pTablaParametrica)
        {
            try
            {
                return vTablaParametricaDAL.InsertarTablaParametrica(pTablaParametrica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarTablaParametrica(TablaParametrica pTablaParametrica)
        {
            try
            {
                return vTablaParametricaDAL.ModificarTablaParametrica(pTablaParametrica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarTablaParametrica(TablaParametrica pTablaParametrica)
        {
            try
            {
                return vTablaParametricaDAL.EliminarTablaParametrica(pTablaParametrica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public TablaParametrica ConsultarTablaParametrica(int pIdTablaParametrica)
        {
            try
            {
                return vTablaParametricaDAL.ConsultarTablaParametrica(pIdTablaParametrica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TablaParametrica> ConsultarTablaParametricas(String pCodigoTablaParametrica, String pNombreTablaParametrica, Boolean? pEstado)
        {
            try
            {
                return vTablaParametricaDAL.ConsultarTablaParametricas(pCodigoTablaParametrica, pNombreTablaParametrica, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
