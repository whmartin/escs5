using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class SexoBLL
    {
        private SexoDAL vSexoDAL;
        public SexoBLL()
        {
            vSexoDAL = new SexoDAL();
        }
        public int InsertarSexo(Sexo pSexo)
        {
            try
            {
                return vSexoDAL.InsertarSexo(pSexo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarSexo(Sexo pSexo)
        {
            try
            {
                return vSexoDAL.ModificarSexo(pSexo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarSexo(Sexo pSexo)
        {
            try
            {
                return vSexoDAL.EliminarSexo(pSexo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public Sexo ConsultarSexo(int pIdSexo)
        {
            try
            {
                return vSexoDAL.ConsultarSexo(pIdSexo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Sexo> ConsultarSexos(String pCodigoSexo, String pNombreSexo, Boolean? pEstado)
        {
            try
            {
                return vSexoDAL.ConsultarSexos(pCodigoSexo, pNombreSexo, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
