using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class OrigenCapitalBLL
    {
        private OrigenCapitalDAL vOrigenCapitalDAL;
        public OrigenCapitalBLL()
        {
            vOrigenCapitalDAL = new OrigenCapitalDAL();
        }
        public int InsertarOrigenCapital(OrigenCapital pOrigenCapital)
        {
            try
            {
                return vOrigenCapitalDAL.InsertarOrigenCapital(pOrigenCapital);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarOrigenCapital(OrigenCapital pOrigenCapital)
        {
            try
            {
                return vOrigenCapitalDAL.ModificarOrigenCapital(pOrigenCapital);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarOrigenCapital(OrigenCapital pOrigenCapital)
        {
            try
            {
                return vOrigenCapitalDAL.EliminarOrigenCapital(pOrigenCapital);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public OrigenCapital ConsultarOrigenCapital(int pIdOrigenCapital)
        {
            try
            {
                return vOrigenCapitalDAL.ConsultarOrigenCapital(pIdOrigenCapital);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<OrigenCapital> ConsultarOrigenCapitals(String pCodigoOrigenCapital, String pNombreOrigenCapital, Boolean? pEstado)
        {
            try
            {
                return vOrigenCapitalDAL.ConsultarOrigenCapitals(pCodigoOrigenCapital, pNombreOrigenCapital, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
