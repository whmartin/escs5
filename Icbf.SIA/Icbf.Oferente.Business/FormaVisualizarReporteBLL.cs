using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class FormaVisualizarReporteBLL
    {
        private FormaVisualizarReporteDAL vFormaVisualizarReporteDAL;
        public FormaVisualizarReporteBLL()
        {
            vFormaVisualizarReporteDAL = new FormaVisualizarReporteDAL();
        }
        public int InsertarFormaVisualizarReporte(FormaVisualizarReporte pFormaVisualizarReporte)
        {
            try
            {
                return vFormaVisualizarReporteDAL.InsertarFormaVisualizarReporte(pFormaVisualizarReporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarFormaVisualizarReporte(FormaVisualizarReporte pFormaVisualizarReporte)
        {
            try
            {
                return vFormaVisualizarReporteDAL.ModificarFormaVisualizarReporte(pFormaVisualizarReporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarFormaVisualizarReporte(FormaVisualizarReporte pFormaVisualizarReporte)
        {
            try
            {
                return vFormaVisualizarReporteDAL.EliminarFormaVisualizarReporte(pFormaVisualizarReporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public FormaVisualizarReporte ConsultarFormaVisualizarReporte(int pIdFormaVisualizarReporte)
        {
            try
            {
                return vFormaVisualizarReporteDAL.ConsultarFormaVisualizarReporte(pIdFormaVisualizarReporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<FormaVisualizarReporte> ConsultarFormaVisualizarReportes(String pCodigoFormaVisualizarReporte, String pNombreFormaVisualizarReporte, Boolean? pEstado)
        {
            try
            {
                return vFormaVisualizarReporteDAL.ConsultarFormaVisualizarReportes(pCodigoFormaVisualizarReporte, pNombreFormaVisualizarReporte, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
