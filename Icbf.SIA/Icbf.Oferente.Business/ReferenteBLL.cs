using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.SIA.Business;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// Definición de clase ReferenteBLL
    /// </summary>
    public class ReferenteBLL
    {
        /// <summary>
        /// Declaración de Objeto DAL
        /// Y Constructor donde se creal el objeto DAL
        /// </summary>
        private ReferenteDAL vReferenteDAL;
        public ReferenteBLL()
        {
            vReferenteDAL = new ReferenteDAL();
        }
        /// <summary>
        /// Inserta un Referente
        /// </summary>
        /// <param name="pReferente"></param>
        /// <returns></returns>
        public int InsertarReferente(Referente pReferente)
        {
            try
            {
                return vReferenteDAL.InsertarReferente(pReferente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Modifica un Referente
        /// </summary>
        /// <param name="pReferente"></param>
        /// <returns></returns>
        public int ModificarReferente(Referente pReferente)
        {
            try
            {
                return vReferenteDAL.ModificarReferente(pReferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar los Referentes por Id de Tercero
        /// </summary>
        /// <param name="pIdReferente"></param>
        /// <returns></returns>
        public List<Referente> ConsultarReferentes(int pIdTercero)
        {
            try
            {
                return vReferenteDAL.ConsultarReferentes(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
