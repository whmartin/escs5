using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class TipoOrganizacionBLL
    {
        private TipoOrganizacionDAL vTipoOrganizacionDAL;
        public TipoOrganizacionBLL()
        {
            vTipoOrganizacionDAL = new TipoOrganizacionDAL();
        }
        public int InsertarTipoOrganizacion(TipoOrganizacion pTipoOrganizacion)
        {
            try
            {
                return vTipoOrganizacionDAL.InsertarTipoOrganizacion(pTipoOrganizacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarTipoOrganizacion(TipoOrganizacion pTipoOrganizacion)
        {
            try
            {
                return vTipoOrganizacionDAL.ModificarTipoOrganizacion(pTipoOrganizacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarTipoOrganizacion(TipoOrganizacion pTipoOrganizacion)
        {
            try
            {
                return vTipoOrganizacionDAL.EliminarTipoOrganizacion(pTipoOrganizacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public TipoOrganizacion ConsultarTipoOrganizacion(int pIdTipoOrganizacion)
        {
            try
            {
                return vTipoOrganizacionDAL.ConsultarTipoOrganizacion(pIdTipoOrganizacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoOrganizacion> ConsultarTipoOrganizacions(String pCodigoTipoOrganizacion, String pNombreTipoOrganizacion, Boolean? pEstado)
        {
            try
            {
                return vTipoOrganizacionDAL.ConsultarTipoOrganizacions(pCodigoTipoOrganizacion, pNombreTipoOrganizacion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
