using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// Definición de clase AcuerdosBLL
    /// </summary>
    public class AcuerdosBLL
    {
        /// <summary>
        /// Declaración de clase DAL 
        /// Y Constructor donde se crea el objeto DAL vAcuerdosDAL
        /// </summary>
        private AcuerdosDAL vAcuerdosDAL;
        public AcuerdosBLL()
        {
            vAcuerdosDAL = new AcuerdosDAL();
        }
        /// <summary>
        /// Insertar Acuerdos
        /// </summary>
        /// <param name="pAcuerdos"></param>
        /// <returns></returns>
        public int InsertarAcuerdos(Acuerdos pAcuerdos)
        {
            try
            {
                return vAcuerdosDAL.InsertarAcuerdos(pAcuerdos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modificar Acuerdos
        /// </summary>
        /// <param name="pAcuerdos"></param>
        /// <returns></returns>
        public int ModificarAcuerdos(Acuerdos pAcuerdos)
        {
            try
            {
                return vAcuerdosDAL.ModificarAcuerdos(pAcuerdos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Acuerdos
        /// </summary>
        /// <param name="pAcuerdos"></param>
        /// <returns></returns>
        public int EliminarAcuerdos(Acuerdos pAcuerdos)
        {
            try
            {
                return vAcuerdosDAL.EliminarAcuerdos(pAcuerdos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Acuerdos
        /// </summary>
        /// <param name="pIdAcuerdo"></param>
        /// <returns></returns>
        public Acuerdos ConsultarAcuerdos(int pIdAcuerdo)
        {
            try
            {
                return vAcuerdosDAL.ConsultarAcuerdos(pIdAcuerdo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consultar Acuerdo Vigente
        /// </summary>
        /// <returns></returns>
        public Acuerdos ConsultarAcuerdoVigente()
        {
            try
            {
                return vAcuerdosDAL.ConsultarAcuerdoVigente();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Lista de Acuerdoss
        /// </summary>
        /// <param name="pNombre"></param>
        /// <returns></returns>
        public List<Acuerdos> ConsultarAcuerdoss(String pNombre)
        {
            try
            {
                return vAcuerdosDAL.ConsultarAcuerdoss(pNombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
