using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class ZonaResidenciaBLL
    {
        private ZonaResidenciaDAL vZonaResidenciaDAL;
        public ZonaResidenciaBLL()
        {
            vZonaResidenciaDAL = new ZonaResidenciaDAL();
        }
        public int InsertarZonaResidencia(ZonaResidencia pZonaResidencia)
        {
            try
            {
                return vZonaResidenciaDAL.InsertarZonaResidencia(pZonaResidencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarZonaResidencia(ZonaResidencia pZonaResidencia)
        {
            try
            {
                return vZonaResidenciaDAL.ModificarZonaResidencia(pZonaResidencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarZonaResidencia(ZonaResidencia pZonaResidencia)
        {
            try
            {
                return vZonaResidenciaDAL.EliminarZonaResidencia(pZonaResidencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public ZonaResidencia ConsultarZonaResidencia(int pIdZonaResidencia)
        {
            try
            {
                return vZonaResidenciaDAL.ConsultarZonaResidencia(pIdZonaResidencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ZonaResidencia> ConsultarZonaResidencias(String pCodigoZonaResidencia, String pNombreZonaResidencia, Boolean? pEstado)
        {
            try
            {
                return vZonaResidenciaDAL.ConsultarZonaResidencias(pCodigoZonaResidencia, pNombreZonaResidencia, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
