using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// Definición de clase DocumentoIdentificacionTerceroBLL
    /// </summary>
    public class DocumentoIdentificacionTerceroBLL
    {
        /// <summary>
        /// Declaración de objeto DAL
        /// </summary>
        private DocumentoIdentificacionTerceroDAL vDocumentoIdentificacionTerceroDAL;
        /// <summary>
        /// Constructor de clase DocumentoIdentificacionTerceroBLL, se intancía el obeto DAL
        /// </summary>
        public DocumentoIdentificacionTerceroBLL()
        {
            vDocumentoIdentificacionTerceroDAL = new DocumentoIdentificacionTerceroDAL();
        }
        /// <summary>
        /// Inserta un Documento de Identificacion de Tercero
        /// </summary>
        /// <param name="pDocumentoIdentificacionTercero"></param>
        /// <returns></returns>
        public int InsertarDocumentoIdentificacionTercero(DocumentoIdentificacionTercero pDocumentoIdentificacionTercero)
        {
            try
            {
                return vDocumentoIdentificacionTerceroDAL.InsertarDocumentoIdentificacionTercero(pDocumentoIdentificacionTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        ///  Modifica un Documento de Identificacion de Tercero
        /// </summary>
        /// <param name="pDocumentoIdentificacionTercero"></param>
        /// <returns></returns>
        public int ModificarDocumentoIdentificacionTercero(DocumentoIdentificacionTercero pDocumentoIdentificacionTercero)
        {
            try
            {
                return vDocumentoIdentificacionTerceroDAL.ModificarDocumentoIdentificacionTercero(pDocumentoIdentificacionTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        ///  Elimina un Documento de Identificacion de Tercero
        /// </summary>
        /// <param name="pDocumentoIdentificacionTercero"></param>
        /// <returns></returns>
        public int EliminarDocumentoIdentificacionTercero(DocumentoIdentificacionTercero pDocumentoIdentificacionTercero)
        {
            try
            {
                return vDocumentoIdentificacionTerceroDAL.EliminarDocumentoIdentificacionTercero(pDocumentoIdentificacionTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        ///  Consultar un Documento de Identificacion de Terceros
        /// </summary>
        /// <param name="pIdTercero"></param>
        /// <returns></returns>
        public List<DocumentoIdentificacionTercero> ConsultarDocIdentificacionTerceros(int pIdTercero)
        {
            try
            {
                return vDocumentoIdentificacionTerceroDAL.ConsultarDocIdentificacionTerceros(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
