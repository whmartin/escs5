using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// Definiciíon de clase IdoneidadOferenteBLL
    /// </summary>
    public class IdoneidadOferenteBLL
    {
        /// <summary>
        /// Declaración de Objeto DAL
        /// Y Constructor donde se crea el objeto DAL vIdoneidadOferenteDAL
        /// </summary>
        private IdoneidadOferenteDAL vIdoneidadOferenteDAL;
        public IdoneidadOferenteBLL()
        {
            vIdoneidadOferenteDAL = new IdoneidadOferenteDAL();
        }

        /// <summary>
        /// Consultar Idoneidad Oferente
        /// </summary>
        /// <param name="IdTercero"></param>
        /// <returns></returns>
        public List<IdoneidadOferente> ConsultarIdoneidadOferente(int IdTercero)
        {
            try
            {
                return vIdoneidadOferenteDAL.ConsultarIdoneidadOferente(IdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
