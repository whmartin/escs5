using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class SistemaOperativoBLL
    {
        private SistemaOperativoDAL vSistemaOperativoDAL;
        public SistemaOperativoBLL()
        {
            vSistemaOperativoDAL = new SistemaOperativoDAL();
        }
        public int InsertarSistemaOperativo(SistemaOperativo pSistemaOperativo)
        {
            try
            {
                return vSistemaOperativoDAL.InsertarSistemaOperativo(pSistemaOperativo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarSistemaOperativo(SistemaOperativo pSistemaOperativo)
        {
            try
            {
                return vSistemaOperativoDAL.ModificarSistemaOperativo(pSistemaOperativo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarSistemaOperativo(SistemaOperativo pSistemaOperativo)
        {
            try
            {
                return vSistemaOperativoDAL.EliminarSistemaOperativo(pSistemaOperativo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SistemaOperativo ConsultarSistemaOperativo(int pIdSistemaOperativo)
        {
            try
            {
                return vSistemaOperativoDAL.ConsultarSistemaOperativo(pIdSistemaOperativo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SistemaOperativo> ConsultarSistemaOperativos(String pSistemaOperativo, String pDescripcion, String pEstado)
        {
            try
            {
                return vSistemaOperativoDAL.ConsultarSistemaOperativos(pSistemaOperativo, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarSistemaOperativo(String pSistemaOperativo)
        {
            try
            {
                return vSistemaOperativoDAL.ConsultarSistemaOperativo(pSistemaOperativo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
