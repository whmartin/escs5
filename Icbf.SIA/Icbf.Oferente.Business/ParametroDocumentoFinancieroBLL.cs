using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// Definición de clase ParametroDocumentoFinancieroBLL
    /// </summary>
    public class ParametroDocumentoFinancieroBLL
    {
        /// <summary>
        /// Declaración de Objeto DAL
        /// Y Constructo de clase donde se crea el Objeto DAL vParametroDocumentoFinancieroDAL
        /// </summary>
        private ParametroDocumentoFinancieroDAL vParametroDocumentoFinancieroDAL;
        public ParametroDocumentoFinancieroBLL()
        {
            vParametroDocumentoFinancieroDAL = new ParametroDocumentoFinancieroDAL();
        }
        /// <summary>
        /// Insertar Parametro Documento Financiero
        /// </summary>
        /// <param name="pParametroDocumentoFinanciero"></param>
        /// <returns></returns>
        public int InsertarParametroDocumentoFinanciero(ParametroDocumentoFinanciero pParametroDocumentoFinanciero)
        {
            try
            {
                return vParametroDocumentoFinancieroDAL.InsertarParametroDocumentoFinanciero(pParametroDocumentoFinanciero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modificar Parametro Documento Financiero
        /// </summary>
        /// <param name="pParametroDocumentoFinanciero"></param>
        /// <returns></returns>
        public int ModificarParametroDocumentoFinanciero(ParametroDocumentoFinanciero pParametroDocumentoFinanciero)
        {
            try
            {
                return vParametroDocumentoFinancieroDAL.ModificarParametroDocumentoFinanciero(pParametroDocumentoFinanciero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Eliminar Parametro Documento Financiero
        /// </summary>
        /// <param name="pParametroDocumentoFinanciero"></param>
        /// <returns></returns>
        public int EliminarParametroDocumentoFinanciero(ParametroDocumentoFinanciero pParametroDocumentoFinanciero)
        {
            try
            {
                return vParametroDocumentoFinancieroDAL.EliminarParametroDocumentoFinanciero(pParametroDocumentoFinanciero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Parametro Documento Financiero
        /// </summary>
        /// <param name="pIDParametroDocFinanciero"></param>
        /// <returns></returns>
        public ParametroDocumentoFinanciero ConsultarParametroDocumentoFinanciero(int pIDParametroDocFinanciero)
        {
            try
            {
                return vParametroDocumentoFinancieroDAL.ConsultarParametroDocumentoFinanciero(pIDParametroDocFinanciero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de Parametr oDocumento Financieros
        /// </summary>
        /// <param name="pDescripcion"></param>
        /// <param name="pIdModalidad"></param>
        /// <param name="pObligatorio"></param>
        /// <returns></returns>
        public List<ParametroDocumentoFinanciero> ConsultarParametroDocumentoFinancieros(String pDescripcion, int? pIdModalidad, Boolean? pObligatorio)
        {
            try
            {
                return vParametroDocumentoFinancieroDAL.ConsultarParametroDocumentoFinancieros(pDescripcion, pIdModalidad, pObligatorio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Parametro Documento Financieros_Documento
        /// </summary>
        /// <param name="pObligatorio"></param>
        /// <param name="rup"></param>
        /// <param name="Activo"></param>
        /// <returns></returns>
        public List<ParametroDocumentoFinanciero> ConsultarParametroDocumentoFinancieros_Documento(Boolean pObligatorio, Boolean rup, Boolean Activo)
        {
            try
            {
                return vParametroDocumentoFinancieroDAL.ConsultarParametroDocumentoFinancieros_Documento(pObligatorio,rup,Activo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Parametro Documento Financieros_Documento_ID
        /// </summary>
        /// <param name="pObligatorio"></param>
        /// <param name="vIDFinanciero"></param>
        /// <param name="rup"></param>
        /// <returns></returns>
        public List<ParametroDocumentoFinanciero> ConsultarParametroDocumentoFinancieros_Documento_ID(Boolean pObligatorio, int vIDFinanciero, Boolean? rup)
        {
            try
            {
                return vParametroDocumentoFinancieroDAL.ConsultarParametroDocumentoFinancieros_Documento_ID(pObligatorio,vIDFinanciero,rup);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
