using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// Definic��n de clase TelTercerosBLL
    /// </summary>
    public class TelTercerosBLL
    {
        /// <summary>
        /// Declaraci�n de Objeto DAL 
        /// Y Constructor de clase donde se crea el objeto DAL vTelTercerosDAL
        /// </summary>
        private TelTercerosDAL vTelTercerosDAL;
        public TelTercerosBLL()
        {
            vTelTercerosDAL = new TelTercerosDAL();
        }
        /// <summary>
        /// Insertar Tel Terceros
        /// </summary>
        /// <param name="pTelTerceros"></param>
        /// <returns></returns>
        public int InsertarTelTerceros(TelTerceros pTelTerceros)
        {
            try
            {
                return vTelTercerosDAL.InsertarTelTerceros(pTelTerceros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarTelTerceros(TelTerceros pTelTerceros)
        {
            try
            {
                return vTelTercerosDAL.ModificarTelTerceros(pTelTerceros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarTelTerceros(TelTerceros pTelTerceros)
        {
            try
            {
                return vTelTercerosDAL.EliminarTelTerceros(pTelTerceros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public TelTerceros ConsultarTelTerceros(int pIdTelTercero)
        {
            try
            {
                return vTelTercerosDAL.ConsultarTelTerceros(pIdTelTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        ///  Consultar Tel Terceros IdTercero
        /// </summary>
        /// <param name="pIdTercero"></param>
        /// <returns></returns>
        public TelTerceros ConsultarTelTercerosIdTercero(int pIdTercero)
        {
            try
            {
                return vTelTercerosDAL.ConsultarTelTercerosIdTercero(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TelTerceros> ConsultarTelTerceross(int? pIdTercero)
        {
            try
            {
                return vTelTercerosDAL.ConsultarTelTerceross(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
