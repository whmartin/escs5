﻿using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Business
{
    public class TipoObservacionesBLL
    {
        private TipoObservacionesDAL vTipoObservacionesDAL;
        public TipoObservacionesBLL()
        {
            vTipoObservacionesDAL = new TipoObservacionesDAL();
        }

        public List<TipoObservaciones> ConsultarTiposObservacioness(bool pEstado)
        {
            try
            {
                return vTipoObservacionesDAL.ConsultarTiposObservacioness(pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
