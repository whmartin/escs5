using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.SIA.Business;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// Definición de clase TerceroDatoAdicionalBLL
    /// </summary>
    public class TerceroDatoAdicionalBLL
    {
        /// <summary>
        /// Declaración de Objeto DAL
        /// Y Constructor donde se creal el objeto DAL
        /// </summary>
        private TerceroDatoAdicionalDAL vTerceroDatoAdicionalDAL;
        public TerceroDatoAdicionalBLL()
        {
            vTerceroDatoAdicionalDAL = new TerceroDatoAdicionalDAL();
        }
        /// <summary>
        /// Inserta un TerceroDatoAdicional
        /// </summary>
        /// <param name="pTerceroDatoAdicional"></param>
        /// <returns></returns>
        public int InsertarTerceroDatoAdicional(TerceroDatoAdicional pTerceroDatoAdicional)
        {
            try
            {
                return vTerceroDatoAdicionalDAL.InsertarTerceroDatoAdicional(pTerceroDatoAdicional);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Modifica un TerceroDatoAdicional
        /// </summary>
        /// <param name="pTerceroDatoAdicional"></param>
        /// <returns></returns>
        public int ModificarTerceroDatoAdicional(TerceroDatoAdicional pTerceroDatoAdicional)
        {
            try
            {
                return vTerceroDatoAdicionalDAL.ModificarTerceroDatoAdicional(pTerceroDatoAdicional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar TerceroDatoAdicional por Id
        /// </summary>
        /// <param name="pIdTerceroDatoAdicional"></param>
        /// <returns></returns>
        public TerceroDatoAdicional ConsultarTerceroDatoAdicional(int pIdTercero)
        {
            try
            {
                return vTerceroDatoAdicionalDAL.ConsultarTerceroDatoAdicional(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
