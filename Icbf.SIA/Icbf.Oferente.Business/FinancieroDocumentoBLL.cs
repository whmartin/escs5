using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// definición de calse FinancieroDocumentoBLL
    /// </summary>
    public class FinancieroDocumentoBLL
    {
        /// <summary>
        /// Declaración de objeto DAL 
        /// Y construnctor de clase donde se creal el objeto DAL vFinancieroDocumentoDAL
        /// </summary>
        private FinancieroDocumentoDAL vFinancieroDocumentoDAL;
        public FinancieroDocumentoBLL()
        {
            vFinancieroDocumentoDAL = new FinancieroDocumentoDAL();
        }
        /// <summary>
        /// Insertar Financiero Documento
        /// </summary>
        /// <param name="pFinancieroDocumento"></param>
        /// <returns></returns>
        public int InsertarFinancieroDocumento(FinancieroDocumento pFinancieroDocumento)
        {
            try
            {
                return vFinancieroDocumentoDAL.InsertarFinancieroDocumento(pFinancieroDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modificar Financiero Documento
        /// </summary>
        /// <param name="pFinancieroDocumento"></param>
        /// <returns></returns>
        public int ModificarFinancieroDocumento(FinancieroDocumento pFinancieroDocumento)
        {
            try
            {
                return vFinancieroDocumentoDAL.ModificarFinancieroDocumento(pFinancieroDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Financiero Documento
        /// </summary>
        /// <param name="pFinancieroDocumento"></param>
        /// <returns></returns>
        public int EliminarFinancieroDocumento(int pFinancieroDocumento)
        {
            try
            {
                return vFinancieroDocumentoDAL.EliminarFinancieroDocumento(pFinancieroDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Financiero Documento
        /// </summary>
        /// <param name="pIdFinancieroDocumento"></param>
        /// <returns></returns>
        public FinancieroDocumento ConsultarFinancieroDocumento(int pIdFinancieroDocumento)
        {
            try
            {
                return vFinancieroDocumentoDAL.ConsultarFinancieroDocumento(pIdFinancieroDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de Financiero Documentos
        /// </summary>
        /// <param name="pIdFinanciero"></param>
        /// <param name="pIdParametroDocumentoFinanciero"></param>
        /// <returns></returns>
        public List<FinancieroDocumento> ConsultarFinancieroDocumentos(int? pIdFinanciero, int? pIdParametroDocumentoFinanciero)
        {
            try
            {
                return vFinancieroDocumentoDAL.ConsultarFinancieroDocumentos(pIdFinanciero, pIdParametroDocumentoFinanciero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
