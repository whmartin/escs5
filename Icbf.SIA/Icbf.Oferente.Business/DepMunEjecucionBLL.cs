using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class DepMunEjecucionBLL
    {
        private DepMunEjecucionDAL vDepMunEjecucionDAL;
        public DepMunEjecucionBLL()
        {
            vDepMunEjecucionDAL = new DepMunEjecucionDAL();
        }
        public int InsertarDepMunEjecucion(DepMunEjecucion pDepMunEjecucion)
        {
            try
            {
                return vDepMunEjecucionDAL.InsertarDepMunEjecucion(pDepMunEjecucion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarDepMunEjecucion(DepMunEjecucion pDepMunEjecucion)
        {
            try
            {
                return vDepMunEjecucionDAL.ModificarDepMunEjecucion(pDepMunEjecucion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarDepMunEjecucion(DepMunEjecucion pDepMunEjecucion)
        {
            try
            {
                return vDepMunEjecucionDAL.EliminarDepMunEjecucion(pDepMunEjecucion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public DepMunEjecucion ConsultarDepMunEjecucion(int pIdDepMunEjecucion)
        {
            try
            {
                return vDepMunEjecucionDAL.ConsultarDepMunEjecucion(pIdDepMunEjecucion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DepMunEjecucion> ConsultarDepMunEjecucions(int vIdExpEntidad, int? pIdDepartamento, int? pIdMunicipio)
        {
            try
            {
                return vDepMunEjecucionDAL.ConsultarDepMunEjecucions(vIdExpEntidad, pIdDepartamento, pIdMunicipio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarDepMunEjecucionxDepartamento(int pIdExpEntidad, int pIdDepartamento)
        {
            try
            {
                return vDepMunEjecucionDAL.EliminarDepMunEjecucionxDepartamento(pIdExpEntidad, pIdDepartamento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
