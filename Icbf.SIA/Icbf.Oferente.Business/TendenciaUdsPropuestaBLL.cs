using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class TendenciaUdsPropuestaBLL
    {
        private TendenciaUdsPropuestaDAL vTendenciaUdsPropuestaDAL;
        public TendenciaUdsPropuestaBLL()
        {
            vTendenciaUdsPropuestaDAL = new TendenciaUdsPropuestaDAL();
        }
        public int InsertarTendenciaUdsPropuesta(TendenciaUdsPropuesta pTendenciaUdsPropuesta)
        {
            try
            {
                return vTendenciaUdsPropuestaDAL.InsertarTendenciaUdsPropuesta(pTendenciaUdsPropuesta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarTendenciaUdsPropuesta(TendenciaUdsPropuesta pTendenciaUdsPropuesta)
        {
            try
            {
                return vTendenciaUdsPropuestaDAL.ModificarTendenciaUdsPropuesta(pTendenciaUdsPropuesta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarTendenciaUdsPropuesta(TendenciaUdsPropuesta pTendenciaUdsPropuesta)
        {
            try
            {
                return vTendenciaUdsPropuestaDAL.EliminarTendenciaUdsPropuesta(pTendenciaUdsPropuesta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public TendenciaUdsPropuesta ConsultarTendenciaUdsPropuesta(int pIdTendenciaUdsPropuesta)
        {
            try
            {
                return vTendenciaUdsPropuestaDAL.ConsultarTendenciaUdsPropuesta(pIdTendenciaUdsPropuesta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TendenciaUdsPropuesta> ConsultarTendenciaUdsPropuestas(String pCodigoTendenciaUdsPropuesta, String pNombreTendenciaUdsPropuesta, Boolean? pEstado)
        {
            try
            {
                return vTendenciaUdsPropuestaDAL.ConsultarTendenciaUdsPropuestas(pCodigoTendenciaUdsPropuesta, pNombreTendenciaUdsPropuesta, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
