﻿using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;

namespace Icbf.Oferente.Business
{
    public class EquipoEvaluadoresBLL
    {
        private EquipoEvaluadoresDAL vEquipoEvaluadoresDAL;

        public EquipoEvaluadoresBLL()
        {
            vEquipoEvaluadoresDAL = new EquipoEvaluadoresDAL();
        }

        public int InsertarEquipoEvaluadores(EquipoEvaluadores pEquipoEvaluadores)
        {
            try
            {
                return vEquipoEvaluadoresDAL.InsertarEquipoEvaluadores(pEquipoEvaluadores);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<EquipoEvaluadores> ConsultarEquiposEvaluadoress(int pIdUsuarioCoordinador)
        {
            try
            {
                return vEquipoEvaluadoresDAL.ConsultarEquiposEvaluadoress(pIdUsuarioCoordinador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<EquipoEvaluadores> ConsultarEquiposEvaluadoresss(string pNumeroDocumento, string pNombreCoordinador, string pNombreEquipoEvaluador)
        {
            try
            {
                return vEquipoEvaluadoresDAL.ConsultarEquiposEvaluadoresss(pNumeroDocumento, pNombreCoordinador, pNombreEquipoEvaluador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public EquipoEvaluadores ConsultarEquipoEvaluador(int pIdEquipoEvaluador)
        {
            try
            {
                return vEquipoEvaluadoresDAL.ConsultarEquipoEvaluador(pIdEquipoEvaluador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarEquipoEvaluador(EquipoEvaluadores pEquipoEvaluador)
        {
            try
            {
                return vEquipoEvaluadoresDAL.ModificarEquipoEvaluador(pEquipoEvaluador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<EquipoEvaluadores> ConsultarTodosEquiposEvaluadoress()
        {
            try
            {
                return vEquipoEvaluadoresDAL.ConsultarTodosEquiposEvaluadoress();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
