using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// Definición de clase TipoPersonaBLL
    /// </summary>
    public class TipoPersonaBLL
    {
        /// <summary>
        /// Declaración de Objeto DAL
        /// Y Constructor de clase donde se crea el Objeto DAL vTipoPersonaDAL 
        /// </summary>
        private TipoPersonaDAL vTipoPersonaDAL;
        public TipoPersonaBLL()
        {
            vTipoPersonaDAL = new TipoPersonaDAL();
        }

        /// <summary>
        ///  Consultar TipoPersona
        /// </summary>
        /// <param name="pIdTipoPersona"></param>
        /// <returns></returns>
        public TipoPersona ConsultarTipoPersona(int pIdTipoPersona)
        {
            try
            {
                return vTipoPersonaDAL.ConsultarTipoPersona(pIdTipoPersona);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consulta Tipo Personas
        /// </summary>
        /// <param name="pCodigoTipoPersona"></param>
        /// <param name="pNombreTipoPersona"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoPersona> ConsultarTipoPersonas(String pCodigoTipoPersona, String pNombreTipoPersona, Boolean? pEstado)
        {
            try
            {
                return vTipoPersonaDAL.ConsultarTipoPersonas(pCodigoTipoPersona, pNombreTipoPersona, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
