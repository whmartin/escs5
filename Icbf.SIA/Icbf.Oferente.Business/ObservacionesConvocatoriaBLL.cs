﻿using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Business
{
    public class ObservacionesConvocatoriaBLL
    {

        private ObservacionesConvocatoriaDAL vObservacionesConvocatoriaDAL;

        public ObservacionesConvocatoriaBLL()
        {
            vObservacionesConvocatoriaDAL = new ObservacionesConvocatoriaDAL();
        }

        public List<ObservacionesConvocatoria> ConsultarObservacionessConvocatoria(int pIdUsuario)
        {
            try
            {
                return vObservacionesConvocatoriaDAL.ConsultarObservacionessConvocatoria(pIdUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
