using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// Definici�n de clase PoblacionAtendidaBLL
    /// </summary>
    public class PoblacionAtendidaBLL
    {
        /// <summary>
        /// declareaci�pn de objeto DAL 
        /// Y Constructor de clase donde se crea el objeto dAL vPoblacionAtendidaDAL
        /// </summary>
        private PoblacionAtendidaDAL vPoblacionAtendidaDAL;
        public PoblacionAtendidaBLL()
        {
            vPoblacionAtendidaDAL = new PoblacionAtendidaDAL();
        }
        /// <summary>
        /// Insertar Poblacion Atendida
        /// </summary>
        /// <param name="pPoblacionAtendida"></param>
        /// <returns></returns>
        public int InsertarPoblacionAtendida(PoblacionAtendida pPoblacionAtendida)
        {
            try
            {
                return vPoblacionAtendidaDAL.InsertarPoblacionAtendida(pPoblacionAtendida);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modificar Poblacion Atendida
        /// </summary>
        /// <param name="pPoblacionAtendida"></param>
        /// <returns></returns>
        public int ModificarPoblacionAtendida(PoblacionAtendida pPoblacionAtendida)
        {
            try
            {
                return vPoblacionAtendidaDAL.ModificarPoblacionAtendida(pPoblacionAtendida);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        ///  Elimina rPoblacion Atendida
        /// </summary>
        /// <param name="pPoblacionAtendida"></param>
        /// <returns></returns>
        public int EliminarPoblacionAtendida(PoblacionAtendida pPoblacionAtendida)
        {
            try
            {
                return vPoblacionAtendidaDAL.EliminarPoblacionAtendida(pPoblacionAtendida);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Poblacion Atendida
        /// </summary>
        /// <param name="pIdPoblacionAtendida"></param>
        /// <returns></returns>
        public PoblacionAtendida ConsultarPoblacionAtendida(int pIdPoblacionAtendida)
        {
            try
            {
                return vPoblacionAtendidaDAL.ConsultarPoblacionAtendida(pIdPoblacionAtendida);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de Poblaciones Atendidas
        /// </summary>
        /// <param name="pPoblacionAtendida"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<PoblacionAtendida> ConsultarPoblacionAtendidas(String pPoblacionAtendida, String pDescripcion, String pEstado)
        {
            try
            {
                return vPoblacionAtendidaDAL.ConsultarPoblacionAtendidas(pPoblacionAtendida, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Poblacion Atendida
        /// </summary>
        /// <param name="pPoblacionAtendida"></param>
        /// <returns></returns>
        public int ConsultarPoblacionAtendida(String pPoblacionAtendida)
        {
            try
            {
                return vPoblacionAtendidaDAL.ConsultarPoblacionAtendida(pPoblacionAtendida);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
