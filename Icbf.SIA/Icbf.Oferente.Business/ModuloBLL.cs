﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// Definición de clase ModuloBLL
    /// </summary>
    public class ModuloBLL
    {
        /// <summary>
        /// Declasración de objeto DAL
        /// Y Constructor donde se crea el Objeto DAL  vModuloDAL
        /// </summary>
        private ModuloDAL vModuloDAL;
        public ModuloBLL()
        {
            vModuloDAL = new ModuloDAL();
        }
        /// <summary>
        /// Insertar un Modulo
        /// </summary>
        /// <param name="pModulo"></param>
        /// <returns></returns>
        public int InsertarModulo(Modulo pModulo)
        {
            try
            {
                return vModuloDAL.InsertarModulo(pModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modificar un Modulo
        /// </summary>
        /// <param name="pModulo"></param>
        /// <returns></returns>
        public int ModificarModulo(Modulo pModulo)
        {
            try
            {
                return vModuloDAL.ModificarModulo(pModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Eliminar un Modulo
        /// </summary>
        /// <param name="pModulo"></param>
        /// <returns></returns>
        public int EliminarModulo(Modulo pModulo)
        {
            try
            {
                return vModuloDAL.EliminarModulo(pModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Orden Modulo
        /// </summary>
        /// <param name="IdEntidad"></param>
        /// <param name="IdModulo"></param>
        /// <returns></returns>
        public int EliminarOrdenModulo(int IdEntidad, int IdModulo)
        {
            try
            {
                return vModuloDAL.EliminarOrdenModulo(IdEntidad, IdModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Modulo
        /// </summary>
        /// <param name="pIdModulo"></param>
        /// <returns></returns>
        public Modulo ConsultarModulo(int pIdModulo)
        {
            try
            {
                return vModuloDAL.ConsultarModulo(pIdModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Una lista de Modulos
        /// </summary>
        /// <param name="pNombreModulo"></param>
        /// <param name="pOrden"></param>
        /// <param name="pEstado"></param>
        /// <param name="pRuta"></param>
        /// <returns></returns>
        public List<Modulo> ConsultarModulos(String pNombreModulo, int? pOrden, String pEstado, String pRuta)
        {
            try
            {
                return vModuloDAL.ConsultarModulos(pNombreModulo, pOrden, pEstado, pRuta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Modulos Orden
        /// </summary>
        /// <param name="pNombreModulo"></param>
        /// <param name="pOrden"></param>
        /// <param name="pEstado"></param>
        /// <param name="pRuta"></param>
        /// <returns></returns>
        public List<Modulo> ConsultarModulosOrden(String pNombreModulo, int? pOrden, String pEstado, String pRuta)
        {
            try
            {
                return vModuloDAL.ConsultarModulosOrden(pNombreModulo, pOrden, pEstado, pRuta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Modulo ConsultarModuloCodigo(int pCodigo)
        {
            try
            {
                return vModuloDAL.ConsultarModuloCodigo(pCodigo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


    }
}
