using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class PlantaFisicaUdsBLL
    {
        private PlantaFisicaUdsDAL vPlantaFisicaUdsDAL;
        public PlantaFisicaUdsBLL()
        {
            vPlantaFisicaUdsDAL = new PlantaFisicaUdsDAL();
        }
        public int InsertarPlantaFisicaUds(PlantaFisicaUds pPlantaFisicaUds)
        {
            try
            {
                return vPlantaFisicaUdsDAL.InsertarPlantaFisicaUds(pPlantaFisicaUds);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarPlantaFisicaUds(PlantaFisicaUds pPlantaFisicaUds)
        {
            try
            {
                return vPlantaFisicaUdsDAL.ModificarPlantaFisicaUds(pPlantaFisicaUds);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarPlantaFisicaUds(PlantaFisicaUds pPlantaFisicaUds)
        {
            try
            {
                return vPlantaFisicaUdsDAL.EliminarPlantaFisicaUds(pPlantaFisicaUds);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public PlantaFisicaUds ConsultarPlantaFisicaUds(int pIdPlantaFisicaUds)
        {
            try
            {
                return vPlantaFisicaUdsDAL.ConsultarPlantaFisicaUds(pIdPlantaFisicaUds);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<PlantaFisicaUds> ConsultarPlantaFisicaUdss(String pCodigoPlantaFisicaUds, String pNombrePlantaFisicaUds, Boolean? pEstado)
        {
            try
            {
                return vPlantaFisicaUdsDAL.ConsultarPlantaFisicaUdss(pCodigoPlantaFisicaUds, pNombrePlantaFisicaUds, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
