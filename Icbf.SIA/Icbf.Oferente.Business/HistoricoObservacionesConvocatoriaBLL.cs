﻿using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Business
{
    public class HistoricoObservacionesConvocatoriaBLL
    {
        private HistoricoObservacionesConvocatoriaDAL vHistoricoObservacionesConvocatoriaDAL;

        public HistoricoObservacionesConvocatoriaBLL()
        {
            vHistoricoObservacionesConvocatoriaDAL = new HistoricoObservacionesConvocatoriaDAL();
        }


        public int InsertarHistoricoObservacionesConvocatoria(HistoricoObservacionesConvocatoria pHistoricoObservacionesConvocatoria)
        {
            try
            {
                return vHistoricoObservacionesConvocatoriaDAL.InsertarHistoricoObservacionesConvocatoria(pHistoricoObservacionesConvocatoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public HistoricoObservacionesConvocatoria ConsultarHistoricoObservacionesConvocatoriaPorGestionMasReciente(int pIdGestionObservacionNueva)
        {
            try
            {
                return vHistoricoObservacionesConvocatoriaDAL.ConsultarHistoricoObservacionesConvocatoriaPorGestionMasReciente(pIdGestionObservacionNueva);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
