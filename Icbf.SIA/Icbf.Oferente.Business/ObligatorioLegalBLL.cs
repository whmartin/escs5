using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class ObligatorioLegalBLL
    {
        private ObligatorioLegalDAL vObligatorioLegalDAL;

        public ObligatorioLegalBLL()
        {
            vObligatorioLegalDAL = new ObligatorioLegalDAL();
        }
        public int InsertarObligatorioLegal(ObligatorioLegal pObligatorioLegal)
        {
            try
            {
                return vObligatorioLegalDAL.InsertarObligatorioLegal(pObligatorioLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarObligatorioLegal(ObligatorioLegal pObligatorioLegal)
        {
            try
            {
                return vObligatorioLegalDAL.ModificarObligatorioLegal(pObligatorioLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarObligatorioLegal(ObligatorioLegal pObligatorioLegal)
        {
            try
            {
                return vObligatorioLegalDAL.EliminarObligatorioLegal(pObligatorioLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ObligatorioLegal ConsultarObligatorioLegal(int pIdObligatorioLegal)
        {
            try
            {
                return vObligatorioLegalDAL.ConsultarObligatorioLegal(pIdObligatorioLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ObligatorioLegal> ConsultarObligatorioLegals(String pCodigoObligatorioLegal, String pDescripcion, String pEstado)
        {
            try
            {
                return vObligatorioLegalDAL.ConsultarObligatorioLegals(pCodigoObligatorioLegal, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarCodigoObligatorioLegal(String pCodigoObligatorioLegal)
        {
            try
            {
                return vObligatorioLegalDAL.ConsultarCodigoObligatorioLegal(pCodigoObligatorioLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
