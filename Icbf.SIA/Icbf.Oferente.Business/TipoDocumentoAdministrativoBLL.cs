using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// Definición de clase TipoDocumentoAdministrativoBLL
    /// </summary>
    public class TipoDocumentoAdministrativoBLL
    {
        /// <summary>
        /// Declaración de Objeto DAL
        /// Y Construcción donde se crea el Objeto DAL vTipoDocumentoAdministrativoDAL
        /// </summary>
        private TipoDocumentoAdministrativoDAL vTipoDocumentoAdministrativoDAL;
        //
        public TipoDocumentoAdministrativoBLL()
        {
            vTipoDocumentoAdministrativoDAL = new TipoDocumentoAdministrativoDAL();
        }
        /// <summary>
        /// Insertar Tipo Documento Administrativo
        /// </summary>
        /// <param name="pTipoDocumentoAdministrativo"></param>
        /// <returns></returns>
        public int InsertarTipoDocumentoAdministrativo(TipoDocumentoAdministrativo pTipoDocumentoAdministrativo)
        {
            try
            {
                return vTipoDocumentoAdministrativoDAL.InsertarTipoDocumentoAdministrativo(pTipoDocumentoAdministrativo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modificar Tipo Documento Administrativo
        /// </summary>
        /// <param name="pTipoDocumentoAdministrativo"></param>
        /// <returns></returns>
        public int ModificarTipoDocumentoAdministrativo(TipoDocumentoAdministrativo pTipoDocumentoAdministrativo)
        {
            try
            {
                return vTipoDocumentoAdministrativoDAL.ModificarTipoDocumentoAdministrativo(pTipoDocumentoAdministrativo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Tipo Documento Administrativo
        /// </summary>
        /// <param name="pTipoDocumentoAdministrativo"></param>
        /// <returns></returns>
        public int EliminarTipoDocumentoAdministrativo(TipoDocumentoAdministrativo pTipoDocumentoAdministrativo)
        {
            try
            {
                return vTipoDocumentoAdministrativoDAL.EliminarTipoDocumentoAdministrativo(pTipoDocumentoAdministrativo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Tipo Documento Administrativo
        /// </summary>
        /// <param name="pIdTipoDocAdministrativo"></param>
        /// <returns></returns>
        public TipoDocumentoAdministrativo ConsultarTipoDocumentoAdministrativo(int pIdTipoDocAdministrativo)
        {
            try
            {
                return vTipoDocumentoAdministrativoDAL.ConsultarTipoDocumentoAdministrativo(pIdTipoDocAdministrativo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Tipo Documento Administrativos
        /// </summary>
        /// <param name="pIdAdministrativo"></param>
        /// <param name="pIdTipoDocAdministrativo"></param>
        /// <param name="pPersonal"></param>
        /// <param name="pInstituciones"></param>
        /// <param name="pSeleccion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoDocumentoAdministrativo> ConsultarTipoDocumentoAdministrativos(int? pIdAdministrativo, int? pIdTipoDocAdministrativo, int? pPersonal, int? pInstituciones, int? pSeleccion, int? pEstado)
        {
            try
            {
                return vTipoDocumentoAdministrativoDAL.ConsultarTipoDocumentoAdministrativos(pIdAdministrativo, pIdTipoDocAdministrativo, pPersonal, pInstituciones, pSeleccion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
