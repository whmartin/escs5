using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// Definición de clase AdministrativoDocumentoBLL
    /// </summary>
    public class AdministrativoDocumentoBLL
    {
        /// <summary>
        /// Declaración de objeto DAL
        /// Y Constructor donde se creal el objeto DAL vAdministrativoDocumentoDAL
        /// </summary>
        private AdministrativoDocumentoDAL vAdministrativoDocumentoDAL;
        public AdministrativoDocumentoBLL()
        {
            vAdministrativoDocumentoDAL = new AdministrativoDocumentoDAL();
        }
        /// <summary>
        /// Insertar Administrativo Documento
        /// </summary>
        /// <param name="pAdministrativoDocumento"></param>
        /// <returns></returns>
        public int InsertarAdministrativoDocumento(AdministrativoDocumento pAdministrativoDocumento)
        {
            try
            {
                return vAdministrativoDocumentoDAL.InsertarAdministrativoDocumento(pAdministrativoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modificar Administrativo Documento
        /// </summary>
        /// <param name="pAdministrativoDocumento"></param>
        /// <returns></returns>
        public int ModificarAdministrativoDocumento(AdministrativoDocumento pAdministrativoDocumento)
        {
            try
            {
                return vAdministrativoDocumentoDAL.ModificarAdministrativoDocumento(pAdministrativoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        ///  Eliminar Administrativo Documento
        /// </summary>
        /// <param name="pAdministrativoDocumento"></param>
        /// <returns></returns>
        public int EliminarAdministrativoDocumento(AdministrativoDocumento pAdministrativoDocumento)
        {
            try
            {
                return vAdministrativoDocumentoDAL.EliminarAdministrativoDocumento(pAdministrativoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Administrativo Documento
        /// </summary>
        /// <param name="pIdAdministrativoDocumento"></param>
        /// <returns></returns>
        public AdministrativoDocumento ConsultarAdministrativoDocumento(int pIdAdministrativoDocumento)
        {
            try
            {
                return vAdministrativoDocumentoDAL.ConsultarAdministrativoDocumento(pIdAdministrativoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Lista de  Administrativo Documentos
        /// </summary>
        /// <param name="pIdAdministrativo"></param>
        /// <param name="pIdTipoDocAdministrativo"></param>
        /// <returns></returns>
        public List<AdministrativoDocumento> ConsultarAdministrativoDocumentos(int? pIdAdministrativo, int? pIdTipoDocAdministrativo)
        {
            try
            {
                return vAdministrativoDocumentoDAL.ConsultarAdministrativoDocumentos(pIdAdministrativo, pIdTipoDocAdministrativo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
