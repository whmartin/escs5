﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// Definición de clase RankingOferenteBLL
    /// </summary>
    public class RankingOferenteBLL
    {
        /// <summary>
        /// Declaración de objetoDAL
        /// Y Constructor donde se crea Objeto DAL  vRankingOferenteDAL
        /// </summary>
        private RankingOferenteDAL vRankingOferenteDAL;
        public RankingOferenteBLL()
        {
            vRankingOferenteDAL = new RankingOferenteDAL();
        }
        /// <summary>
        /// Inserta un Ranking Oferente
        /// </summary>
        /// <param name="pIdoneidadOferente"></param>
        /// <returns></returns>
        public int InsertarRankingOferente(IdoneidadOferente pIdoneidadOferente)
        {
            try
            {
                return vRankingOferenteDAL.InsertarRankingOferente(pIdoneidadOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        ///  Modifica un Ranking Oferente
        /// </summary>
        /// <param name="pIdoneidadOferente"></param>
        /// <returns></returns>
        public int ModificarRankingOferente(IdoneidadOferente pIdoneidadOferente)
        {
            try
            {
                return vRankingOferenteDAL.ModificarRankingOferente(pIdoneidadOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Calcular Ranking Oferente
        /// </summary>
        /// <param name="pIdEntidad"></param>
        /// <param name="pIdTercero"></param>
        /// <returns></returns>
        public int CalcularRankingOferente(int pIdEntidad, int pIdTercero)
        {
            try
            {
                return vRankingOferenteDAL.CalcularRankingOferente(pIdEntidad, pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}

