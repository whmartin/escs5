using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class DocumentoIdentificacionBLL
    {
        private DocumentoIdentificacionDAL vDocumentoIdentificacionDAL;
        public DocumentoIdentificacionBLL()
        {
            vDocumentoIdentificacionDAL = new DocumentoIdentificacionDAL();
        }
        public int InsertarDocumentoIdentificacion(DocumentoIdentificacion pDocumentoIdentificacion)
        {
            try
            {
                return vDocumentoIdentificacionDAL.InsertarDocumentoIdentificacion(pDocumentoIdentificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarDocumentoIdentificacion(DocumentoIdentificacion pDocumentoIdentificacion)
        {
            try
            {
                return vDocumentoIdentificacionDAL.ModificarDocumentoIdentificacion(pDocumentoIdentificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarDocumentoIdentificacion(DocumentoIdentificacion pDocumentoIdentificacion)
        {
            try
            {
                return vDocumentoIdentificacionDAL.EliminarDocumentoIdentificacion(pDocumentoIdentificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DocumentoIdentificacion ConsultarDocumentoIdentificacion(int pIdDocumentoIdentificacion)
        {
            try
            {
                return vDocumentoIdentificacionDAL.ConsultarDocumentoIdentificacion(pIdDocumentoIdentificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DocumentoIdentificacion> ConsultarDocumentoIdentificacions(String pCodigoDocumentoIdentificacion, String pDescripcion, String pEstado)
        {
            try
            {
                return vDocumentoIdentificacionDAL.ConsultarDocumentoIdentificacions(pCodigoDocumentoIdentificacion, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarCodigoDocumentoIdentificacion(String pCodigoDocumentoIdentificacion)
        {
            try
            {
                return vDocumentoIdentificacionDAL.ConsultarCodigoDocumentoIdentificacion(pCodigoDocumentoIdentificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
