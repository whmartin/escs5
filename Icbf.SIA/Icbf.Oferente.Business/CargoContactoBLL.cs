using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// Definición de clase CargoContactoBLL
    /// </summary>
    public class CargoContactoBLL
    {
        /// <summary>
        /// Declaración de onjeto DAL
        /// Y Contructor de clase donde se crea el objeto DAL vCargoContactoDAL
        /// </summary>
        private CargoContactoDAL vCargoContactoDAL;
        public CargoContactoBLL()
        {
            vCargoContactoDAL = new CargoContactoDAL();
        }

        /// <summary>
        /// Insertar Cargo Contacto
        /// </summary>
        /// <param name="pCargoContacto"></param>
        /// <returns></returns>
        public int InsertarCargoContacto(CargoContacto pCargoContacto)
        {
            try
            {
                return vCargoContactoDAL.InsertarCargoContacto(pCargoContacto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modificar Cargo Contacto
        /// </summary>
        /// <param name="pCargoContacto"></param>
        /// <returns></returns>
        public int ModificarCargoContacto(CargoContacto pCargoContacto)
        {
            try
            {
                return vCargoContactoDAL.ModificarCargoContacto(pCargoContacto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Cargo Contacto
        /// </summary>
        /// <param name="pCargoContacto"></param>
        /// <returns></returns>
        public int EliminarCargoContacto(CargoContacto pCargoContacto)
        {
            try
            {
                return vCargoContactoDAL.EliminarCargoContacto(pCargoContacto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Cargo Contacto
        /// </summary>
        /// <param name="pIdCargoContacto"></param>
        /// <returns></returns>
        public CargoContacto ConsultarCargoContacto(int pIdCargoContacto)
        {
            try
            {
                return vCargoContactoDAL.ConsultarCargoContacto(pIdCargoContacto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista Cargo Contactos
        /// </summary>
        /// <param name="pCodigoCargoContacto"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<CargoContacto> ConsultarCargoContactos(String pCodigoCargoContacto, String pDescripcion, String pEstado)
        {
            try
            {
                return vCargoContactoDAL.ConsultarCargoContactos(pCodigoCargoContacto, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Codigo Cargo Contacto
        /// </summary>
        /// <param name="pCodigoCargoContacto"></param>
        /// <returns></returns>
        public int ConsultarCodigoCargoContacto(String pCodigoCargoContacto)
        {
            try
            {
                return vCargoContactoDAL.ConsultarCodigoCargoContacto(pCodigoCargoContacto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
