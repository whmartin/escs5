using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// Definición de clase AdministrativoBLL
    /// </summary>
    public class AdministrativoBLL
    {
        /// <summary>
        /// Declaración de objeto DAL
        /// Y Constructor donde se crea el objeto DAL vAdministrativoDAL
        /// </summary>
        private AdministrativoDAL vAdministrativoDAL;
        public AdministrativoBLL()
        {
            vAdministrativoDAL = new AdministrativoDAL();
        }
        /// <summary>
        /// Insertar Administrativo
        /// </summary>
        /// <param name="pAdministrativo"></param>
        /// <returns></returns>
        public int InsertarAdministrativo(Administrativo pAdministrativo)
        {
            try
            {
                return vAdministrativoDAL.InsertarAdministrativo(pAdministrativo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modificar Administrativo
        /// </summary>
        /// <param name="pAdministrativo"></param>
        /// <returns></returns>
        public int ModificarAdministrativo(Administrativo pAdministrativo)
        {
            try
            {
                return vAdministrativoDAL.ModificarAdministrativo(pAdministrativo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Administrativo
        /// </summary>
        /// <param name="pAdministrativo"></param>
        /// <returns></returns>
        public int EliminarAdministrativo(Administrativo pAdministrativo)
        {
            try
            {
                return vAdministrativoDAL.EliminarAdministrativo(pAdministrativo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Administrativo
        /// </summary>
        /// <param name="pIdAdministrativo"></param>
        /// <returns></returns>
        public Administrativo ConsultarAdministrativo(int pIdAdministrativo)
        {
            try
            {
                return vAdministrativoDAL.ConsultarAdministrativo(pIdAdministrativo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una Lista de Administrativos
        /// </summary>
        /// <param name="pIdOferente"></param>
        /// <param name="pAnho"></param>
        /// <returns></returns>
        public List<Administrativo> ConsultarAdministrativos(Decimal pIdOferente, int? pAnho)
        {
            try
            {
                return vAdministrativoDAL.ConsultarAdministrativos(pIdOferente, pAnho);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
