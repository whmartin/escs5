using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class TipoRegimenTributarioBLL
    {
        private TipoRegimenTributarioDAL vTipoRegimenTributarioDAL;
        public TipoRegimenTributarioBLL()
        {
            vTipoRegimenTributarioDAL = new TipoRegimenTributarioDAL();
        }
        public int InsertarTipoRegimenTributario(TipoRegimenTributario pTipoRegimenTributario)
        {
            try
            {
                return vTipoRegimenTributarioDAL.InsertarTipoRegimenTributario(pTipoRegimenTributario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarTipoRegimenTributario(TipoRegimenTributario pTipoRegimenTributario)
        {
            try
            {
                return vTipoRegimenTributarioDAL.ModificarTipoRegimenTributario(pTipoRegimenTributario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarTipoRegimenTributario(TipoRegimenTributario pTipoRegimenTributario)
        {
            try
            {
                return vTipoRegimenTributarioDAL.EliminarTipoRegimenTributario(pTipoRegimenTributario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public TipoRegimenTributario ConsultarTipoRegimenTributario(int pIdTipoRegimenTributario)
        {
            try
            {
                return vTipoRegimenTributarioDAL.ConsultarTipoRegimenTributario(pIdTipoRegimenTributario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoRegimenTributario> ConsultarTipoRegimenTributarios(String pCodigoTipoRegimenTributario, String pNombreTipoRegimenTributario, Boolean? pEstado)
        {
            try
            {
                return vTipoRegimenTributarioDAL.ConsultarTipoRegimenTributarios(pCodigoTipoRegimenTributario, pNombreTipoRegimenTributario, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
