using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class EstadoContratoBLL
    {
        private EstadoContratoDAL vEstadoContratoDAL;
        public EstadoContratoBLL()
        {
            vEstadoContratoDAL = new EstadoContratoDAL();
        }
        public int InsertarEstadoContrato(EstadoContrato pEstadoContrato)
        {
            try
            {
                return vEstadoContratoDAL.InsertarEstadoContrato(pEstadoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarEstadoContrato(EstadoContrato pEstadoContrato)
        {
            try
            {
                return vEstadoContratoDAL.ModificarEstadoContrato(pEstadoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarEstadoContrato(EstadoContrato pEstadoContrato)
        {
            try
            {
                return vEstadoContratoDAL.EliminarEstadoContrato(pEstadoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public EstadoContrato ConsultarEstadoContrato(int pIdEstadoContrato)
        {
            try
            {
                return vEstadoContratoDAL.ConsultarEstadoContrato(pIdEstadoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<EstadoContrato> ConsultarEstadoContratos(String pCodigoEstadoContrato, String pDescripcion, String pEstado)
        {
            try
            {
                return vEstadoContratoDAL.ConsultarEstadoContratos(pCodigoEstadoContrato, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarCodigoEstadoContrato(String pCodigoEstadoContrato)
        {
            try
            {
                return vEstadoContratoDAL.ConsultarCodigoEstadoContrato(pCodigoEstadoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
