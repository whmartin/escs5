﻿using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Business
{
    public class EstadoObservacionesBLL
    {
        private EstadoObservacionesDAL vEstadoObservacionesDAL;
        public EstadoObservacionesBLL()
        {
            vEstadoObservacionesDAL = new EstadoObservacionesDAL();
        }
        public EstadoObservaciones ConsultarEstadoObservacionPorCodigoEstado(string pCodigoEstado)
        {
            try
            {
                return vEstadoObservacionesDAL.ConsultarEstadoObservacionPorCodigoEstado(pCodigoEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
