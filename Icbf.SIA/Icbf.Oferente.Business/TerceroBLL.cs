using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.SIA.Business;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// Definici�n de clase TerceroBLL
    /// </summary>
    public class TerceroBLL
    {
        /// <summary>
        /// Declaraci�n de Objeto DAL
        /// Y Constructor donde se creal el objeto DAL
        /// </summary>
        private TerceroDAL vTerceroDAL;
        public TerceroBLL()
        {
            vTerceroDAL = new TerceroDAL();
        }
        /// <summary>
        /// Insertar Representante Legal
        /// </summary>
        /// <param name="pTercero"></param>
        /// <returns></returns>
        public int InsertarRepresentanteLegal(Tercero pTercero)
        {
            try
            {
                return vTerceroDAL.InsertarRepresentanteLegal(pTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Insertar Tercero Proveedor
        /// </summary>
        /// <param name="pTercero"></param>
        /// <returns></returns>
        public int InsertarTerceroProveedor(Tercero pTercero)
        {
            try
            {
                return vTerceroDAL.InsertarTerceroProveedor(pTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Inserta un Tercero
        /// </summary>
        /// <param name="pTercero"></param>
        /// <returns></returns>
        public int InsertarTercero(Tercero pTercero)
        {
            try
            {
                validacionesTerceroInsert(pTercero);
                //if (pTercero.ProviderUserKey == null) 
                //{
                //    UsuarioBLL objUsuarioBLL = new UsuarioBLL();
                //    var vUsuario = new Usuario
                //    {
                //        NumeroDocumento = pTercero.NumeroIdentificacion ,
                //        DV = pTercero.DigitoVerificacion.ToString() ,
                //        PrimerNombre = pTercero.PrimerNombre .Trim(),
                //        SegundoNombre = pTercero.SegundoNombre,
                //        PrimerApellido = pTercero.PrimerApellido,
                //        SegundoApellido = pTercero.SegundoApellido ,
                //        CorreoElectronico = pTercero.Email.Trim(),
                //        NombreUsuario = pTercero.Email.Trim(),
                //        Contrasena = pTercero.Email.Trim(),
                //        IdTipoDocumento = pTercero.IdDListaTipoDocumento ,
                //        IdTipoPersona = int.Parse(pTercero.IdTipoPersona.ToString()) ,
                //        RazonSocial = pTercero.RazonSocial.Trim(),
                //        Rol = "PROVEEDORES",
                //        //CodigoValidacion = Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 15),
                //        Estado = true,
                //        UsuarioCreacion = "Administrador"
                //    };
                //    var vResultado = objUsuarioBLL.InsertarUsuario(vUsuario);
                //    if (vResultado != 1) new Exception("La operaci�n no se complet� satisfactoriamente, verifique por favor.");
                //    Usuario obj = objUsuarioBLL.ConsultarUsuario(pTercero.IdDListaTipoDocumento, pTercero.NumeroIdentificacion);
                //    pTercero.ProviderUserKey = obj.Providerkey;
                //}

                return vTerceroDAL.InsertarTercero(pTercero);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Inserta un Tercero
        /// </summary>
        /// <param name="pTercero"></param>
        /// <returns></returns>
        public int InsertarCargarTercero(Tercero pTercero)
        {
            try
            {
                return vTerceroDAL.InsertarCargarTercero(pTercero);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Inserta Tercero con IdTemporal
        /// </summary>
        /// <param name="pTercero"></param>
        /// <param name="idTemporal"></param>
        /// <returns></returns>
        public int InsertarTercero(Tercero pTercero, string idTemporal)
        {
            try
            {
                validacionesTerceroInsert(pTercero);
                return vTerceroDAL.InsertarTercero(pTercero, idTemporal);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Insertar un Tercero Repesentante Legal
        /// </summary>
        /// <param name="pIdTercero"></param>
        /// <param name="IdRepresentanteLegal"></param>
        /// <returns></returns>
        public int InsertarTerceroRepesentanteLegal(int pIdTercero, int IdRepresentanteLegal)
        {
            try
            {

                return vTerceroDAL.InsertarTerceroRepesentanteLegal(pIdTercero, IdRepresentanteLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Modifica un Tercero
        /// </summary>
        /// <param name="pTercero"></param>
        /// <returns></returns>
        public int ModificarTercero(Tercero pTercero, string idTemporal)
        {
            try
            {
                validacionesTerceroEdit(pTercero);
                return vTerceroDAL.ModificarTercero(pTercero, idTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarTercero(Tercero pTercero)
        {
            try
            {
                validacionesTerceroEdit(pTercero);
                return vTerceroDAL.ModificarTercero(pTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Tercero por Id
        /// </summary>
        /// <param name="pIdTercero"></param>
        /// <returns></returns>
        public Tercero ConsultarTercero(int pIdTercero)
        {
            try
            {
                return vTerceroDAL.ConsultarTercero(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consultar Tercero por Tipo y Numero Identificacion
        /// </summary>
        /// <param name="pTipoidentificacion"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <returns></returns>
        public Tercero ConsultarTerceroTipoNumeroIdentificacion(int? pTipoidentificacion, string pNumeroIdentificacion)
        {
            try
            {
                return vTerceroDAL.ConsultarTerceroTipoNumeroIdentificacion(pTipoidentificacion, pNumeroIdentificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Tercero por Tipo y Numero Identificacion
        /// </summary>
        /// <param name="pTipoidentificacion"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <returns></returns>
        public Tercero ConsultarTerceroTipoNumeroIdentificacionParaIntegrantes(int pTipoidentificacion, string pNumeroIdentificacion)
        {
            try
            {
                return vTerceroDAL.ConsultarTerceroTipoNumeroIdentificacionParaIntegrantes(pTipoidentificacion, pNumeroIdentificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consultar un lista de Terceros de acuerdo a varios parametros
        /// </summary>
        /// <param name="vIdDListaTipoDocumento"></param>
        /// <param name="vIdEstadoTercero"></param>
        /// <param name="vIdTipoPersona"></param>
        /// <param name="vNumeroIdentificacion"></param>
        /// <param name="vPrimerNombre"></param>
        /// <param name="vSegundoNombre"></param>
        /// <param name="vPrimerApellido"></param>
        /// <param name="vSegundoApellido"></param>
        /// <returns></returns>
        public List<Tercero> ConsultarTerceros(int? vIdDListaTipoDocumento, int? vIdEstadoTercero, int? vIdTipoPersona, int? vNumeroIdentificacion,
                                                string vPrimerNombre, string vSegundoNombre, string vPrimerApellido, string vSegundoApellido)
        {
            try
            {
                return vTerceroDAL.ConsultarTerceros(vIdDListaTipoDocumento, vIdEstadoTercero, vIdTipoPersona,
                                                     vNumeroIdentificacion,
                                                     vPrimerNombre, vSegundoNombre, vPrimerApellido, vSegundoApellido);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Tercero> ConsultarTerceros(int? vIdDListaTipoDocumento, int? vIdEstadoTercero, int? vIdTipoPersona, string vNumeroIdentificacion)
        {
            try
            {
                if (vIdDListaTipoDocumento == -1 || vIdDListaTipoDocumento == 0) vIdDListaTipoDocumento = null;
                if (vIdEstadoTercero == 0 || vIdEstadoTercero == -1) vIdEstadoTercero = null;
                if (vIdTipoPersona == 0 || vIdTipoPersona == -1) vIdTipoPersona = null;
                if (vNumeroIdentificacion == "") vNumeroIdentificacion = null;
                return vTerceroDAL.ConsultarTerceros(vIdDListaTipoDocumento, vIdEstadoTercero, vIdTipoPersona, vNumeroIdentificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Tercero> ConsultarTerceros_Proveedores_Validados(int TipoConsulta, int? vIdTipoPersona, int? vIdDListaTipoDocumento, string vNumeroIdentificacion, string vNombreTercero, string usuario)
        {
            try
            {
                if (vIdDListaTipoDocumento == -1 || vIdDListaTipoDocumento == 0) vIdDListaTipoDocumento = null;
                if (vNombreTercero == "") vNombreTercero = null;
                if (vIdTipoPersona == 0 || vIdTipoPersona == -1) vIdTipoPersona = null;
                if (vNumeroIdentificacion == "") vNumeroIdentificacion = null;
                return vTerceroDAL.ConsultarTerceros_Proveedores_Validados(TipoConsulta, vIdTipoPersona, vIdDListaTipoDocumento, vNumeroIdentificacion, vNombreTercero, usuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }



        /// <summary>
        /// Metodo que consulta los terceros asociados a un usuario, se utiliza en el m�dulo de Proveedores
        /// </summary>
        /// <param name="vIdDListaTipoDocumento"></param>
        /// <param name="vIdEstadoTercero"></param>
        /// <param name="vIdTipoPersona"></param>
        /// <param name="vNumeroIdentificacion"></param>
        /// <param name="vUsuario"></param>
        /// <returns></returns>
        public List<Tercero> ConsultarTerceros(int? vIdDListaTipoDocumento, int? vIdEstadoTercero, int? vIdTipoPersona, string vNumeroIdentificacion, string vUsuario, string vtercero, int? vClaseActividad)
        {
            try
            {
                if (vIdDListaTipoDocumento == -1 || vIdDListaTipoDocumento == 0) vIdDListaTipoDocumento = null;
                if (vIdEstadoTercero == 0 || vIdEstadoTercero == -1) vIdEstadoTercero = null;
                if (vIdTipoPersona == 0 || vIdTipoPersona == -1) vIdTipoPersona = null;
                if (vNumeroIdentificacion == "") vNumeroIdentificacion = null;
                if (vUsuario == "") vUsuario = null;
                if (vtercero == "") vtercero = null;
                if (vClaseActividad == -1) vClaseActividad = null;
                return vTerceroDAL.ConsultarTerceros(vIdDListaTipoDocumento, vIdEstadoTercero, vIdTipoPersona, vNumeroIdentificacion, vUsuario, vtercero, vClaseActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Metodo que consulta los terceros asociados a un usuario, que no son Proveedores; se utiliza en el m�dulo de Proveedores
        /// </summary>
        /// <param name="vIdDListaTipoDocumento"></param>
        /// <param name="vIdEstadoTercero"></param>
        /// <param name="vIdTipoPersona"></param>
        /// <param name="vNumeroIdentificacion"></param>
        /// <param name="vUsuario"></param>
        /// <returns></returns>
        public List<Tercero> ConsultarTercerosNoProveedores(int? vIdDListaTipoDocumento, int? vIdEstadoTercero, int? vIdTipoPersona, string vNumeroIdentificacion, string vUsuario, string vtercero)
        {
            try
            {
                if (vIdDListaTipoDocumento == -1 || vIdDListaTipoDocumento == 0) vIdDListaTipoDocumento = null;
                if (vIdEstadoTercero == 0 || vIdEstadoTercero == -1) vIdEstadoTercero = null;
                if (vIdTipoPersona == 0 || vIdTipoPersona == -1) vIdTipoPersona = null;
                if (vNumeroIdentificacion == "") vNumeroIdentificacion = null;
                if (vUsuario == "") vUsuario = null;
                if (vtercero == "") vtercero = null;
                return vTerceroDAL.ConsultarTercerosNoProveedores(vIdDListaTipoDocumento, vIdEstadoTercero, vIdTipoPersona, vNumeroIdentificacion, vUsuario, vtercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Metodo que consulta los terceros para validar, se utiliza en el m�dulo de Proveedores
        /// </summary>
        /// <param name="vIdDListaTipoDocumento"></param>
        /// <param name="vIdEstadoTercero"></param>
        /// <param name="vIdTipoPersona"></param>
        /// <param name="vNumeroIdentificacion"></param>
        /// <param name="vUsuario"></param>
        /// <returns></returns>
        public List<Tercero> ConsultarTerceros(int? vIdDListaTipoDocumento, int? vIdEstadoTercero, int? vIdTipoPersona, string vNumeroIdentificacion, DateTime vFechaRegistro, string vTercero)
        {
            try
            {
                if (vIdDListaTipoDocumento == -1 || vIdDListaTipoDocumento == 0) vIdDListaTipoDocumento = null;
                if (vIdEstadoTercero == 0 || vIdEstadoTercero == -1) vIdEstadoTercero = null;
                if (vIdTipoPersona == 0 || vIdTipoPersona == -1) vIdTipoPersona = null;
                if (vNumeroIdentificacion == "") vNumeroIdentificacion = null;

                return vTerceroDAL.ConsultarTerceros(vIdDListaTipoDocumento, vIdEstadoTercero, vIdTipoPersona, vNumeroIdentificacion, vFechaRegistro, vTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Tercero ProviderUserKey
        /// </summary>
        /// <param name="ProviderUserKey"></param>
        /// <returns></returns>
        public Tercero ConsultarTerceroProviderUserKey(string ProviderUserKey)
        {
            try
            {
                return vTerceroDAL.ConsultarTerceroProviderUserKey(ProviderUserKey);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Tercero ConsultarTerceroPorEmail(string ProviderUserKey)
        {
            try
            {
                return vTerceroDAL.ConsultarTerceroPorEmail(ProviderUserKey);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar un Reprentante Legal Tercero
        /// </summary>
        /// <param name="pIdTercero"></param>
        /// <returns></returns>
        public int ConsultarReprentanteLogalTercero(int pIdTercero)
        {
            try
            {
                return vTerceroDAL.ConsultarReprentanteLogalTercero(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }




        /// <summary>
        /// Validaciones al insertar un tercero de acuerdo al  CU002
        /// </summary>
        /// <param name="obj"></param>
        public void validacionesTerceroInsert(Tercero obj)
        {
            try
            {
                TipoPersonaBLL objBll = new TipoPersonaBLL();
                TipoPersona tipoper = new TipoPersona();
                if (obj.Email.Trim() == "") throw new Exception("Registre un correo electr�nico v�lido ");
                if (obj.IdTipoPersona == null || obj.IdTipoPersona == 0 || obj.IdTipoPersona == -1) throw new Exception("Seleccione Tipo de Persona");
                tipoper = objBll.ConsultarTipoPersona(int.Parse(obj.IdTipoPersona.ToString()));
                if (obj.IdDListaTipoDocumento == 0 || obj.IdDListaTipoDocumento == -1) throw new Exception("Seleccione un tipo de identificaci�n");
                if (obj.NumeroIdentificacion.Trim() == "") throw new Exception("Registre el n�mero de identificaci�n");
                List<Tercero> lista = new List<Tercero>();
                lista = this.ConsultarTerceros(null, null, null, obj.NumeroIdentificacion);
                if (lista.Count > 0) throw new Exception("Ya se ha registrado el n�mero de identificaci�n");
                if (obj.IdEstadoTercero == -1 || obj.IdEstadoTercero == 0) throw new Exception("Ingrese el estado del tercero");
                if (tipoper.CodigoTipoPersona == "001")
                {
                    if (obj.FechaExpedicionId == null) throw new Exception("Registre la fecha de expedici�n del documento de identificaci�n");
                    if (obj.FechaExpedicionId > DateTime.Now) throw new Exception("La fecha de expedici�n no puede superar la fecha actual");
                    if (obj.FechaNacimiento == null) throw new Exception("Registre la fecha de nacimiento");
                    if (obj.FechaNacimiento > DateTime.Now) throw new Exception("La fecha de nacimiento no puede superar la fecha actual");
                    if (edad(DateTime.Parse(obj.FechaNacimiento.ToString())) < 18) throw new Exception("El tercero como m�nimo debe tener 18 a�os, int�ntelo de nuevo");
                    //if (obj.FechaExpedicionId > obj.FechaNacimiento) throw new Exception("Entre las  fechas de expedici�n y de nacimiento debe haber  diferencia m�nima 18 a�os, int�ntelo de nuevo");
                    if (DateDiff(DateInterval.Year, Convert.ToDateTime(obj.FechaNacimiento), Convert.ToDateTime(obj.FechaExpedicionId)) < 18)
                        throw new Exception("Entre las  fechas de expedici�n y de nacimiento debe haber  diferencia m�nima 18 a�os, int�ntelo de nuevo");
                    if (obj.PrimerNombre.Trim() == "") throw new Exception("Registre su primer nombre");
                    if (obj.Sexo == "-1") throw new Exception("Seleccione un valor de la lista");
                }
                else
                {
                    if (obj.RazonSocial.Trim() == "") throw new Exception("Registre su raz�n social");
                    if (obj.Sexo == "-1") obj.Sexo = "";
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Validaciones al editar un tercero de acuerdo al  CU002
        /// </summary>
        /// <param name="obj"></param>
        public void validacionesTerceroEdit(Tercero obj)
        {
            try
            {
                TipoPersonaBLL objBll = new TipoPersonaBLL();
                TipoPersona tipoper = new TipoPersona();
                if (obj.IdTipoPersona == null || obj.IdTipoPersona == 0 || obj.IdTipoPersona == -1) throw new Exception("Seleccione Tipo de Persona");
                tipoper = objBll.ConsultarTipoPersona(int.Parse(obj.IdTipoPersona.ToString()));
                if (obj.IdEstadoTercero == -1 || obj.IdEstadoTercero == 0) throw new Exception("Ingrese el estado del tercero");
                if (tipoper.CodigoTipoPersona == "001")
                {
                    if (obj.FechaExpedicionId == null) throw new Exception("Registre la fecha de expedici�n del documento de identificaci�n");
                    if (obj.FechaExpedicionId > DateTime.Now) throw new Exception("La fecha de expedici�n no puede superar la fecha actual");
                    if (obj.FechaNacimiento == null) throw new Exception("Registre la fecha de nacimiento");
                    if (obj.FechaNacimiento > DateTime.Now) throw new Exception("La fecha de nacimiento no puede superar la fecha actual");
                    if (edad(DateTime.Parse(obj.FechaNacimiento.ToString())) < 18) throw new Exception("El tercero como m�nimo debe tener 18 a�os, int�ntelo de nuevo");
                    //if (obj.FechaExpedicionId > obj.FechaNacimiento) throw new Exception("Entre las  fechas de expedici�n y de nacimiento debe haber  diferencia m�nima 18 a�os, int�ntelo de nuevo");
                    if (DateDiff(DateInterval.Year, Convert.ToDateTime(obj.FechaNacimiento), Convert.ToDateTime(obj.FechaExpedicionId)) < 18)
                        throw new Exception("Entre las  fechas de expedici�n y de nacimiento debe haber  diferencia m�nima 18 a�os, int�ntelo de nuevo");
                    if (obj.PrimerNombre.Trim() == "") throw new Exception("Registre su primer nombre");
                    if (obj.Sexo == "-1") throw new Exception("Seleccione un valor de la lista");
                }
                else
                {
                    if (obj.RazonSocial.Trim() == "") throw new Exception("Registre su raz�n social");
                    if (obj.Sexo == "-1") obj.Sexo = "";
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Devuelve un valor Long que especifica el n�mero de
        /// intervalos de tiempo entre dos valores Date.
        /// </summary>
        /// <param name="interval">Obligatorio. Valor de enumeraci�n
        /// DateInterval o expresi�n String que representa el intervalo
        /// de tiempo que se desea utilizar como unidad de diferencia
        /// entre Date1 y Date2.</param>
        /// <param name="date1">Obligatorio. Date. Primer valor de
        /// fecha u hora que se desea utilizar en el c�lculo.</param>
        /// <param name="date2">Obligatorio. Date. Segundo valor de
        /// fecha u hora que se desea utilizar en el c�lculo.</param>
        /// <returns></returns>
        public static long DateDiff(DateInterval interval, DateTime date1, DateTime date2)
        {
            long rs = 0;
            TimeSpan diff = date2.Subtract(date1);
            switch (interval)
            {
                case DateInterval.Day:
                case DateInterval.DayOfYear:
                    rs = (long)diff.TotalDays;
                    break;
                case DateInterval.Hour:
                    rs = (long)diff.TotalHours;
                    break;
                case DateInterval.Minute:
                    rs = (long)diff.TotalMinutes;
                    break;
                case DateInterval.Month:
                    rs = (date2.Month - date1.Month) + (12 * TerceroBLL.DateDiff(DateInterval.Year, date1, date2));
                    break;
                case DateInterval.Quarter:
                    rs = (long)Math.Ceiling((double)(TerceroBLL.DateDiff(DateInterval.Month, date1, date2) / 3.0));
                    break;
                case DateInterval.Second:
                    rs = (long)diff.TotalSeconds;
                    break;
                case DateInterval.Weekday:
                case DateInterval.WeekOfYear:
                    rs = (long)(diff.TotalDays / 7);
                    break;
                case DateInterval.Year:
                    rs = date2.Year - date1.Year;
                    break;
            }//switch
            return rs;
        }//DateDiff

        /// <summary>
        /// Enumerados que definen los tipos de
        /// intervalos de tiempo posibles.
        /// </summary>
        public enum DateInterval
        {
            Day,
            DayOfYear,
            Hour,
            Minute,
            Month,
            Quarter,
            Second,
            Weekday,
            WeekOfYear,
            Year
        }

        public int edad(DateTime fechaNacimiento)
        {
            //Obtengo la diferencia en a�os.
            int edad = DateTime.Now.Year - fechaNacimiento.Year;

            //Obtengo la fecha de cumplea�os de este a�o.
            DateTime nacimientoAhora = fechaNacimiento.AddYears(edad);
            //Le resto un a�o si la fecha actual es anterior
            //al d�a de nacimiento.
            if (DateTime.Now.Date < nacimientoAhora.Date)
            {
                edad--;
            }

            return edad;
        }
        //public List<Tercero> ConsultarTerceros(String pNumeroIdentificacion, String pPrimerNombre, String pSegundoNombre, String pPrimerApellido, String pSegundoApellido, String pEmail, String pSexo, DateTime? pFechaExpedicionId, DateTime? pFechaNacimiento)
        //{
        //    try
        //    {
        //        return vTerceroDAL.ConsultarTerceros(pNumeroIdentificacion, pPrimerNombre, pSegundoNombre, pPrimerApellido, pSegundoApellido, pEmail, pSexo, pFechaExpedicionId, pFechaNacimiento);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        public bool PuedeTerceroCambiarEstado(int pIdTercero)
        {
            try
            {
                return vTerceroDAL.PuedeTerceroCambiarEstado(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Modifica la Raz�n Social de un Tercero
        /// </summary>
        /// <param name="pIdTercero"></param>
        /// /// <param name="pRazonSocial"></param>
        /// <returns></returns>
        public bool ModificarRazonSocialTercero(int pIdTercero, string pRazonSocial)
        {
            try
            {
                return vTerceroDAL.ModificarRazonSocialTercero(pIdTercero, pRazonSocial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Tercero por CorreoElectronico
        /// </summary>
        /// <param name="pCorreoElectronico"></param>
        /// <returns></returns>
        public Tercero ConsultarTerceroPorCorreo(string pCorreoElectronico)
        {
            try
            {
                return vTerceroDAL.ConsultarTerceroPorCorreo(pCorreoElectronico);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Tercero> ConsultarTerceroFuente(String pTipoPersona, String pTipoidentificacion, String pIdentificacion
           , String pProveedor)
        {
            try
            {
                return vTerceroDAL.ConsultarTerceroFuente(pTipoPersona, pTipoidentificacion, pIdentificacion, pProveedor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }



    }
}
