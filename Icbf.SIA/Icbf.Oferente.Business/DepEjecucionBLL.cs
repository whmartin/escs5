﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class DepEjecucionBLL
    {
        private DepEjecucionDAL vDepEjecucionDAL;
        public DepEjecucionBLL()
        {
            vDepEjecucionDAL = new DepEjecucionDAL();
        }
        public int InsertarDepEjecucion(DepEjecucion pDepEjecucion)
        {
            try
            {
                return vDepEjecucionDAL.InsertarDepEjecucion(pDepEjecucion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarDepEjecucion(DepEjecucion pDepEjecucion)
        {
            try
            {
                return vDepEjecucionDAL.ModificarDepEjecucion(pDepEjecucion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarDepEjecucion(DepEjecucion pDepEjecucion)
        {
            try
            {
                return vDepEjecucionDAL.EliminarDepEjecucion(pDepEjecucion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DepEjecucion ConsultarDepEjecucion(int pIdDepEjecucion)
        {
            try
            {
                return vDepEjecucionDAL.ConsultarDepEjecucion(pIdDepEjecucion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DepEjecucion> ConsultarDepEjecucions(int vIdExpEntidad, int? pIdDepartamento)
        {
            try
            {
                return vDepEjecucionDAL.ConsultarDepEjecucions(vIdExpEntidad, pIdDepartamento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
