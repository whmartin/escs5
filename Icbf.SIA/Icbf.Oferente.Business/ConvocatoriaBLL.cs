﻿using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;

namespace Icbf.Oferente.Business
{
    public class ConvocatoriaBLL
    {
        private ConvocatoriaDAL _convocatoriaDAL;
        public ConvocatoriaBLL()
        {
            _convocatoriaDAL = new ConvocatoriaDAL();
        }

        public List<Convocatoria> ObtenerConvocatorias(int? numero, DateTime? fechaPublicacionD, DateTime? fechaPublicacionF, int? Estado, int? Regional)
        {
            try
            {
                return _convocatoriaDAL.ObtenerConvocatorias(numero, fechaPublicacionD, fechaPublicacionF, Estado, Regional);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Dictionary<int, string> ConsultarEstados()
        {
            try
            {
                return _convocatoriaDAL.ConsultarEstados();
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Convocatoria ConsultarConvocatoriaPorId(int id)
        {
            try
            {
                return _convocatoriaDAL.ConsultarConvocatoriaPorId(id);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarConvocatoria(Convocatoria item)
        {
            try
            {
                return _convocatoriaDAL.ActualizarConvocatoria(item);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int CrearConvocatoria(Convocatoria item)
        {
            try
            {
                return _convocatoriaDAL.CrearConvocatoria(item);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarSolicitanteConvocatoriaLupa(Convocatoria item)
        {
            try
            {
               return  _convocatoriaDAL.InsertarSolicitanteConvocatoriaLupa(item);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            };
        }

        public int CambiarEstadoConvocatoria(int idConvocatoria)
        {
            try
            {
               return  _convocatoriaDAL.CambiarEstadoConvocatoria(idConvocatoria);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            };
        }

        public IDictionary<int, string> ConsultarTipoObservaciones()
        {
            try
            {
                return _convocatoriaDAL.ConsultarTipoObservaciones();
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Convocatoria> ConsultarConvocatoriass(string pNombreConvocatoria, int pNumero)
        {
            try
            {
                return _convocatoriaDAL.ConsultarConvocatoriass(pNombreConvocatoria, pNumero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
