using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class TipoIdentificacionPersonaNaturalBLL
    {
        private TipoIdentificacionPersonaNaturalDAL vTipoIdentificacionPersonaNaturalDAL;
        public TipoIdentificacionPersonaNaturalBLL()
        {
            vTipoIdentificacionPersonaNaturalDAL = new TipoIdentificacionPersonaNaturalDAL();
        }
        public int InsertarTipoIdentificacionPersonaNatural(TipoIdentificacionPersonaNatural pTipoIdentificacionPersonaNatural)
        {
            try
            {
                return vTipoIdentificacionPersonaNaturalDAL.InsertarTipoIdentificacionPersonaNatural(pTipoIdentificacionPersonaNatural);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarTipoIdentificacionPersonaNatural(TipoIdentificacionPersonaNatural pTipoIdentificacionPersonaNatural)
        {
            try
            {
                return vTipoIdentificacionPersonaNaturalDAL.ModificarTipoIdentificacionPersonaNatural(pTipoIdentificacionPersonaNatural);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarTipoIdentificacionPersonaNatural(TipoIdentificacionPersonaNatural pTipoIdentificacionPersonaNatural)
        {
            try
            {
                return vTipoIdentificacionPersonaNaturalDAL.EliminarTipoIdentificacionPersonaNatural(pTipoIdentificacionPersonaNatural);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public TipoIdentificacionPersonaNatural ConsultarTipoIdentificacionPersonaNatural(int pIdTipoIdentificacionPersonaNatural)
        {
            try
            {
                return vTipoIdentificacionPersonaNaturalDAL.ConsultarTipoIdentificacionPersonaNatural(pIdTipoIdentificacionPersonaNatural);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoIdentificacionPersonaNatural> ConsultarTipoIdentificacionPersonaNaturals(String pCodigoTipoIdentificacionPersonaNatural, String pNombreTipoIdentificacionPersonaNatural, Boolean? pEstado)
        {
            try
            {
                return vTipoIdentificacionPersonaNaturalDAL.ConsultarTipoIdentificacionPersonaNaturals(pCodigoTipoIdentificacionPersonaNatural, pNombreTipoIdentificacionPersonaNatural, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
