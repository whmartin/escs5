using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// Definición de clase InfraestructuraBLL
    /// </summary>
    public class InfraestructuraBLL
    {
        /// <summary>
        /// Declaración de objeto DAL
        /// Y constructor donde se crea el objeto DAL vInfraestructuraDAL
        /// </summary>
        private InfraestructuraDAL vInfraestructuraDAL;
        public InfraestructuraBLL()
        {
            vInfraestructuraDAL = new InfraestructuraDAL();
        }
        /// <summary>
        /// Insertar Infraestructura
        /// </summary>
        /// <param name="pInfraestructura"></param>
        /// <returns></returns>
        public int InsertarInfraestructura(Infraestructura pInfraestructura)
        {
            try
            {
                return vInfraestructuraDAL.InsertarInfraestructura(pInfraestructura);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modificar Infraestructura
        /// </summary>
        /// <param name="pInfraestructura"></param>
        /// <returns></returns>
        public int ModificarInfraestructura(Infraestructura pInfraestructura)
        {
            try
            {
                return vInfraestructuraDAL.ModificarInfraestructura(pInfraestructura);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        ///  Eliminar Infraestructura
        /// </summary>
        /// <param name="pInfraestructura"></param>
        /// <returns></returns>
        public int EliminarInfraestructura(Infraestructura pInfraestructura)
        {
            try
            {
                return vInfraestructuraDAL.EliminarInfraestructura(pInfraestructura);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Infraestructura
        /// </summary>
        /// <param name="pIdInfraestructura"></param>
        /// <returns></returns>
        public Infraestructura ConsultarInfraestructura(int pIdInfraestructura)
        {
            try
            {
                return vInfraestructuraDAL.ConsultarInfraestructura(pIdInfraestructura);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar un lista de Infraestructuras
        /// </summary>
        /// <param name="pAnho"></param>
        /// <param name="pIdOferente"></param>
        /// <returns></returns>
        public List<Infraestructura> ConsultarInfraestructuras(int? pAnho, Decimal? pIdOferente)
        {
            try
            {
                return vInfraestructuraDAL.ConsultarInfraestructuras(pAnho, pIdOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
