using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class IdentificacionBLL
    {
        private IdentificacionDAL vIdentificacionDAL;
        public IdentificacionBLL()
        {
            vIdentificacionDAL = new IdentificacionDAL();
        }
        public int InsertarIdentificacion(Identificacion pIdentificacion)
        {
            try
            {
                return vIdentificacionDAL.InsertarIdentificacion(pIdentificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarIdentificacion(Identificacion pIdentificacion)
        {
            try
            {
                return vIdentificacionDAL.ModificarIdentificacion(pIdentificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
       

        public Identificacion ConsultarIdentificacion(int pIdentificacion)
        {
            try
            {
                return vIdentificacionDAL.ConsultarIdentificacion(pIdentificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Identificacion> ConsultarIdentificacions(String pAno, String pNombreEntidadOferente, String pSigla, String pTipodeidentificación, String pNúmeroIdentificación, String pDV)
        {
            try
            {
                return vIdentificacionDAL.ConsultarIdentificacions(pAno, pNombreEntidadOferente, pSigla, pTipodeidentificación, pNúmeroIdentificación, pDV);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consultar Entiga Generica Parametros
        /// </summary>
        /// <param name="Tabla"></param>
        /// <param name="IdCompo"></param>
        /// <param name="ValoCampo"></param>
        /// <returns></returns>
        public List<EntigaGenericaParametros> ConsultarEntigaGenericaParametros(string Tabla, string IdCompo, string ValoCampo)
        {
            try
            {
                return vIdentificacionDAL.ConsultarEntigaGenericaParametros(Tabla, IdCompo, ValoCampo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
