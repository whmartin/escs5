﻿using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Business
{
    public class ConvocatoriasEquiposEvaluadoresBLL
    {
        private ConvocatoriasEquiposEvaluadoresDAL vConvocatoriasEquiposEvaluadoresDAL;

        public ConvocatoriasEquiposEvaluadoresBLL()
        {
            vConvocatoriasEquiposEvaluadoresDAL = new ConvocatoriasEquiposEvaluadoresDAL();
        }

        public int InsertarConvocatoriasEquiposEvaluadores(ConvocatoriasEquiposEvaluadores pConvocatoriasEquiposEvaluadores)
        {
            try
            {
                return vConvocatoriasEquiposEvaluadoresDAL.InsertarConvocatoriasEquiposEvaluadores(pConvocatoriasEquiposEvaluadores);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ConvocatoriasEquiposEvaluadores> ConsultarConvocatoriassEquiposEvaluadores(int pIdEquipoEvaluador)
        {
            try
            {
                return vConvocatoriasEquiposEvaluadoresDAL.ConsultarConvocatoriassEquiposEvaluadores(pIdEquipoEvaluador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarConvocatoriasEquiposEvaluadores(ConvocatoriasEquiposEvaluadores pConvocatoriasEquiposEvaluadores)
        {
            try
            {
                return vConvocatoriasEquiposEvaluadoresDAL.EliminarConvocatoriasEquiposEvaluadores(pConvocatoriasEquiposEvaluadores);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
