using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// definición de clase NumeroTrabajadorBLL
    /// </summary>
    public class NumeroTrabajadorBLL
    {
        /// <summary>
        /// declaración de Objeto DAL
        /// y Constructor donde se creael objeto DAL vNumeroTrabajadorDAL
        /// </summary>
        private NumeroTrabajadorDAL vNumeroTrabajadorDAL;
        public NumeroTrabajadorBLL()
        {
            vNumeroTrabajadorDAL = new NumeroTrabajadorDAL();
        }
        /// <summary>
        /// Insertar Numero Trabajador
        /// </summary>
        /// <param name="pNumeroTrabajador"></param>
        /// <returns></returns>
        public int InsertarNumeroTrabajador(NumeroTrabajador pNumeroTrabajador)
        {
            try
            {
                return vNumeroTrabajadorDAL.InsertarNumeroTrabajador(pNumeroTrabajador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        ///  Modificar un Numero Trabajador
        /// </summary>
        /// <param name="pNumeroTrabajador"></param>
        /// <returns></returns>
        public int ModificarNumeroTrabajador(NumeroTrabajador pNumeroTrabajador)
        {
            try
            {
                return vNumeroTrabajadorDAL.ModificarNumeroTrabajador(pNumeroTrabajador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Eliminar Numero Trabajador
        /// </summary>
        /// <param name="pNumeroTrabajador"></param>
        /// <returns></returns>
        public int EliminarNumeroTrabajador(NumeroTrabajador pNumeroTrabajador)
        {
            try
            {
                return vNumeroTrabajadorDAL.EliminarNumeroTrabajador(pNumeroTrabajador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar NumeroTrabajador
        /// </summary>
        /// <param name="pIdNumeroTrabajador"></param>
        /// <returns></returns>
        public NumeroTrabajador ConsultarNumeroTrabajador(int pIdNumeroTrabajador)
        {
            try
            {
                return vNumeroTrabajadorDAL.ConsultarNumeroTrabajador(pIdNumeroTrabajador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de NumeroTrabajador
        /// </summary>
        /// <param name="pCodigoNumeroTrabajador"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<NumeroTrabajador> ConsultarNumeroTrabajadors(String pCodigoNumeroTrabajador, String pDescripcion, String pEstado)
        {
            try
            {
                return vNumeroTrabajadorDAL.ConsultarNumeroTrabajadors(pCodigoNumeroTrabajador, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar CodigoNumeroTrabajador
        /// </summary>
        /// <param name="pCodigoNumeroTrabajador"></param>
        /// <returns></returns>
        public int ConsultarCodigoNumeroTrabajador(String pCodigoNumeroTrabajador)
        {
            try
            {
                return vNumeroTrabajadorDAL.ConsultarCodigoNumeroTrabajador(pCodigoNumeroTrabajador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
