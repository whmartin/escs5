using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class TotalExperienciaPrimerInfanciaBLL
    {
        private TotalExperienciaPrimerInfanciaDAL vTotalExperienciaPrimerInfanciaDAL;
        public TotalExperienciaPrimerInfanciaBLL()
        {
            vTotalExperienciaPrimerInfanciaDAL = new TotalExperienciaPrimerInfanciaDAL();
        }
        public int InsertarTotalExperienciaPrimerInfancia(TotalExperienciaPrimerInfancia pTotalExperienciaPrimerInfancia)
        {
            try
            {
                return vTotalExperienciaPrimerInfanciaDAL.InsertarTotalExperienciaPrimerInfancia(pTotalExperienciaPrimerInfancia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarTotalExperienciaPrimerInfancia(TotalExperienciaPrimerInfancia pTotalExperienciaPrimerInfancia)
        {
            try
            {
                return vTotalExperienciaPrimerInfanciaDAL.ModificarTotalExperienciaPrimerInfancia(pTotalExperienciaPrimerInfancia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarTotalExperienciaPrimerInfancia(TotalExperienciaPrimerInfancia pTotalExperienciaPrimerInfancia)
        {
            try
            {
                return vTotalExperienciaPrimerInfanciaDAL.EliminarTotalExperienciaPrimerInfancia(pTotalExperienciaPrimerInfancia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public TotalExperienciaPrimerInfancia ConsultarTotalExperienciaPrimerInfancia(int pIdTotalExperienciaPrimerInfancia)
        {
            try
            {
                return vTotalExperienciaPrimerInfanciaDAL.ConsultarTotalExperienciaPrimerInfancia(pIdTotalExperienciaPrimerInfancia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TotalExperienciaPrimerInfancia> ConsultarTotalExperienciaPrimerInfancias(String pCodigoTotalExperienciaPrimerInfancia, String pNombreTotalExperienciaPrimerInfancia, Boolean? pEstado)
        {
            try
            {
                return vTotalExperienciaPrimerInfanciaDAL.ConsultarTotalExperienciaPrimerInfancias(pCodigoTotalExperienciaPrimerInfancia, pNombreTotalExperienciaPrimerInfancia, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
