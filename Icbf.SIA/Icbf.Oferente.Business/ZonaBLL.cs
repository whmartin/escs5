using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class ZonaBLL
    {
        private ZonaDAL vZonaDAL;
        public ZonaBLL()
        {
            vZonaDAL = new ZonaDAL();
        }
        public int InsertarZona(Zona pZona)
        {
            try
            {
                return vZonaDAL.InsertarZona(pZona);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarZona(Zona pZona)
        {
            try
            {
                return vZonaDAL.ModificarZona(pZona);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarZona(Zona pZona)
        {
            try
            {
                return vZonaDAL.EliminarZona(pZona);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public Zona ConsultarZona(int pIdZona)
        {
            try
            {
                return vZonaDAL.ConsultarZona(pIdZona);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Zona> ConsultarZonas(String pCodigoZona, String pNombreZona, Boolean? pEstado)
        {
            try
            {
                return vZonaDAL.ConsultarZonas(pCodigoZona, pNombreZona, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
