using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// Definición de clase ConsultarOferentesIdoneosBLL
    /// </summary>
    public class ConsultarOferentesIdoneosBLL
    {
        /// <summary>
        /// Declaración de objeto DAL
        /// YConstructor donde se crea el objeto DAL vConsultarOferentesIdoneosDAL
        /// </summary>
        private ConsultarOferentesIdoneosDAL vConsultarOferentesIdoneosDAL;

        public ConsultarOferentesIdoneosBLL()
        {
            vConsultarOferentesIdoneosDAL = new ConsultarOferentesIdoneosDAL();
        }
        
        /// <summary>
        /// Consultar Consultar Oferentes Idoneos
        /// </summary>
        /// <param name="pIdentificacion"></param>
        /// <param name="pSigla"></param>
        /// <param name="pNombreEntidadOferente"></param>
        /// <param name="pDepartamentoUbicacionSede"></param>
        /// <param name="pMunicipioUbicacionSede"></param>
        /// <param name="pDepartamentoUbicacionCaracterizacion"></param>
        /// <param name="pMunicipioUbicacionCaracterizacion"></param>
        /// <param name="pFormaVisualizarReporte"></param>
        /// <returns></returns>
        public List<ConsultarOferentesIdoneos> ConsultarConsultarOferentesIdoneos(int? pIdentificacion,
                                                                                  string pSigla,
                                                                                  string pNombreEntidadOferente,  
                                                                                  string pDepartamentoUbicacionSede,
                                                                                  string pMunicipioUbicacionSede,
                                                                                  string pDepartamentoUbicacionCaracterizacion,
                                                                                  string pMunicipioUbicacionCaracterizacion,
                                                                                  string pFormaVisualizarReporte)
        {
            try
            {
                return vConsultarOferentesIdoneosDAL.ConsultarConsultarOferentesIdoneos(pIdentificacion,
                                                                                        pSigla,
                                                                                        pNombreEntidadOferente,
                                                                                        pDepartamentoUbicacionSede,
                                                                                        pMunicipioUbicacionSede,
                                                                                        pDepartamentoUbicacionCaracterizacion,
                                                                                        pMunicipioUbicacionCaracterizacion,
                                                                                        pFormaVisualizarReporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
