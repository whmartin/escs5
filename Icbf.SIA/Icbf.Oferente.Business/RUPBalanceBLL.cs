using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class RUPBalanceBLL
    {
        private RUPBalanceDAL vRUPBalanceDAL;
        public RUPBalanceBLL()
        {
            vRUPBalanceDAL = new RUPBalanceDAL();
        }
        public int InsertarRUPBalance(RUPBalance pRUPBalance)
        {
            try
            {
                return vRUPBalanceDAL.InsertarRUPBalance(pRUPBalance);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarRUPBalance(RUPBalance pRUPBalance)
        {
            try
            {
                return vRUPBalanceDAL.ModificarRUPBalance(pRUPBalance);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarRUPBalance(RUPBalance pRUPBalance)
        {
            try
            {
                return vRUPBalanceDAL.EliminarRUPBalance(pRUPBalance);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public RUPBalance ConsultarRUPBalance(int pIdRUPBalance)
        {
            try
            {
                return vRUPBalanceDAL.ConsultarRUPBalance(pIdRUPBalance);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<RUPBalance> ConsultarRUPBalances(String pCodigoRUPBalance, String pNombreRUPBalance, Boolean? pEstado)
        {
            try
            {
                return vRUPBalanceDAL.ConsultarRUPBalances(pCodigoRUPBalance, pNombreRUPBalance, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
