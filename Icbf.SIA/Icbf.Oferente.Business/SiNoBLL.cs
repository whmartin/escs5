using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class SiNoBLL
    {
        private SiNoDAL vSiNoDAL;
        public SiNoBLL()
        {
            vSiNoDAL = new SiNoDAL();
        }
        public int InsertarSiNo(SiNo pSiNo)
        {
            try
            {
                return vSiNoDAL.InsertarSiNo(pSiNo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarSiNo(SiNo pSiNo)
        {
            try
            {
                return vSiNoDAL.ModificarSiNo(pSiNo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarSiNo(SiNo pSiNo)
        {
            try
            {
                return vSiNoDAL.EliminarSiNo(pSiNo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public SiNo ConsultarSiNo(int pIdSiNo)
        {
            try
            {
                return vSiNoDAL.ConsultarSiNo(pIdSiNo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SiNo> ConsultarSiNos(String pCodigoSiNo, String pNombreSiNo, Boolean? pEstado)
        {
            try
            {
                return vSiNoDAL.ConsultarSiNos(pCodigoSiNo, pNombreSiNo, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
