using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class TipoComodatoUdsPropuestaBLL
    {
        private TipoComodatoUdsPropuestaDAL vTipoComodatoUdsPropuestaDAL;
        public TipoComodatoUdsPropuestaBLL()
        {
            vTipoComodatoUdsPropuestaDAL = new TipoComodatoUdsPropuestaDAL();
        }
        public int InsertarTipoComodatoUdsPropuesta(TipoComodatoUdsPropuesta pTipoComodatoUdsPropuesta)
        {
            try
            {
                return vTipoComodatoUdsPropuestaDAL.InsertarTipoComodatoUdsPropuesta(pTipoComodatoUdsPropuesta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarTipoComodatoUdsPropuesta(TipoComodatoUdsPropuesta pTipoComodatoUdsPropuesta)
        {
            try
            {
                return vTipoComodatoUdsPropuestaDAL.ModificarTipoComodatoUdsPropuesta(pTipoComodatoUdsPropuesta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarTipoComodatoUdsPropuesta(TipoComodatoUdsPropuesta pTipoComodatoUdsPropuesta)
        {
            try
            {
                return vTipoComodatoUdsPropuestaDAL.EliminarTipoComodatoUdsPropuesta(pTipoComodatoUdsPropuesta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public TipoComodatoUdsPropuesta ConsultarTipoComodatoUdsPropuesta(int pIdTipoComodatoUdsPropuesta)
        {
            try
            {
                return vTipoComodatoUdsPropuestaDAL.ConsultarTipoComodatoUdsPropuesta(pIdTipoComodatoUdsPropuesta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoComodatoUdsPropuesta> ConsultarTipoComodatoUdsPropuestas(String pCodigoTipoComodatoUdsPropuesta, String pNombreTipoComodatoUdsPropuesta, Boolean? pEstado)
        {
            try
            {
                return vTipoComodatoUdsPropuestaDAL.ConsultarTipoComodatoUdsPropuestas(pCodigoTipoComodatoUdsPropuesta, pNombreTipoComodatoUdsPropuesta, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
