using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// Definciión de clase TerceroModuloBLL
    /// </summary>
    public class TerceroModuloBLL
    {
        /// <summary>
        /// Declaración de objeto DAL
        /// Y Constructor de clase donde se crea el objeto DAL vTerceroModuloDAL
        /// </summary>
        private TerceroModuloDAL vTerceroModuloDAL;
        public TerceroModuloBLL()
        {
            vTerceroModuloDAL = new TerceroModuloDAL();
        }

        /// <summary>
        /// Insertar TerceroModulo
        /// </summary>
        /// <param name="pTerceroModulo"></param>
        /// <returns></returns>
        public int InsertarTerceroModulo(TerceroModulo pTerceroModulo)
        {
            try
            {
                return vTerceroModuloDAL.InsertarTerceroModulo(pTerceroModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

         /// <summary>
         /// Eliminar Tercero Entidad Modulo
         /// </summary>
         /// <param name="pTerceroModulo"></param>
         /// <returns></returns>
        public int EliminarTerceroEntidadModulo(TerceroModulo pTerceroModulo)
        {
            try
            {
                return vTerceroModuloDAL.EliminarTerceroEntidadModulo(pTerceroModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Tercero Modulos
        /// </summary>
        /// <param name="pIDTercero"></param>
        /// <param name="pIDModulo"></param>
        /// <returns></returns>
        public List<TerceroModulo> ConsultarTerceroModulos(int? pIDTercero, int? pIDModulo)
        {
            try
            {
                return vTerceroModuloDAL.ConsultarTerceroModulos(pIDTercero, pIDModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
