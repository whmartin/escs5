﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class EstadoTerceroBLL
    {
        private EstadoTerceroDAL vEstadoTerceroDAL;
        public EstadoTerceroBLL()
        {
            vEstadoTerceroDAL = new EstadoTerceroDAL();
        }
        

        public List<EstadoTercero> ConsultarEstadosTercero( Boolean? pEstado)
        {
            try
            {
                return vEstadoTerceroDAL.ConsultarEstadosTerceros(pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public EstadoTercero ConsultarEstadoTercero(int id)
        {
            try
            {
                return vEstadoTerceroDAL.ConsultarEstadoTercero(id);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public EstadoTercero ConsultarEstadoTercero(string codigoTercero)
        {
            try
            {
                return vEstadoTerceroDAL.ConsultarEstadoTercero(codigoTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
