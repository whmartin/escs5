﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class ObservacionesPendientesBLL
    {
        private ObservacionesPendientesDAL vObservacionesPendientesDAL;

        public ObservacionesPendientesBLL()
        {
            vObservacionesPendientesDAL = new ObservacionesPendientesDAL();
        }

        public List<ObservacionesPendientes> ConsultarObservacionesPendientessEvaluador(string pNumeroDocumento)
        {
            try
            {
                return vObservacionesPendientesDAL.ConsultarObservacionesPendientessEvaluador(pNumeroDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ObservacionesPendientes> ConsultarObservacionesPendientessCoordinador(string pNumeroDocumento)
        {
            try
            {
                return vObservacionesPendientesDAL.ConsultarObservacionesPendientessCoordinador(pNumeroDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public ObservacionesPendientes ConsultarGestionMasRecienteAUnaObservacion(int pIdObservacionConvocatoria)
        {
            try
            {
                return vObservacionesPendientesDAL.ConsultarGestionMasRecienteAUnaObservacion(pIdObservacionConvocatoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarGestionObservacionConvocatoria(ObservacionesPendientes pObservacionesPendientes)
        {
            try
            {
                return vObservacionesPendientesDAL.InsertarGestionObservacionConvocatoria(pObservacionesPendientes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarObservacionConvocatoria(ObservacionesPendientes pObservacionesPendientes)
        {
            try
            {
                return vObservacionesPendientesDAL.ModificarObservacionConvocatoria(pObservacionesPendientes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ObservacionesPendientes> ConsultarObservacionesPendientess()
        {
            try
            {
                return vObservacionesPendientesDAL.ConsultarObservacionesPendientess();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ObservacionesPendientes> ConsultarObservacionesPendientessContratista()
        {
            try
            {
                return vObservacionesPendientesDAL.ConsultarObservacionesPendientessContratista();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ObservacionesPendientes ConsultarGestionMasRecienteAUnaObservacionDelEvaluadorParaContratista(int pIdObservacionConvocatoria)
        {
            try
            {
                return vObservacionesPendientesDAL.ConsultarGestionMasRecienteAUnaObservacionDelEvaluadorParaContratista(pIdObservacionConvocatoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarDescripcionGestionObservacionConvocatoria(ObservacionesPendientes pObservacionesPendientes)
        {
            try
            {
                return vObservacionesPendientesDAL.ModificarDescripcionGestionObservacionConvocatoria(pObservacionesPendientes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
