using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class FechaConvocatoriaBLL
    {
        private FechaConvocatoriaDAL vFechaConvocatoriaDAL;
        public FechaConvocatoriaBLL()
        {
            vFechaConvocatoriaDAL = new FechaConvocatoriaDAL();
        }

        public int InsertarFechaConvocatoria(FechaConvocatoria pFechaConvocatoria)
        {
            try
            {
                return vFechaConvocatoriaDAL.InsertarFechaConvocatoria(pFechaConvocatoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarFechaConvocatoria(FechaConvocatoria pFechaConvocatoria)
        {
            try
            {
                return vFechaConvocatoriaDAL.ModificarFechaConvocatoria(pFechaConvocatoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarFechaConvocatoria(FechaConvocatoria pFechaConvocatoria)
        {
            try
            {
                return vFechaConvocatoriaDAL.EliminarFechaConvocatoria(pFechaConvocatoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public FechaConvocatoria ConsultarFechaConvocatoria(int pIdFechaConvocatoria)
        {
            try
            {
                return vFechaConvocatoriaDAL.ConsultarFechaConvocatoria(pIdFechaConvocatoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<FechaConvocatoria> ConsultarFechaConvocatorias(DateTime? pFechaIncio, DateTime? pFechaTerminacion)
        {
            try
            {
                return vFechaConvocatoriaDAL.ConsultarFechaConvocatorias(pFechaIncio, pFechaTerminacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
