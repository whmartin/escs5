﻿using Excel;
using Icbf.Utilities.Exceptions;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Icbf.Oferente.Business.Utilidad
{
    /// <summary>
    /// Clase para manejar la lógica de negocio de Util.
    /// </summary>
    public static class Util
    {
        /// <summary>
        /// Obtiene valor entero de cadena si es posible convertirlo, sino retorna cero.
        /// </summary>
        /// <param name="value">String a convertir</param>
        /// <returns>Entero resultado</returns>
        public static int GetIntOrDefault(string value)
        {
            return GetIntOrDefault(value, 0);
        }

        /// <summary>
        /// Obtiene valor entero de cadena si es posible convertilo, sino retorna defaultValue.
        /// </summary>
        /// <param name="value">String a convertir</param>
        /// <param name="defaultValue">Entero por defecto</param>
        /// <returns>Entero resultado</returns>
        public static int GetIntOrDefault(string value, int defaultValue)
        {
            int result = 0;

            if (int.TryParse(value, out result))
            {
                return result;
            }
            else
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Obtiene valor decimal de cadena si es posible convertirlo, sino retorna cero.
        /// </summary>
        /// <param name="value">String a convertir</param>
        /// <param name="replaceDecimalPoints">ReplaceDecimalPoints a filtrar</param>
        /// <returns>Decimal resultado</returns>Public static decimal
        public static decimal GetDecimalOrDefault(string value, bool replaceDecimalPoints = false)
        {
            return GetDecimalOrDefault(value, 0.0m, replaceDecimalPoints);
        }

        /// <summary>
        /// Obtiene valor entero de cadena si es posible convertilo, sino retorna defaultValue.
        /// </summary>
        /// <param name="value">String a convertir</param>
        /// <param name="defaultValue">Entero por defecto</param>
        /// <param name="replaceDecimalPoints">ReplaceDecimalPoints a filtrar</param>
        /// <returns>Resultado de la operación</returns>Public static decimal
        public static decimal GetDecimalOrDefault(string value, decimal defaultValue, bool replaceDecimalPoints = false)
        {
            if (replaceDecimalPoints)
            {
                value = value.Replace(",", string.Empty).Replace(".", ",");
            }

            decimal result = 0;

            if (decimal.TryParse(value, out result))
            {
                return result;
            }
            else
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Método para Validar si el valor del porcentaje esta en el rango permitido.
        /// </summary>
        /// <param name="percent">Porcentaje a filtrar</param>
        /// <param name="percentIsDecimal">Porcentaje en decimal a filtrar</param>
        /// <returns>True si es valido y false en caso contrario</returns>public static bool
        public static bool IsValidPercent(decimal percent, bool percentIsDecimal = true)
        {
            if (!percentIsDecimal && (percent < 0.00m || percent > 100.00m))
            {
                return false;
            }

            if (percentIsDecimal && (percent < 0.0000m || percent > 1.0000m))
            {
                return false;
            }

            return true;
        }

        public static bool IsValidEmail(string email)
        {
            string expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";

            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, String.Empty).Length == 0)
                { return true; }
                else
                { return false; }
            }
            else
            { return false; }
        }

        public static bool IsValidLetters(string campo)
        {
            string expresion = "^[a-zA-Z ñáéíóúÑÁÉÍÓÚ]+$";

            if (Regex.IsMatch(campo, expresion))
            {
                if (Regex.Replace(campo, expresion, String.Empty).Length == 0)
                { return true; }
                else
                { return false; }
            }
            else
            { return false; }
        }

        public static bool IsValidNumbers(string campo)
        {
            string expresion = "[0-9]";

            if (Regex.IsMatch(campo, expresion))
            {
                if (Regex.Replace(campo, expresion, String.Empty).Length == 0)
                { return true; }
                else
                { return false; }
            }
            else
            { return false; }
        }

        /// <summary>
        /// Devuelve el numero ingresado redondeado segun el parametro "Precision".
        /// </summary>
        /// <param name="value">Porcentaje a filtrar</param>
        /// <param name="precision">Precision ha aplicar</param>
        /// <returns>Value redondeando segun Precision</returns>public static decimal
        public static decimal RoundValue(decimal value, int precision)
        {
            decimal result = 0;
            ////precision = -3; Redondea al multiplo de mil mas cercano.
            ////precision = -2; Redondea al multiplo de cien mas cercano.
            ////precision = -1; Redondea al multiplo de diez mas cercano.

            //// debe estar entre -4 y 15:
            if (precision < -4 && precision > 15)
            {
                return value;
            }
            ////throw new ArgumentOutOfRangeException("precision", "Must be and integer between -4 and 15");

            if (precision >= 0)
            {
                result = Math.Round(value, precision);
            }
            else
            {
                precision = (int)Math.Pow(10, Math.Abs(precision));
                value = value + (5 * precision / 10);
                {
                    result = Math.Round(value - (value % precision), 0);
                }
            }

            return result;
        }

        /// <summary>
        /// Convierte valor decimal en cadena con los separadores decimales y de miles.
        /// </summary>
        /// <param name="value">Valor a convertir</param>
        /// <returns>Cadena de texto convertida</returns>public static string
        public static string DecimalFormat(decimal value)
        {
            return string.Format(System.Globalization.CultureInfo.GetCultureInfo("es-CO"), "{0:N}", value);
        }

        /// <summary>
        /// Convierte valor decimal en cadena con los separadores decimales, de miles y simbolo $.
        /// </summary>
        /// <param name="value">Valor a convertir</param>
        /// <returns>Cadena de texto convertida</returns>Public static string
        public static string CurrencyFormat(decimal value)
        {
            return string.Format(System.Globalization.CultureInfo.GetCultureInfo("es-CO"), "{0:C}", value);
        }

        /// <summary>
        /// Convierte valor decimal en cadena con los separadores decimales, de miles y porcentaje.
        /// </summary>
        /// <param name="value">Valor a convertir</param>
        /// <returns>Cadena de texto convertida</returns>public static string
        public static string PercentFormat(decimal value)
        {
            return string.Format(System.Globalization.CultureInfo.GetCultureInfo("es-ES"), "{0:P}", value);
        }

        /// <summary>
        /// Método para Obtener CSV de DataTable.
        /// </summary>
        /// <param name="response">Objeto response pagina web</param>
        /// <param name="pData">Data a tratar</param>
        /// <param name="nombreArchivo">Nombre del achivo</param>
        public static void GenerateCSVFromDataTable(HttpResponse response, DataTable pData, string nombreArchivo)
        {
            StringBuilder sb = new StringBuilder();

            IEnumerable<string> columnNames = pData.Columns.Cast<DataColumn>().Select(column => column.ColumnName);
            sb.AppendLine(string.Join(";", columnNames));

            foreach (DataRow row in pData.Rows)
            {
                IEnumerable<string> fields = row.ItemArray.Select(field => field.ToString());
                sb.AppendLine(string.Join(";", fields));
            }

            string textoCsv = sb.ToString();

            if (response != null && !String.IsNullOrEmpty(textoCsv))
            {
                response.Clear();
                response.ClearContent();
                response.ClearHeaders();
                response.Charset = UTF8Encoding.Default.WebName;
                response.ContentEncoding = UTF8Encoding.Default;
                response.Buffer = true;
                response.BufferOutput = true;
                response.Cache.SetCacheability(HttpCacheability.NoCache);
                response.Cache.SetExpires(DateTime.Now);
                response.ContentType = "application/vnd.ms-excel";
                response.AppendHeader(
                    "Content-Disposition",
                    String.Format(
                    "{0}; filename=\"{1}\"; size={2}; creation-date=\"{3}\"; modification-date=\"{3}\"; read-date=\"{3}\"",
                    !String.IsNullOrEmpty(nombreArchivo) && nombreArchivo.EndsWith(".xls", StringComparison.InvariantCultureIgnoreCase) ? "attachment" : "inline",
                    !String.IsNullOrEmpty(nombreArchivo) ? nombreArchivo : "reporteExcel",
                    textoCsv.Length,
                    DateTime.Now.ToString("R")));
                response.AppendHeader("Content-Length", textoCsv.Length.ToString());
                response.Write(textoCsv);
                response.End();
            }
        }

        /// <summary>
        /// Genera Excel de DataTable.
        /// </summary>
        /// <param name="response">Objeto response pagina web</param>
        /// <param name="pData">Data a tratar</param>
        /// <param name="pNombreArchivo">Nombre del achivo</param>
        public static void GenerateExcelFromDataTable(HttpResponse response, DataTable pData, string pNombreArchivo)
        {
            if (response != null)
            {
                string vDataHtml = ConvertDataTableToHTML(pData);
                GenerarXLS(response, vDataHtml, pNombreArchivo);
            }
        }

        /// <summary>
        /// Método GenerarXLS.
        /// </summary>
        /// <param name="response">Response a filtrar</param>
        /// <param name="textoHtml">Texto Html a filtrar</param>
        /// <param name="nombreArchivo">Nombre Archiva a filtrar</param>
        public static void GenerarXLS(HttpResponse response, string textoHtml, string nombreArchivo)
        {
            if (response != null && !string.IsNullOrEmpty(textoHtml))
            {
                nombreArchivo = nombreArchivo.Replace(".xlsx", string.Empty).Replace(".xls", string.Empty) + ".xls";

                response.Clear();
                response.ClearContent();
                response.ClearHeaders();
                response.Charset = UTF8Encoding.Default.WebName;
                response.ContentEncoding = UTF8Encoding.Default;
                response.Buffer = true;
                response.BufferOutput = true;
                response.Cache.SetCacheability(HttpCacheability.NoCache);
                response.Cache.SetExpires(DateTime.Now);
                response.ContentType = "application/vnd.ms-excel";
                response.AppendHeader(
                "Content-Disposition",
                string.Format(
                "{0}; filename=\"{1}\"; size={2}; creation-date=\"{3}\"; modification-date=\"{3}\"; read-date=\"{3}\"",
                    !string.IsNullOrEmpty(nombreArchivo) && nombreArchivo.EndsWith(
                     ".xls",
                     StringComparison.InvariantCultureIgnoreCase) ? "attachment" : "inline",
                    !string.IsNullOrEmpty(nombreArchivo) ? nombreArchivo : "reporteExcel",
                    textoHtml.Length,
                    DateTime.Now.ToString("R")));
                response.AppendHeader(
                "Content-Length",
                textoHtml.Length.ToString());
                response.Write(textoHtml);
                response.End();
            }
        }

        /// <summary>
        /// Convierte DataTable a HTML.
        /// </summary>
        /// <param name="dt">DataTable a filtrar</param>
        /// <returns>Cadena resultante de la conversión</returns>
        public static string ConvertDataTableToHTML(DataTable dt)
        {
            if (dt.Rows.Count == 0)
            {
                return string.Empty;
            }

            StringBuilder builder = new StringBuilder();
            builder.Append("<html>");
            builder.Append("<head>");
            builder.Append("<title>");
            builder.Append("Page-");
            builder.Append(Guid.NewGuid());
            builder.Append("</title>");
            builder.Append("</head>");
            builder.Append("<body>");
            builder.Append("<table border='1px' cellpadding='5' cellspacing='0' ");
            builder.Append("style='border: solid 1px Silver; font-size: x-small;'>");
            builder.Append("<tr align='left' valign='top'>");
            foreach (DataColumn c in dt.Columns)
            {
                builder.Append("<td align='left' valign='top'><b>");
                builder.Append(HttpUtility.HtmlEncode(c.ColumnName));
                builder.Append("</b></td>");
            }

            builder.Append("</tr>");
            foreach (DataRow r in dt.Rows)
            {
                builder.Append("<tr align='left' valign='top'>");
                foreach (DataColumn c in dt.Columns)
                {
                    builder.Append("<td align='left' valign='top'>");
                    builder.Append(r[c.ColumnName]);
                    builder.Append("</td>");
                }

                builder.Append("</tr>");
            }

            builder.Append("</table>");
            builder.Append("</body>");
            builder.Append("</html>");

            return builder.ToString();
        }

        /// <summary>
        /// Obtiene mes en letras de una fecha dada.
        /// </summary>
        /// <param name="dateTime">Fecha a manejar</param>
        /// <returns>Cadena del mes</returns>
        public static string ToMonthName(this DateTime dateTime)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(
                                                CultureInfo.GetCultureInfo("es-ES").DateTimeFormat.GetMonthName(dateTime.Month));
        }

        /// <summary>
        /// Obtiene mes en letras (abreviado) de una fecha dada.
        /// </summary>
        /// <param name="dateTime">Fecha a filtrar</param>
        /// <returns>Mes en letras</returns>
        public static string ToShortMonthName(this DateTime dateTime)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(
                                                CultureInfo.GetCultureInfo("es-ES").DateTimeFormat.GetAbbreviatedMonthName(dateTime.Month));
        }

        /// <summary>
        ///  Método para Obtener el DataTable del Archivo Excel guaradado en el servidor.
        /// </summary>
        /// <param name="pFilePath">Ruta del Archivo</param> 
        /// <returns>DtArchivo requerido</returns> public DataTable
        public static DataTable GetDataTableFromFile(string pFilePath)
        {
            DataTable dtArchivo = new DataTable();

            try
            {
                string vFileExt = Path.GetExtension(pFilePath);
                OleDbConnection con = null;
                ////Se extable conexión del archivo excel guardado, según su extensión.
                if (vFileExt == ".xls")
                {
                    con = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + pFilePath + ";Extended Properties='Excel 8.0;HDR=NO;IMEX=1;';");
                }
                else if (vFileExt == ".xlsx")
                {
                    con = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + pFilePath + ";Extended Properties='Excel 12.0;HDR=NO;IMEX=1;';");
                }
                else
                {
                    throw new Exception("Extension no valida para cargar en DataTable");
                }

                con.Open();
                ////Se asigna un datatable con el esquema del archivo
                DataTable dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                ////Se obtiene la hoja del archivo excel
                string vGetExcelSheetName = dt.Rows[0]["Table_Name"].ToString();
                OleDbCommand cmdExcel = new OleDbCommand(@"SELECT * FROM [" + vGetExcelSheetName + @"]", con);
                OleDbDataAdapter daExcel = new OleDbDataAdapter(cmdExcel);
                daExcel.Fill(dtArchivo);
                con.Close();
                ////elimina filas vacias
                dtArchivo = dtArchivo.Rows.Cast<DataRow>().Where(
                                  row => row.ItemArray.Any(field => !(field is System.DBNull))).CopyToDataTable();
                return dtArchivo;
            }
            catch (UserInterfaceException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Método para Obtener el DataTable del Archivo Excel guardado en el servidor
        /// </summary>
        /// <param name="pFilePath">Ruta del Archivo</param> 
        /// <param name="pFirstRowAsColumnNames">Primera fila de los nombres de las columnas</param>
        /// <returns>DtArchivo requerido</returns> Public DataTable
        public static DataTable GetDataTableFromFile2(string pFilePath, bool pFirstRowAsColumnNames = false)
        {
            DataTable dtArchivo = new DataTable();

            try
            {
                ////string vFileExt = Path.GetExtension(pFilePath);
                ////FileStream stream = File.Open(pFilePath, FileMode.Open, FileAccess.Read);

                var file = new FileInfo(pFilePath);

                using (var stream = new FileStream(pFilePath, FileMode.Open))
                {
                    IExcelDataReader excelReader;

                    ////Se extable conexión del archivo excel guardado, según su extensión
                    if (file.Extension == ".xls")
                    {
                        excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                    }
                    else if (file.Extension == ".xlsx")
                    {
                        excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    }
                    else
                    {
                        throw new Exception("Extension no valida para cargar en DataTable");
                    }

                    excelReader.IsFirstRowAsColumnNames = pFirstRowAsColumnNames;
                    DataSet dsData = excelReader.AsDataSet();

                    if (dsData != null && dsData.Tables.Count > 0)
                    {
                        dtArchivo = dsData.Tables[0];
                    }

                    ////elimina filas vacias
                    dtArchivo = dtArchivo.Rows.Cast<DataRow>().Where(
                                      row => row.ItemArray.Any(field => !(field is System.DBNull))).CopyToDataTable();
                    return dtArchivo;
                }
            }
            catch (UserInterfaceException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Método para Obtener el DataTable del Archivo Excel guardado en el servidor
        /// </summary>
        /// <param name="pFilePath">Ruta del Archivo</param> 
        /// <returns>DtArchivo requerido</returns> public DataTable
        public static DataTable GetDataTableFromFile3(string pFilePath, int? pTipoArchivo)
        {
            try
            {
                var file = new FileInfo(pFilePath);

                //// Read excel file.
                IWorkbook wb = WorkbookFactory.Create(
                    new FileStream(
                        Path.GetFullPath(pFilePath),
                               FileMode.Open,
                               FileAccess.Read,
                               FileShare.ReadWrite));

                ISheet ws = wb.GetSheetAt(0);

                DataTable dt = new DataTable();
                int headerColumns = 0;
                int rowColumns = 0;

                if (ws.LastRowNum > 0)
                {
                    headerColumns = ws.GetRow(0).Cells.Count;
                    for (int c = 0; c < headerColumns; c++)
                    {
                        dt.Columns.Add(ws.GetRow(0).GetCell(c).ToString(), typeof(string));
                    }
                }

                int vrow = 1;

                for (int row = vrow; row <= ws.LastRowNum; row++)
                {
                    if (ws.GetRow(row) != null)
                    {
                        bool Validacion = true;
                        for (int ncell = 1; ncell <= 34; ncell++)
                        {
                            ICell cell = ws.GetRow(row).GetCell(ncell, MissingCellPolicy.RETURN_NULL_AND_BLANK);

                            if (cell != null)
                            {
                                if (string.IsNullOrEmpty(cell.ToString().Replace(" ", "")))
                                {
                                    Validacion = false;                                    
                                }
                                else
                                {
                                    Validacion = true;
                                    break;
                                }
                            }
                        }

                        if (Validacion)
                        {
                            rowColumns = ws.GetRow(row).Cells.Count;
                            if (rowColumns > 1)
                            {
                                if (rowColumns > headerColumns)
                                {
                                    for (int j = headerColumns; j < rowColumns; j++)
                                    {
                                        dt.Columns.Add(ws.GetRow(row).GetCell(j).ToString(), typeof(string));
                                    }

                                    headerColumns = rowColumns;
                                }

                                DataRow dr = dt.NewRow();
                                ICell celda;
                                for (int j = 0; j < rowColumns; j++)
                                {
                                    celda = ws.GetRow(row).GetCell(j);
                                    dr[j] = (celda == null ? string.Empty : celda.ToString());
                                }

                                if (rowColumns < headerColumns)
                                {
                                    for (int j = rowColumns; j < headerColumns; j++)
                                    {
                                        celda = ws.GetRow(row).GetCell(j);
                                        dr[j] = (celda == null ? string.Empty : celda.ToString());
                                    }
                                }

                                dt.Rows.Add(dr);
                            }
                        }
                    }
                }

                ////elimina filas vacias
                if (dt.Rows.Count > 0)
                {
                    dt = dt.Rows.Cast<DataRow>().Where(
                                 row => row.ItemArray.Any(field => !(field is System.DBNull))).CopyToDataTable();
                }
                else
                {
                    throw new Exception("El archivo no contiene datos, verifique por favor.");
                }

                return dt;
            }
            catch (UserInterfaceException ex)
            {
                throw ex;
            }
        }
    }
}
