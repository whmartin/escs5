using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class EstadoBLL
    {
        private EstadoDAL vEstadoDAL;
        public EstadoBLL()
        {
            vEstadoDAL = new EstadoDAL();
        }
        public int InsertarEstado(Estado pEstado)
        {
            try
            {
                return vEstadoDAL.InsertarEstado(pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarEstado(Estado pEstado)
        {
            try
            {
                return vEstadoDAL.ModificarEstado(pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarEstado(Estado pEstado)
        {
            try
            {
                return vEstadoDAL.EliminarEstado(pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public Estado ConsultarEstado(int pIdEstado)
        {
            try
            {
                return vEstadoDAL.ConsultarEstado(pIdEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Estado> ConsultarEstados(String pCodigoEstado, String pNombreEstado, Boolean? pEstado)
        {
            try
            {
                return vEstadoDAL.ConsultarEstados(pCodigoEstado, pNombreEstado, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
