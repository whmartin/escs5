using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// Definición de clase EntidadProvOferenteBLL
    /// </summary>
    public class EntidadProvOferenteBLL
    {
        /// <summary>
        /// Declareación de objeto DAL
        /// Y CONstructor donde se crea el Objeto DAL vEntidadProvOferenteDAL
        /// </summary>
        private EntidadProvOferenteDAL vEntidadProvOferenteDAL;
        public EntidadProvOferenteBLL()
        {
            vEntidadProvOferenteDAL = new EntidadProvOferenteDAL();
        }


        /// <summary>
        ///  Consultar una Entidad ProvOferente
        /// </summary>
        /// <param name="pIdEntidad"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public EntidadProvOferente ConsultarEntidadProvOferente(int pIdEntidad, bool pEstado)
        {
            try
            {
                return vEntidadProvOferenteDAL.ConsultarEntidadProvOferente(pIdEntidad, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public string ConsultarEstadoValidacionEntidadProvOferente(int pIdEntidad)
        {
            try
            {
                return vEntidadProvOferenteDAL.ConsultarEstadoValidacionEntidadProvOferente(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        
        /// <summary>
        /// Cambiar Estado de una Entidad ProvOferente
        /// </summary>
        /// <param name="pIdEntidadProvOferente"></param>
        /// <returns></returns>
        public int CambiarEstadoEntidadProvOferente(int pIdEntidadProvOferente)
        {
            try
            {
                return vEntidadProvOferenteDAL.CambiarEstadoEntidadProvOferente(pIdEntidadProvOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Cambiar Estado Entidad ProvOferente Validacion
        /// </summary>
        /// <param name="pEstadoValidacion"></param>
        /// <param name="pIdEntidadProvOferente"></param>
        /// <returns></returns>
        public int CambiarEstadoEntidadProvOferenteValidacion(string pEstadoValidacion, int pIdEntidadProvOferente)
        {
            try
            {
                return vEntidadProvOferenteDAL.CambiarEstadoEntidadProvOferenteValidacion(pEstadoValidacion, pIdEntidadProvOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Inserta una Entidad ProvOferente
        /// </summary>
        /// <param name="pEntidadProvOferente"></param>
        /// <returns></returns>
        public int InsertarEntidadProvOferente(EntidadProvOferente pEntidadProvOferente)
        {
            try
            {
                return vEntidadProvOferenteDAL.InsertarEntidadProvOferente(pEntidadProvOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modifica una Entidad ProvOferente
        /// </summary>
        /// <param name="pEntidadProvOferente"></param>
        /// <returns></returns>
        public int ModificarEntidadProvOferente(EntidadProvOferente pEntidadProvOferente)
        {
            try
            {
                return vEntidadProvOferenteDAL.ModificarEntidadProvOferente(pEntidadProvOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Elimina una Entidad ProvOferente
        /// </summary>
        /// <param name="pEntidadProvOferente"></param>
        /// <returns></returns>
        public int EliminarEntidadProvOferente(EntidadProvOferente pEntidadProvOferente)
        {
            try
            {
                return vEntidadProvOferenteDAL.EliminarEntidadProvOferente(pEntidadProvOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consulta una Entidad ProvOferente
        /// </summary>
        /// <param name="pIDENTIDAD"></param>
        /// <param name="pTIPOENTOFPROV"></param>
        /// <param name="pIDTERCERO"></param>
        /// <param name="pIDTERCEROREPLEGAL"></param>
        /// <param name="pIDTIPOCIIUPRINCIPAL"></param>
        /// <param name="pIDTIPOCIIUSECUNDARIO"></param>
        /// <param name="pIDTIPOREGTRIB"></param>
        /// <param name="pIDTIPOSECTOR"></param>
        /// <param name="pIDTIPOACTIVIDAD"></param>
        /// <param name="pIDTIPOCLASEENTIDAD"></param>
        /// <param name="pIDTIPORAMAPUBLICA"></param>
        /// <param name="pIDTIPOENTIDAD"></param>
        /// <param name="pIDTIPONIVELGOB"></param>
        /// <param name="pIDTIPONIVELORGANIZACIONAL"></param>
        /// <param name="pIDTIPOORIGENCAPITAL"></param>
        /// <param name="pIDTIPOCERTIFICADORCALIDAD"></param>
        /// <param name="pIDTIPOCERTIFICATAMANO"></param>
        /// <param name="pIDTIPONATURALEZAJURID"></param>
        /// <param name="pIDTIPORANGOSACTIVOS"></param>
        /// <returns></returns>
        public EntidadProvOferente ConsultarEntidadProvOferente(int pIDENTIDAD, int pTIPOENTOFPROV, int pIDTERCERO, int pIDTERCEROREPLEGAL, int pIDTIPOCIIUPRINCIPAL, int pIDTIPOCIIUSECUNDARIO, int pIDTIPOREGTRIB, int pIDTIPOSECTOR, int pIDTIPOACTIVIDAD, int pIDTIPOCLASEENTIDAD, int pIDTIPORAMAPUBLICA, int pIDTIPOENTIDAD, int pIDTIPONIVELGOB, int pIDTIPONIVELORGANIZACIONAL, int pIDTIPOORIGENCAPITAL, int pIDTIPOCERTIFICADORCALIDAD, int pIDTIPOCERTIFICATAMANO, int pIDTIPONATURALEZAJURID, int pIDTIPORANGOSACTIVOS)
        {
            try
            {
                return vEntidadProvOferenteDAL.ConsultarEntidadProvOferente(pIDENTIDAD, pTIPOENTOFPROV, pIDTERCERO, pIDTERCEROREPLEGAL, pIDTIPOCIIUPRINCIPAL, pIDTIPOCIIUSECUNDARIO, pIDTIPOREGTRIB, pIDTIPOSECTOR, pIDTIPOACTIVIDAD, pIDTIPOCLASEENTIDAD, pIDTIPORAMAPUBLICA, pIDTIPOENTIDAD, pIDTIPONIVELGOB, pIDTIPONIVELORGANIZACIONAL, pIDTIPOORIGENCAPITAL, pIDTIPOCERTIFICADORCALIDAD, pIDTIPOCERTIFICATAMANO, pIDTIPONATURALEZAJURID, pIDTIPORANGOSACTIVOS);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consulta una Entidad ProvOferente
        /// </summary>
        /// <param name="pIDTERCERO"></param>
        /// <returns></returns>
        public EntidadProvOferente ConsultarEntidadProvOferente(int pIDTERCERO)
        {
            try
            {
                return vEntidadProvOferenteDAL.ConsultarEntidadProvOferente(pIDTERCERO);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de Entidad ProvOferentes
        /// </summary>
        /// <returns></returns>
        public List<EntidadProvOferente> ConsultarEntidadProvOferentes()
        {
            try
            {
                return vEntidadProvOferenteDAL.ConsultarEntidadProvOferentes();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta un lista de Entidad ProvOferentes de acuerdo a un IDTERCERO
        /// </summary>
        /// <param name="pIDTERCERO"></param>
        /// <returns></returns>
        public List<EntidadProvOferente> ConsultarEntidadProvOferentes(int? pIDTERCERO)
        {
            try
            {
                return vEntidadProvOferenteDAL.ConsultarEntidadProvOferentes(pIDTERCERO);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de Entidad ProvOferentes Validados
        /// </summary>
        /// <returns></returns>
         public List<EntidadProvOferente> ConsultarEntidadProvOferentesValidados()
        {
            try
            {
                return vEntidadProvOferenteDAL.ConsultarEntidadProvOferentesValidados();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
