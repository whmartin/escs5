﻿using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;

namespace Icbf.Oferente.Business
{
    public class EvaluadorBLL
    {
        private EvaluadorDAL vEvaluadorDAL;

        public EvaluadorBLL()
        {
            vEvaluadorDAL = new EvaluadorDAL();
        }

        public int InsertarEvaluador(Evaluador pEvaluador)
        {
            try
            {
                return vEvaluadorDAL.InsertarEvaluador(pEvaluador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Evaluador ConsultarEvaluador(int pIdUsuario)
        {
            try
            {
                return vEvaluadorDAL.ConsultarEvaluador(pIdUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Evaluador> ConsultarEvaluadoress(int pIdEquipoEvaluador, bool pActivo)
        {
            try
            {
                return vEvaluadorDAL.ConsultarEvaluadoress(pIdEquipoEvaluador, pActivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<Evaluador> ConsultarEvaluadoressEstado(bool pActivo)
        {
            try
            {
                return vEvaluadorDAL.ConsultarEvaluadoressEstado(pActivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarEvaluador(Evaluador pEvaluador)
        {
            try
            {
                return vEvaluadorDAL.ModificarEvaluador(pEvaluador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarEvaluadoress(Evaluador pEvaluador)
        {
            try
            {
                return vEvaluadorDAL.EliminarEvaluadoress(pEvaluador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Evaluador ConsultarEvaluadorDisponibleParaAsignarObservacion(int pIdEquipoEvaluador, int pIdTipoObservacion)
        {
            try
            {
                return vEvaluadorDAL.ConsultarEvaluadorDisponibleParaAsignarObservacion(pIdEquipoEvaluador, pIdTipoObservacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
