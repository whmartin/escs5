using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// Definici�n de clase NaturalezaJuridicaBLL
    /// </summary>
    public class NaturalezaJuridicaBLL
    {
        /// <summary>
        /// Declarac��n de objeto DAL 
        /// YConstructor donde se crea el objeto DAL vNaturalezaJuridicaDAL
        /// </summary>
        private NaturalezaJuridicaDAL vNaturalezaJuridicaDAL;
        public NaturalezaJuridicaBLL()
        {
            vNaturalezaJuridicaDAL = new NaturalezaJuridicaDAL();
        }

        /// <summary>
        /// Consultar Naturaleza Juridicas
        /// </summary>
        /// <param name="pCodigoNaturalezaJuridica"></param>
        /// <param name="pNombreNaturalezaJuridica"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<NaturalezaJuridica> ConsultarNaturalezaJuridicas(String pCodigoNaturalezaJuridica, String pNombreNaturalezaJuridica, Boolean? pEstado)
        {
            try
            {
                return vNaturalezaJuridicaDAL.ConsultarNaturalezaJuridicas(pCodigoNaturalezaJuridica, pNombreNaturalezaJuridica, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
