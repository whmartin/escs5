using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class OpcionalLegalBLL
    {
        private OpcionalLegalDAL vOpcionalLegalDAL;
        public OpcionalLegalBLL()
        {
            vOpcionalLegalDAL = new OpcionalLegalDAL();
        }
        public int InsertarOpcionalLegal(OpcionalLegal pOpcionalLegal)
        {
            try
            {
                return vOpcionalLegalDAL.InsertarOpcionalLegal(pOpcionalLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarOpcionalLegal(OpcionalLegal pOpcionalLegal)
        {
            try
            {
                return vOpcionalLegalDAL.ModificarOpcionalLegal(pOpcionalLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarOpcionalLegal(OpcionalLegal pOpcionalLegal)
        {
            try
            {
                return vOpcionalLegalDAL.EliminarOpcionalLegal(pOpcionalLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public OpcionalLegal ConsultarOpcionalLegal(int pIdOpcionalLegal)
        {
            try
            {
                return vOpcionalLegalDAL.ConsultarOpcionalLegal(pIdOpcionalLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<OpcionalLegal> ConsultarOpcionalLegals(String pCodigoOpcionalLegal, String pDescripcion, String pEstado)
        {
            try
            {
                return vOpcionalLegalDAL.ConsultarOpcionalLegals(pCodigoOpcionalLegal, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarCodigoOpcionalLegal(String pCodigoOpcionalLegal)
        {
            try
            {
                return vOpcionalLegalDAL.ConsultarCodigoOpcionalLegal(pCodigoOpcionalLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
