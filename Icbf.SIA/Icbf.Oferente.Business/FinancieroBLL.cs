using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    /// <summary>
    /// Definición de clase FinancieroBLL
    /// </summary>
    public class FinancieroBLL
    {
        /// <summary>
        /// Declaración de objeto DAL
        /// Y Constructor de clase donde se crea el objeto DAL vFinancieroDAL
        /// </summary>
        private FinancieroDAL vFinancieroDAL;
        public FinancieroBLL()
        {
            vFinancieroDAL = new FinancieroDAL();
        }
        /// <summary>
        /// Insertar Financiero
        /// </summary>
        /// <param name="pFinanciero"></param>
        /// <returns></returns>
        public int InsertarFinanciero(Financiero pFinanciero)
        {
            try
            {
                return vFinancieroDAL.InsertarFinanciero(pFinanciero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modificar Financiero
        /// </summary>
        /// <param name="pFinanciero"></param>
        /// <returns></returns>
        public int ModificarFinanciero(Financiero pFinanciero)
        {
            try
            {
                return vFinancieroDAL.ModificarFinanciero(pFinanciero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Eliminar Financiero
        /// </summary>
        /// <param name="pFinanciero"></param>
        /// <returns></returns>
        public int EliminarFinanciero(Financiero pFinanciero)
        {
            try
            {
                return vFinancieroDAL.EliminarFinanciero(pFinanciero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        ///  Consultar Financiero
        /// </summary>
        /// <param name="pIDInfoFin"></param>
        /// <returns></returns>
        public Financiero ConsultarFinanciero(int pIDInfoFin)
        {
            try
            {
                return vFinancieroDAL.ConsultarFinanciero(pIDInfoFin);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consultar Financieros
        /// </summary>
        /// <param name="pAnio"></param>
        /// <param name="IdOFerente"></param>
        /// <returns></returns>
        public List<Financiero> ConsultarFinancieros(int? pAnio,int? IdOFerente)
        {
            try
            {
                return vFinancieroDAL.ConsultarFinancieros(pAnio, IdOFerente);
                
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}

