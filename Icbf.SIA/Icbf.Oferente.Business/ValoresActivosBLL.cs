using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class ValoresActivosBLL
    {
        private ValoresActivosDAL vValoresActivosDAL;
        public ValoresActivosBLL()
        {
            vValoresActivosDAL = new ValoresActivosDAL();
        }
        public int InsertarValoresActivos(ValoresActivos pValoresActivos)
        {
            try
            {
                return vValoresActivosDAL.InsertarValoresActivos(pValoresActivos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarValoresActivos(ValoresActivos pValoresActivos)
        {
            try
            {
                return vValoresActivosDAL.ModificarValoresActivos(pValoresActivos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarValoresActivos(ValoresActivos pValoresActivos)
        {
            try
            {
                return vValoresActivosDAL.EliminarValoresActivos(pValoresActivos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ValoresActivos ConsultarValoresActivos(int pIdValoresActivos)
        {
            try
            {
                return vValoresActivosDAL.ConsultarValoresActivos(pIdValoresActivos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ValoresActivos> ConsultarValoresActivoss(String pCodigoValoresActivos, String pDescripcion, String pEstado)
        {
            try
            {
                return vValoresActivosDAL.ConsultarValoresActivoss(pCodigoValoresActivos, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarCodigoValoresActivos(String pCodigoValoresActivos)
        {
            try
            {
                return vValoresActivosDAL.ConsultarCodigoValoresActivos(pCodigoValoresActivos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
