using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class ValidarBLL
    {
        private ValidarDAL vValidarDAL;
        public ValidarBLL()
        {
            vValidarDAL = new ValidarDAL();
        }
        public int InsertarValidar(Validar pValidar)
        {
            try
            {
                return vValidarDAL.InsertarValidar(pValidar);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarValidar(Validar pValidar)
        {
            try
            {
                return vValidarDAL.ModificarValidar(pValidar);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarValidar(Validar pValidar)
        {
            try
            {
                return vValidarDAL.EliminarValidar(pValidar);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public Validar ConsultarValidar()
        {
            try
            {
                return vValidarDAL.ConsultarValidar();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Validar> ConsultarValidars(int? pIdTipoPersona, int? pIdTipoIdentificacion, String pNumeroIdentificacion, String pOferente, int? pIdEstado)
        {
            try
            {
                return vValidarDAL.ConsultarValidars(pIdTipoPersona, pIdTipoIdentificacion, pNumeroIdentificacion, pOferente, pIdEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoPersona> ConsultarTipoPersona()
        {
            try
            {
                return vValidarDAL.ConsultarTipoPersona();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoIdentificacion> ConsultarTipoIdentificacion(String pTipoPersona)
        {
            try
            {
                return vValidarDAL. ConsultarTipoIdentificacion(pTipoPersona);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ValidarOferente> ConsultarValidarOferente(int pIdTercero, int pIdOferente)
        {
            try
            {
                return vValidarDAL.ConsultarValidarOferente(pIdTercero, pIdOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarUsuarioValidacion(int pIdOferente, string pUsuarioProtege)
        {
            try
            {
                return vValidarDAL.ModificarUsuarioValidacion(pIdOferente, pUsuarioProtege);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public string ConsultarUsuarioProtegeValidar(int pIdOferente)
        {
            try
            {
                return vValidarDAL.ConsultarUsuarioProtegeValidar(pIdOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
