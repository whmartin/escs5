﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class DocumentoExperienciaBLL
    {
        private DocumentoExperienciaDAL vDocumentoExperienciaDAL;

        public DocumentoExperienciaBLL()
        {
            vDocumentoExperienciaDAL = new DocumentoExperienciaDAL();
        }
        public int InsertarObligatorioExperiencia(DocumentoExperiencia pDocumentoExperiencia)
        {
            try
            {
                return vDocumentoExperienciaDAL.InsertarObligatorioExperiencia(pDocumentoExperiencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarObligatorioExperiencia(DocumentoExperiencia pDocumentoExperiencia)
        {
            try
            {
                return vDocumentoExperienciaDAL.ModificarObligatorioExperiencia(pDocumentoExperiencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarObligatorioExperiencia(DocumentoExperiencia pDocumentoExperiencia)
        {
            try
            {
                return vDocumentoExperienciaDAL.EliminarObligatorioExperiencia(pDocumentoExperiencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DocumentoExperiencia ConsultarObligatorioExperiencia(int pIdDocumentoExperiencia)
        {
            try
            {
                return vDocumentoExperienciaDAL.ConsultarObligatorioExperiencia(pIdDocumentoExperiencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DocumentoExperiencia> ConsultarObligatorioExperiencias(String pCodigoDocumentoExperiencia, String pDescripcion, String pEstado)
        {
            try
            {
                return vDocumentoExperienciaDAL.ConsultarObligatorioExperiencias(pCodigoDocumentoExperiencia, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarCodigoObligatorioExperiencia(String pCodigoDocumentoExperiencia)
        {
            try
            {
                return vDocumentoExperienciaDAL.ConsultarCodigoObligatorioExperiencia(pCodigoDocumentoExperiencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}

