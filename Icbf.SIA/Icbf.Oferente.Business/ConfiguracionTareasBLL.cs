﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Business
{
    public class ConfiguracionTareasBLL
    {
        private ConfiguracionTareasDAL vConfiguracionTareasDAL;

        public ConfiguracionTareasBLL()
        {
            vConfiguracionTareasDAL = new ConfiguracionTareasDAL();
        }


        /// <summary>
        /// Insertar ConfiguracionTareas
        /// </summary>
        /// <param name="pConfiguracionTareas"></param>
        /// <returns></returns>
        public int InsertarConfiguracionTareas(ConfiguracionTareas pConfiguracionTareas)
        {
            try
            {
                return vConfiguracionTareasDAL.InsertarConfiguracionTareas(pConfiguracionTareas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar ConfiguracionTareas
        /// </summary>
        /// <returns></returns>
        public ConfiguracionTareas ConsultarConfiguracionTareas()
        {
            try
            {
                return vConfiguracionTareasDAL.ConsultarConfiguracionTareas();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Modificar ConfiguracionTareas
        /// </summary>
        /// <param name="pConfiguracionTareas"></param>
        /// <returns></returns>
        public int ModificarConfiguracionTareas(ConfiguracionTareas pConfiguracionTareas)
        {
            try
            {
                return vConfiguracionTareasDAL.ModificarConfiguracionTareas(pConfiguracionTareas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
