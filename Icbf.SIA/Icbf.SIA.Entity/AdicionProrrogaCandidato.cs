using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    /// <summary>
    /// Este modulo almacenar la informacion de las adiciones y prorrogas del candidato
    /// </summary>
    public class AdicionProrrogaCandidato : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdAdicionProrrogaCandidato
        {
            get;
            set;
        }
        public int IdCupoArea
        {
            get;
            set;
        }
        public int IdProyeccionPresuspuesto
        {
            get;
            set;
        }
        public bool Rechazado
        {
            get;
            set;
        }

        public String RazonRechazo
        {
            get;
            set;
        }
        public bool Aprobado
        {
            get;
            set;
        }
        public DateTime FechaIncioProyeccion
        {
            get;
            set;
        }
        public DateTime FechaFinProyeccion
        {
            get;
            set;
        }
        public Decimal ValorAdicionado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public AdicionProrrogaCandidato()
        {
        }
    }
}
