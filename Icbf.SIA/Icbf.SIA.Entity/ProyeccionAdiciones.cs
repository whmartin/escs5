using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    /// <summary>
    /// Permite la manipulación de la proyección de los presupuestos
    /// </summary>
    public class ProyeccionAdiciones : Icbf.Seguridad.Entity.EntityAuditoria
    {
       
        public int IdRegional
        {
            get;
            set;
        }
        public int IdAdicionProyeccion
        {
            get;
            set;
        }
        public bool Aprobacion
        {
            get;
            set;
        }

        public bool Rechazado
        {
            get;
            set;
        }
        public int IdProyeccionPresupuestos
        {
            get;
            set;
        }
        public int IdVigencia
        {
            get;
            set;
        }
        public int IdRecurso
        {
            get;
            set;
        }
        public int IdRubro
        {
            get;
            set;
        }
        public int IdArea
        {
            get;
            set;
        }
        public string DescrArea
        {
            get;
            set;
        }

        public string NombreArea
        {
            get;
            set;
        }

        public Decimal ValorCupo
        {
            get;
            set;
        }
        public Decimal ValorInicial
        {
            get;
            set;
        }

        public Decimal ValorAdicionado
        {
            get;
            set;
        }
        public Decimal ValorCupoAprobado
        {
            get;
            set;
        }
        public int TotalCupos
        {
            get;
            set;
        }
        public int TotalCuposAprobado
        {
            get;
            set;
        }
        public int EstadoProceso
        {
            get;
            set;
        }

        public String DescEstadoProceso { get; set; }

        public String RazonRechazo { get; set; }

        public string Descripcion
        {
            get;
            set;
        }

        public bool Aprobado
        {
            get;
            set;
        }
        public string CodEstadoProceso
        {
            get;
            set;
        }
        public String UsuarioAprobo
        {
            get;
            set;
        }
        public DateTime? FechaAprobacion
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }

        public string NombreRegional { get; set; }
        public string AcnoVigencia { get; set; }
        public String CodigoRubro { get; set; }

        public String DescripcionEstado { get; set; }

        public String DESCTIPORECURSO { get; set; }
        public int IdEstadoProceso { get; set; }
        public string CodEstado { get; set; }
        public string DescripcionRubro { get; set; }
        public int NecesidadesAsignadas { get; set; }
        public int NecesidadesDisponibles { get; set; }
        public ProyeccionAdiciones()
        {
        }


    }
}

