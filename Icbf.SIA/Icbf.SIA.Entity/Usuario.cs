﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    public class Usuario : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdUsuario
        /// </summary>
        public int IdUsuario
        {
            set;
            get;
        }
        /// <summary>
        /// Propiedad TipoUsuario
        /// </summary>
        public int TipoUsuario
        {
            set;
            get;
        }
        /// <summary>
        /// Propiedad Providerkey
        /// </summary>
        public String Providerkey
        {
            set;
            get;
        }
        /// <summary>
        /// Propiedad IdTipoPersona
        /// </summary>
        public int IdTipoPersona
        {
            set;
            get;
        }
        /// <summary>
        /// Propiedad IdTipoDocumento
        /// </summary>
        public int IdTipoDocumento
        {
            set;
            get;
        }
        /// <summary>
        /// Propiedad TipoDocumento
        /// </summary>
        public String TipoDocumento
        {
            set;
            get;
        }
        /// <summary>
        /// Propiedad NumeroDocumento
        /// </summary>
        public String NumeroDocumento
        {
            set;
            get;
        }
        /// <summary>
        /// Propiedad DV
        /// </summary>
        public String DV
        {
            set;
            get;
        }
        /// <summary>
        /// Propiedad PrimerNombre
        /// </summary>
        public String PrimerNombre
        {
            set;
            get;
        }
        /// <summary>
        /// Propiedad SegundoNombre
        /// </summary>
        public String SegundoNombre
        {
            set;
            get;
        }
        /// <summary>
        /// Propiedad PrimerApellido
        /// </summary>
        public String PrimerApellido
        {
            set;
            get;
        }
        /// <summary>
        /// Propiedad SegundoApellido
        /// </summary>
        public String SegundoApellido
        {
            set;
            get;
        }
        /// <summary>
        /// Propiedad RazonSocial
        /// </summary>
        public String RazonSocial
        {
            set;
            get;
        }
        /// <summary>
        /// Propiedad TelefonoContacto
        /// </summary>
        public String TelefonoContacto
        {
            set;
            get;
        }
        /// <summary>
        /// Propiedad CorreoElectronico
        /// </summary>
        public String CorreoElectronico
        {
            set;
            get;
        }
        /// <summary>
        /// Propiedad NombreUsuario
        /// </summary>
        public String NombreUsuario
        {
            set;
            get;
        }
        /// <summary>
        /// Propiedad Contrasena
        /// </summary>
        public String Contrasena
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Rol
        /// </summary>
        public String Rol
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoValidacion
        /// </summary>
        public String CodigoValidacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad tipo Usuario
        /// </summary>
        public int IdTipoUsuario
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Regional
        /// </summary>
        public int? IdRegional
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Contratista
        /// </summary>
        public int? IdEntidadContratista
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCreacion
        /// </summary>
        public String UsuarioCreacion
        {
            set;
            get;
        }
        /// <summary>
        /// Propiedad UsuarioModificacion
        /// </summary>
        public String UsuarioModificacion
        {
            set;
            get;
        }
        /// <summary>
        /// Propiedad FechaCreacion
        /// </summary>
        public DateTime FechaCreacion
        {
            set;
            get;
        }
        /// <summary>
        /// Propiedad FechaModificacion
        /// </summary>
        public DateTime FechaModificacion
        {
            set;
            get;
        }

        public bool ExisteTercero { get; set; }

        public string NombreArea { get; set; }
        public string NombreCompleto { get; set; }
        
        public Usuario()
        {
        }
    }
}
