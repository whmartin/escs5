using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    /// <summary>
    /// Permite la manipulación de la categoría de los valores y el nivel
    /// </summary>
    public class CategoriaValores : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdCategoriaValor
        {
            get;
            set;
        }
        public int IdCategoriaEmpleados
        {
            get;
            set;
        }
        public int Nivel
        {
            get;
            set;
        }
        public Decimal ValorMinimo
        {
            get;
            set;
        }
        public Decimal ValorMaximo
        {
            get;
            set;
        }
        public int IdVigencia
        {
            get;
            set;
        }
        public String AcnioVigencia
        {
            get;
            set;
        }
        public string CategoriaEmpleado { get; set; }
        public Boolean Activo
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public CategoriaValores()
        {
        }
    }
}
