using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    public class Regional : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdRegional
        {
            get;
            set;
        }
        public String CodigoRegional
        {
            get;
            set;
        }
        public String NombreRegional
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public String CodigoNombreRegional
        {
            get;
            set;
        }
        public String CodigoRegionalPCI { get; set; }
        public Regional()
        {
        }
    }
}
