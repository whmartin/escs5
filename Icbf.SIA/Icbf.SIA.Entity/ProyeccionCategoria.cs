using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    /// <summary>
    /// Este modulo sirve para guardar la distrubucion de las proyecciones
    /// </summary>
    public class ProyeccionCategoria : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IDProyeccionCategoria
        {
            get;
            set;
        }

        public int IdProyeccionPresupuestos
        {
            get;
            set;
        }
        public int IDProyeccion
        {
            get;
            set;
        }
        public int IDCategoria
        {
            get;
            set;
        }

        //Descripcion categoria
        public string Descripcion
        {
            get;
            set;
        }
        public int Cantidad
        {
            get;
            set;
        }
        public int CantidadAsignada
        {
            get;
            set;
        }
        public int CantidadDisponible
        {
            get;
            set;
        }
        public int CantidadAprobada
        {
            get;
            set;
        }
        public string NombreRegional
        {
            get;
            set;
        }
        public string NombreArea
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public int IdCategoriaValor
        {
            get;
            set;
        }
        public int Nivel
        {
            get;
            set;
        }
        public int IdEstadoProceso
        {
            get;
            set;
        }
        public string CodEstadoProceso
        {
            get;
            set;
        }
        public String DescEstadoProceso
        {
            get;
            set;
        }
        public String DescripcionRechazo
        {
            get;
            set;
        }
        public bool? Aprobado
        {
            get;
            set;
        }
        public ProyeccionCategoria()
        {
        }
    }
}
