﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity.Concursales
{
    [Serializable]
    public class ProcesoConcursalDocumento
    {
        public int IdDocumento { get; set; }
        public int IdProceso { get; set; }
        public string NombreDocumento { get; set; }
        public string NombreOriginal { get; set; }
        public int IdTipoDocumento { get; set; }
        public string TipoDocumento { get; set; }
        public int IdMedioPublicacion { get; set; }
        public string MedioPublicacion { get; set; }
        public int IdTipoPublicacion { get; set; }
        public string TipoPublicacion { get; set; }
        public DateTime FechaPublicacion { get; set; }
        public string UsuarioCrea { get; set; }

        public DateTime  FechaCreacion { get; set; }
    }
}
