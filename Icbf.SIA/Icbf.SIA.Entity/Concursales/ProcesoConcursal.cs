﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity.Concursales
{
    public class ProcesoConcursal
    {
        public int IdProceso { get; set; }
        public string Consecutivo { get; set; }
        public decimal? CapitalInicial { get; set; }
        public decimal? InteresesInicial { get; set; }
        public decimal? SaldoCapital { get; set; }
        public decimal? SaldoInteres { get; set; }
        public string CodigoRegional { get; set; }
        public string Regional { get; set; }
        public int IdTercero { get; set; }
        public string NumeroIdentificacion { get; set; }
        public string RazonSocial { get; set; }
        public int IdEstado { get; set; }
        public string NombreEstado { get; set; }
        public string CodigoEstado { get; set; }
        public String UsuarioAsignado { get; set; }
        public int? IdRegionalTraslado { get; set; }
        public int? IdEstadoOriginal { get; set; }
        public string RegionalTraslado { get; set; }
    }

    public class ProcesoConcursalNovedad
    {
        public string Observaciones { get; set; }
        public int ? IdRegionalTraslado { get; set; }
        public int IdProceso { get; set; }
        public string UsuarioNovedad { get; set; }
        public string Accion { get; set; }
    }

    public class ProcesoConcursalGestionNovedad
    {
        public string Observaciones { get; set; }
        public int IdProceso { get; set; }
        public string UsuarioNovedad { get; set; }
        public string UsuarioAsignar { get; set; }
        public string Estado { get; set; }
        public string Accion { get; set; }
    }


    public class ProcesoConcursalGestion
    { 
        public int IdProcesoConcursalGestion { get; set; }
        public int IdProcesoConcursal { get; set; }
        public int IdEstado { get; set; }
        public string Estado { get; set; }
        public string Observaciones { get; set; }
        public string Usuario { get; set; }
        public DateTime Fecha { get; set; }
    }
}
