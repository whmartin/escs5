﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity.Concursales
{
    public class CarteraActiva
    {
        public string CodRegionalICBF { get; set; }
        public string CodRegionalPCI { get; set; }
        public string DescripcionPCI { get; set; }
        public string identificacion { get; set; }
        public string razonSocial { get; set; }
        public decimal capitalInicial { get; set; }
        public decimal interesesInicial { get; set; }
        public decimal saldoCapital { get; set; }
        public decimal saldoIntereses { get; set; }
        public string liquidacion { get; set; }
        public string resolucion { get; set; }
        public int idCarteraActiva { get; set; }
        public int IdTercero { get; set; }
        public string UsuarioCrea { get; set; }
        public string IdsDocumentos { get; set; }
    }

    public class CarteraAgrupada
    {
        public string identificacion { get; set; }
        public string razonSocial { get; set; }
        public decimal capitalInicial { get; set; }
        public decimal interesesInicial { get; set; }
        public decimal saldoCapital { get; set; }
        public decimal saldoIntereses { get; set; }
    }
}
