﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Seguridad.Entity;

namespace Icbf.SIA.Entity.Concursales
{
    public class Clasificador : EntityAuditoria
    {
        public int IdClasificador { get; set; }
        public Boolean Estado { get; set; }
        public string Nombre { get; set; }        
        public Boolean RequiereObservaciones { get; set; }
        public Boolean RequiereAuxiliar1 { get; set; }
        public Boolean RequiereAuxiliar2 { get; set; }
        public string LabelAuxiliar1 { get; set; }
        public string LabelAuxiliar2 { get; set; }
        public string UsuarioCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaCrea { get; set; }
        public DateTime FechaModifica { get; set; }
        public Boolean EsCustom { get; set; }
        public string ParametricaCustom { get; set; }

    }

    public class Valor : EntityAuditoria
    {
        public int IdValor { get; set; }
        public Boolean Estado { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int IdClasificador { get; set; }
        public Boolean Observaciones { get; set; }
        public string Auxiliar1 { get; set; }
        public string Auxiliar2 { get; set; }
        public string UsuarioCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaCrea { get; set; }
        public DateTime FechaModifica { get; set; }
    }

    public class TipoProcesoNaturaleza : EntityAuditoria
    {
        public int IdTipoProcesoNaturaleza { get; set; }
        public int IdTipoProceso { get; set; }
        public int IdNaturaleza { get; set; }
        public Boolean Estado { get; set; }
        public string UsuarioCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaCrea { get; set; }
        public DateTime FechaModifica { get; set; }
        public string Nombre { get; set; }
        public string Nombre2 { get; set; }
    }

}
