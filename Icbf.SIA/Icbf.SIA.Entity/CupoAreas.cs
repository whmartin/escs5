using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    /// <summary>
    /// Permite la manipulación de los cupos de las áreas
    /// </summary>
    public class CupoAreas : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdCupoArea
        {
            get;
            set;
        }

        public int IdAdicionProrroga
        {
            get;
            set;
        }
    public int IdProyeccionPresupuestos
        {
            get;
            set;
        }
        public int IdCategoriaEmpleado
        {
            get;
            set;
        }
        public String NombreArea
        {
            get;
            set;
        }
        public String ConsecutivoInterno
        {
            get;
            set;
        }
        public int IdObjeto
        {
            get;
            set;
        }
        public String Objeto
        {
            get;
            set;
        }
        public String IdDependenciaKactus
        {
            get;
            set;
        }
        public int? IdProveedor
        {
            get;
            set;
        }
        public DateTime FechaIngresoICBF
        {
            get;
            set;
        }
        public int IdCategoriaValores
        {
            get;
            set;
        }
        public Decimal HonorarioBase
        {
            get;
            set;
        }
        public Decimal IvaHonorario
        {
            get;
            set;
        }
        public Decimal PorcentajeIVA
        {
            get;
            set;
        }
        public DateTime FechaInicialProyectado
        {
            get;
            set;
        }
        public DateTime FechaFinalProyectado
        {
            get;
            set;
        }

        public int TiempoProyectadoMeses
        {
            get;
            set;
        }
        public int TiempoProyectadoDias
        {
            get;
            set;
        }
        public int TiempoProyectadoAnios
        {
            get;
            set;
        }
        public Int32 IdEstadoProceso
        {
            get;
            set;
        }
        public String CodEstadoProceso
        {
            get;
            set;
        }
        public String UsuarioAprobo
        {
            get;
            set;
        }
        public DateTime FechaAprobacion
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public decimal TotalHonorarioTiempoProyectado
        {
            get;
            set;
        }

        public string NombreDependenciaSolicitante { get; set; }
        public string CategoriaEmpleados { get; set; }

        public string NombreTercero { get; set; }
        public Int32 Nivel { get; set; }


        public CupoAreas()
        {
        }
    }
}
