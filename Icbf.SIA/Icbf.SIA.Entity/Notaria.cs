﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    using System;

    /// <summary>
    /// Clase Entidad Notarias con sus atributos para transportar los datos entre las diferentes capas. 
    /// </summary>
    [Serializable]
    public class Notaria : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Obtiene o establece CodNotaria
        /// </summary>
        public string CodNotaria { get; set; }

        /// <summary>
        /// Obtiene o establece NombreNotaria
        /// </summary>
        public string NombreNotaria { get; set; }

        /// <summary>
        /// Obtiene o establece DepartamentoNotaria
        /// </summary>
        public string DepartamentoNotaria { get; set; }

        /// <summary>
        /// Obtiene o establece CiudadNotaria
        /// </summary>
        public string CiudadNotaria { get; set; }

    }
}
