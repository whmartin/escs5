using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    /// <summary>
    /// Definición de clase TipoSituacionEmocional
    /// </summary>
    public class TipoSituacionEmocional : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdTipoSituacionEmocional
        /// </summary>
        public int IdTipoSituacionEmocional
        {
            get;
            set;
        }
        /// <summary>
        /// propiedad CodigoTipoSituacionEmocional
        /// </summary>
        public String CodigoTipoSituacionEmocional
        {
            get;
            set;
        }
        /// <summary>
        /// propiedad NombreTipoSituacionEmocional
        /// </summary>
        public String NombreTipoSituacionEmocional
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public String Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase TipoSituacionEmocional
        /// </summary>
        public TipoSituacionEmocional()
        {
        }
    }
}
