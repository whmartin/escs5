
using System;

namespace Icbf.SIA.Entity
{
    [Serializable]
    public class Municipio : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public String CodigoMunicipio
        {
            get;
            set;
        }
        public String NombreMunicipio
        {
            get;
            set;
        }
        public int IdMunicipio
        {
            get;
            set;
        }
        public int IdDepartamento
        {
            get;
            set;
        }
       
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public Municipio()
        {
        }
    }
}