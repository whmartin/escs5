
using System;

namespace Icbf.SIA.Entity
{
    [Serializable]
    public class Departamento : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdDepartamento
        {
            get;
            set;
        }
        public String CodigoDepartamento
        {
            get;
            set;
        }
        public String NombreDepartamento
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public int IdPais
        {
            get;
            set;
        }
      
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public Departamento()
        {
        }
    }
}
