using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    /// <summary>
    /// Este modulo almacenar la informacion de las adiciones del candidato
    /// </summary>
    public class HistoricoProrrogaCandidato : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdHistoricoProrrogaCandidato
        {
            get;
            set;
        }
        public int IdCupoArea
        {
            get;
            set;
        }
        public int IdProyeccionPresuspuesto
        {
            get;
            set;
        }
        public DateTime FechaIncioAnterior
        {
            get;
            set;
        }
        public DateTime FechaIncioNueva
        {
            get;
            set;
        }
        public DateTime FechaFinAnterior
        {
            get;
            set;
        }
        public DateTime FechaFinNueva
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public HistoricoProrrogaCandidato()
        {
        }
    }
}
