using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    public class RepresentanteLegal : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdRepresentanteLegal
        /// </summary>
        public int IdRepresentanteLegal
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoDocumento
        /// </summary>
        public int IdTipoDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodDocumento
        /// </summary>
        public String CodDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdDepartamento
        /// </summary>
        public int IdDepartamento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreDepartamento
        /// </summary>
        public String NombreDepartamento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public int IdMunicipio
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreMunicipio
        /// </summary>
        public String NombreMunicipio
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdCentroPoblado
        /// </summary>
        public int IdCentroPoblado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdComuna
        /// </summary>
        public int IdComuna
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdBarrio
        /// </summary>
        public int IdBarrio
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Identificacion
        /// </summary>
        public String Identificacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PrimerNombre
        /// </summary>
        public String PrimerNombre
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad SegundoNombre
        /// </summary>
        public String SegundoNombre
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PrimerApellido
        /// </summary>
        public String PrimerApellido
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad SegundoApellido
        /// </summary>
        public String SegundoApellido
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Telefono
        /// </summary>
        public String Telefono
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Celular
        /// </summary>
        public String Celular
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ZonaUbicacion
        /// </summary>
        public String ZonaUbicacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad TipoCabecera
        /// </summary>
        public String TipoCabecera
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreZonaResto
        /// </summary>
        public String NombreZonaResto
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Direccion
        /// </summary>
        public String Direccion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CorreoElectronico
        /// </summary>
        public String CorreoElectronico
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public RepresentanteLegal()
        {
        }
    }
}
