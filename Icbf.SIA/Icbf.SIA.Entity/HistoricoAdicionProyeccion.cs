using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    /// <summary>
    /// Este modulo almacenar la informacion de rechazo de las adiciones
    /// </summary>
    public class HistoricoAdicionProyeccion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdHistoricoAdicionProyeccion
        {
            get;
            set;
        }
        public int IdAdicionProyeccion
        {
            get;
            set;
        }
        public String RazonRechazo
        {
            get;
            set;
        }
        public Boolean Rechazado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public HistoricoAdicionProyeccion()
        {
        }
    }
}
