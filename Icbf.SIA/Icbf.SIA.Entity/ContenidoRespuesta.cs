﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.SIA.Entity
{
    public class ContenidoRespuesta
    {
        public List<RegistroParametrica> ListRegistroParametrica { get; set; }
        public Int64 IdParametrica { get; set; }
        public string CodigoParametrica { get; set; }
        public string NombreParametrica { get; set; }
        public string DescripcionParametrica { get; set; }
        public Int64 TipoParametrica { get; set; }
        public bool EstadoParametrica { get; set; }
        public string Componente { get; set; }
        public string _NombreCampo { get; set; }
        public string _Respuesta { get; set; }
        public string[] ListCamposParametrica { get; set; }
        public string FechaCreacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string FechaModificacion { get; set; }
        public string UsuarioModificacion { get; set; }
    }
}
