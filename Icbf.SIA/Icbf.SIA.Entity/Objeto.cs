using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    /// <summary>
    /// Permite la manipulación de los objetos de los cupos
    /// </summary>
    public class Objeto : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdObjetoCupos
        {
            get;
            set;
        }
        public String Titulo
        {
            get;
            set;
        }
        public String DescripcionObjeto
        {
            get;
            set;
        }
        public Boolean Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public Objeto()
        {
        }
    }
}
