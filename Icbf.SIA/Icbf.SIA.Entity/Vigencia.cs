using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    /// <summary>
    /// Definición de clase Vigencia
    /// </summary>
    public class Vigencia : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdVigencia
        /// </summary>
        public int IdVigencia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad AcnoVigencia
        /// </summary>
        public int AcnoVigencia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiead Activo
        /// </summary>
        public String Activo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase Vigencia
        /// </summary>
        public Vigencia()
        {
        }
    }
}
