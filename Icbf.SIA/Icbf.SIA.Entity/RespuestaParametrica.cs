﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.SIA.Entity
{
    public class RespuestaParametrica
    {
        public string CodigoRespuesta { get; set; }

        public string MensajeRespuesta { get; set; }

        public List<ContenidoRespuesta> ContenidoRespuesta { get; set; }
    }
}
