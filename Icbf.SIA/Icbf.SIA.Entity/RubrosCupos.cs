using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    /// <summary>
    /// Permite la manipulación de los rubros
    /// </summary>
    public class RubrosCupos : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdRubroCupos
        {
            get;
            set;
        }
        public String CodigoRubro
        {
            get;
            set;
        }
        public String DescripcionRubro
        {
            get;
            set;
        }
        public String DescripcionRubroCompleto
        {
            get;
            set;
        }
        public Boolean Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public RubrosCupos()
        {
        }
    }
}
