using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    public class FormatoArchivo : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdFormatoArchivo
        /// </summary>
        public int IdFormatoArchivo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoEstructura
        /// </summary>
        public int IdTipoEstructura
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdModalidad
        /// </summary>
        public int IdModalidad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Separador
        /// </summary>
        public String Separador
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad TablaTemporal
        /// </summary>
        public String TablaTemporal
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Extension
        /// </summary>
        public String Extension
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public String Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        //public List<Columna> Columnas
        //{
        //    set;
        //    get;
        //}

        //public bool ValidarCampos
        //{
        //    get;
        //    set;
        //}


        //public string NombreModalidad
        //{
        //    get;
        //    set;
        //}
        public FormatoArchivo()
        {
        }
    }
}
