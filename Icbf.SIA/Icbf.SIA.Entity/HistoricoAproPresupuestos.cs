﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    public class HistoricoAproPresupuestos
    {
        public String MotivoDevolucion { get; set; }
        public DateTime FechaCrea { get; set; }
        public DateTime FechaRechazo { get; set; }
        public string UsuarioRechazo { get; set; }
    }
}
