using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    /// <summary>
    /// Este modulo almacenar la adicion de proyecciones por modalidad de proveedores
    /// </summary>
    public class AdicionProyeccion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdAdicionProyeccion
        {
            get;
            set;
        }
        public int IdProyeccionPresupuestos
        {
            get;
            set;
        }
        public Decimal ValorAdicionado
        {
            get;
            set;
        }
        public Boolean Aprobado
        {
            get;
            set;
        }

        public Boolean Rechazado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public AdicionProyeccion()
        {
        }
    }
}
