using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    public class Reporte : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdReporte {get;set;}
        public String NombreReporte {get;set;}
        public String Descripcion {get;set;}
        public String Servidor {get;set;}
        public String Carpeta {get;set;}
        public String NombreArchivo {get;set;}
        public String UsuarioCrea {get;set;}
        public String UsuarioModifica {get;set;}
        public DateTime FechaCrea {get;set;}
        public DateTime FechaModifica {get;set;}
        public Reporte(){}
    }
}
