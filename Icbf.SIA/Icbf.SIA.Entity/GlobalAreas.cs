﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    public class GlobalAreas
    {
        public int IdArea { get; set; }
        public int CodArea { get; set; }
        public string NombreArea { get; set; }
        public int EstadoArea { get; set; }

    }
}
