﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    public class TipoRecursoFinPptal
    {
        public int IdTipoRecursoFinPptal { get; set; }
        public string CodTipoRecursoFinPptal { get; set; }
        public string DescTipoRecurso { get; set; }
        public bool Estado { get; set; }
    }
}
