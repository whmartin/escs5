using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    /// <summary>
    /// Permite la manipulación de la categoría de los empleados
    /// </summary>
    public class CategoriaEmpleado : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdCategoria
        {
            get;
            set;
        }
        public String Descripcion
        {
            get;
            set;
        }
        public String Codigo
        {
            get;
            set;
        }
        public String CodigoProducto
        {
            get;
            set;
        }
        public int Vigencia
        {
            get;
            set;
        }
        public Boolean Activo
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public CategoriaEmpleado()
        {
        }
    }
}
