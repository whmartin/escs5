﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    using System;

    public partial class RegistroParametrica
    {
        public Int64 IdRegistro { get; set; }
        public Int64 IdParametrica { get; set; }
        public string CodigoRegistro { get; set; }
        public string NombreRegistro { get; set; }
        public string DescripcionRegistro { get; set; }
        public bool EstadoRegistro { get; set; }
        public string Campo0 { get; set; }
        public string Campo1 { get; set; }
        public string Campo2 { get; set; }
        public string Campo3 { get; set; }
        public string Campo4 { get; set; }
        public string Campo5 { get; set; }
        public string Campo6 { get; set; }
        public string Campo7 { get; set; }
        public string Campo8 { get; set; }
        public string Campo9 { get; set; }
    }
}
