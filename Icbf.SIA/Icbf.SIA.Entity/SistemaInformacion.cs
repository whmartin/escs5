﻿//-----------------------------------------------------------------------
// <copyright file="SistemaInformacion.cs" company="ICBF"> 
// Copyright (c) 2017 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase SistemaInformacion.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>20/12/2017</date>
//-----------------------------------------------------------------------

using System;

namespace Icbf.SIA.Entity
{
    /// <summary>
    /// Clase entidad para registro de SistemaInformacion
    /// </summary>
    public class SistemaInformacion
    {
        /// <summary>
        /// Gets or sets IdSistemaInformacion. Propiedad PK autonumérica
        /// </summary>
        /// <value>El IdSistemaInformacion</value>
        public int IdSistemaInformacion { get; set; }

        /// <summary>
        /// Gets or sets NombreSistemaInformacion.
        /// </summary>
        /// <value>El NombreSistemaInformacion</value>
        public string NombreSistemaInformacion { get; set; }

        /// <summary>
        /// Gets or sets UsuarioCrea.
        /// </summary>
        /// <value>El UsuarioCrea</value>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Gets or sets FechaCrea.
        /// </summary>
        /// <value>El FechaCrea</value>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Gets or sets UsuarioModifica.
        /// </summary>
        /// <value>El UsuarioModifica</value>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Gets or sets FechaModifica.
        /// </summary>
        /// <value>El FechaModifica</value>
        public DateTime? FechaModifica { get; set; }
    }
}
