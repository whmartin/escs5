﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    public class AreasInternasNMF
    {
        public int IdArea { get; set; }
        public string CodArea { get; set; }
        public string NombreArea { get; set; }
        public int EstadoArea { get; set; }

    }
}
