using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    public class TipoDocumento : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdTipoDocumento
        /// </summary>
        public int IdTipoDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Codigo
        /// </summary>
        public String Codigo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Nombre
        /// </summary>
        public String Nombre
        {
            get;
            set;
        }
        public TipoDocumento()
        {
        }
    }
}
