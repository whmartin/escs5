﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Entity
{
    public class CatGralRubrosPptalGasto
    {
        public int IdCatalogo { get; set; }
        public string RubroSIIF { get; set; }
        public string TipoRubro { get; set; }
        public string CtaPrograma { get; set; }
        public string DescripcionRubro { get; set; }
        public bool Estado { get; set; }
        public string DescripcionRubroCompleto { get; set; }
      
	
    }
}
