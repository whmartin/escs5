﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using Icbf.Oferente.Business;

namespace Icbf.Oferente.Service
{
    //Carlos Cubillos
    /// <summary>
    /// Definición de clase OferenteService
    /// </summary>
    public partial class OferenteService
    {
        /// <summary>
        /// Creación de Objeto vTelTercerosBLL
        /// </summary>
        private TelTercerosBLL vTelTercerosBLL = new TelTercerosBLL();
        
        /// <summary>
        /// Insertar Tel Terceros
        /// </summary>
        /// <param name="pTelTerceros"></param>
        /// <returns></returns>
        public int InsertarTelTerceros(TelTerceros pTelTerceros)
        {
            try
            {
                return vTelTercerosBLL.InsertarTelTerceros(pTelTerceros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        
        /// <summary>
        /// Consultar Tel Terceros IdTercero
        /// </summary>
        /// <param name="pIdTercero"></param>
        /// <returns></returns>
        public TelTerceros ConsultarTelTercerosIdTercero(int pIdTercero)
        {
            try
            {
                return vTelTercerosBLL.ConsultarTelTercerosIdTercero(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
     
        
    }
}
