﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using Icbf.Oferente.Business;

namespace Icbf.Oferente.Service
{
    /// <summary>
    /// Definciión de clase OferenteServiceTP
    /// </summary>
    public partial class OferenteService
    {
        /// <summary>
        /// Consultar TipoPersona
        /// </summary>
        /// <param name="pIdTipoPersona"></param>
        /// <returns></returns>
        public TipoPersona ConsultarTipoPersona(int pIdTipoPersona)
        {
            try
            {
                return vTipoPersonaBLL.ConsultarTipoPersona(pIdTipoPersona);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consulta Tipo Personas.
        /// </summary>
        /// <param name="pCodigoTipoPersona"></param>
        /// <param name="pNombreTipoPersona"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoPersona> ConsultarTipoPersonas(String pCodigoTipoPersona, String pNombreTipoPersona, Boolean? pEstado)
        {
            try
            {
                return vTipoPersonaBLL.ConsultarTipoPersonas(pCodigoTipoPersona, pNombreTipoPersona, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

   

    }
}