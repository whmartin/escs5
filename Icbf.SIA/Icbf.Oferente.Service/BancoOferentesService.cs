﻿using Icbf.Oferente.Business;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;

namespace Icbf.Oferente.Service
{
    public class BancoOferentesService
    {
        private ConfiguracionTareasBLL _vConfiguracionTareasBLL;
        private EquipoEvaluadoresBLL _vEquipoEvaluadoresBLL;
        private TipoObservacionesBLL _vTipoObservacionesBLL;
        private EvaluadorBLL _vEvaluadorBLL;
        private ObservacionesConvocatoriaBLL _vObservacionesConvocatoriaBLL;
        private ObservacionesPendientesBLL _vObservacionesPendientesBLL;
        private HistoricoObservacionesConvocatoriaBLL _vHistoricoObservacionesConvocatoriaBLL;
        private ConvocatoriaBLL _vConvocatoriaBLL;
        private ConvocatoriasEquiposEvaluadoresBLL _vConvocatoriasEquiposEvaluadoresBLL;
        private EstadoObservacionesBLL _vEstadoObservacionesBLL;

        public BancoOferentesService()
        {
            _vConfiguracionTareasBLL = new ConfiguracionTareasBLL();
            _vEquipoEvaluadoresBLL = new EquipoEvaluadoresBLL();
            _vTipoObservacionesBLL = new TipoObservacionesBLL();
            _vEvaluadorBLL = new EvaluadorBLL();
            _vObservacionesConvocatoriaBLL = new ObservacionesConvocatoriaBLL();
            _vObservacionesPendientesBLL = new ObservacionesPendientesBLL();
            _vHistoricoObservacionesConvocatoriaBLL = new HistoricoObservacionesConvocatoriaBLL();
            _vConvocatoriaBLL = new ConvocatoriaBLL();
            _vConvocatoriasEquiposEvaluadoresBLL = new ConvocatoriasEquiposEvaluadoresBLL();
            _vEstadoObservacionesBLL = new EstadoObservacionesBLL();
        }

        #region ConfiguracionTareas
        /// <summary>
        /// Insertar ConfiguracionTareas
        /// </summary>
        /// <param name="pConfiguracionTareas"></param>
        /// <returns></returns>
        public int InsertarConfiguracionTareas(ConfiguracionTareas pConfiguracionTareas)
        {
            try
            {
                return _vConfiguracionTareasBLL.InsertarConfiguracionTareas(pConfiguracionTareas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar ConfiguracionTareas
        /// </summary>
        /// <returns></returns>
        public ConfiguracionTareas ConsultarConfiguracionTareas()
        {
            try
            {
                return _vConfiguracionTareasBLL.ConsultarConfiguracionTareas();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
       
        /// <summary>
        /// Modificar ConfiguracionTareas
        /// </summary>
        /// <param name="pConfiguracionTareas"></param>
        /// <returns></returns>
        public int ModificarConfiguracionTareas(ConfiguracionTareas pConfiguracionTareas)
        {
            try
            {
                return _vConfiguracionTareasBLL.ModificarConfiguracionTareas(pConfiguracionTareas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Convocatorias

        public List<Convocatoria> ConsultarConvocatorias(int? numero, DateTime? fechaPublicacionD, DateTime? fechaPublicacionP, int? Estado, int? Regional)
        {
            try
            {
                return new ConvocatoriaBLL().ObtenerConvocatorias(numero, fechaPublicacionD, fechaPublicacionP, Estado, Regional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Dictionary<int, string> ConsultarEstados()
        {
            try
            {
                return new ConvocatoriaBLL().ConsultarEstados();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Convocatoria ConsultarConvocatoriaPorId(int id)
        {
            try
            {
                return new ConvocatoriaBLL().ConsultarConvocatoriaPorId(id);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int CrearConvocatoria(Convocatoria item)
        {
            try
            {
                return new ConvocatoriaBLL().CrearConvocatoria(item);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarConvocatoria(Convocatoria item)
        {
            try
            {
                return new ConvocatoriaBLL().ActualizarConvocatoria(item);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarSolicitanteConvocatoriaLupa(Convocatoria vConvocatoria)
        {
            try
            {
                return new ConvocatoriaBLL().InsertarSolicitanteConvocatoriaLupa(vConvocatoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int CambiarEstadoConvocatoria(int idConvocatoria)
        {
            try
            {
                return new ConvocatoriaBLL().CambiarEstadoConvocatoria(idConvocatoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public IDictionary<int, string> ConsultarTipoObservaciones()
        {
            try
            {
                return new ConvocatoriaBLL().ConsultarTipoObservaciones();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Convocatoria> ConsultarConvocatoriass(string pNombreConvocatoria,int pNumero)
        {
            try
            {
                return _vConvocatoriaBLL.ConsultarConvocatoriass(pNombreConvocatoria, pNumero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region "EquipoEvaluadores"
        public int InsertarEquipoEvaluadores(EquipoEvaluadores pEquipoEvaluadores)
        {
            try
            {
                return _vEquipoEvaluadoresBLL.InsertarEquipoEvaluadores(pEquipoEvaluadores);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<EquipoEvaluadores> ConsultarEquiposEvaluadoress(int pIdUsuarioCoordinador)
        {
            try
            {
                return _vEquipoEvaluadoresBLL.ConsultarEquiposEvaluadoress(pIdUsuarioCoordinador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<EquipoEvaluadores> ConsultarEquiposEvaluadoresss(string pNumeroDocumento, string pNombreCoordinador, string pNombreEquipoEvaluador)
        {
            try
            {
                return _vEquipoEvaluadoresBLL.ConsultarEquiposEvaluadoresss(pNumeroDocumento, pNombreCoordinador, pNombreEquipoEvaluador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public EquipoEvaluadores ConsultarEquipoEvaluador(int pIdEquipoEvaluador)
        {
            try
            {
                return _vEquipoEvaluadoresBLL.ConsultarEquipoEvaluador(pIdEquipoEvaluador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarEquipoEvaluador(EquipoEvaluadores pEquipoEvaluador)
        {
            try
            {
                return _vEquipoEvaluadoresBLL.ModificarEquipoEvaluador(pEquipoEvaluador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoObservaciones> ConsultarTiposObservacioness(bool pEstado)
        {
            try
            {
                return _vTipoObservacionesBLL.ConsultarTiposObservacioness(pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<EquipoEvaluadores> ConsultarTodosEquiposEvaluadoress()
        {
            try
            {
                return _vEquipoEvaluadoresBLL.ConsultarTodosEquiposEvaluadoress();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region "Evaluador"
        public int InsertarEvaluador(Evaluador pEvaluador)
        {
            try
            {
                return _vEvaluadorBLL.InsertarEvaluador(pEvaluador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarEvaluador(Evaluador pEvaluador)
        {
            try
            {
                return _vEvaluadorBLL.ModificarEvaluador(pEvaluador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Evaluador ConsultarEvaluador(int pIdUsuario)
        {
            try
            {
                return _vEvaluadorBLL.ConsultarEvaluador(pIdUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Evaluador> ConsultarEvaluadoress(int pIdEquipoEvaluador, bool pActivo)
        {
            try
            {
                return _vEvaluadorBLL.ConsultarEvaluadoress(pIdEquipoEvaluador, pActivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Evaluador> ConsultarEvaluadoressEstado(bool pActivo)
        {
            try
            {
                return _vEvaluadorBLL.ConsultarEvaluadoressEstado(pActivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public int EliminarEvaluadoress(Evaluador pEvaluador)
        {
            try
            {
                return _vEvaluadorBLL.EliminarEvaluadoress(pEvaluador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Evaluador ConsultarEvaluadorDisponibleParaAsignarObservacion(int pIdEquipoEvaluador, int pIdTipoObservacion)
        {
            try
            {
                return _vEvaluadorBLL.ConsultarEvaluadorDisponibleParaAsignarObservacion(pIdEquipoEvaluador, pIdTipoObservacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region "ObservacionesConvocatoria"
        public List<ObservacionesConvocatoria> ConsultarObservacionessConvocatoria(int pIdUsuario)
        {
            try
            {
                return _vObservacionesConvocatoriaBLL.ConsultarObservacionessConvocatoria(pIdUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region "ObservacionesPendientes"
        public List<ObservacionesPendientes> ConsultarObservacionesPendientessEvaluador(string pNumeroDocumento)
        {
            try
            {
                return _vObservacionesPendientesBLL.ConsultarObservacionesPendientessEvaluador(pNumeroDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ObservacionesPendientes ConsultarGestionMasRecienteAUnaObservacion(int pIdObservacionConvocatoria)
        {
            try
            {
                return _vObservacionesPendientesBLL.ConsultarGestionMasRecienteAUnaObservacion(pIdObservacionConvocatoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarGestionObservacionConvocatoria(ObservacionesPendientes pObservacionesPendientes)
        {
            try
            {
                return _vObservacionesPendientesBLL.InsertarGestionObservacionConvocatoria(pObservacionesPendientes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarObservacionConvocatoria(ObservacionesPendientes pObservacionesPendientes)
        {
            try
            {
                return _vObservacionesPendientesBLL.ModificarObservacionConvocatoria(pObservacionesPendientes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ObservacionesPendientes> ConsultarObservacionesPendientessCoordinador(string pNumeroDocumento)
        {
            try
            {
                return _vObservacionesPendientesBLL.ConsultarObservacionesPendientessCoordinador(pNumeroDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ObservacionesPendientes> ConsultarObservacionesPendientess()
        {
            try
            {
                return _vObservacionesPendientesBLL.ConsultarObservacionesPendientess();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ObservacionesPendientes> ConsultarObservacionesPendientessContratista()
        {
            try
            {
                return _vObservacionesPendientesBLL.ConsultarObservacionesPendientessContratista();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ObservacionesPendientes ConsultarGestionMasRecienteAUnaObservacionDelEvaluadorParaContratista(int pIdObservacionConvocatoria)
        {
            try
            {
                return _vObservacionesPendientesBLL.ConsultarGestionMasRecienteAUnaObservacionDelEvaluadorParaContratista(pIdObservacionConvocatoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarDescripcionGestionObservacionConvocatoria(ObservacionesPendientes pObservacionesPendientes)
        {
            try
            {
                return _vObservacionesPendientesBLL.ModificarDescripcionGestionObservacionConvocatoria(pObservacionesPendientes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region "HistoricoObservacionesConvocatoria"
        public int InsertarHistoricoObservacionesConvocatoria(HistoricoObservacionesConvocatoria pHistoricoObservacionesConvocatoria)
        {
            try
            {
                return _vHistoricoObservacionesConvocatoriaBLL.InsertarHistoricoObservacionesConvocatoria(pHistoricoObservacionesConvocatoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public HistoricoObservacionesConvocatoria ConsultarHistoricoObservacionesConvocatoriaPorGestionMasReciente(int pIdGestionObservacionNueva)
        {
            try
            {
                return _vHistoricoObservacionesConvocatoriaBLL.ConsultarHistoricoObservacionesConvocatoriaPorGestionMasReciente(pIdGestionObservacionNueva);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region "EstadoObservaciones"
        public EstadoObservaciones ConsultarEstadoObservacionPorCodigoEstado(string pCodigoEstado)
        {
            try
            {
                return _vEstadoObservacionesBLL.ConsultarEstadoObservacionPorCodigoEstado(pCodigoEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region "ConvocatoriasEquiposEvaluadores"
        public int InsertarConvocatoriasEquiposEvaluadores(ConvocatoriasEquiposEvaluadores pConvocatoriasEquiposEvaluadores)
        {
            try
            {
                return _vConvocatoriasEquiposEvaluadoresBLL.InsertarConvocatoriasEquiposEvaluadores(pConvocatoriasEquiposEvaluadores);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ConvocatoriasEquiposEvaluadores> ConsultarConvocatoriassEquiposEvaluadores(int pIdEquipoEvaluador)
        {
            try
            {
                return _vConvocatoriasEquiposEvaluadoresBLL.ConsultarConvocatoriassEquiposEvaluadores(pIdEquipoEvaluador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarConvocatoriasEquiposEvaluadores(ConvocatoriasEquiposEvaluadores pConvocatoriasEquiposEvaluadores)
        {
            try
            {
                return _vConvocatoriasEquiposEvaluadoresBLL.EliminarConvocatoriasEquiposEvaluadores(pConvocatoriasEquiposEvaluadores);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion
    }
}
