﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using Icbf.Oferente.Business;

namespace Icbf.Oferente.Service
{
    /// <summary>
    /// Definición de clase OferenteService
    /// </summary>
    public partial class OferenteService
    {
        private TerceroBLL vTerceroBLL;
        private ReferenteBLL vReferenteBLL;
        private TerceroDatoAdicionalBLL vTerceroDatoAdicionalBLL;
        private ExperienciaBLL vExperienciaBLL;
        private ValidarBLL vValidarBLL;
        private ModuloBLL vModuloBLL;
        private NumeroTrabajadorBLL vNumeroTrabajadorBLL;
        private CargoContactoBLL vCargoContactoBLL;
        private FinancieroBLL vFinancieroBLL;
        private IdentificacionBLL vIdentificacionBLL;
        private PoblacionAtendidaBLL vPoblacionAtendidaBLL;
        private ValoresActivosBLL vValoresActivosBLL;
        private SistemaOperativoBLL vSistemaOperativoBLL;
        private EstadoContratoBLL vEstadoContratoBLL;
        private DocumentoIdentificacionBLL vDocumentoIdentificacionBLL;
        private ObligatorioLegalBLL vObligatorioLegalBLL;
        private OpcionalLegalBLL vOpcionalLegalBLL;
        private AdministrativoBLL vAdministrativoBLL;
        private InfraestructuraBLL vInfraestructuraBLL;
        private AdministrativoDocumentoBLL vAdministrativoDocumentoBLL;
        private TipoDocumentoAdministrativoBLL vTipoDocumentoAdministrativoBLL;
        private EntidadProvOferenteBLL vEntidadProvOferenteBLL;

        private TablaParametricaBLL vTablaParametricaBLL;
        private EstadoBLL vEstadoBLL;
        private NaturalezaJuridicaBLL vNaturalezaJuridicaBLL;
        private RUPBalanceBLL vRUPBalanceBLL;
        private TotalExperienciaPrimerInfanciaBLL vTotalExperienciaPrimerInfanciaBLL;
        private SiNoBLL vSiNoBLL;
        private TipoIdentificacionPersonaNaturalBLL vTipoIdentificacionPersonaNaturalBLL;
        private TendenciaUdsPropuestaBLL vTendenciaUdsPropuestaBLL;
        private TipoComodatoUdsPropuestaBLL vTipoComodatoUdsPropuestaBLL;
        private PlantaFisicaUdsBLL vPlantaFisicaUdsBLL;
        private FormaVisualizarReporteBLL vFormaVisualizarReporteBLL;
        private ModalidadAtenderBLL vModalidadAtenderBLL;
        private TipoPersonaBLL vTipoPersonaBLL;
        private SexoBLL vSexoBLL;
        private TipoRegimenTributarioBLL vTipoRegimenTributarioBLL;
        private OrigenCapitalBLL vOrigenCapitalBLL;
        private TipoOrganizacionBLL vTipoOrganizacionBLL;
        private ZonaResidenciaBLL vZonaResidenciaBLL;
        private ZonaBLL vZonaBLL;
        private DepMunEjecucionBLL vDepMunEjecucionBLL;
        private DepEjecucionBLL vDepEjecucionBLL;
        private DocumentoExperienciaBLL vDocumentoExperienciaBLL;
        private TerceroModuloBLL vTerceroModuloBLL;
        private ParametroDocumentoFinancieroBLL vParametroDocumentoFinancieroBLL;

        private DocumentoIdentificacionTerceroBLL vDocumentoIdentificacionTerceroBLL;
        private FinancieroDocumentoBLL vFinancieroDocumentoBLL;

        private AcuerdosBLL vAcuerdos;

        private FechaConvocatoriaBLL vFechaConvocatoriaBLL;

        private ConsultarOferentesIdoneosBLL vConsultarOferentesIdoneos;

        private IdoneidadOferenteBLL vIdoneidadOferenteBLL;
        private RankingOferenteBLL vRankingOferenteBLL;
        /// <summary>
        /// Constructor de clase OferenteService, se ralizan las intancias a objetos de caoa Business
        /// </summary>
        public OferenteService()
        {
            vFechaConvocatoriaBLL = new FechaConvocatoriaBLL();
            vTerceroBLL = new TerceroBLL();
            vReferenteBLL = new ReferenteBLL();
            vTerceroDatoAdicionalBLL = new TerceroDatoAdicionalBLL();
            vExperienciaBLL = new ExperienciaBLL();
            vValidarBLL = new ValidarBLL();
            vModuloBLL = new ModuloBLL();
            vNumeroTrabajadorBLL = new NumeroTrabajadorBLL();
            vCargoContactoBLL = new CargoContactoBLL();
            vFinancieroBLL = new FinancieroBLL();
            vIdentificacionBLL = new IdentificacionBLL();
            vPoblacionAtendidaBLL = new PoblacionAtendidaBLL();
            //vValoresActivosBLL = new ValoresActivosBLL();
            //vSistemaOperativoBLL = new SistemaOperativoBLL();
            //vEstadoContratoBLL = new EstadoContratoBLL();
            //vDocumentoIdentificacionBLL = new DocumentoIdentificacionBLL();
            //vObligatorioLegalBLL = new ObligatorioLegalBLL();
            //vOpcionalLegalBLL = new OpcionalLegalBLL();
            vAdministrativoBLL = new AdministrativoBLL();
            vInfraestructuraBLL = new InfraestructuraBLL();
            vAdministrativoDocumentoBLL = new AdministrativoDocumentoBLL();
            vTipoDocumentoAdministrativoBLL = new TipoDocumentoAdministrativoBLL();

            vEntidadProvOferenteBLL = new EntidadProvOferenteBLL();

            /**Mexico**/
            vValoresActivosBLL = new ValoresActivosBLL();
            vSistemaOperativoBLL = new SistemaOperativoBLL();
            vEstadoContratoBLL = new EstadoContratoBLL();
            vDocumentoIdentificacionBLL = new DocumentoIdentificacionBLL();
            vObligatorioLegalBLL = new ObligatorioLegalBLL();
            vOpcionalLegalBLL = new OpcionalLegalBLL();

            vTablaParametricaBLL = new TablaParametricaBLL();
            vEstadoBLL = new EstadoBLL();
            vNaturalezaJuridicaBLL = new NaturalezaJuridicaBLL();
            vRUPBalanceBLL = new RUPBalanceBLL();
            vTotalExperienciaPrimerInfanciaBLL = new TotalExperienciaPrimerInfanciaBLL();
            vSiNoBLL = new SiNoBLL();
            vTipoIdentificacionPersonaNaturalBLL = new TipoIdentificacionPersonaNaturalBLL();
            vTendenciaUdsPropuestaBLL = new TendenciaUdsPropuestaBLL();
            vTipoComodatoUdsPropuestaBLL = new TipoComodatoUdsPropuestaBLL();
            vPlantaFisicaUdsBLL = new PlantaFisicaUdsBLL();
            vFormaVisualizarReporteBLL = new FormaVisualizarReporteBLL();
            vModalidadAtenderBLL = new ModalidadAtenderBLL();
            vTipoPersonaBLL = new TipoPersonaBLL();
            vSexoBLL = new SexoBLL();
            vTipoRegimenTributarioBLL = new TipoRegimenTributarioBLL();
            vOrigenCapitalBLL = new OrigenCapitalBLL();
            vTipoOrganizacionBLL = new TipoOrganizacionBLL();
            vZonaResidenciaBLL = new ZonaResidenciaBLL();
            vZonaBLL = new ZonaBLL();
            vDepMunEjecucionBLL = new DepMunEjecucionBLL();
            vDepEjecucionBLL = new DepEjecucionBLL();
            vDocumentoExperienciaBLL = new DocumentoExperienciaBLL();
            vTerceroModuloBLL = new TerceroModuloBLL();

            vDocumentoIdentificacionTerceroBLL = new DocumentoIdentificacionTerceroBLL();
            vParametroDocumentoFinancieroBLL = new ParametroDocumentoFinancieroBLL();
            vFinancieroDocumentoBLL = new FinancieroDocumentoBLL();

            vAcuerdos = new AcuerdosBLL();

            vConsultarOferentesIdoneos = new ConsultarOferentesIdoneosBLL();

            vIdoneidadOferenteBLL = new IdoneidadOferenteBLL();
            vRankingOferenteBLL = new RankingOferenteBLL();
        }

        #region Identificacion
        /// <summary>
        /// Inserta un Documento de Identificacion de Tercero]
        /// </summary>
        /// <param name="pDocumentoIdentificacionTercero"></param>
        /// <returns></returns>
        public int InsertarDocumentoIdentificacionTercero(DocumentoIdentificacionTercero pDocumentoIdentificacionTercero)
        {
            try
            {
                return vDocumentoIdentificacionTerceroBLL.InsertarDocumentoIdentificacionTercero(pDocumentoIdentificacionTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modifica un Documento de Identificacion de Tercero
        /// </summary>
        /// <param name="pDocumentoIdentificacionTercero"></param>
        /// <returns></returns>
        public int ModificarDocumentoIdentificacionTercero(DocumentoIdentificacionTercero pDocumentoIdentificacionTercero)
        {
            try
            {
                return vDocumentoIdentificacionTerceroBLL.ModificarDocumentoIdentificacionTercero(pDocumentoIdentificacionTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Elimina un Documento de Identificacion de Tercero
        /// </summary>
        /// <param name="pDocumentoIdentificacionTercero"></param>
        /// <returns></returns>
        public int EliminarDocumentoIdentificacionTercero(DocumentoIdentificacionTercero pDocumentoIdentificacionTercero)
        {
            try
            {
                return vDocumentoIdentificacionTerceroBLL.EliminarDocumentoIdentificacionTercero(pDocumentoIdentificacionTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consultar un Documento de Identificacion de Terceros
        /// </summary>
        /// <param name="pIdTercero"></param>
        /// <returns></returns>
        public List<DocumentoIdentificacionTercero> ConsultarDocIdentificacionTerceros(int pIdTercero)
        {
            try
            {
                return vDocumentoIdentificacionTerceroBLL.ConsultarDocIdentificacionTerceros(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoIdentificacionPersonaNatural> ConsultarTipoIdentificacionPersonaNaturals(String pCodigoTipoIdentificacionPersonaNatural, String pNombreTipoIdentificacionPersonaNatural, Boolean? pEstado)
        {
            try
            {
                return vTipoIdentificacionPersonaNaturalBLL.ConsultarTipoIdentificacionPersonaNaturals(pCodigoTipoIdentificacionPersonaNatural, pNombreTipoIdentificacionPersonaNatural, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Consultar Entiga Generica Parametros
        /// </summary>
        /// <param name="Tabla"></param>
        /// <param name="IdCompo"></param>
        /// <param name="ValoCampo"></param>
        /// <returns></returns>
        public List<EntigaGenericaParametros> ConsultarEntigaGenericaParametros(string Tabla, string IdCompo, string ValoCampo)
        {
            try
            {
                return vIdentificacionBLL.ConsultarEntigaGenericaParametros(Tabla, IdCompo, ValoCampo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una Entidad ProvOferente
        /// </summary>
        /// <param name="pIdEntidad"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public EntidadProvOferente ConsultarEntidadProvOferente(int pIdEntidad, bool pEstado)
        {
            try
            {
                return vEntidadProvOferenteBLL.ConsultarEntidadProvOferente(pIdEntidad, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Cambiar Estado de una Entidad ProvOferente
        /// </summary>
        /// <param name="pIdEntidadProvOferente"></param>
        /// <returns></returns>
        public int CambiarEstadoEntidadProvOferente(int pIdEntidadProvOferente)
        {
            try
            {
                return vEntidadProvOferenteBLL.CambiarEstadoEntidadProvOferente(pIdEntidadProvOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Cambiar Estado Entidad ProvOferente Validacion
        /// </summary>
        /// <param name="pEstadoValidacion"></param>
        /// <param name="pIdEntidadProvOferente"></param>
        /// <returns></returns>
        public int CambiarEstadoEntidadProvOferenteValidacion(string pEstadoValidacion, int pIdEntidadProvOferente)
        {
            try
            {
                return vEntidadProvOferenteBLL.CambiarEstadoEntidadProvOferenteValidacion(pEstadoValidacion, pIdEntidadProvOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta una Entidad ProvOferente
        /// </summary>
        /// <param name="pEntidadProvOferente"></param>
        /// <returns></returns>
        public int InsertarEntidadProvOferente(EntidadProvOferente pEntidadProvOferente)
        {
            try
            {
                return vEntidadProvOferenteBLL.InsertarEntidadProvOferente(pEntidadProvOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modifica una Entidad ProvOferente
        /// </summary>
        /// <param name="pEntidadProvOferente"></param>
        /// <returns></returns>
        public int ModificarEntidadProvOferente(EntidadProvOferente pEntidadProvOferente)
        {
            try
            {
                return vEntidadProvOferenteBLL.ModificarEntidadProvOferente(pEntidadProvOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Elimina una Entidad ProvOferente
        /// </summary>
        /// <param name="pEntidadProvOferente"></param>
        /// <returns></returns>
        public int EliminarEntidadProvOferente(EntidadProvOferente pEntidadProvOferente)
        {
            try
            {
                return vEntidadProvOferenteBLL.EliminarEntidadProvOferente(pEntidadProvOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consulta una Entidad ProvOferente
        /// </summary>
        /// <param name="pIDENTIDAD"></param>
        /// <param name="pTIPOENTOFPROV"></param>
        /// <param name="pIDTERCERO"></param>
        /// <param name="pIDTERCEROREPLEGAL"></param>
        /// <param name="pIDTIPOCIIUPRINCIPAL"></param>
        /// <param name="pIDTIPOCIIUSECUNDARIO"></param>
        /// <param name="pIDTIPOREGTRIB"></param>
        /// <param name="pIDTIPOSECTOR"></param>
        /// <param name="pIDTIPOACTIVIDAD"></param>
        /// <param name="pIDTIPOCLASEENTIDAD"></param>
        /// <param name="pIDTIPORAMAPUBLICA"></param>
        /// <param name="pIDTIPOENTIDAD"></param>
        /// <param name="pIDTIPONIVELGOB"></param>
        /// <param name="pIDTIPONIVELORGANIZACIONAL"></param>
        /// <param name="pIDTIPOORIGENCAPITAL"></param>
        /// <param name="pIDTIPOCERTIFICADORCALIDAD"></param>
        /// <param name="pIDTIPOCERTIFICATAMANO"></param>
        /// <param name="pIDTIPONATURALEZAJURID"></param>
        /// <param name="pIDTIPORANGOSACTIVOS"></param>
        /// <returns></returns>
        public EntidadProvOferente ConsultarEntidadProvOferente(int pIDENTIDAD, int pTIPOENTOFPROV, int pIDTERCERO, int pIDTERCEROREPLEGAL, int pIDTIPOCIIUPRINCIPAL, int pIDTIPOCIIUSECUNDARIO, int pIDTIPOREGTRIB, int pIDTIPOSECTOR, int pIDTIPOACTIVIDAD, int pIDTIPOCLASEENTIDAD, int pIDTIPORAMAPUBLICA, int pIDTIPOENTIDAD, int pIDTIPONIVELGOB, int pIDTIPONIVELORGANIZACIONAL, int pIDTIPOORIGENCAPITAL, int pIDTIPOCERTIFICADORCALIDAD, int pIDTIPOCERTIFICATAMANO, int pIDTIPONATURALEZAJURID, int pIDTIPORANGOSACTIVOS)
        {
            try
            {
                return vEntidadProvOferenteBLL.ConsultarEntidadProvOferente(pIDENTIDAD, pTIPOENTOFPROV, pIDTERCERO, pIDTERCEROREPLEGAL, pIDTIPOCIIUPRINCIPAL, pIDTIPOCIIUSECUNDARIO, pIDTIPOREGTRIB, pIDTIPOSECTOR, pIDTIPOACTIVIDAD, pIDTIPOCLASEENTIDAD, pIDTIPORAMAPUBLICA, pIDTIPOENTIDAD, pIDTIPONIVELGOB, pIDTIPONIVELORGANIZACIONAL, pIDTIPOORIGENCAPITAL, pIDTIPOCERTIFICADORCALIDAD, pIDTIPOCERTIFICATAMANO, pIDTIPONATURALEZAJURID, pIDTIPORANGOSACTIVOS);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una Entidad ProvOferente
        /// </summary>
        /// <param name="pIDTERCERO"></param>
        /// <returns></returns>
        public EntidadProvOferente ConsultarEntidadProvOferente(int pIDTERCERO)
        {
            try
            {
                return vEntidadProvOferenteBLL.ConsultarEntidadProvOferente(pIDTERCERO);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de Entidad ProvOferentes
        /// </summary>
        /// <returns></returns>
        public List<EntidadProvOferente> ConsultarEntidadProvOferentes()
        {
            try
            {
                return vEntidadProvOferenteBLL.ConsultarEntidadProvOferentes();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta un lista de Entidad ProvOferentes de acuerdo a un IDTERCERO
        /// </summary>
        /// <param name="pIDTERCERO"></param>
        /// <returns></returns>
        public List<EntidadProvOferente> ConsultarEntidadProvOferentes(int? pIDTERCERO)
        {
            try
            {
                return vEntidadProvOferenteBLL.ConsultarEntidadProvOferentes(pIDTERCERO);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de Entidad ProvOferentes Validados
        /// </summary>
        /// <returns></returns>
        public List<EntidadProvOferente> ConsultarEntidadProvOferentesValidados()
        {
            try
            {
                return vEntidadProvOferenteBLL.ConsultarEntidadProvOferentesValidados();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        #endregion

        #region Ranking oferente
        /// <summary>
        /// Inserta un Ranking Oferente
        /// </summary>
        /// <param name="pIdoneidadOferente"></param>
        /// <returns></returns>
        public int InsertarRankingOferente(IdoneidadOferente pIdoneidadOferente)
        {
            try
            {
                return vRankingOferenteBLL.InsertarRankingOferente(pIdoneidadOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modifica un Ranking Oferente
        /// </summary>
        /// <param name="pIdoneidadOferente"></param>
        /// <returns></returns>
        public int ModificarRankingOferente(IdoneidadOferente pIdoneidadOferente)
        {
            try
            {
                return vRankingOferenteBLL.ModificarRankingOferente(pIdoneidadOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Calcular Ranking Oferente
        /// </summary>
        /// <param name="pIdEntidad"></param>
        /// <param name="pIdTercero"></param>
        /// <returns></returns>
        public int CalcularRankingOferente(int pIdEntidad, int pIdTercero)
        {
            try
            {
                return vRankingOferenteBLL.CalcularRankingOferente(pIdEntidad, pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Tercero
        /// <summary>
        /// Insertar Representante Legal
        /// </summary>
        /// <param name="pTercero"></param>
        /// <returns></returns>
        public int InsertarRepresentanteLegal(Tercero pTercero)
        {
            try
            {
                return vTerceroBLL.InsertarRepresentanteLegal(pTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Inserta un Tercero
        /// </summary>
        /// <param name="pTercero"></param>
        /// <returns></returns>
        public int InsertarTercero(Tercero pTercero)
        {
            try
            {
                return vTerceroBLL.InsertarTercero(pTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }

        }
        /// <summary>
        /// Inserta un Tercero
        /// </summary>
        /// <param name="pTercero"></param>
        /// <returns></returns>
        public int InsertarCargarTercero(Tercero pTercero)
        {
            try
            {
                return vTerceroBLL.InsertarCargarTercero(pTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }

        }
        /// <summary>
        /// Inserta Tercero con IdTemporal
        /// </summary>
        /// <param name="pTercero"></param>
        /// <param name="idTemporal"></param>
        /// <returns></returns>
        public int InsertarTercero(Tercero pTercero, string idTemporal)
        {
            try
            {

                return vTerceroBLL.InsertarTercero(pTercero, idTemporal);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        /// <summary>
        /// Modifica un Tercero
        /// </summary>
        /// <param name="pTercero"></param>
        /// <returns></returns>
        public int ModificarTercero(Tercero pTercero, string idTemporal)
        {
            try
            {
                return vTerceroBLL.ModificarTercero(pTercero, idTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }

        }

        public int ModificarTercero(Tercero pTercero)
        {
            try
            {
                return vTerceroBLL.ModificarTercero(pTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }

        }

        /// <summary>
        /// Modifica La razón Social de un Tercero
        /// </summary>
        /// <param name="pIdTercero"></param>
        /// <param name="pRazonSocial"></param>
        /// <returns></returns>
        public bool ModificarRazonSocialTercero(int pIdTercero, string pRazonSocial)
        {
            try
            {
                return vTerceroBLL.ModificarRazonSocialTercero(pIdTercero, pRazonSocial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }

        }

        /// <summary>
        /// Consultar Tercero ProviderUserKey
        /// </summary>
        /// <param name="ProviderUserKey"></param>
        /// <returns></returns>
        public Tercero ConsultarTerceroProviderUserKey(string ProviderUserKey)
        {
            try
            {
                return vTerceroBLL.ConsultarTerceroProviderUserKey(ProviderUserKey);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Tercero por Id
        /// </summary>
        /// <param name="pIdTercero"></param>
        /// <returns></returns>
        public Tercero ConsultarTercero(int pIdTercero)
        {
            try
            {
                return vTerceroBLL.ConsultarTercero(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Tercero> ConsultarTerceroFuente(String pTipoPersona, String pTipoidentificacion, String pIdentificacion
          , String pProveedor)
        {
            try
            {
                return vTerceroBLL.ConsultarTerceroFuente(pTipoPersona, pTipoidentificacion, pIdentificacion, pProveedor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Tercero por correo electrónico
        /// </summary>
        /// <param name="pCorreoElectronico"></param>
        /// <returns></returns>
        public Tercero ConsultarTerceroPorCorreo(string pCorreoElectronico)
        {
            try
            {
                return vTerceroBLL.ConsultarTerceroPorCorreo(pCorreoElectronico);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consultar Tercero por Tipo y Numero Identificacion
        /// </summary>
        /// <param name="pTipoidentificacion"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <returns></returns>
        public Tercero ConsultarTerceroTipoNumeroIdentificacion(int? pTipoidentificacion, string pNumeroIdentificacion)
        {
            try
            {
                return vTerceroBLL.ConsultarTerceroTipoNumeroIdentificacion(pTipoidentificacion, pNumeroIdentificacion);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar un lista de Terceros de acuerdo a varios parametros
        /// </summary>
        /// <param name="vIdDListaTipoDocumento"></param>
        /// <param name="vIdEstadoTercero"></param>
        /// <param name="vIdTipoPersona"></param>
        /// <param name="vNumeroIdentificacion"></param>
        /// <param name="vPrimerNombre"></param>
        /// <param name="vSegundoNombre"></param>
        /// <param name="vPrimerApellido"></param>
        /// <param name="vSegundoApellido"></param>
        /// <returns></returns>
        public List<Tercero> ConsultarTerceros(int? vIdDListaTipoDocumento, int? vIdEstadoTercero, int? vIdTipoPersona, int? vNumeroIdentificacion,
                                               string vPrimerNombre, string vSegundoNombre, string vPrimerApellido, string vSegundoApellido)
        {
            try
            {
                return vTerceroBLL.ConsultarTerceros(vIdDListaTipoDocumento, vIdEstadoTercero, vIdTipoPersona,
                                                     vNumeroIdentificacion,
                                                     vPrimerNombre, vSegundoNombre, vPrimerApellido, vSegundoApellido);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Insertar un Tercero Repesentante Legal
        /// </summary>
        /// <param name="pIdTercero"></param>
        /// <param name="IdRepresentanteLegal"></param>
        /// <returns></returns>
        public int InsertarTerceroRepesentanteLegal(int pIdTercero, int IdRepresentanteLegal)
        {
            try
            {

                return vTerceroBLL.InsertarTerceroRepesentanteLegal(pIdTercero, IdRepresentanteLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar un Reprentante Legal Tercero
        /// </summary>
        /// <param name="pIdTercero"></param>
        /// <returns></returns>
        public int ConsultarReprentanteLogalTercero(int pIdTercero)
        {
            try
            {
                return vTerceroBLL.ConsultarReprentanteLogalTercero(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        #endregion

        #region "Referente"

        /// <summary>
        /// Inserta un Referente
        /// </summary>
        /// <param name="pReferente"></param>
        /// <returns></returns>
        public int InsertarReferente(Referente pReferente)
        {
            try
            {
                return vReferenteBLL.InsertarReferente(pReferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }

        }
        /// <summary>
        /// Modifica un Referente
        /// </summary>
        /// <param name="pReferente"></param>
        /// <returns></returns>
        public int ModificarReferente(Referente pReferente)
        {
            try
            {
                return vReferenteBLL.ModificarReferente(pReferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }

        }
        /// <summary>
        /// Consultar Referente por Id
        /// </summary>
        /// <param name="pIdReferente"></param>
        /// <returns></returns>
        public List<Referente> ConsultarReferentes(int pIdTercero)
        {
            try
            {
                return vReferenteBLL.ConsultarReferentes(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region "TerceroDatoAdicional"

        /// <summary>
        /// Inserta un TerceroDatoAdicional
        /// </summary>
        /// <param name="pTerceroDatoAdicional"></param>
        /// <returns></returns>
        public int InsertarTerceroDatoAdicional(TerceroDatoAdicional pTerceroDatoAdicional)
        {
            try
            {
                return vTerceroDatoAdicionalBLL.InsertarTerceroDatoAdicional(pTerceroDatoAdicional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }

        }
        /// <summary>
        /// Modifica un TerceroDatoAdicional
        /// </summary>
        /// <param name="pTerceroDatoAdicional"></param>
        /// <returns></returns>
        public int ModificarTerceroDatoAdicional(TerceroDatoAdicional pTerceroDatoAdicional)
        {
            try
            {
                return vTerceroDatoAdicionalBLL.ModificarTerceroDatoAdicional(pTerceroDatoAdicional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }

        }
        /// <summary>
        /// Consultar TerceroDatoAdicional por Id
        /// </summary>
        /// <param name="pIdTerceroDatoAdicional"></param>
        /// <returns></returns>
        public TerceroDatoAdicional ConsultarTerceroDatoAdicional(int pIdTercero)
        {
            try
            {
                return vTerceroDatoAdicionalBLL.ConsultarTerceroDatoAdicional(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Modulo
        /// <summary>
        /// Insertar un Modulo
        /// </summary>
        /// <param name="pModulo"></param>
        /// <returns></returns>
        public int InsertarModulo(Modulo pModulo)
        {
            try
            {
                return vModuloBLL.InsertarModulo(pModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modificar un Modulo
        /// </summary>
        /// <param name="pModulo"></param>
        /// <returns></returns>
        public int ModificarModulo(Modulo pModulo)
        {
            try
            {
                return vModuloBLL.ModificarModulo(pModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Eliminar un Modulo
        /// </summary>
        /// <param name="pModulo"></param>
        /// <returns></returns>
        public int EliminarModulo(Modulo pModulo)
        {
            try
            {
                return vModuloBLL.EliminarModulo(pModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Orden Modulo
        /// </summary>
        /// <param name="IdEntidad"></param>
        /// <param name="IdModulo"></param>
        /// <returns></returns>
        public int EliminarOrdenModulo(int IdEntidad, int IdModulo)
        {
            try
            {
                return vModuloBLL.EliminarOrdenModulo(IdEntidad, IdModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consultar Modulo
        /// </summary>
        /// <param name="pIdModulo"></param>
        /// <returns></returns>
        public Modulo ConsultarModulo(int pIdModulo)
        {
            try
            {
                return vModuloBLL.ConsultarModulo(pIdModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Una lista de Modulos
        /// </summary>
        /// <param name="pNombreModulo"></param>
        /// <param name="pOrden"></param>
        /// <param name="pEstado"></param>
        /// <param name="pRuta"></param>
        /// <returns></returns>
        public List<Modulo> ConsultarModulos(String pNombreModulo, int? pOrden, String pEstado, String pRuta)
        {
            try
            {
                return vModuloBLL.ConsultarModulos(pNombreModulo, pOrden, pEstado, pRuta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Modulos Orden
        /// </summary>
        /// <param name="pNombreModulo"></param>
        /// <param name="pOrden"></param>
        /// <param name="pEstado"></param>
        /// <param name="pRuta"></param>
        /// <returns></returns>
        public List<Modulo> ConsultarModulosOrden(String pNombreModulo, int? pOrden, String pEstado, String pRuta)
        {
            try
            {
                return vModuloBLL.ConsultarModulosOrden(pNombreModulo, pOrden, pEstado, pRuta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region NumeroTrabajador
        /// <summary>
        /// Insertar Numero Trabajador
        /// </summary>
        /// <param name="pNumeroTrabajador"></param>
        /// <returns></returns>
        public int InsertarNumeroTrabajador(NumeroTrabajador pNumeroTrabajador)
        {
            try
            {
                return vNumeroTrabajadorBLL.InsertarNumeroTrabajador(pNumeroTrabajador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modificar un Numero Trabajador
        /// </summary>
        /// <param name="pNumeroTrabajador"></param>
        /// <returns></returns>
        public int ModificarNumeroTrabajador(NumeroTrabajador pNumeroTrabajador)
        {
            try
            {
                return vNumeroTrabajadorBLL.ModificarNumeroTrabajador(pNumeroTrabajador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Numero Trabajador
        /// </summary>
        /// <param name="pNumeroTrabajador"></param>
        /// <returns></returns>
        public int EliminarNumeroTrabajador(NumeroTrabajador pNumeroTrabajador)
        {
            try
            {
                return vNumeroTrabajadorBLL.EliminarNumeroTrabajador(pNumeroTrabajador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar NumeroTrabajador
        /// </summary>
        /// <param name="pIdNumeroTrabajador"></param>
        /// <returns></returns>
        public NumeroTrabajador ConsultarNumeroTrabajador(int pIdNumeroTrabajador)
        {
            try
            {
                return vNumeroTrabajadorBLL.ConsultarNumeroTrabajador(pIdNumeroTrabajador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de NumeroTrabajador  
        /// </summary>
        /// <param name="pCodigoNumeroTrabajador"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<NumeroTrabajador> ConsultarNumeroTrabajadors(String pCodigoNumeroTrabajador, String pDescripcion, String pEstado)
        {
            try
            {
                return vNumeroTrabajadorBLL.ConsultarNumeroTrabajadors(pCodigoNumeroTrabajador, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar CodigoNumeroTrabajador
        /// </summary>
        /// <param name="pCodigoNumeroTrabajador"></param>
        /// <returns></returns>
        public int ConsultarCodigoNumeroTrabajador(String pCodigoNumeroTrabajador)
        {
            try
            {
                return vNumeroTrabajadorBLL.ConsultarCodigoNumeroTrabajador(pCodigoNumeroTrabajador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region CargoContacto
        /// <summary>
        /// Insertar Cargo Contacto
        /// </summary>
        /// <param name="pCargoContacto"></param>
        /// <returns></returns>
        public int InsertarCargoContacto(CargoContacto pCargoContacto)
        {
            try
            {
                return vCargoContactoBLL.InsertarCargoContacto(pCargoContacto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modificar Cargo Contacto
        /// </summary>
        /// <param name="pCargoContacto"></param>
        /// <returns></returns>
        public int ModificarCargoContacto(CargoContacto pCargoContacto)
        {
            try
            {
                return vCargoContactoBLL.ModificarCargoContacto(pCargoContacto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Cargo Contacto
        /// </summary>
        /// <param name="pCargoContacto"></param>
        /// <returns></returns>
        public int EliminarCargoContacto(CargoContacto pCargoContacto)
        {
            try
            {
                return vCargoContactoBLL.EliminarCargoContacto(pCargoContacto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Cargo Contacto
        /// </summary>
        /// <param name="pIdCargoContacto"></param>
        /// <returns></returns>
        public CargoContacto ConsultarCargoContacto(int pIdCargoContacto)
        {
            try
            {
                return vCargoContactoBLL.ConsultarCargoContacto(pIdCargoContacto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista Cargo Contactos
        /// </summary>
        /// <param name="pCodigoCargoContacto"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<CargoContacto> ConsultarCargoContactos(String pCodigoCargoContacto, String pDescripcion, String pEstado)
        {
            try
            {
                return vCargoContactoBLL.ConsultarCargoContactos(pCodigoCargoContacto, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Consultar Codigo Cargo Contacto
        /// </summary>
        /// <param name="pCodigoCargoContacto"></param>
        /// <returns></returns>
        public int ConsultarCodigoCargoContacto(String pCodigoCargoContacto)
        {
            try
            {
                return vCargoContactoBLL.ConsultarCodigoCargoContacto(pCodigoCargoContacto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region PoblacionAtendida
        /// <summary>
        /// Insertar Poblacion Atendida
        /// </summary>
        /// <param name="pPoblacionAtendida"></param>
        /// <returns></returns>
        public int InsertarPoblacionAtendida(PoblacionAtendida pPoblacionAtendida)
        {
            try
            {
                return vPoblacionAtendidaBLL.InsertarPoblacionAtendida(pPoblacionAtendida);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modificar Poblacion Atendida
        /// </summary>
        /// <param name="pPoblacionAtendida"></param>
        /// <returns></returns>
        public int ModificarPoblacionAtendida(PoblacionAtendida pPoblacionAtendida)
        {
            try
            {
                return vPoblacionAtendidaBLL.ModificarPoblacionAtendida(pPoblacionAtendida);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Elimina rPoblacion Atendida
        /// </summary>
        /// <param name="pPoblacionAtendida"></param>
        /// <returns></returns>
        public int EliminarPoblacionAtendida(PoblacionAtendida pPoblacionAtendida)
        {
            try
            {
                return vPoblacionAtendidaBLL.EliminarPoblacionAtendida(pPoblacionAtendida);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Poblacion Atendida
        /// </summary>
        /// <param name="pIdPoblacionAtendida"></param>
        /// <returns></returns>
        public PoblacionAtendida ConsultarPoblacionAtendida(int pIdPoblacionAtendida)
        {
            try
            {
                return vPoblacionAtendidaBLL.ConsultarPoblacionAtendida(pIdPoblacionAtendida);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de Poblaciones Atendidas
        /// </summary>
        /// <param name="pPoblacionAtendida"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<PoblacionAtendida> ConsultarPoblacionAtendidas(String pPoblacionAtendida, String pDescripcion, String pEstado)
        {
            try
            {
                return vPoblacionAtendidaBLL.ConsultarPoblacionAtendidas(pPoblacionAtendida, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Poblacion Atendida
        /// </summary>
        /// <param name="pPoblacionAtendida"></param>
        /// <returns></returns>
        public int ConsultarPoblacionAtendida(String pPoblacionAtendida)
        {
            try
            {
                return vPoblacionAtendidaBLL.ConsultarPoblacionAtendida(pPoblacionAtendida);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region "IdoneidadOferente"
        /// <summary>
        /// Consultar Idoneidad Oferente
        /// </summary>
        /// <param name="IdTercero"></param>
        /// <returns></returns>
        public List<IdoneidadOferente> ConsultarIdoneidadOferente(int IdTercero)
        {
            try
            {
                return vIdoneidadOferenteBLL.ConsultarIdoneidadOferente(IdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Consultar Naturaleza Juridicas
        /// </summary>
        /// <param name="pCodigoNaturalezaJuridica"></param>
        /// <param name="pNombreNaturalezaJuridica"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<NaturalezaJuridica> ConsultarNaturalezaJuridicas(String pCodigoNaturalezaJuridica, String pNombreNaturalezaJuridica, Boolean? pEstado)
        {
            try
            {
                return vNaturalezaJuridicaBLL.ConsultarNaturalezaJuridicas(pCodigoNaturalezaJuridica, pNombreNaturalezaJuridica, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region "Financiero"
        /// <summary>
        /// Insertar Financiero
        /// </summary>
        /// <param name="pFinanciero"></param>
        /// <returns></returns>
        public int InsertarFinanciero(Financiero pFinanciero)
        {
            try
            {
                return vFinancieroBLL.InsertarFinanciero(pFinanciero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modificar Financiero
        /// </summary>
        /// <param name="pFinanciero"></param>
        /// <returns></returns>
        public int ModificarFinanciero(Financiero pFinanciero)
        {
            try
            {
                return vFinancieroBLL.ModificarFinanciero(pFinanciero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Financiero
        /// </summary>
        /// <param name="pFinanciero"></param>
        /// <returns></returns>
        public int EliminarFinanciero(Financiero pFinanciero)
        {
            try
            {
                return vFinancieroBLL.EliminarFinanciero(pFinanciero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Financiero
        /// </summary>
        /// <param name="pIDInfoFin"></param>
        /// <returns></returns>
        public Financiero ConsultarFinanciero(int pIDInfoFin)
        {
            try
            {
                return vFinancieroBLL.ConsultarFinanciero(pIDInfoFin);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Lista de Financieros
        /// </summary>
        /// <param name="pAnio"></param>
        /// <param name="pIdOferente"></param>
        /// <returns></returns>
        public List<Financiero> ConsultarFinancieros(int? pAnio, int? pIdOferente)
        {
            try
            {
                return vFinancieroBLL.ConsultarFinancieros(pAnio, pIdOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Administrativo
        /// <summary>
        /// Insertar Administrativo
        /// </summary>
        /// <param name="pAdministrativo"></param>
        /// <returns></returns>
        public int InsertarAdministrativo(Administrativo pAdministrativo)
        {
            try
            {
                return vAdministrativoBLL.InsertarAdministrativo(pAdministrativo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modificar Administrativo
        /// </summary>
        /// <param name="pAdministrativo"></param>
        /// <returns></returns>
        public int ModificarAdministrativo(Administrativo pAdministrativo)
        {
            try
            {
                return vAdministrativoBLL.ModificarAdministrativo(pAdministrativo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Administrativo
        /// </summary>
        /// <param name="pAdministrativo"></param>
        /// <returns></returns>
        public int EliminarAdministrativo(Administrativo pAdministrativo)
        {
            try
            {
                return vAdministrativoBLL.EliminarAdministrativo(pAdministrativo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Administrativo
        /// </summary>
        /// <param name="pIdAdministrativo"></param>
        /// <returns></returns>
        public Administrativo ConsultarAdministrativo(int pIdAdministrativo)
        {
            try
            {
                return vAdministrativoBLL.ConsultarAdministrativo(pIdAdministrativo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una Lista de Administrativos
        /// </summary>
        /// <param name="pIdEntidad"></param>
        /// <param name="pAnho"></param>
        /// <returns></returns>
        public List<Administrativo> ConsultarAdministrativos(Decimal pIdEntidad, int? pAnho)
        {
            try
            {
                return vAdministrativoBLL.ConsultarAdministrativos(pIdEntidad, pAnho);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Infraestructura
        /// <summary>
        /// Insertar Infraestructura
        /// </summary>
        /// <param name="pInfraestructura"></param>
        /// <returns></returns>
        public int InsertarInfraestructura(Infraestructura pInfraestructura)
        {
            try
            {
                return vInfraestructuraBLL.InsertarInfraestructura(pInfraestructura);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modificar Infraestructura
        /// </summary>
        /// <param name="pInfraestructura"></param>
        /// <returns></returns>
        public int ModificarInfraestructura(Infraestructura pInfraestructura)
        {
            try
            {
                return vInfraestructuraBLL.ModificarInfraestructura(pInfraestructura);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Infraestructura
        /// </summary>
        /// <param name="pInfraestructura"></param>
        /// <returns></returns>
        public int EliminarInfraestructura(Infraestructura pInfraestructura)
        {
            try
            {
                return vInfraestructuraBLL.EliminarInfraestructura(pInfraestructura);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        ///  Consultar Infraestructura
        /// </summary>
        /// <param name="pIdInfraestructura"></param>
        /// <returns></returns>
        public Infraestructura ConsultarInfraestructura(int pIdInfraestructura)
        {
            try
            {
                return vInfraestructuraBLL.ConsultarInfraestructura(pIdInfraestructura);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar un lista de Infraestructuras
        /// </summary>
        /// <param name="pAnho"></param>
        /// <param name="pIdEntidad"></param>
        /// <returns></returns>
        public List<Infraestructura> ConsultarInfraestructuras(int? pAnho, Decimal? pIdEntidad)
        {
            try
            {
                return vInfraestructuraBLL.ConsultarInfraestructuras(pAnho, pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region AdministrativoDocumento
        /// <summary>
        /// Insertar Administrativo Documento
        /// </summary>
        /// <param name="pAdministrativoDocumento"></param>
        /// <returns></returns>
        public int InsertarAdministrativoDocumento(AdministrativoDocumento pAdministrativoDocumento)
        {
            try
            {
                return vAdministrativoDocumentoBLL.InsertarAdministrativoDocumento(pAdministrativoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modificar Administrativo Documento
        /// </summary>
        /// <param name="pAdministrativoDocumento"></param>
        /// <returns></returns>
        public int ModificarAdministrativoDocumento(AdministrativoDocumento pAdministrativoDocumento)
        {
            try
            {
                return vAdministrativoDocumentoBLL.ModificarAdministrativoDocumento(pAdministrativoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Administrativo Documento
        /// </summary>
        /// <param name="pAdministrativoDocumento"></param>
        /// <returns></returns>
        public int EliminarAdministrativoDocumento(AdministrativoDocumento pAdministrativoDocumento)
        {
            try
            {
                return vAdministrativoDocumentoBLL.EliminarAdministrativoDocumento(pAdministrativoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Administrativo Documento
        /// </summary>
        /// <param name="pIdAdministrativoDocumento"></param>
        /// <returns></returns>
        public AdministrativoDocumento ConsultarAdministrativoDocumento(int pIdAdministrativoDocumento)
        {
            try
            {
                return vAdministrativoDocumentoBLL.ConsultarAdministrativoDocumento(pIdAdministrativoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Lista de  Administrativo Documentos
        /// </summary>
        /// <param name="pIdAdministrativo"></param>
        /// <param name="pIdTipoDocAdministrativo"></param>
        /// <returns></returns>
        public List<AdministrativoDocumento> ConsultarAdministrativoDocumentos(int? pIdAdministrativo, int? pIdTipoDocAdministrativo)
        {
            try
            {
                return vAdministrativoDocumentoBLL.ConsultarAdministrativoDocumentos(pIdAdministrativo, pIdTipoDocAdministrativo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Insertar Tipo Documento Administrativo
        /// </summary>
        /// <param name="pTipoDocumentoAdministrativo"></param>
        /// <returns></returns>
        public int InsertarTipoDocumentoAdministrativo(TipoDocumentoAdministrativo pTipoDocumentoAdministrativo)
        {
            try
            {
                return vTipoDocumentoAdministrativoBLL.InsertarTipoDocumentoAdministrativo(pTipoDocumentoAdministrativo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modificar Tipo Documento Administrativo
        /// </summary>
        /// <param name="pTipoDocumentoAdministrativo"></param>
        /// <returns></returns>
        public int ModificarTipoDocumentoAdministrativo(TipoDocumentoAdministrativo pTipoDocumentoAdministrativo)
        {
            try
            {
                return vTipoDocumentoAdministrativoBLL.ModificarTipoDocumentoAdministrativo(pTipoDocumentoAdministrativo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Tipo Documento Administrativo
        /// </summary>
        /// <param name="pTipoDocumentoAdministrativo"></param>
        /// <returns></returns>
        public int EliminarTipoDocumentoAdministrativo(TipoDocumentoAdministrativo pTipoDocumentoAdministrativo)
        {
            try
            {
                return vTipoDocumentoAdministrativoBLL.EliminarTipoDocumentoAdministrativo(pTipoDocumentoAdministrativo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Tipo Documento Administrativo
        /// </summary>
        /// <param name="pIdTipoDocAdministrativo"></param>
        /// <returns></returns>
        public TipoDocumentoAdministrativo ConsultarTipoDocumentoAdministrativo(int pIdTipoDocAdministrativo)
        {
            try
            {
                return vTipoDocumentoAdministrativoBLL.ConsultarTipoDocumentoAdministrativo(pIdTipoDocAdministrativo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Tipo Documento Administrativos
        /// </summary>
        /// <param name="pIdAdministrativo"></param>
        /// <param name="pIdTipoDocAdministrativo"></param>
        /// <param name="pPersonal"></param>
        /// <param name="pInstituciones"></param>
        /// <param name="pSeleccion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoDocumentoAdministrativo> ConsultarTipoDocumentoAdministrativos(int? pIdAdministrativo, int? pIdTipoDocAdministrativo, int? pPersonal, int? pInstituciones, int? pSeleccion, int? pEstado)
        {
            try
            {
                return vTipoDocumentoAdministrativoBLL.ConsultarTipoDocumentoAdministrativos(pIdAdministrativo, pIdTipoDocAdministrativo, pPersonal, pInstituciones, pSeleccion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region"TerceroModulo"
        /// <summary>
        /// Insertar TerceroModulo
        /// </summary>
        /// <param name="pTerceroModulo"></param>
        /// <returns></returns>
        public int InsertarTerceroModulo(TerceroModulo pTerceroModulo)
        {
            try
            {
                return vTerceroModuloBLL.InsertarTerceroModulo(pTerceroModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modificar Tercero Modulo
        /// </summary>
        /// <param name="pTerceroModulo"></param>
        /// <returns></returns>
        public int ModificarTerceroModulo(TerceroModulo pTerceroModulo)
        {
            try
            {
                return 0;// vTerceroModuloBLL.ModificarTerceroModulo(pTerceroModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Eliminar Tercero Modulo
        /// </summary>
        /// <param name="pTerceroModulo"></param>
        /// <returns></returns>
        public int EliminarTerceroModulo(TerceroModulo pTerceroModulo)
        {
            try
            {
                return 0;// vTerceroModuloBLL.EliminarTerceroModulo(pTerceroModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Tercero Entidad Modulo
        /// </summary>
        /// <param name="pTerceroModulo"></param>
        /// <returns></returns>
        public int EliminarTerceroEntidadModulo(TerceroModulo pTerceroModulo)
        {
            try
            {
                return vTerceroModuloBLL.EliminarTerceroEntidadModulo(pTerceroModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Tercero Modulo
        /// </summary>
        /// <param name="IDTerceroModulo"></param>
        /// <returns></returns>
        public TerceroModulo ConsultarTerceroModulo(int IDTerceroModulo)
        {
            try
            {
                return null;// vTerceroModuloBLL.ConsultarTerceroModulo(IDTerceroModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Consultar Tercero Modulos
        /// </summary>
        /// <param name="pIDTercero"></param>
        /// <param name="pIDModulo"></param>
        /// <returns></returns>
        public List<TerceroModulo> ConsultarTerceroModulos(int? pIDTercero, int? pIDModulo)
        {
            try
            {
                return vTerceroModuloBLL.ConsultarTerceroModulos(pIDTercero, pIDModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        //

        #region"ParametroDocumentoFinanciero"
        /// <summary>
        /// Insertar Parametro Documento Financiero
        /// </summary>
        /// <param name="pParametroDocumentoFinanciero"></param>
        /// <returns></returns>
        public int InsertarParametroDocumentoFinanciero(ParametroDocumentoFinanciero pParametroDocumentoFinanciero)
        {
            try
            {
                return vParametroDocumentoFinancieroBLL.InsertarParametroDocumentoFinanciero(pParametroDocumentoFinanciero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modificar Parametro Documento Financiero
        /// </summary>
        /// <param name="pParametroDocumentoFinanciero"></param>
        /// <returns></returns>
        public int ModificarParametroDocumentoFinanciero(ParametroDocumentoFinanciero pParametroDocumentoFinanciero)
        {
            try
            {
                return vParametroDocumentoFinancieroBLL.ModificarParametroDocumentoFinanciero(pParametroDocumentoFinanciero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Parametro Documento Financiero
        /// </summary>
        /// <param name="pParametroDocumentoFinanciero"></param>
        /// <returns></returns>
        public int EliminarParametroDocumentoFinanciero(ParametroDocumentoFinanciero pParametroDocumentoFinanciero)
        {
            try
            {
                return vParametroDocumentoFinancieroBLL.EliminarParametroDocumentoFinanciero(pParametroDocumentoFinanciero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Parametro Documento Financiero
        /// </summary>
        /// <param name="pIDParametroDocFinanciero"></param>
        /// <returns></returns>
        public ParametroDocumentoFinanciero ConsultarParametroDocumentoFinanciero(int pIDParametroDocFinanciero)
        {
            try
            {
                return vParametroDocumentoFinancieroBLL.ConsultarParametroDocumentoFinanciero(pIDParametroDocFinanciero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de Parametr oDocumento Financieros
        /// </summary>
        /// <param name="pDescripcion"></param>
        /// <param name="pIdModalidad"></param>
        /// <param name="pObligatorio"></param>
        /// <returns></returns>
        public List<ParametroDocumentoFinanciero> ConsultarParametroDocumentoFinancieros(String pDescripcion, int? pIdModalidad, Boolean? pObligatorio)
        {
            try
            {
                return vParametroDocumentoFinancieroBLL.ConsultarParametroDocumentoFinancieros(pDescripcion, pIdModalidad, pObligatorio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Parametro Documento Financieros_Documento
        /// </summary>
        /// <param name="pObligatorio"></param>
        /// <param name="rup"></param>
        /// <param name="Activo"></param>
        /// <returns></returns>
        public List<ParametroDocumentoFinanciero> ConsultarParametroDocumentoFinancieros_Documento(Boolean pObligatorio, Boolean rup, Boolean Activo)
        {
            try
            {
                return vParametroDocumentoFinancieroBLL.ConsultarParametroDocumentoFinancieros_Documento(pObligatorio, rup, Activo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Parametro Documento Financieros_Documento_ID
        /// </summary>
        /// <param name="pObligatorio"></param>
        /// <param name="vidFinanciero"></param>
        /// <param name="rup"></param>
        /// <returns></returns>
        public List<ParametroDocumentoFinanciero> ConsultarParametroDocumentoFinancieros_Documento_ID(Boolean pObligatorio, int vidFinanciero, Boolean? rup)
        {
            try
            {
                return vParametroDocumentoFinancieroBLL.ConsultarParametroDocumentoFinancieros_Documento_ID(pObligatorio, vidFinanciero, rup);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region FinancieroDocumento

        /// <summary>
        /// Insertar Financiero Documento
        /// </summary>
        /// <param name="pFinancieroDocumento"></param>
        /// <returns></returns>
        public int InsertarFinancieroDocumento(FinancieroDocumento pFinancieroDocumento)
        {
            try
            {
                return vFinancieroDocumentoBLL.InsertarFinancieroDocumento(pFinancieroDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modificar Financiero Documento
        /// </summary>
        /// <param name="pFinancieroDocumento"></param>
        /// <returns></returns>
        public int ModificarFinancieroDocumento(FinancieroDocumento pFinancieroDocumento)
        {
            try
            {
                return vFinancieroDocumentoBLL.ModificarFinancieroDocumento(pFinancieroDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Financiero Documento
        /// </summary>
        /// <param name="pFinancieroDocumento"></param>
        /// <returns></returns>
        public int EliminarFinancieroDocumento(int pFinancieroDocumento)
        {
            try
            {
                return vFinancieroDocumentoBLL.EliminarFinancieroDocumento(pFinancieroDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Financiero Documento
        /// </summary>
        /// <param name="pIdFinancieroDocumento"></param>
        /// <returns></returns>
        public FinancieroDocumento ConsultarFinancieroDocumento(int pIdFinancieroDocumento)
        {
            try
            {
                return vFinancieroDocumentoBLL.ConsultarFinancieroDocumento(pIdFinancieroDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de Financiero Documentos
        /// </summary>
        /// <param name="pIdFinanciero"></param>
        /// <param name="pIdParametroDocumentoFinanciero"></param>
        /// <returns></returns>
        public List<FinancieroDocumento> ConsultarFinancieroDocumentos(int? pIdFinanciero, int? pIdParametroDocumentoFinanciero)
        {
            try
            {
                return vFinancieroDocumentoBLL.ConsultarFinancieroDocumentos(pIdFinanciero, pIdParametroDocumentoFinanciero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Acuerdos
        /// <summary>
        /// Insertar Acuerdos
        /// </summary>
        /// <param name="pAcuerdos"></param>
        /// <returns></returns>
        public int InsertarAcuerdos(Acuerdos pAcuerdos)
        {
            try
            {
                return vAcuerdos.InsertarAcuerdos(pAcuerdos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modificar Acuerdos
        /// </summary>
        /// <param name="pAcuerdos"></param>
        /// <returns></returns>
        public int ModificarAcuerdos(Acuerdos pAcuerdos)
        {
            try
            {
                return vAcuerdos.ModificarAcuerdos(pAcuerdos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Acuerdos
        /// </summary>
        /// <param name="pAcuerdos"></param>
        /// <returns></returns>
        public int EliminarAcuerdos(Acuerdos pAcuerdos)
        {
            try
            {
                return vAcuerdos.EliminarAcuerdos(pAcuerdos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Acuerdos
        /// </summary>
        /// <param name="pIdAcuerdo"></param>
        /// <returns></returns>
        public Acuerdos ConsultarAcuerdos(int pIdAcuerdo)
        {
            try
            {
                return vAcuerdos.ConsultarAcuerdos(pIdAcuerdo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Lista de Acuerdoss
        /// </summary>
        /// <param name="pNombre"></param>
        /// <returns></returns>
        public List<Acuerdos> ConsultarAcuerdoss(String pNombre)
        {
            try
            {
                return vAcuerdos.ConsultarAcuerdoss(pNombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Acuerdo Vigente
        /// </summary>
        /// <returns></returns>
        public Acuerdos ConsultarAcuerdoVigente()
        {
            try
            {
                return vAcuerdos.ConsultarAcuerdoVigente();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region "ConsultarOferentesIdoneos"
        /// <summary>
        /// Consultar Consultar Oferentes Idoneos
        /// </summary>
        /// <param name="pIdentificacion"></param>
        /// <param name="pSigla"></param>
        /// <param name="pNombreEntidadOferente"></param>
        /// <param name="pDepartamentoUbicacionSede"></param>
        /// <param name="pMunicipioUbicacionSede"></param>
        /// <param name="pDepartamentoUbicacionCaracterizacion"></param>
        /// <param name="pMunicipioUbicacionCaracterizacion"></param>
        /// <param name="pFormaVisualizarReporte"></param>
        /// <returns></returns>
        public List<ConsultarOferentesIdoneos> ConsultarConsultarOferentesIdoneos(int? pIdentificacion,
                                                                                  string pSigla,
                                                                                  string pNombreEntidadOferente,
                                                                                  string pDepartamentoUbicacionSede,
                                                                                  string pMunicipioUbicacionSede,
                                                                                  string pDepartamentoUbicacionCaracterizacion,
                                                                                  string pMunicipioUbicacionCaracterizacion,
                                                                                  string pFormaVisualizarReporte)
        {
            try
            {
                return vConsultarOferentesIdoneos.ConsultarConsultarOferentesIdoneos(pIdentificacion,
                                                                                     pSigla,
                                                                                     pNombreEntidadOferente,
                                                                                     pDepartamentoUbicacionSede,
                                                                                     pMunicipioUbicacionSede,
                                                                                     pDepartamentoUbicacionCaracterizacion,
                                                                                     pMunicipioUbicacionCaracterizacion,
                                                                                     pFormaVisualizarReporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
        #region Proveedor
        /// <summary>
        /// Insertar Tercero Proveedor
        /// </summary>
        /// <param name="pTercero"></param>
        /// <returns></returns>
        public int InsertarTerceroProveedor(Tercero pTercero)
        {
            try
            {
                return vTerceroBLL.InsertarTerceroProveedor(pTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region TipoOrganizacion


        public TipoOrganizacion ConsultarTipoOrganizacion(int pIdTipoOrganizacion)
        {
            try
            {
                return vTipoOrganizacionBLL.ConsultarTipoOrganizacion(pIdTipoOrganizacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoOrganizacion> ConsultarTipoOrganizacions(String pCodigoTipoOrganizacion, String pNombreTipoOrganizacion, Boolean? pEstado)
        {
            try
            {
                return vTipoOrganizacionBLL.ConsultarTipoOrganizacions(pCodigoTipoOrganizacion, pNombreTipoOrganizacion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
    }
}