﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Business;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.Service
{
    /// <summary>
    /// Definición de clase OferenteService
    /// </summary>
    public partial class OferenteService
    {

        //#region Experiencia

        //public int InsertarExperiencia(Experiencia pExperiencia)
        //{
        //    try
        //    {
        //        return vExperienciaBLL.InsertarExperiencia(pExperiencia);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}
        //public int ModificarExperiencia(Experiencia pExperiencia)
        //{
        //    try
        //    {
        //        return vExperienciaBLL.ModificarExperiencia(pExperiencia);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}
        //public int EliminarExperiencia(Experiencia pExperiencia)
        //{
        //    try
        //    {
        //        return vExperienciaBLL.EliminarExperiencia(pExperiencia);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        //public Experiencia ConsultarExperiencia(int pIdExpEntidad)
        //{
        //    try
        //    {
        //        return vExperienciaBLL.ConsultarExperiencia(pIdExpEntidad);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        //public List<Experiencia> ConsultarExperiencias(int pIdOferente)
        //{
        //    try
        //    {
        //        return vExperienciaBLL.ConsultarExperiencias(pIdOferente);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        //public List<TotalExperienciaPrimerInfancia> ConsultarTotalExperienciaPrimerInfancia()
        //{
        //    try
        //    {
        //        return vExperienciaBLL.ConsultarTotalExperienciaPrimerInfancia();
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        //public List<SiNo> ConsultarSiNo()
        //{
        //    try
        //    {
        //        return vExperienciaBLL.ConsultarSiNo();
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        //public List<EstadoLiquidacionContrato> ConsultarEstadoLiquidacionContrato()
        //{
        //    try
        //    {
        //        return vExperienciaBLL.ConsultarEstadoLiquidacionContrato();
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        //public List<ModalidadAtendida> ConsultarModalidadAtendida()
        //{
        //    try
        //    {
        //        return vExperienciaBLL.ConsultarModalidadAtendida();
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        //public List<PoblacionAtendida> ConsultarPoblacionAtendida()
        //{
        //    try
        //    {
        //        return vExperienciaBLL.ConsultarPoblacionAtendida();
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        //public List<EntidadContratante> ConsultarEntidadContratante()
        //{
        //    try
        //    {
        //        return vExperienciaBLL.ConsultarEntidadContratante();
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}
        /// <summary>
        /// Realiza una consulta del salario mínimo específico de un año
        /// </summary>
        /// <param name="pAno"></param>
        /// <returns></returns>
        public SalarioMinimo ConsultarSalarioMinimo(int pAno, string descripcion = null)
        {
            try
            {
                return vExperienciaBLL.ConsultarSalarioMinimo(pAno, descripcion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Consultar Experiencia Departamento
        /// </summary>
        /// <returns></returns>
        public List<ExperienciaDepartamento> ConsultarExperienciaDepartamento()
        {
            try
            {
                return vExperienciaBLL.ConsultarExperienciaDepartamento();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Experiencia Municipio
        /// </summary>
        /// <param name="pIdDepartamento"></param>
        /// <returns></returns>
        public List<ExperienciaMunicipio> ConsultarExperienciaMunicipio(int pIdDepartamento)
        {
            try
            {
                return vExperienciaBLL.ConsultarExperienciaMunicipio(pIdDepartamento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

  
    }
}